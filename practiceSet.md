## **题目集**

### 开篇词

持之以恒，有志者事竟成；

天道酬勤，苦心人天不负；

---

在你开始刷题，走上程序设计竞赛之路之前，一定要好好想想，再问问自己几个问题：我要不要做这件事情，为什么做这件事情，我能从这件事情里获得什么，我能不能坚持下去做这件事情。实话实说，走这条路是有点“苦”的，如果不能找到坚持做这件事情的意义，你很难坚持下去，而你如果只是三天打鱼两天晒网，看心情刷题，那也做不好这件事情，如果三心二意，想着随便搞一下，临时突击一下，耍耍小聪明，去参加比赛像碰运气一样，那是做不好这件事情。虽然，大学生涯美好，想去尝试很多事情，而竞赛这个事情可以顺便玩一下，听着似乎不错的样子，这想法没错，但是带着这样的想法，程序设计竞赛的路走不远。

大学里有很多竞赛，有的靠创意，灵光一现中出成果，有的靠团队，大力出奇迹，有的靠运气，不知怎么就中奖了，有的看队友，给力点就能鸡犬升天，当然也有靠一点点努力、靠一点点积累得到的，但是这些一般平时细水长流，赛前宵衣旰食，阶段性的努力可以得到相应的回报；而我们的程序设计竞赛，周期长，训练量大，花时间，费脑子，要持之以恒地努力，投入大量的精力、不下苦功夫难见成效。

那既然说得这么“苦”,是不是要劝退大家的意思，不是的哈，这些苦是对于普通人来说的，对于参加竞赛的同学来说，苦中自有苦中乐。

从相对功利的角度看，从蓝桥杯、浙江省大学生程序设计比赛、天梯赛等等算法类的竞赛中获奖，对于个人在校期间的评优评先，就业，升学等诸多方便都能提供较大的帮助，在信息技术领域，算法竞赛的成绩很能体现一个人的技术水平。

就个人的技术能力而言，程序设计与算法是一位计算机软件从业者的基本素养，程序设计与算法能力是技术人员立身于该行业的底气所在。厚积薄发，触类旁通，扎实的程序设计与算法能力，有助于快速学习计算机软件中其他领域的知识和技能。

把眼光看向程序设计与算法等技术能力之外，竞赛训练带给人的其他个人能力上的提升则显得更为重要：

0. 团队协作与沟通

无论是个人赛还是团队赛，在竞赛之路上都不该是一个人独自前行，在训练的过程中与小伙伴互相交流互相学习，共同促进共同进步。

1. 理解、思考与逻辑思维能力

看懂题目是第一关，长长一段话，理解题目传到给我们的要求，这类训练其实可以得到与阅读书籍相似的效果。看懂题目之后想解决方案就需要开动大脑，调动脑细胞和记忆，挖掘潜在的解法，说的好听点就是开发大脑了，在这个过程中，需要把一个问题一步步拆解，用一句句逻辑严密的代码组织起来，那算是一件考验人的事情。

2. 知行合一的有效实践

人其实很渺小，身处社会的我们很多时候都随波逐流，连自己都没办法左右。多数时候空有各种奇奇怪怪，天马行空的想法，却总是局限于想想而已，没能踏出行动的那一步。算法世界给我们提供了从想法到实现的过程，从遇到一个问题，到想到一个法子，再一步一步地编码实现，再通过测试用例测试效果，一边调试一边改，根据结果修正想法，最终用代码实现自己的想法。每解决一个问题，就是自己的想法真正实施，付诸实践并最终想法得到验证的过程。每一次AC通过，都是自己有能力改变世界的一小步行动。

3. 不畏艰难，攻坚克难的品质

红军不怕远征难， 万水千山只等闲 。看到困难有人选择避开困难，而有人选择解决困难，人总是趋利避害的，逃避困难也一种自卫的本能，无可厚非，但是迎难而上，攻坚克难的品质却更让人钦佩。算法竞赛里不乏难题，废寝忘食，通宵达旦，绞尽脑汁最终AC他，此时此刻，你解决的不仅仅是一道难题，还战胜了内心深处孤苦胆小的自己。

4. 解决问题的能力

前面说的是不怕难，但是面对难题，也不是有雄心壮志就可以的，像无头苍蝇那样乱撞解决不了问题，就算瞎猫遇到死耗子，也不是每次都有的运气，解决问题讲究方式方法。通过解决一道道超出自身能力的算法题，我们其实在训练自己的那套解决问题的思维路径。难度超出自身能力怎么办——>查阅书籍找类似题型——>上网看题解——>解释看不懂怎么办——>找老师同学交流探讨——>网上找大神求助——>遇到人类知识的盲区，此问题除了你尚未有他人研究——>恭喜你，人类的进步要靠你了。

5. 坚持不懈的品质

世上无难事，只怕无恒心。做事情最怕半途而废，一开始的万丈激情，敌不过无情的苍茫岁月，时间一长，很容易失去继续下去的耐心。板凳坐得十年冷，万水千山尽是闲，意志坚定，能够抵挡岁月侵蚀的人，自有滴水石穿，铁杵成针的一天。做题需要坚持，临时抱佛脚成不了气候。

---

上面说的这些显得有点那么“上纲上线”，或者说为了骗大家参与进来而冠冕堂皇地扯这些，那么我也就是这么一说，大家怎么想我也无从得知，只能说是如鱼饮水，冷暖自知了吧，大家各自体会去吧。

当然了，不从功利的角度看，而单纯地从兴趣的角度来参与程序设计竞赛，那这件事就变得相对有趣些，未来也能在这条路上走得更远。

### 基础语法

主要是涉及到基本的输入输出，或者是一些语法的基本使用，STL库的基本使用；

##### [POJ1000] A+B Problem

题目来源：http://poj.org/problem?id=1000

就是最基础的输入输出，这题作为例子

```c++
/****************************************
  *Author:  doger
  *Contact:  897955302@qq.com
  *Description:  程序思路
*****************************************/
#include<iostream>
using namespace std;
int main(){
  int a,b;
  cin>>a>>b;
  cout<<a+b;
  return 0;
}
```

##### **[PTA] 简单输出整数**

题目来源:[https://pintia.cn/problem-sets/14/problems/733](https://pintia.cn/problem-sets/14/problems/733)

本题要求实现一个函数，对给定的正整数`N`，打印从1到`N`的全部正整数。

```c++
/*********************************************************************************
  *Author:  mvvp
  *Contact:  2512834769@qq.com
  *Description:  采用人类前沿技术解决此类问题
**********************************************************************************/
#include <iostream>
using namespace std;
void PrintN ( int N ) {
	for (int i = 1; i <= N; cout << i++ << endl);
}
int main () {
	int N;
	cin >> N;
	PrintN( N );
	return 0;
}
```

##### **[蓝桥] 试题 历届真题 找数字【第一届】【决赛】【本科组】**

题目来源：http://lx.lanqiao.cn/problem.page?gpid=T3094

25这个数字很特别，25的平方等于625，刚好其末两位是25本身。除了25，还有其它的两位数有这个特征吗?请编写程序，寻找所有这样的两位数：它的平方的末两位是这个数字本身。

```java
import java.util.Math;

/*********************************************************************************
 * Author:  Ordinary
 * Contact:  ordinary@zjgsdx.edu.cn
 * Description:  遍历所有的两位数，并判断是否符合条件
 */
public class Main {
    public static void main(String[] args) {
        for (int i = 10; i < 100; i++) {
            if (i * i % 100 == i) {
                System.out.println(i);
            }
        }
    }
}
```

##### [PTA] 字符串排序

题目来源：https://pintia.cn/problems/437/PROBLEM_DESCRIPTION

问题描述：输入为由空格分隔的5个非空字符串，每个字符串不包括空格、制表符、换行符等空白字符，长度小于80。 输入为由空格分隔的5个非空字符串，每个字符串不包括空格、制表符、换行符等空白字符，长度小于80。 

```java
/*********************************************************************************
  *Author:  crazy杨
  *Contact:  1678951138@qq.com
  *Description:  通过TreeSet这个集合的性质，调用无参的时候是通过自然排序的方式对值进行排序。
*********************************************************************************/

import java.util.HashSet;
import java.util.Scanner;
import java.util.TreeSet;
public class Main {
    public static void main(String[] args) {
        System.out.println("After sorted:");
        Scanner sc=new Scanner(System.in);
        String[] a=new String[5];
        for(int i=0;i<5;i++){
            a[i]=sc.next();
        }
        TreeSet<String> hs=new TreeSet<String>();
        for(int i=0;i<a.length;i++){
            hs.add(a[i]);
        }
        for(String s:hs){
            System.out.println(s);
        }
    }
}
```
##### [蓝桥杯] 试题 历届真题 时间显示【第十二届】【省赛】【B组】

题目来源：http://lx.lanqiao.cn/problem.page?gpid=T2909
问题描述：小蓝要和朋友合作开发一个时间显示的网站。在服务器上，朋友已经获取了当前的时间，用一个整数表示，值为从 19701970 年 11 月 11 日 00:00:0000:00:00 到当前时刻经过的毫秒数。现在，小蓝要在客户端显示出这个时间。小蓝不用显示出年月日，只需要显示出时分秒即可，毫秒也不用显示，直接舍去即可。给定一个用整数表示的时间，请将这个时间对应的时分秒输出。

```c++
/*********************************************************************************
  *Author:  crazy杨
  *Contact:  1678951138@qq.com
  *Description:  通过对时间的认识一小时为3600秒，一分钟为60秒。这样的一个方式后对其进行计算
*********************************************************************************/
#include<iostream>
using namespace std;
int main(){
    long long  a;
    scanf("%lld",&a);
    a=a/1000;
    int ss=a%60;
    a=a/60;
    int mm=a%60;
    a=a/60;
    int hh=a%24;
    printf("%02d:%02d:%02d\n",hh,mm,ss);
    return 0;
}
```

##### [蓝桥杯] 试题 算法提高 阶乘

题目来源：[“蓝桥杯”练习系统 (lanqiao.cn)](http://lx.lanqiao.cn/problem.page?gpid=T1625)
问题描述：求一个数的阶乘

```c++
/*********************************************************************************
  *Author:  麦
  *Contact:  
  *Description:  高精度乘法其实是高精度与高精度加法类似,任意数值型类型都没法存过大的数字，只能用字符串或者字符数组来代替，由于蓝桥杯不支持tostring()所以就手动写了个函数，按照平时纸上的乘法运算规则来做就行，反转去掉前置0后即是结果，我想也许还会有更好的写法，日后再来修改。
*********************************************************************************/
#include <iostream>
#include <string>
#include <algorithm>
using namespace std;
string multi(string A, string B);
string res = "1";
//数值转字符串
string to_String(int n) {
	string result;
	while (n) {
		char c = '0' + n % 10 ;
		result = c + result;
		n /= 10;
	}
	return result;
}
int main() {
	int N;
	cin >> N;
	for (int i = 2; i <= N; i++) {
		res = multi(res, to_String(i));
	}
	int p = 0;
	while (res[p] == '0') {
		p++;
	}
	for (; p < res.size(); p++) {
		cout << res[p];
	}
}
string multi(string A, string B) {
    //结果
	string result ;
	for (int i = 0; i < 100; i++) {
		result += "00000000000000000000000000000000000000000000000000";//5000个'0'
	}
	int index = 0;
	for (int i = A.size() - 1; i >= 0; i--) {
		int x = index;
		for (int j = B.size() - 1; j >= 0; j--) {
			int num = (A[i] - '0') * (B[j] - '0');
			result[x + 1] += num / 10;
			result[x] += num % 10;
			if (result[x] > '9') {
				result[x + 1] += (result[x] - '0') / 10;
				result[x] = (result[x] - '0') % 10 + '0';
			}
			x++;
		}
		index++;
	}
	reverse(result.begin(), result.end());
	return result;
}
```

##### [acwing]整数

原题链接：[3787. 整除 - AcWing题库](https://www.acwing.com/problem/content/3790/) 

```
/****************************************
  *Author:  杨杰
  *Contact:  
  *Description: 先求出余数再用b减去余数
*****************************************/
#include <iostream>
#include <cstring>
#include <algorithm>

using namespace std;

int main()
{
    int T;
    cin >> T;
    while (T -- )
    {
        int a, b;
        cin >> a >> b;
        int r = a % b;
        if (!r) puts("0");
        else cout << b - r << endl;
    }
    return 0;
}
```



### 基础数据结构

#### 链表

##### [力扣]反转链表 II
题目来源：https://leetcode-cn.com/problems/reverse-linked-list-ii/

问题描述：给你单链表的头指针 head 和两个整数 left 和 right ，其中 left <= right 。请你反转从位置 left 到位置 right 的链表节点，返回 反转后的链表 。

```c++
/*********************************************************************************
  *Author:  东黎c丶
  *Contact:  2206984585@qq.com
  *Description：用栈来存储从left到right的节点，记录两端的节点，一个一个弹出并连接即可
*********************************************************************************/
class Solution {
public:
    ListNode* reverseBetween(ListNode* head, int left, int right) {
        stack<ListNode*> st;    //用栈来进行反转链表
        ListNode* temp = (ListNode*)malloc(sizeof(ListNode));   //搞个空头结点方便反转
        temp->next = head;
        ListNode* L = temp; //用来定位到需要反转的最左边的前一个结点
        ListNode* R = head; //用来定位到需要反转的最右边的下一个结点
        int num = left;
        while(left > 1){
            head = head->next;
            L = L ->next;
            left--;
        }
        st.push(head);
        while(num < right){
            head = head->next;
            st.push(head);
            num++;
        }
        R = head->next;
        ListNode* tempL = L;
        while(!st.empty()){
            tempL->next  = st.top();
            st.pop();
            tempL = tempL->next;
        }
        tempL->next = R;
        return temp->next;  //temp是一个空的头结点
    }
};
```



##### [力扣]2. 两数相加

题目来源：https://leetcode.cn/problems/add-two-numbers

```c++
/****************************************
  *Author:  MA
  *Contact:  897039531@qq.com
  *Description:  
*****************************************/
class Solution {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode p1 = l1, p2 = l2;
        ListNode dummy = new ListNode(-1);
        ListNode p = dummy;
        int carry = 0, newVal = 0;
        while (p1 != null || p2 != null || carry > 0) {
            newVal = (p1 == null ? 0: p1.val) + (p2 == null ? 0: p2.val) + carry; 
            carry = newVal / 10;
            newVal %= 10;
            p.next = new ListNode(newVal);
            p1 = p1 == null? null: p1.next;
            p2 = p2 == null? null: p2.next;
            p = p.next;
        }
        return dummy.next;
    }
}
```



##### [力扣]141. 环形链表

题目来源：https://leetcode.cn/problems/linked-list-cycle

```c++
/****************************************
  *Author:  MA
  *Contact:  897039531@qq.com
  *Description:  快慢指针
*****************************************/
public class Solution {
    public boolean hasCycle(ListNode head) {
        ListNode s = head, f = head;
        while(f != null && f.next != null){ 
            s = s.next;
            f = f.next.next;
            if(s == f) return true;
        }
        return false;
    }
}
```



##### [力扣]203. 移除链表元素

题目来源：https://leetcode.cn/problems/remove-linked-list-elements

```c++
/****************************************
  *Author:  MA
  *Contact:  897039531@qq.com
  *Description:  
*****************************************/
class Solution {
    public ListNode removeElements(ListNode head, int val) {
        ListNode dummyHead = new ListNode(0);
        dummyHead.next = head;
        ListNode temp = dummyHead;
        while (temp.next != null) {
            if (temp.next.val == val) {
                temp.next = temp.next.next;
            } else {
                temp = temp.next;
            }
        }
        return dummyHead.next;
    }
}
```



#### 栈和队列

##### [蓝桥杯] 单词分析

题目来源：http://lx.lanqiao.cn/problem.page?gpid=T801

```c++
/*********************************************************************************
  *Author:  crazy杨
  *Contact:  1678951138@qq.com
  *Description:  通过vector和队列queue再利用他们特有性质进行循环计算
*********************************************************************************/
#include <iostream>
#include <string>
#include <vector>
#include <stdlib.h>
#include <queue>
using namespace std;

int main() {
    string str;
    queue<int>q;
    queue<int>u;
    getline(cin, str);
    int max = 0;
    vector<char>s;
    for (int i = 0; i < str.size(); i++) {
        int m = 1;
        if (str[i] == -1) {
            continue;
        }
        for (int j = i + 1; j < str.size(); j++) {
            if (str[i] == str[j]) {
                m++;
                str[j] = -1;
            }
        }
        if (max <= m) {
            max = m;
            q.push(m);
            u.push(i);
        }
    }
    while (!q.empty()) {
        if (q.front() == max) {
            s.push_back(str[u.front()]);
            q.pop();
            u.pop();
        } else {
            q.pop();
            u.pop();
        }

    }
    char temp;
    for (int i = 0; i < s.size () - 1; i++) {
        for (int j = 0; j < s.size() - i - 1; j++) {
            if (s[j] > s[j + 1]) {
                temp = s[j];
                s[j] = s[j + 1];
                s[j + 1] = temp;
            }
        }
    }
    cout << s[0] << endl << max;
    return 0;
}
```

##### [leetcode 295周赛] 使数组按非递减顺序排列

题目来源：https://leetcode.cn/contest/weekly-contest-295/problems/steps-to-make-array-non-decreasing/

代码:

```c++
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description:  我们可以把这一条序列看成在弱递增序列里插入一些乱序的数字,而我们要做的就是找出在原
  非递减数列里插入的那些数字被移除的位次,而我们知道，假定一个元素是迟早要被移除的，那么如果这个元素是
  小于前面元素的，则他就会在当次被移除,而如果他是大于前面元素的，则他会在下一轮被移除，前面的元素也是
  如此,而这些要被移除的元素是分段的，每一段放在左右两个不需要被移除的元素之间，所以我们可以把他分块，
  只要求出每一块里移除最后一个元素时所用的次数,在把每一块的次数比较求出max就是最终答案,而由题意知，
  第一个元素是必须保留的，所以在遍历一次之后我们就知道要分的块的左右端点，然后分别求出次数再求出max，
  这样的思路加上合适的数据结构(推荐set或者模拟链表)来优化是可以模拟过去的,但在这个思路上，其实还有一
  种更巧妙的数据结构，那就是栈，对照这个例子:100 1 2 3 5 1 2 3 6 1 2 3 7 1 2 3 8,往下看:
**********************************************************************************/
class Solution {
public://这里的递增散列指在序列中任意取部分数构成的递增序列
    int totalSteps(vector<int>& a) {
        stack<int>ss;
        int i,n=a.size(),sum=0;//sum是某个要剔除的块中的最长递增散列的长度
        int st[100001]={0};
        ss.push(n-1);//因为第一个是必定要留下的，而最后一个却不一定，所以直接逆推
        for(i=n-2;i>=0;i--){
            while(!ss.empty()&&a[ss.top()]<a[i]){//如果当前元素大于后面的元素先清算掉比它小
                st[i]=max(st[ss.top()],st[i]+1);//的叠层,然后它自己挤进去维护单调性
                ss.pop();sum=max(sum,st[i]);//所以st是记录每一次后面的最长递增散列的长度
            }ss.push(i);//push维护单调性是在把所有比当前小的元素pop掉之后
        }return sum;//事实上遍历完后留在栈里的就是非递减数组了
    }
 };
//单独讲解:max(st[ss.top()],st[i]+1)表示{以当前遍历到的比它小的元素为头的递增散列长度}和
//{之前遍历到的比它小的元素为头的递增散列长度再加上当前这个比之前那个元素更大的元素(因为当前元素
//比前面都大,所以以前面任何元素为头都没有关系，等于在以前面的元素为头的最长散列中加入了当前这个
//元素(没赋值之前st[i]记录的是上一个最长的散列长度))，即“+1”}(它指a[i])(遍历指while循环)
//所以最长散列即1 2 3 5 7 8
```

##### [leetcode]接雨水

题目链接: https://leetcode.cn/problems/trapping-rain-water/

代码:

```c++
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description: 遍历左右两边时有两种状态,遇到更低的和遇到更高的,当遇到低的时,累计上对应高度乘上1单位
  宽度的雨水体积,遇到高的时,将前面所累积的雨水先计算完然后重新累计,这样左边一遍能将最高柱子之前的雨水都
  累计完,遍历右边时将右边雨水累计,两者相加就是总量.
  	ps:遇到大的弹出,反之压入,用的是单调栈的思想.
**********************************************************************************/
class Solution {
public:
    int trap(vector<int>& a) {
        int m1=a[0],sum=0,m2=0;
        int i,n=a.size();
        for(i=1;i<n;i++){//左
            if(a[i]>=m1){//遇到更高的柱子
                sum+=m2;
                m2=0;m1=a[i];
            }else m2+=m1-a[i];
        }m1=a[n-1],m2=0;
        for(i=n-2;i>=0;i--){//右
            if(a[i]>m1){//遇到更高的柱子
                sum+=m2;
                m2=0;m1=a[i];
            }else m2+=m1-a[i];
        }return sum;
    }
};
```

##### [acwing]替换空格

题目来源：[16. 替换空格 - AcWing题库](https://www.acwing.com/problem/content/description/17/) 



```
/*********************************************************************************
  *Author:  crazy	
  *Contact:  
  *Description:  我们没有string这种好用的模板，需要自己malloc出char数组来存储答案。
**********************************************************************************/
class Solution {
public:
    string replaceSpaces(string &str) {
        string res;
        for (auto x : str)
            if (x == ' ')
                res += "%20";
            else
                res += x;
        return res;
    }
};
```

##### [acwing]三个元素

题目来源:[4500. 三个元素 - AcWing题库](https://www.acwing.com/problem/content/4503/) 

```
/*********************************************************************************
  *Author:  crazy	
  *Contact:  
  *Description:  我们利用数组直接进行转换，再用栈来存取出现的数字。
**********************************************************************************/
#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

int main() {
	int n;
	cin >> n;
	vector<int>s;
	int a[n];
	int b[n];
	for (int i = 0; i < n; i++) {
		cin >> a[i];
		b[i] = a[i];
	}
	sort(a, a + n);
	int sum = 1;
	s.push_back(a[0]);
	for (int i = 1; i < n; i++) {
		int flag = 1;
		for (int j = 0; j < i; j++) {
			if (a[i] == a[j]) {
				flag = 0;
			}
		}
		if (flag == 1) {
			s.push_back(a[i]);
			sum++;
		}
		if (sum == 3) {
			break;
		}
	}
	if (s.size() < 3) {
		cout << "-1 " << "-1 " << "-1" << endl;
		return 0;
	}
	for (int i = 0; i < s.size(); i++) {
		for (int j = n - 1; j >= 0; j--) {
			if (s[i] == b[j]) {
				cout << j + 1 << " ";
				break;
			}
		}
	}
	return 0;
}
```

##### [ZOJ]Anagrams by Stack

题目来源：[ZOJ (pintia.cn)](https://zoj.pintia.cn/problem-sets/91827364500/problems/91827364503)

代码如下：

```c++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description: dfs回溯
*****************************************/
#include<iostream>
#include<string>
#include<stack>
#include<vector>
using namespace std;
string a,b;
stack<char>build;
vector<char>operate;
int length;
void dfs(int iPush,int iPop){
    if(iPush==length && iPop==length){
        for(int i=0;i<operate.size();i++)
            cout<<operate[i]<<" ";
        cout<<endl;
    }
    if(iPush+1<=length){
        build.push(a[iPush]);
        operate.push_back('i');
        dfs(iPush+1,iPop);
        build.pop();
        operate.pop_back();
    }
    if(iPop+1<=iPush && iPop+1<=length && build.top()==b[iPop]){
        char temp=build.top();
        build.pop();
        operate.push_back('o');
        dfs(iPush,iPop+1);
        build.push(temp);
        operate.pop_back();
    }
}
int main(){
    while(cin>>a>>b){
        length=a.length();
        cout<<"["<<endl;
        dfs(0,0);
        cout<<"]"<<endl;
    }
    return 0;
}
```

##### [ZOJ]Web Navigation

题目来源：[ZOJ (pintia.cn)](https://zoj.pintia.cn/problem-sets/91827364500/problems/91827364560)

代码如下:

```C++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description: 模拟
*****************************************/
#include<iostream>
#include<stack>
using namespace std;
int main(){
    int n;
    cin>>n;
    while(n--){
        stack<string>s1;
        stack<string>s2;
        string str="http://www.acm.org/";
        s1.push(str);
        while(cin>>str&&str!="QUIT"){
            string temp;
            if(str=="VISIT"){
                cin>>temp;
                s1.push(temp);
                while(!s2.empty())s2.pop();
                cout<<temp<<endl;
            }else if(str=="BACK"){
                if(s1.size()==1)cout<<"Ignored"<<endl;
                else{
                    temp=s1.top();
                    s1.pop();
                    s2.push(temp);
                    temp=s1.top();
                    cout<<temp<<endl;
                }
            }else if(str=="FORWARD"){
                if(s2.size()==0)cout<<"Ignored"<<endl;
                else{
                    temp=s2.top();
                    s2.pop();
                    s1.push(temp);
                    cout<<temp<<endl;
                }
            }
        }
        if(n>0)cout<<endl;
    }
    return 0;
}
```



#### 二叉树

#### 哈希表

##### [leetcode] 前k个高频元素

题目来源:https://leetcode.cn/problems/top-k-frequent-elements/

代码:

```c++
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description:  用无序表记录每个数出现个数,排序并挑选前k个数.
**********************************************************************************/
class Solution {
public:
    vector<int> topKFrequent(vector<int>& a, int k) {
        int i,n=a.size();
        vector<pair<int,int>> st;
        vector<int> sr;
        unordered_map<int,int> s1;
        for(i=0;i<n;i++){//先统计数目
            s1[a[i]]++;
        }unordered_map<int,int>s3,s2;
        for(i=0;i<n;i++){
            if(!s3[a[i]])st.push_back({s1[a[i]],a[i]});
            s3[a[i]]=1;
        }sort(st.begin(),st.end());
        int p=st.size();
        for(i=p-1;i>=p-k;i--){
            sr.push_back(st[i].second);
        }return sr;
    }
};
```

##### [AcWing]子数组异或和

题目来源：[4507. 子数组异或和 - AcWing题库](https://www.acwing.com/problem/content/4510/)

代码如下：

```c++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description: 在0到i寻找有多少个前缀和与i相同，奇偶性相同，哈希表分别存i为奇数和i为偶数时候的值
*****************************************/
#include <bits/stdc++.h>
using namespace std;
long res;
unordered_map<int,int> a[2];
int main()
{
	int n;
	cin>>n;
	a[0][0]=1;
	for(int i=1,x,s;i<=n;i++){
		cin>>x;
		res+=a[i&1][s^=j]++;
	}
	cout<<res;
	return 0;
}

```

##### [AcWing]集合查询

题目来源：[4604. 集合询问 - AcWing题库](https://www.acwing.com/problem/content/4607/)

代码如下：

```c++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description: 将+-的元素直接转化为二进制存入哈希，遇到？时直接输出
*****************************************/
#include <bits/stdc++.h>
using namespace std;
int t;
char x;
string temp;
int main()
{
	unordered_map<int,int> num;
	cin>>t;
	while(t--){
		cin>>x>>temp;
		string s="";
		for(int i=0;i<temp.size();i++){
			if(temp[i]%2==0)s+='0';
			else s+='1';
		}
		int index=atoi(s.c_str());
		if(x=='+'){
			 num[index]++;
		}else if(x=='-'){
			 num[index]--;
		}else if(x=='?'){
			index=atoi(temp.c_str());
			cout<<num[index]<<endl;
		}
		}
	return 0;
}
```



#### 堆

##### [leetcode] 滑动窗口最大值

题目来源：https://leetcode.cn/problems/sliding-window-maximum/

代码:

```c++
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description:  题意是扫描一遍求出每个固定块中的最大值,也就是说我们要不断地插入和删除值然后求某个值
  那直接用堆或者优先队列来维护都是可以的.
**********************************************************************************/
class Solution {
public:
    vector<int> maxSlidingWindow(vector<int>& a, int k) {
        vector<int> ss;
        int i,n=a.size();
        int l=0;
        set<int> ch;
        unordered_map<int,int> ct;//如果要用set那要注意集合的互异性,这里的技巧是用一个无序表记录
        for(i=0;i<n;i++){
            ch.insert(-a[i]);
            ct[-a[i]]++;
            if(i>=k-1)ss.push_back(-(*(ch.begin())));
            if((i-l+1)>=k){
                ch.erase(-a[l]);
                ct[-a[l]]--;
                if(ct[-a[l]])ch.insert(-a[l]);
                l++;
            }
        }return ss;
    }
};
```

##### [杭电oj] The kth great number

题目来源:https://acm.hdu.edu.cn/showproblem.php?pid=4006

代码:

```c++
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description:  小根堆模板题.
**********************************************************************************/
#include<bits/stdc++.h>
typedef long long ll;
using namespace std;	
int main(){
	int n,m,t;
	char c;
	while(~scanf("%d%d",&n,&m)){     
	    priority_queue<int, vector<int>, greater<int> > q;//用greater使其从大到小排列
	    for(int i=0;i<n;i++){
		    scanf("%c",&c);
		    if(c=='I'){
			    scanf("%d",&t);
			    q.push(t); 
			    if(q.size() >m)q.pop() ;
		    }else printf("%d\n",q.top() ); 
	        }
	    }
    return 0;
}
```



### 水题

简单题，用来提升自信的那种。

##### [LeetCode661]  图片平滑器

题目来源：https://leetcode-cn.com/problems/image-smoother/

代码如下：

```c++
/****************************************
  *Author:  doger
  *Contact:  897955302@qq.com
  *Description:  按题意实现即可
*****************************************/
class Solution{
    public:
        vector<vector<int>> imageSmoother(vector<vector<int>> &img)
        {
            int a1, a2, a3, b1, b2, b3, c1, c2, c3, num;
            vector<vector<int>> result;
            for (int i = 0; i < img.size(); i++)
            {
                vector<int> temp;
                for (int j = 0; j < img[0].size(); j++)
                {
                    num = 0; //计数因子
                    a1 = 0;a2 = 0;a3 = 0;
                    b1 = 0;b2 = 0;b3 = 0;
                    c1 = 0;c2 = 0;c3 = 0;
                    if (i - 1 >= 0 && j - 1 >= 0)
                    {
                        a1 = img[i - 1][j - 1];
                        num++;
                    }
                    if (i - 1 >= 0 && j >= 0)
                    {
                        a2 = img[i - 1][j];
                        num++;
                    }
                    if (i - 1 >= 0 && j + 1 < img[0].size())
                    {
                        a3 = img[i - 1][j + 1];
                        num++;
                    }
                    if (i >= 0 && j - 1 >= 0)
                    {
                        b1 = img[i][j - 1];
                        num++;
                    }
                    b2 = img[i][j];
                    num++;
                    if (i >= 0 && j + 1 < img[0].size())
                    {
                        b3 = img[i][j + 1];
                        num++;
                    }
                    if (i + 1 < img.size() && j - 1 >= 0)
                    {
                        c1 = img[i + 1][j - 1];
                        num++;
                    }
                    if (i + 1 < img.size() && j >= 0)
                    {
                        c2 = img[i + 1][j];
                        num++;
                    }
                    if (i + 1 < img.size() && j + 1 < img[0].size())
                    {
                        c3 = img[i + 1][j + 1];
                        num++;
                    }
                    temp.push_back((a1 + a2 + a3 + b1 + b2 + b3 + c1 + c2 + c3) / num);
                }
                result.push_back(temp);
            }
            return result;
        }
};
```

##### [Codeforces] 629A Far Relative’s Birthday Cake

题目描述：Door's family is going celebrate Famil Doors's birthday party. They love Famil Door so they are planning to make his birthday cake weird!The cake is a *n* × *n* square consisting of equal squares with side length 1. Each square is either empty or consists of a single chocolate. They bought the cake and randomly started to put the chocolates on the cake. The value of Famil Door's happiness will be equal to the number of pairs of cells with chocolates that are in the same row or in the same column of the cake. Famil Doors's family is wondering what is the amount of happiness of Famil going to be?Please, note that any pair can be counted no more than once, as two different cells can't share both the same row and the same column.

题目来源：https://codeforces.com/problemset/problem/629/A

代码如下:

```c++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description:  记录横向巧克力和竖向巧克力数量n，
  C(n,2)
*****************************************/
#include<bits/stdc++.h>
using namespace std;
char a[105][105];
int b[105];
int main(){
    ios::sync_with_stdio(false);
    int res=0;
    int n;
    cin>>n;
    for(int i=0;i<n;i++){
        int js=0;
        for(int j=0;j<n;j++){
            cin>>a[i][j];  
            if(a[i][j]=='C'){
                js++;
                b[j]++;
            }
        }
        res+=(js*(js-1))/2;
    }
    for(int i=0;i<n;i++){
        if(b[i]>=2){
            res+=(b[i]*(b[i]-1))/2;
        }
    }
    cout<<res;
    return 0;
}
```

##### [LeetCode 295周赛] 价格减免

题目来源：https://leetcode.cn/problems/apply-discount-to-prices/

代码:

```c++
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description: 事实证明，双百代码p用没有，时间是金钱但不是运行时间，大家水题尽量做快哦
  	ps:后来我水了一下，哇，自由度真的很高，粘贴代码就能直接摸鱼，我一整天都是开心的。
**********************************************************************************/
class Solution {
public:
    typedef long long ll;
    string discountPrices(string ss, int t) {
        ss+=' ';
        string st;
        int i,n=ss.size();
        for(i=0;i<n;){
            if(ss[i]!='$'){
                while(ss[i]!=' '){
                    st+=ss[i++];
                }
            }else {
                st+='$';
                i++;
                string sp;
                while(ss[i]!=' '&&ss[i]<='9'&&ss[i]>='0'){
                    sp+=ss[i++];
                }if(ss[i]==' '&&sp!=""){
                    ll k=stoll(sp);
                    ll k1=(ll)((k*1.0*((100-t)/100.0)+0.005)*100);
                    sp=to_string(k1);
                    if(k1<10)sp='0'+sp;
                    if(k1<100)sp='0'+sp;
                    int yu=sp.size();
                    for(int j=0;j<yu;j++){
                        if(yu-j==2)st+='.';
                        st+=sp[j];
                    }
                }else {
                    st=st+sp;
                    while(ss[i]!=' '){
                        st+=ss[i++];
                    }
                }
            }i++;if(i<n-1)st+=' ';
        }return st;
    }
};
```

##### [acwing] 第53次周赛

题目来源：[4425. 改变数字 - AcWing题库](https://www.acwing.com/problem/content/4428/) 

```c++
 /*********************************************************************************
 *Author:  crazy杨
  *Contact:  1678951138@qq.com
  *Description: 通过使用vector可变数组，先把字符串转换为数字数组，在吧sum把每个数组都加上
  再用一个循环把各个都便利就可以出来
*****************************/
#include <iostream>
#include <vector>
using namespace std;

int main() {
	long long int x;
	cin >> x;
	vector<int>s;
	long long int m;
	long long int q;
	while (x != 0) {
		int k = x % 10
		if (9 - k < k) {
			s.push_back(9 - k);
		} else {
			s.push_back(k);
		}
		if (x / 10 == 0) {
			q = x;
		}
		x = x / 10;
	}
	if (s[s.size() - 1] == 0) {
		s[s.size() - 1] = q;
	}
	for (int i = s.size() - 1; i >= 0; i--) {
		cout << s[i];
	}
	return 0;
}
```

##### [力扣] 第294次周赛

题目来源：[2278. 字母在字符串中的百分比 - 力扣（LeetCode）](https://leetcode.cn/problems/percentage-of-letter-in-string/) 

```c++
 /*********************************************************************************
 *Author:  crazy杨
  *Contact:  1678951138@qq.com
  *Description:在java中是要用length(),记住最后得出的结果一定要在原来的先*100，不然出现的会是0
*****************************/
class Solution {
public:
    int percentageLetter(string s, char letter) {  
        int k=0;
        int m=0;
        for(int i=0;i<s.length();i++){
            if(s[i]==letter){
              k++;
            }
            m++;
        }
        return k*100/m;
    }
};
```



##### [acwing]第54次周赛

题目来源：[4428. 字符串 - AcWing题库](https://www.acwing.com/problem/content/4431/) 

```c++
 /*********************************************************************************
 *Author:  crazy杨
  *Contact:  1678951138@qq.com
  *Description:先将每个字符变成大写或者小写，然后再把出现过的重复字符变成字符'0';
*****************************/

#include <iostream>
#include <string>
using namespace std;

int main() {
	int n;
	cin >> n;
	char a[n];
	for (int i = 0; i < n; i++) {
		cin >> a[i];
	}
	int m = 0;
	for (int i = 0; i < n; i++) {
		if (a[i] == '0') {
			continue;
		}
		m++;
		for (int j = i + 1; j < n; j++) {
			if (a[j] == a[i] || a[j] == a[i] + 32 || a[j] == a[i] - 32) {
				a[j] = '0';
			}
		}
	}

	if (m == 26) {
		cout << "YES";
	} else {
		cout << "NO";
	}
	return 0;
}
```



##### [力扣] 第295次周赛

题目来源：[6090. 极大极小游戏 - 力扣（LeetCode）](https://leetcode.cn/problems/min-max-game/) 

```c++
 /*********************************************************************************
 *Author:  crazy杨
  *Contact:  1678951138@qq.com
  *Description:利用queue这个然后每次用过的都将他s.pop()消除留下最后一个1即为return的值
*****************************/
class Solution {
public:
    int minMaxGame(vector<int>& nums) {
        	queue<int>s;
        	if(nums.size()==1){
        		return nums[0];
			}
	for (int i = 0; i < nums.size(); i++) {
		int m = i % 2;
		if (m == 0 && i < nums.size() / 2) {
			int c = min(nums[2 * i], nums[2 * i + 1]);
			s.push(c);
		} else if (m != 0 && i <  nums.size() / 2) {
			int k = max(nums[2 * i], nums[2 * i + 1]);
			s.push(k);
		}
	}


	while (s.size() != 1) {
		int u = s.size();
		int a[s.size()];
		for (int j = 0; j < u; j++) {
			a[j] = s.front();
			s.pop();
		}
		for (int i = 0; i < u; i++) {
			int m = i % 2;
			if (m == 0 && i < u / 2) {
				int c = min(a[2 * i], a[2 * i + 1]);
				s.push(c);
			} else if (m != 0 && i < u / 2) {
				int k = max(a[2 * i], a[2 * i + 1]);
				s.push(k);
			}
		}
	}
	return s.front();
    }
};

```

##### [LeetCode] 不含特殊楼层的最大连续楼层数

题目来源：https://leetcode.cn/problems/maximum-consecutive-floors-without-special-floors/

代码:

```c++
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description: 易得
  	ps:水题保命，见怪勿怪
**********************************************************************************/
class Solution {
public:
    int maxConsecutive(int t1, int t2, vector<int>& a) {
        sort(a.begin(),a.end());
        int i,n=a.size();
        int ss=a[0]-t1;
        for(i=1;i<n;i++){
            ss=max(ss,a[i]-a[i-1]-1);
        }return max(ss,t2-a[n-1]);
    }
};
```

##### [LeetCode] 统计圆内格点数目

题目来源：https://leetcode.cn/problems/count-lattice-points-inside-a-circle/

代码:

```c++
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description: 枚举带水
  	ps:九指不沾阳春水
**********************************************************************************/
class Solution {
public:
    int countLatticePoints(vector<vector<int>>& c) {
        int ss[301][301]={0};
        int i,j,k,n=c.size(),pp=0;
        for(i=0;i<n;i++){
            int l=c[i][0],r=c[i][1],d=c[i][2];
            for(j=l-d;j<=l+d;j++){
                for(k=r-d;k<=r+d;k++){
                    if((j-l)*(j-l)+(k-r)*(k-r)<=d*d){
                        int p1=(j+300)%300,p2=(k+300)%300;
                        if(ss[p1][p2]==0)pp++;
                        ss[p1][p2]++;
                    }
                }
            }
        }return pp;
    }
};
```



##### [acwing]最长子序列

题目来源：[4479. 最长子序列 - AcWing题库](https://www.acwing.com/problem/content/4482/) 

```c++
 /*********************************************************************************
 *Author:  crazy杨
  *Contact:  1678951138@qq.com
  *Description:遍历俩个数组再用vector把出现过得数字都记录下来再遍历输出
*****************************/
#include <iostream>
#include <vector>
using namespace std;


int main() {
	vector<int>s;
	int n, m;
	cin >> n >> m;
	int a[n], b[m];
	for (int i = 0; i < n; i++) {
		cin >> a[i];
	}
	for (int i = 0; i < m; i++) {
		cin >> b[i];
	}
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			if (a[i] == b[j]) {
				s.push_back(a[i]);
				break;
			}
		}
	}
	for (int i = 0; i < s.size(); i++) {
		cout << s[i] << " ";
	}
	return 0;
}
```

##### [LeetCode] 个位数字为K的整数之和

题目来源：[个位数字为 K 的整数之和 - 力扣 (LeetCode) 竞赛](https://leetcode.cn/contest/weekly-contest-298/problems/sum-of-numbers-with-units-digit-k/) 

```c++
 /*********************************************************************************
 *Author:  crazy杨
  *Contact:  1678951138@qq.com
  *Description:两次循环不能有重复，都%10相加后为num则sum++最后return sum；
*****************************/
#iclass Solution {
public:
    int minimumNumbers(int num, int k) {
        if(num==0){
            return 0;       
        }
        if(num==k){
            return 1;
        }
        int sum=0;
        for(int i=0;i<=num;i++){
            if(i%10==k){
                int p=num-i;
                if(p%10==k&&p>i){
                    sum++;
                }
            }
        }
        if(sum==0){
            return -1;
        }
        return sum;
    }
}

```

```c++
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description: 数理逻辑
  	ps:水题写快
**********************************************************************************/
class Solution {
public:
    int minimumNumbers(int p, int k) {
        if(!p)return 0;
        int i,t=p%10,j;
        for(i=1;i<11;i++){
            if((k*i)%10==t){
                int tt=k*i;
                if(tt<=p&&(p-tt)%10==0)return i;
            }
        }return -1;
    }
};
```



##### [acwing] 分组

题目来源：[4482. 分组 - AcWing题库](https://www.acwing.com/problem/content/4485/) 

代码:

```c++
 /*********************************************************************************
 *Author:  crazy杨
  *Contact:  1678951138@qq.com
  *Description:求出出现最多的，即为下限，最多的几个那么答案就是几个
*****************************/
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
	vector<int>s;
	int n;
	cin >> n;
	int a[n];
	for (int i = 0; i < n; i++) {
		cin >> a[i];
	}
	for (int i = 0; i < n; i++) {
		int sum = 1;
		for (int j = i + 1; j < n; j++) {
			if (a[i] == a[j]) {
				sum++;
			}
		}
		s.push_back(sum);
	}
	sort(s.begin(), s.end());
	cout << s[s.size() - 1];
	return 0;
}

```

```c++
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description: 贪心统计重复元素
  	ps:三无水产
**********************************************************************************/
#include<bits/stdc++.h>
using namespace std;
int main(){
    int a[110]={0},t,n,i,ss=0;
    cin>>n;
    for(i=0;i<n;i++){
        cin>>t;
        a[t]++;
        ss=max(ss,a[t]);
    }cout<<ss;
    return 0;
}
```

##### [LeetCode] 小于等于 K 的最长二进制子序列

题目来源：https://leetcode.cn/problems/longest-binary-subsequence-less-than-or-equal-to-k/

代码:

```c++
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description: 贪
  	ps:水题写快
**********************************************************************************/
class Solution {
    typedef long long ll;
public:
    int longestSubsequence(string s, int k) {
        ll ss=0,t=0;
        int i,n=s.size();
        int st=0,sr=0;
        for(i=0;i<n;i++){
            ll x=s[i]-'0';
            st++;
            ss=ss*2+x;
            if(t)t*=2;
            else if(x)t=1;
            if(ss>k){
                st--;
                sr=max(sr,st);
                ss-=t;
                ll j;
                for(j=1;j<=ss;j*=2);
                t=j/2;
            }
        }return max(st,sr);
    }
};
```

##### [LeetCode]盛最多水的容器

题目来源：https://leetcode.cn/problems/container-with-most-water/

题目描述：给定一个长度为 n 的整数数组 height 。有 n 条垂线，第 i 条线的两个端点是 (i, 0) 和 (i, height[i]) 。

找出其中的两条线，使得它们与 x 轴共同构成的容器可以容纳最多的水。

```c++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description: 双指针
*****************************************/
class Solution {
public:
    int maxArea(vector<int>& height) {
        int res=0,i=0,j=height.size()-1;
        while(i<j){
            res=height[i]<height[j] ?
            max(res,(j-i)*height[i++]):
            max(res,(j-i)*height[j--]);
        }
        return res;
    }
};
```

##### [LeetCode] 多数元素 II

题目来源：https://leetcode.cn/problems/majority-element-ii/solution/229-duo-shu-yuan-su-ii-by-stormsunshine-5gud/

代码:

```c++
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description: 计数
  	ps:晾干
**********************************************************************************/
class Solution {
public:
    unordered_map<int,int> ss;
    vector<int> majorityElement(vector<int>& a) {
        vector<int>t;
        for(auto i: a){
            ss[i]++;
            if(ss[i]==a.size()/3+1)t.push_back(i);
        }return t;
    }
};
```

##### [acwing 59周赛] 数组操作

题目来源: https://www.acwing.com/problem/content/4494/

代码:

```c++
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description: 简单模拟
**********************************************************************************/
#include<bits/stdc++.h>
using namespace std;
int main(){
	int n,i;
	int s1=0,s2=0,s3=0,t;
	cin>>n;
	for(i=0;i<n;i++){
		cin>>t;
		s1+=t;
		s2+=t;
		s3=min(s3,s2);
	}cout<<s1-s3;
	return 0;
} 
```

##### [acwing]奇偶判断

题目来源[4606. 奇偶判断 - AcWing题库](https://www.acwing.com/problem/content/4609/) 

```
  /*********************************************************************************
 *Author:  crazy杨
  *Contact:  1678951138@qq.com
  *Description:直接用m[m.size-1]求出最后一个整数
*****************************/
#include <iostream>
#include <string>
using namespace std;

int main() {
	string m;
	cin >> m;
	int k = m[m.size() - 1] - 48;
	if (k % 2 == 0) {
		cout << "0";
	} else {
		cout << "1";
	}
	return 0;
}
```



```c++
  /*********************************************************************************
 *Author:  crazy杨
  *Contact:  1678951138@qq.com
  *Description:先算出所有的总和sum,再把从第一个开始相加用vector数组存取后取最小的s[0]再用sum-s[0];
*****************************/
include <iostream>

include <vector>

include <algorithm>

using namespace std;

int main() {

	vector<int>s;

	int n;

	cin >> n;

	int a[n];

	for (int i = 0; i < n; i++) {

		cin >> a[i];

	}

	int sum = 0;

	for (int i = 0; i < n; i++) {

		sum += a[i];

	}

	int sum1 = a[0];

	s.push_back(a[0]);

	for (int j = 1; j < n; j++) {

		sum1 += a[j];

		s.push_back(sum1);

	}

	sort(s.begin(), s.end());

	if (s[0] > 0) {

		s[0] = 0;

	}

	cout << sum - s[0];

	return 0;

}
```

##### [acwing 59周赛] 减法操作

题目来源: https://www.acwing.com/problem/content/4495/

代码:

```c++
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description: 分奇偶两类,如果是偶数就是一直减2的,次数就是n/2,如果是奇数则它的因子一定是奇数,
  找出那个最小奇质因子,减去一次后n成为偶数,总次数就是变成偶数后的n/2+1.
  	ps:水题写快
**********************************************************************************/
#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
int main(){
	ll n,i,ss=1;
	cin>>n;
	if(!(n%2))ss=n/2;
	else for(i=3;i<=sqrt(n);i+=2){
			if(!(n%i)){
				ss=1+(n-i)/2;
				break;
			}
		}
	cout<<ss;
	return 0;
}
```

~~~c++
/******************************
  *Author:  陈家乐
  *Contact:  
  *Description: 
******************************/
#include<iostream>
#include<cstring>
#include<algorithm>
#include<cmath>
using namespace std;
int ss(long long n);
long long div(long long n);
int main() {
	long long n;
	cin>>n;
	if(n%2==0) {
		cout<<n/2;
	} else {
		if(ss(n)) {
			cout<<1;
		} else {
			n-=div(n);
			cout<<n/2+1;
		}
	}
	return 0;
}
int ss(long long n) {
	double m=(double)n;
	long long sqr=sqrt(m);
	sqr+=2;
	for(long long i=2; i<=sqr; i++)
		if(n%i==0) {
			return 0;
		}
	return 1;
}
long long div(long long n) {
	for(long long i=2; i<n; i++) {
		if(n%i==0) {
			return i;
		}
	}

}
~~~



##### [LeetCode]装满杯子需要的最短总时长

题目来源：https://leetcode.cn/problems/minimum-amount-of-time-to-fill-cups/

```c++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description: 每次倒水最多的两个
*****************************************/
class Solution {
public:
    int fillCups(vector<int>& amount) {
        int res=0;
        int b=0;
        while(b>=2){
        res++;
        sort(amount.begin(),amount.end(),greater<int>());
        amount[0]--;
        amount[1]--;
        b=0;
            for(int i=0;i<3;i++){
                if(amount[i]>0)b++;
            }
        }
        if(b==1){
            for(int i=0;i<3;i++){
                if(amount[i]>0){
                    return res+amount[i];
                }
            }
        }
        if(b==0){
            return res;
        }
        return 0;
    }
};
```

##### [acwing] 上车

题目来源：[4419. 上车 - AcWing题库](https://www.acwing.com/problem/content/4422/) 

```
 /*********************************************************************************
 *Author:  crazy杨
  *Contact:  1678951138@qq.com
  *Description:算出剩余的载客数是否大于2就行了。
*****************************/
#include <iostream>
using namespace std;

int main() {
	int n;
	cin >> n;
	int a[n];
	int b[n];
	for (int i = 0; i < n; i++) {
		cin >> a[i] >> b[i];
	}
	int sum = 0;
	for (int i = 0; i < n; i++) {
		if (b[i] - a[i] >= 2) {
			sum++;
		}
	}
	cout << sum;
	return 0;
}
```

##### [acwing]吃饭	

题目来源：[4494. 吃饭 - AcWing题库](https://www.acwing.com/problem/content/4497/) 

```
 /*********************************************************************************
 *Author:  crazy杨
  *Contact:  1678951138@qq.com
  *Description:直接判断一个就行
*****************************/
#include<iostream>
using namespace std;
int main(){
	int n,m,k;
	cin>>n>>m>>k;
	if(n<=m&&n<=k){
		cout<<"Yes";
		}
		else{
			cout<<"No";
		}
	return 0;
}
```



##### [acwing]不同整数的个数

题目来源:[4317. 不同正整数的个数 - AcWing题库](https://www.acwing.com/problem/content/4320/) 

```
 /*********************************************************************************
 *Author:  crazy杨
  *Contact:  1678951138@qq.com
  *Description:数组做法设置flag当变量后再判断其a[j]的各个值
*****************************/
#include <iostream>
using namespace std;

int main() {
	int n;
	cin >> n;
	int a[n];
	int count = 0;
	for (int i = 0; i < n; i++) {
		cin >> a[i];
	}
	for (int i = 0; i < n; i++) {
		int flag = 0;
		if (a[i] <= 0) {
			continue;
		}
		for (int j = i + 1; j < n; j++) {
			if (a[i] == a[j]) {
				flag = 1;
				a[j] = 0;
			}
		}
		if (flag == 1) {
			count++;
		} else if (flag == 0) {
			count++;
		}
	}
	cout << count;
	return 0;
}

```



[acwing]分糖果

题目来源：[4497. 分糖果 - AcWing题库](https://www.acwing.com/problem/content/4500/) 

```
 /*********************************************************************************
 *Author:  crazy杨
  *Contact:  1678951138@qq.com
  *Description:其实这题我们就输入n个数，然后输出(a+b+c)/2就完事了，因为c++自动向下取整
*****************************/
#include<bits/stdc++.h>
using namespace std;

int main()
{
    long long n,i;
    cin>>n;
    while(n--){
        long long a,b,c;
        cin>>a>>b>>c;
        cout<<(a+b+c)/2<<'\n';
    }
}

```





### 递归

**递归的思想**

> 递归算法就是将原问题不断分解为规模缩小的子问题，即用同一个方法去解决规模不同的问题.
>

**优缺点：**

>  优点:架构清晰，代码简洁明了，缺点:递归算法本质是栈结构，因此在深度上不可预测，当层数过多不断压栈时，可能会引起栈溢出的崩溃.
>

**总结:**

> 当一个问题的架构是由相同或相似的子问题构成时通常可以运用递归算法解决，某些问题使用递归算法层数较多时注意记忆化剪枝.

##### [PTA]  汉诺塔

题目来源：https://pintia.cn/problem-sets/1466688064595234816/problems/1466688064695898118

问题描述：古代某寺庙中有一个梵塔，塔内有3个座A、B和C，座A上放着64个大小不等的盘，其中大盘在下，小盘在上。有一个和尚想把这64 个盘从座A搬到座B，但一次只能搬一个盘，搬动的盘只允许放在其他两个座上，且大盘不能压在小盘上。输入仅一个n为盘子个数，从a座到b座，c座作为中间过渡，输出搬盘子的路径。

代码：

```c
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description:  经典的递归入门题，通过分析可知：由于一次只能移一片，要移动底下大盘就必须先把
  在它上面的所有盘移走，每一个大盘的移动都是这样一个前提，那么移动n个盘到目标座的步骤是：先移动n-1
  个盘到中间座上，然后把最大盘移动到目标座，最后借助第一个座把n-1个盘移到目标座，而移动n-1个盘和移
  动n个盘同理，放着最大盘的那个座又可以当作中间座，因为其他座的盘子都比这个座上的任意盘小，那么这样
  从大到小的结构都是相似的，是可以递归的，虽然中间座，目标座和起始座在不断改变，但这些都是随着每一
  次要移出的最大盘的位置而改变，那么可以理解为这三个座的相对位置是以每一次最大盘的归属位置为参照，
  那到这思路就出来了。
  	ps:不知大家第一次写这题都是怎么样的，本人有幸浪费一个下午,orz
**********************************************************************************/
#include<stdio.h>
void hanoi(int n,char a,char b,char c){//a,b,c只是变量,只需知道a代表起始座,b代表中间座,c代表目标座，没必要纠结每一次的a,b,c和之前的关系
	if(n==1)printf("%c-->%c\n",a,c);//想想如果只有一个盘,那直接从起始座移到目标座就完了
	else { 
		hanoi(n-1,a,c,b); //那么先从a移动n-1个盘到中间座b(a,c,b)
		printf("%c-->%c\n",a,c); //然后把每一次的最大盘从a移到目标座c
		hanoi(n-1,b,a,c); //再把剩下n-1个盘从中间座b移到c(b,a,c),最后一步~
	}//可以想象递归结束时剩下的n-1个盘都从b移到了c，那么我滴任务完成辣！
}
int main(){
	 int n;
	 scanf("%d",&n);
	 hanoi(n,'a','c','b');//题目要求从a移n个盘到b
	return 0;
}
```

##### [LeetCode172] 阶乘后的零

题目来源：https://leetcode-cn.com/problems/factorial-trailing-zeroes/

代码如下：

```c++
/****************************************
  *Author:  doger
  *Contact:  897955302@qq.com
  *Description:  0是由2*5产生的，而在计算阶乘时，2的个数肯定比5多，所以只要算5的个数即可
*****************************************/
class Solution {
public:
    static int fiveNum(int n){
        int num=0;
        while(n%5==0){
            num++;
            n = n/5;
        }
        return num;
    }
    int trailingZeroes(int n) {
        //递推公式为 f(n) = f(n-1) + fiveNum(n)
        if(n<5){
            return 0;
        }else{
            return trailingZeroes(n-1) + fiveNum(n);
        }
    }
};
```

##### [蓝桥]  高精度计算

题目来源：http://lx.lanqiao.cn/problem.page?gpid=T2039

问题描述：用高精度计算出S=1！+2！+3！+…+n！（n≤50），其中“!”表示阶乘，例如：5！=5*4*3*2*1。输入正整数n，输出计算结果S.

代码:

```c++
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description:  经典的高精度计算变种，直观地去想便是分别去计算高精度然后再相加，但实现的代码
  可能较为繁杂。那么我们不难想到1！+2！+3！+…+n！=1*(1+1*(2+2*(3*(....(n)))))，数据量一眼
  直接递归，不断深入直到出口n，每次出来做一次加法和一次高精乘即可.
**********************************************************************************/
#include <iostream>
using namespace std;
int n,t=0,ss[10000]={0};
void rp(int m){
	int k,p,s;
	if(m>n){
		ss[0]=1;
        return;
	}rp(m+1);
	for(k=s=0;k<=t;k++){
		p=ss[k]*m+s;
		s=p/10;
		ss[k]=p%10;
	} while(s){
		ss[++t]=s%10;
        s/=10;
	} ss[0]++;
} int main() {
	cin>>n;
	rp(2);
	for(int i=t;i>=0;i--)cout<<ss[i];
	return 0;
}
```

##### [蓝桥]  未名湖边的烦恼

题目来源：http://lx.lanqiao.cn/problem.page?gpid=T303

问题描述：每年冬天，北大未名湖上都是滑冰的好地方。北大体育组准备了许多冰鞋，可是人太多了，每天下午收工后，常常一双冰鞋都不剩。 每天早上，租鞋窗口都会排起长龙，假设有还鞋的m个，有需要租鞋的n个。现在的问题是，这些人有多少种排法，可以避免出现体育组没有冰鞋可租的尴尬场面。（两个同样需求的人（比如都是租鞋或都是还鞋）交换位置是同一种排法） 

代码:

```c++
/*********************************************************************************
  *Author:  陈高威
  *Contact:  1259234645@qq.com
  *Description:  
当第i个人要去排队时发现，另一类人已经全部排完了，那么解法只有一种了。
例如：如果前i-1个人中还鞋人的数量大于租鞋人的数量，那么第i个人可以是还鞋人，也可以是租鞋人；如果前i-1个人中还鞋人的数量等于租鞋人的数量，那么第i个人只能是还鞋人。
**********************************************************************************/
#include<iostream>
using namespace std;
int fun(int m,int n)
{
	if(m<n) return 0;
	if(n==0) return 1;
	return fun(m-1,n)+fun(m,n-1); 
}
int main()
{
	int m,n;
	cin>>m>>n;
	cout<<fun(m,n);
	return 0;
}
```

### 排序

##### [LeetCode1996] 游戏中弱角色的数量

题目来源：https://leetcode-cn.com/problems/the-number-of-weak-characters-in-the-game/

思路：如果不考虑攻击力相同的情况，假设他们都不同，那么只需要将攻击力降序排列，然后将防御力从左到右扫面，并且记录一个最大防御力，一旦出现了小于最大防御力的就是出现了一个弱实例；但是实际上是存在攻击力相同的情况的，这时就不能再是单纯地使用上述思考了，此时我们就把相同攻击力的几个数据按照防御力升序排列，那么从左到右扫描过去的时候，如果出现了后面的数比前面的小，那么攻击力肯定是不同的，也就是出现了弱势之人。

代码：

```c++
/****************************************
  *Author:  doger
  *Contact:  897955302@qq.com
  *Description:  注意：攻击力降序排列，防御力升序排列，注意排序函数比较函数参数采用引用写法，不然超时；
*****************************************/
class Solution{
    public:
        static bool cmp(vector<int> &a, vector<int> &b){
            if (a.at(0) != b.at(0))
            {
                //攻击力降序排列
                return a.at(0) > b.at(0);
            }
            else
            {
                //相同攻击力则防御力升序排列
                return a.at(1) < b.at(1);
            }
        }
        int numberOfWeakCharacters(vector<vector<int>> &properties){
            //降序排列
            sort(properties.begin(), properties.end(), cmp);
            int num = 0;
            int maxV = properties.at(0).at(1);
            for (int i = 0; i < properties.size(); i++)
            {
                if (maxV > properties.at(i).at(1))
                {
                    num++;
                }
                if (maxV < properties.at(i).at(1))
                {
                    maxV = properties.at(i).at(1);
                }
            }
            return num;
        }
};
```

##### [acwing] 格斗场

题目来源：[4483. 格斗场 - AcWing题库](https://www.acwing.com/problem/content/4486/) 

```c++
 /*********************************************************************************
 *Author:  crazy杨
  *Contact:  1678951138@qq.com
  *Description：先通过sort从小到大排序，后面比前面大的且不说最后一个则把前面的赋值为-1，如果到最后则直接跳过循环并且留下
*****************************/
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
	int n, k;
	cin >> n >> k;
	int a[n];
	for (int i = 0; i < n; i++) {
		cin >> a[i];
	}
	int sum = 0;
	sort(a, a + n);
	for (int i = 0; i < n; i++) {
		if (a[i] == a[n - 1]) {
			continue;
		}
		if (a[i] == -1) {
			continue;
		}
		if (1<=a[i + 1] - a[i] <= k) {
			if (a[i + 1] != a[i]) {
				a[i] = -1;
				continue;
			}
			
		} else {
			continue;
		}
	}
	for (int i = 0; i < n; i++) {
		if (a[i] != -1) {
			sum++;
		}
	}
	cout << sum;
	return 0;
}
```



##### [acwing]数组操作

题目来源：[4495. 数组操作 - AcWing题库](https://www.acwing.com/problem/content/4498/) 

```
 /*********************************************************************************
 *Author:  crazy杨
  *Contact:  1678951138@qq.com
  *Description：先用sort排序从小到大然后取出最大的判断是不是为0再进行下一步判断
*****************************/
#include <iostream>
#include <algorithm>
using namespace std;

int main() {
	int n, k;
	cin >> n >> k;
	int a[n];
	for (int i = 0; i < n; i++) {
		cin >> a[i];
	}
	while (k--) {
		sort(a, a + n);
		if (a[n - 1] != 0) {
			int flag = 0;
			int x = 0;
			for (int i = 0; i < n; i++) {
				if (flag == 0) {
					if (a[i] != 0) {
						cout << a[i] << endl;
						x = a[i];
						flag = 1;
					}
				}
				a[i] -= x;

			}
		} else {
			cout << 0 << endl;
		}
	}
	return 0;
}
```





```c++
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description: 贪心+排序
  	ps:水题写快
**********************************************************************************/
#include<bits/stdc++.h>
using namespace std;
int main(){
    int n,i,k,l=1,ss=0;
    int a[200010]={0};
    scanf("%d%d",&n,&k);
    for(i=0;i<n;i++){
        scanf("%d",&a[i]);
    }sort(a,a+n);
    for(i=1;i<n;i++){
        if(a[i]==a[i-1])l++;
        else {if(a[i]-a[i-1]>k)ss+=l;l=1;}
    }printf("%d",ss+l);
    return 0;
}
```

##### [POJ]Ultra-QuickSort

题目来源：[2299 -- Ultra-QuickSort (poj.org)](http://poj.org/problem?id=2299)

```c++
/****************************************
  *Author:  陈斌
  *Contact:  1198222765@qq.com
  *Description:  归并排序求逆序数
*****************************************/
#include<iostream>
#include<algorithm>
#include<string.h>
#define int long long
using namespace std;
int a[500005],t[500005];
int res,n;
void dfs(int x,int y){
    if(y-x<=1)return;
    int mid=x+(y-x)/2;
    dfs(x,mid);
    dfs(mid,y);
    int p=x,q=mid,i=x;
    while(p<mid || q<y){
        //q已经填满 || q和p没有满并且a[p]<=a[q]
        if(q>=y || (p<mid && a[p]<=a[q]))t[i++]=a[p++];
        else{
            res+=(mid-p);//mid-p 即为前面有几个数比a[q]的值大 即逆序数 即为所求的需要冒牌排序的次数
            t[i++]=a[q++];
        }
    }
    //把归并好的t赋值给a
    for(i=x;i<y;i++)a[i]=t[i];
}
signed main(){
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    while(cin>>n){
        if(n==0)break;
        memset(a,0,sizeof a);
        memset(t,0,sizeof t);
        for(int i=0;i<n;i++)cin>>a[i];
        res=0;
        dfs(0,n);
        cout<<res<<endl;
    }
}
```

##### [洛谷]P8893 「UOI-R1」智能推荐

题目来源：[P8893 「UOI-R1」智能推荐 - 洛谷 | 计算机科学教育新生态 (luogu.com.cn)](https://www.luogu.com.cn/problem/P8893?contestId=93922)

```c++
/****************************************
  *Author:  陈斌
  *Contact:  1198222765@qq.com
  *Description:  拓扑排序算排序吗(
  从没有入度的点去找与他们连通的点，连通点入度-1，入度为0就存入vector，下一次去做vector里面的去找连通的，存在环所以每个只写一次，加个vis。
*****************************************/
#include <bits/stdc++.h>
#define int long long
using namespace std;
const int N=2e7+5;
const int INF=0x3f3f3f3f;
const int mod=998244353;
int n,k,p,r,t;
vector<int>st;
int vis[N];
int d[N];
struct node{
	int to;
	int next;
}edge[N];
int first[N];
int cnt;
bool ok;
vector<int>ne;
void add(int u,int v){
	edge[++cnt].to=v;
	edge[cnt].next=first[u];
	first[u]=cnt;
}
void find(int u){
	for(int i=first[u];i!=0;i=edge[i].next){
		if(vis[edge[i].to]==1)continue;
		d[edge[i].to]--;
		if(d[edge[i].to]==0){
			ne.emplace_back(edge[i].to);
		}
	}
}
signed main() {
	ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
	cin>>n>>k>>p;
	for(int i=0,j;i<p;i++){
		cin>>j;
		st.emplace_back(j);
		vis[j]=1;
	}
	cin>>r;
	for(int i=0;i<r;i++){
		int p;
		cin>>p;
		int m;
		cin>>m;
		d[p]=m;//p这个点的入度有m
		for(int j=0,x;j<m;j++){
			cin>>x;
			add(x,p);
		}
	}
	for(auto b:st){
		if(b==k){
			cout<<0<<endl;
			return 0;
		}
		find(b);
	}
	t++;
	while(!ne.empty()){
		vector<int>tm=ne;
		ne.clear();
		for(auto b:tm){
			if(b==k){
				cout<<t<<endl;
				return 0;
			}
			find(b);
			vis[b]=1;
		}
		t++;
	}
	cout<<-1<<endl;
}
```



### 搜索

#### 枚举

**枚举的思想**

> 枚举法的**本质**就是从所有候选答案中去搜索正确的解，它的核心思想就是**枚举所有的可能**。

**使用条件**

> 1.可预先确定候选答案的数量
>
> 2.候选答案的范围在求解之前必须有一个确定的集合

##### [蓝桥] 试题 历届真题 特别数的和【第十届】【省赛】【B组】

题目来源：http://lx.lanqiao.cn/problem.page?gpid=T2701

问题描述：小明对数位中含有 2、0、1、9 的数字很感兴趣（不包括前导 0），在 1 到 40 中这样的数包括 1、2、9、10 至 32、39 和 40，共 28 个，他们的和是 574。请问，在 1 到 n 中，所有这样的数的和是多少？

```c++
/*********************************************************************************
  *Author:  东黎c丶
  *Contact:  2206984585@qq.com
  *Description: 从1遍历到n，用find方法找到带有需要的数字加到总数即可
*********************************************************************************/
#include<iostream>
#include<algorithm>
#include<string>
#include<sstream>
using namespace std;
int main() {
	int n;
	cin >> n;
	string s;
	long long sum = 0;
	for (int i = 1; i <= n; i++) {
		stringstream ss;
		ss << i;
		ss >> s;
		if (s.find('2')!=s.npos || s.find('0')!=s.npos || s.find('1')!=s.npos || s.find('9')!=s.npos) {
			sum += i;
		}
	}
	cout << sum;
	return 0;
}
```

##### **[AcWing]整除子串**

题目来源:https://www.acwing.com/problem/content/4429/

问题描述：给定一个由数字组成的字符串 ss，请你计算能够被 44 整除的 ss 的子串数量。子串可以包含前导 0。

```c++
/*********************************************************************************
  *Author:  mai
  *Contact:  
  *Description：每一个数字都能被拆分成 n*100+ab,判断一个数字是否能被4整除，只要判断ab能否被4整除
  若ab能被4整除，那么前面无论如何组合都能得到一个被4整除的数。一个数字的话直接判断
*********************************************************************************/
#include <iostream>
using namespace std;
long long int ans = 0;

int main() {
	string str;
	cin >> str;
	str = " " + str;
	for (int i = 1; i < str.size(); i++) {
		if ((str[i] - '0') % 4 == 0) {
			ans++;
		}
		if (i > 1 && ((str[i - 1] - '0') * 10 + (str[i] - '0')) % 4 == 0) {
			ans += i - 1 ;
		}
	}
	cout << ans;
} 
```

##### [LeetCode]最大波动的子字符串

题目来源：https://leetcode.cn/problems/substring-with-largest-variance/

问题描述：字符串的 波动 定义为子字符串中出现次数 **最多** 的字符次数与出现次数 **最少** 的字符次数之差。给你一个**字符串** **s** ，它只包含小写英文字母。请你返回 s 里所有 **子字符串**的 **最大波动** 值。**子字符串** 是一个字符串的一段连续字符序列。

**提示：** 1 <= s.length <= 1e4

```c++
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description: 题目本质上是求两个字符出现次数的差，他只包含小写字母，所以不用管这个最多和最少的两
  个字符取在哪两个字符上，一眼数据量直接想到枚举这两个字母来举出最大和最小出现次数，那么我们就已经解
  决了这两个字符是什么字符的问题，这样还没做完，因为他是在子字符串里取这两个字符，所以我们还需要去扫
  描区间去贪心，所以步骤就是，先举出两个字符，然后先定下第一个字符的出现次数作为max，另一个为min(因
  为谁为max的情况都会枚举到)，那么我们先贪心地从有max字符的地方开始，然后往后扫描，每次这两个字符的
  个数取个差来更新结果，那么就做完了.
   ps:其实还没做完,请往下看。
**********************************************************************************/
class Solution {
public:
    int largestVariance(string a) {
        int ss=0;
        int i,j,k,n=a.size();
        if(n<3)return 0;
        int st[26]={0};
        for(i=0;i<n;i++){
            st[a[i]-'a']=1;
        }for(i=0;i<26;i++){//枚举第一个字符
            if(st[i])
            for(j=0;j<26;j++){//枚举第二个字符
                if(st[j]&&i!=j){
                    char a1='a'+i,a2='a'+j;
                    int t1=0,t2=0;
                    for(k=0;k<n;k++){
                        if(a[k]==a2&&t1)t2++;
                        if(a[k]==a1)t1++;
                     	if(t1<t2)t1=t2=0;//*那么其他的已经讲完了,还剩个这个
                        if(t2)ss=max(ss,t1-t2);//这就是这道题贪心的思路,也就是当max<min时
                    }if(!t2)ss=max(ss,t1-1);//前面的这一段区间都可以截掉了
                }						//先不说max变min导致后面两个字母反过来重复计算的问题
            }				//最关键的是这样的情况会有后效性,就像那个连续子数组最大和的问题一样
        }return ss;  	//如果遇到前面的滚动过来的和是个负数,那还有带到后面的必要吗
    }					//直接截掉后面的值会更大,所以这就是这道题的写法。
};
```

##### [蓝桥]车的放置

题目来源：http://lx.lanqiao.cn/problem.page?gpid=T2981

问题描述：在一个n*n的棋盘中，每个格子中至多放置一个车，且要保证任何两个车都不能相互攻击，有多少中放法(车与车之间是没有差别的) 

```c++
/*********************************************************************************
  *Author:  陈高威
  *Contact:  1259234645@qq.com
  *Description:  首先只要不把他们放在同一行或者同一列就可以避免相互攻击题目要求不一定在每一行都必须放，即在这一行放不放都是一种方案。因此搜索的时候都会对应两种选择，在这一行放或者不在这一行放，每种选择都要搜索一遍。
**********************************************************************************/
#include  <iostream>
using namespace std;

int N;
long long ans=1; //记录多少种方法
bool visited[10]; //标记被放置的列

void dfs(int step) 
{
    if(step>N) return ; 
    for(int i=1;i<=N;i++){
    	if(!visited[i]) //这一列没有被放置
        {
            visited[i]=true; 
            ans++; 
            dfs(step+1); //换到下一行
            visited[i]=false; //回溯
        }
	}
    dfs(step+1);//第step行没有就step+1行开始放
}
int main()
{
    cin>>N;
    dfs(1);
    cout<<ans;
    return 0;
}

```

##### [力扣 297周赛]公司命名

题目来源：https://leetcode.cn/contest/weekly-contest-297/problems/naming-a-company/

代码:

```c++
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description: 枚举所有小写字母作为头,用表对照,最后筛掉没出现过的
   ps: 思路简单,但这题卡无序表,别问为什么知道会卡,orz
**********************************************************************************/
class Solution {
    typedef long long ll;
public:
    ll mp[26][26];
    ll distinctNames(vector<string>& a) {
        unordered_map<string,int>ss;
        int i,j,n=a.size();
        for(auto &i : a){
            ss[i]=1;
        }for(i=0;i<n;i++){
            string sp=a[i];
            int t=sp[0]-'a';
            for(j=0;j<26;j++){
                sp[0]='a'+j;//枚举头
                if(!ss.count(sp))mp[t][j]++;//查重
          }//为何不直接!ss[sp]去查呐,因为散列表查询速度取决于数据,count方法作用相同采用红黑树,较稳
        }ll sum=0;
        for(i=0;i<26;i++){
            for(j=0;j<26;j++){
                sum+=mp[i][j]*mp[j][i];//筛出头出现过的两个组成部分
            }
        }return sum;
    }
};
```

##### [力扣]按位与结果大于零的最长组合

题目来源：https://leetcode.cn/problems/largest-combination-with-bitwise-and-greater-than-zero/

代码:

```c++
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description: 与运算是两个数相同的位取个交集,那么只要统计出每一位牵扯到的数的个数,取这些个数中的最
  大值即可
   ps:期末炒一波冷饭 QAQ
**********************************************************************************/
class Solution {
public:
    int largestCombination(vector<int>& a) {
        int i,n=a.size();
        int ss=1;
        for(i=0;i<25;i++){//(2^24)是(1024*1024*16)只要比1e7大即可
            int l=0;
            for(auto &k : a){
                if(((1<<i)&k))l++;
            }ss=max(ss,l);
        }return ss; 
    }
};
```

##### [力扣]转角路径的乘积中最多能有几个尾随零

题目来源：https://leetcode.cn/problems/maximum-trailing-zeros-in-a-cornered-path/

代码:

```c++
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description: 对于表格中的每个点,我们先枚举上下左右四个方向,而组成0的2和5因子,我们自然能想到用二
  维前缀分别统计2个因子个数,对于每个点,有4个拐角,对于每个拐角,我们取它的2个因子个数的最小值便是0的数目
**********************************************************************************/
class Solution {
public:
    int maxTrailingZeros(vector<vector<int>>& a) {
        int sum=0;
        int n=a.size(),m=a[0].size();
        int i,j,at[n][m],b[n][m],c[n][m],d[n][m],e[n][m];
        for(i=0;i<n;i++){
            for(j=0;j<m;j++){//统计每个点的2和5因子数
                int rr=a[i][j],rs=0,rp=rr,rq=0;
                while(!(rr%2)){rr/=2;rs++;}at[i][j]=rs;
                while(!(rp%5)){rp/=5;rq++;}a[i][j]=rq;
                b[i][j]=c[i][j]=rq;//两个前缀和里存的是2和5的个数
                d[i][j]=e[i][j]=rs;//避免重复计算
                if(j)b[i][j]+=b[i][j-1],d[i][j]+=d[i][j-1];
                if(i)c[i][j]+=c[i-1][j],e[i][j]+=e[i-1][j];
            }
        }for(i=0;i<n;i++){
            for(j=0;j<m;j++){
                int a1=b[i][j],b1=c[i][j],c1=d[i][j],d1=e[i][j];
                int a2=b[i][m-1],b2=c[n-1][j],c2=d[i][m-1],d2=e[n-1][j];
                int t1=a[i][j],t2=at[i][j];
                int y1=min(a1+b1-t1,c1+d1-t2);//4个方向拐角
                int y2=min(b2-b1+a1,c1+d2-d1);//取两个因子较小数
                int y3=min(a2-a1+b1,d1+c2-c1);//是0的个数
                int y4=min(a2-a1+b2-b1+t1,d2-d1+c2-c1+t2);
                sum=max({sum,y1,y2,y3,y4});//取最多0的拐角
            }
        }return sum;
    }
};
```



#### 二分

##### [LeetCode]寻找峰值

题目来源：https://leetcode-cn.com/problems/find-peak-element/

问题描述:峰值元素是指其值严格大于左右相邻值的元素。给你一个整数数组 nums，找到峰值元素并返回其索引。数组可能包含多个峰值，在这种情况下，返回 任何一个峰值 所在位置即可。你可以假设 nums[-1] = nums[n] = -∞ 。

你必须实现时间复杂度为 O(log n) 的算法来解决此问题。

```c++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description:  二分
*****************************************/
class Solution {
public:
    int findPeakElement(vector<int>& nums) {
        if(nums.size()==1)return 0;
        if(nums.size()==0)return 0;
        if(nums[0]>nums[1])return 0;
        if(nums[nums.size()-1]>nums[nums.size()-2])return nums.size()-1;
        int left=0,right=nums.size()-1,mid;
        while(left<right){
            mid=left+(right-left)/2;
            if(nums[mid]>nums[mid+1]&&nums[mid]>nums[mid-1]){
                break;
            }else if(nums[mid+1]>=nums[mid]){
                left=mid+1;
            }else{
                right=mid;
            }
        }
            return mid;
    }
};
```

##### [力扣]长度最小的子数组

题目来源：https://leetcode.cn/problems/minimum-size-subarray-sum/

代码:

```c++
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description: 因为都是正整数且与和有关,那么会想到可以前缀和建立单调性来个二分，因为所求子数组为连续
  子数组,所以以每一个头存在情况遍历,二分注意边界.
   ps:暴扣
**********************************************************************************/
class Solution {
public:
    int minSubArrayLen(int t, vector<int>& a) {
        int ss=0;//为了方便初始化为0
        int i,n=a.size();
        int b[n];b[0]=a[0];
        for(i=1;i<n;i++)
        b[i]=b[i-1]+a[i];
        for(i=0;i<n;i++){
            int l=i,r=n-1;
            while(l<r){
                int mid = (l + r)>> 1;
                if(b[mid]-b[i]+a[i]>=t)r = mid;//要算上自己哦
                else l = mid + 1;
            }if(b[l]-b[i]+a[i]>=t){
                if(!ss)ss=l-i+1;//这里要特判一下0
                else ss=min(ss,l-i+1);
            }
        }return ss;
    }
};
```



#### BFS

##### [蓝桥]跳马问题

问题来源：http://lx.lanqiao.cn/problem.page?gpid=T2987

问题描述：一个8×8的棋盘上有一个马初始位置为(a,b)，他想跳到(c,d)，问是否可以？如果可以，最少要跳几步？

```C++
/****************************************
  *Author:  麦文鹏
  *Contact:  
  *Description: 经典的BFS，从队列中取出第一个点，开始尝试所有能跳的点，并将符合条件的点存入队列
*****************************************/
#include <iostream>
#include <cstring>
#include <queue>
using namespace std;
typedef pair<int, int> PII;
int a, b, c, d,sum=1;
int G[9][9],visited[9][9], step[9][9];
//9x9地图G，访问标志visited,能否跳到的标志step
const int n = 8;

void bfs(int x, int y, int count);

int main() {
	cin >> a >> b >> c >> d;
	bfs(a, b, 0);
	cout << step[c][d];
}

void bfs(int x, int y, int count) {
	//初始化第一个点
	queue<PII> q;
	q.push({x, y});
	visited[x][y] = 1;	
	while (q.size()) {
		x = q.front().first;
		y = q.front().second;
		int xy[8][8]={{-1,2},{1,2},{2,1},{2,-1},{1,-2},{-1,-2},{-2,-1},{-2,1}};
		for (int i = 0; i < 8; i++) {
			int nx = x + xy[i][0], ny = y + xy[i][1];
			if (nx >= 1 && nx <= n && ny >= 1 && ny <= n && visited[nx][ny] == 0) {
				q.push({nx, ny});
				visited[nx][ny] = 1;
				step[nx][ny] = count + 1;
			}
		}
		q.pop();
		if ( !q.empty() && step[q.front().first][q.front().second] != step[x][y])
			count++;
	}
}
```

##### [LeetCode]滑动谜题

题目来源：https://leetcode.cn/problems/sliding-puzzle/

问题描述：在一个 2 x 3 的板上（board）有 5 块砖瓦，用数字 1~5 来表示, 以及一块空缺用 0 来表示。一次 移动 定义为选择 0 与一个相邻的数字（上下左右）进行交换。最终当板 `board` 的结果是 `[[1,2,3],[4,5,0]]` 谜板被解开。给出一个谜板的初始状态 `board` ，返回最少可以通过多少次移动解开谜板，如果不能解开谜板，则返回 `-1` 。

~~~c++
/****************************************
  *Author:  陈家乐
  *Contact:  784190546@qq.com
  *Description：经典广度优先遍历 
*****************************************/
class Solution {
public:
    vector<string> get(string& status){
        vector<vector<int> > neighbors = {{1,3},{0,2,4},{1,5},{0,4},{1,3,5},{2,4}};
        vector<string> ret;
        int x = status.find('0');
        for(int y : neighbors[x]){
            string statu = status;
            swap(statu[x],statu[y]);
            ret.push_back(statu);
        }
        return ret;
    }
    int slidingPuzzle(vector<vector<int>>& board) {
        string initial;
        for(int i=0;i<2;i++){
            for(int j=0;j<3;j++){
                initial+=char(board[i][j]+'0');
            }
        }
        if(initial == "123450"){
            return 0;
        }
        queue<pair<string,int> > q;
        q.push({initial,0});
        set<string> s;
        s.insert(initial);
        while(!q.empty()){
            string status;
            int step;
            status = q.front().first;
            step = q.front().second;
            q.pop();
            for(auto next_status : get(status)){
                if(!s.count(next_status)){
                    if(next_status == "123450"){
                        return step+1;
                    }
                    q.push({next_status,step+1});
                    s.insert(next_status);
                }
            }
        }
        return -1;
    }
};
~~~

##### [LeetCode 295周赛] 到达角落需要移除障碍物的最小数目

题目来源：https://leetcode.cn/contest/weekly-contest-295/problems/minimum-obstacle-removal-to-reach-corner/

代码:

```c++
/****************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description：一眼带权bfs，注意剪枝
  	ps: 意多言少->少说废话->话不多说->说多无益
*****************************************/
class Solution {
public:
    int minimumObstacles(vector<vector<int>>& a) {
        #define pr pair<int,int>
        int n=a.size(),m=a[0].size();
        int at[n][m];
        memset(at,1000000,sizeof(at));//所求为最小消耗，初始化为无穷
        at[0][0]=0;//题意规定第一个无障碍
        int xx[4]={-1,0,1,0};
        int yy[4]={0,-1,0,1};
	    queue<pr> q;
	    q.push({0,0});//起始坐标0 0
	    while(!q.empty()){
		    pr t=q.front();
		    q.pop();
            int xt=t.first,yt=t.second;
		    for(int i=0;i<4;i++){
			    int x2=xt+xx[i];
			    int y2=yt+yy[i];
			    if(x2>=0&&y2>=0&&x2<n&&y2<m){//和直接最短路区别是无标记，如果不剪枝将会超时
				    if(at[x2][y2]>at[xt][yt]+a[x2][y2]){//剪枝/淘汰路径
                        q.push({x2,y2});
                        at[x2][y2]=at[xt][yt]+a[x2][y2];
                    }
			    }
		    }
	    }return at[n-1][m-1];//终点坐标n-1 m-1
    }
};
```



##### [LeetCode]勇士和地雷阵

题目来源：[“蓝桥杯”练习系统 (lanqiao.cn)](http://lx.lanqiao.cn/problem.page?gpid=T2645)

问题描述：勇士们不小心进入了敌人的地雷阵（用n行n列的矩阵表示，'*'表示某个位置埋有地雷，'-'表示某个位置是安全的），他们各自需要在规定的步数（一步代表走到和当前位置相邻的位置）内绕开地雷到达出口（第一行第一格，即坐标为（0,0）的位置）才能完成任务，告诉你每个勇士的位置（x，y）和规定的步数s，请你判断每个勇士能否顺利完成任务（1代表“能”，-1代表“不能”）。

```c++
/****************************************
  *Author:  麦
  *Contact:  
  *Description:  从(0,0)出发，通过BFS计算到达每一个点的最短距离即可
*****************************************/
#include <iostream>
#include <queue>
using namespace std;
typedef pair<int, int> Pxy;
queue<Pxy> q;

int visited[510][510] = {1}, step[510][510], n;
char G[510][510];
void bfs(int x, int y);

int main() {
	cin >> n;
	for (int i = 0; i < n; i++) {
		cin >> G[i];
	}
	bfs(0, 0);
	int x, y, s;
	while (cin >> x >> y >> s) {
		if (x == 0 && y == 0) {
			cout << 1 << endl;
		} else {
			if (step[x][y] > 0 && step[x][y] <= s) {
				cout << 1 << endl;
			} else {
				cout << -1 << endl;
			}
		}
	}
}

void bfs(int x, int y) {
	q.push({x, y});
	int A[4][2] = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}};
	while (q.size()) {
		for (int i = 0; i < 4; i++) {
			int xx, yy;
			xx = q.front().first + A[i][0];
			yy = q.front().second + A[i][1];
			if (xx >= 0 && xx <= n - 1 && yy >= 0 && yy <= n - 1 && G[xx][yy] != '*' && visited[xx][yy] == 0) {
				visited[xx][yy] = 1;
				step[xx][yy] = step[q.front().first][q.front().second] + 1;
				q.push({xx, yy});
			}
		}
		q.pop();
	}
}
```



#### DFS

##### [ZOJ]Fire Net

题目来源：https://zoj.pintia.cn/problem-sets/91827364500/problems/91827364501

```c++
/****************************************
  *Author:  陈斌
  *Contact:  1198222765@qq.com
  *Description:  N皇后变式
*****************************************/
#include<bits/stdc++.h>
using namespace std;
char coordinate[5][5];
int l[5]={0};
int h[5]={0};
int n;
int res=INT_MIN;
void dfs(int x,int y,int sum){
    if(x==(n-1) && y==(n-1) ){
        if( l[y]==1 || h[x]==1 || coordinate[x][y]=='X' ){
            res=max(res,sum);
            return;
        }else if( l[y]==0 && h[x]==0 || coordinate[x][y]=='.' ){
            res=max(res,sum+1);
            return;
        }
    }
    if(coordinate[x][y]=='X'){
        l[y]=0;
        h[x]=0;
        if((y+1)<n){
            dfs(x,y+1,sum);
        }else{
            dfs(x+1,0,sum);
        }
    }else if(coordinate[x][y]=='.' && l[y]==0 && h[x]==0 ){
        l[y]=1;
        h[x]=1;
        if((y+1)<n){
            dfs(x,y+1,sum+1);
        }else{
            dfs(x+1,0,sum+1);
        }
        l[y]=0;
        h[x]=0;
        if((y+1)<n){
            dfs(x,y+1,sum);
        }else{
            dfs(x+1,0,sum);
        }
    }else if(l[y]==1 || h[x]==1){
        if((y+1)<n){
            dfs(x,y+1,sum);
        }else{
            dfs(x+1,0,sum);
        }
    }
}
int main(){
	while(scanf("%d", &n) && n){
    memset(coordinate,'0',sizeof(coordinate));
    res=INT_MIN;
    if(n==1){
        char x;
        cin>>x;
        if(x=='X'){
            cout<<0<<endl;
        }else{
            cout<<1<<endl;
        }
        continue;
    }
    for(int i=0;i<n;i++){
        for(int j=0;j<n;j++){
            cin>>coordinate[i][j];
        }
    }
    for(int i=0;i<n;i++){
        for(int j=0;j<n;j++){
            memset(l,0,sizeof(l));
            memset(h,0,sizeof(h));
        if(coordinate[i][j]!='X')
        {
            dfs(i,j,0);
        }
        }
    }
    if(res==INT_MIN)res=0;
    cout<<res<<endl;
    }
    return 0;
}
```

##### [LeetCode]岛屿问题

题目来源：https://leetcode.cn/problems/number-of-islands/

问题描述：给你一个由 '1'（陆地）和 '0'（水）组成的的二维网格，请你计算网格中岛屿的数量。

岛屿总是被水包围，并且每座岛屿只能由水平方向和/或竖直方向上相邻的陆地连接形成。

此外，你可以假设该网格的四条边均被水包围。

```c++
/****************************************
  *Author:  陈斌
  *Contact:  1198222765@qq.com
  *Description:  经典dfs
*****************************************/
class Solution {
private:
    void dfs(vector<vector<char>>& grid, int r, int c) {
        int sx=grid.size();
        int zy=grid[0].size();
        grid[r][c]='0';
        if(r-1>=0 && grid[r-1][c]=='1')dfs(grid,r-1,c);
        if(r+1<sx && grid[r+1][c]=='1')dfs(grid,r+1,c);
        if(c-1>=0 && grid[r][c-1]=='1')dfs(grid,r,c-1);
        if(c+1<zy && grid[r][c+1]=='1')dfs(grid,r,c+1);
    }

public:
    int numIslands(vector<vector<char>>& grid) {
        int res=0;
        int sx=grid.size();
        int zy=grid[0].size();
        for(int i=0;i<sx;i++){
            for(int j=0;j<zy;j++){
                if(grid[i][j]=='1'){
                res++;
                dfs(grid,i,j);
                }
            }
        }
        return res;
    }
};
```



##### [PTA]棋盘有地雷的八皇后

问题来源:[题目详情 - 7-12 棋盘有地雷的八皇后问题 (pintia.cn)](https://pintia.cn/problem-sets/1547937217954693120/problems/1558798799819563008)

问题描述:在 8 × 8 的棋盘上摆放八个皇后，使其不能互相攻击 —— 即任意两个皇后都不能处于同一行、同一列或同一条斜线上。
**棋盘某个格子有地雷不能放皇后，问有多少种摆法**

```c++
/****************************************
  *Author:  麦
  *Contact:  
  *Description:  老生常谈八皇后，增加了一个不能放置皇后的地雷点。
*****************************************/
#include <bits/stdc++.h>
using namespace std;
long long G[9][9], ans = 0;
//判断点(x,y)是否能存放皇后
bool check(int x, int y) {
	for (int i = 1; i <= 8; i++) {
		for (int j = 1; j <= 8; j++) {
			if (G[i][j] == 1 && (i == x || j == y || abs(x - i) == abs(y - j))) {
				return false;
			}
		}
	}
	return true;
}
void dfs(int h) {
    //h代表行，for循环在枚举列
	for (int l = 1; l <= 8; l++) {
		if (G[h][l] ) {
			continue;
		}
		if (check(h, l)) {
			G[h][l] = 1;
			if (h == 8 ) {
				ans++;
			} else if (h < 8) {
				dfs(h + 1);
			}
			G[h][l] = 0;
		}
	}
}
int main() {
	int n, m;
	cin >> n >> m;
	G[n][m] = -1;
	dfs(1);
	cout << ans;
	return 0;
}
```

##### [PTA]马走日

题目来源:[题目详情 - 7-4 马走日 (pintia.cn)](https://pintia.cn/problem-sets/1558361330024243200/problems/1558361330103934979)

问题描述：给定n×m大小的棋盘，以及马的初始位置(x，y)，要求不能重复经过棋盘上的同一个点，计算马可以有多少途径遍历棋盘上的所有点

```c++
/****************************************
  *Author:  麦
  *Contact:  
  *Description:  一道硬核dfs题，从基本的尝试4个方向位移变成尝试8个方向的位移，注意地图边界和重复点。简单版的马走日在bfs分类里
*****************************************/
#include <bits/stdc++.h>
using namespace std;
long long ans, n, m, x, y;
int G[11][11];
int moveXY[8][2] = {{-2, 1}, {-2, -1}, {-1, 2}, {1, 2}, {2, -1}, {2, 1}, {1, -2}, {-1, -2}};
void dfs(int x, int y, int cnt) {
	if (cnt == n * m) {
		ans++;
	}
	for (int i = 0; i < 8; i++) {
		int nx = x + moveXY[i][1], ny = y + moveXY[i][0];
		if (nx >= 0 && nx < n  && ny >= 0 && ny < m && !G[nx][ny]) {
			G[nx][ny] = 1;
			dfs(nx, ny, cnt + 1);
			G[nx][ny] = 0;
		}
	}
}
int main() {
	int T;
	cin >> T;
	while (T--) {
		memset(G, 0, sizeof(G));
		ans = 0;
		cin >> n >> m >> x >> y;
		G[x][y] = 1;
		dfs(x, y, 1);
		cout << ans<<endl;
	}
	return 0;
}
```

##### [leetcode] 整数替换

题目来源: https://leetcode.cn/problems/integer-replacement/

代码:

```c++
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description: 因为每一次变换为奇数时需要重新考虑它+1和-1与接下来消除的数哪个更优,而不是单一的状态
  所以,对于每一次的奇数接下来的数,我们都分为两种情况来深入.
**********************************************************************************/
class Solution {
    typedef long long ll;
public:
int ss;
inline void dfs(ll n,int t){
    if(ss&&t>=ss)return;//剪枝
    if(n==1){
        if(!ss)ss=t;
        else ss=min(ss,t);
        return;
    }else {
        if(n%2){
            dfs(n+1,t+1);
            dfs(n-1,t+1);
        }else dfs(n/2,t+1);
    }
} 
    int integerReplacement(int n) {
        dfs(n*1ll,0);
        return ss;
    }
};
```

##### [acwing]看牛

[366. 看牛 - AcWing题库](https://www.acwing.com/problem/content/description/368/) 

```
/****************************************
  *Author:  杨杰
  *Contact:  
  *Description: 
*****************************************/
#include <bits/stdc++.h>
using namespace std;
#define N 10005
#define M 50007
int n, m;
int head[N], tot, ver[M * 2], nxt[M * 2];
int ans[M * 4], t;
int stac[M * 4], top;
inline void add(int x, int y)
{
    ver[++tot] = y;
    nxt[tot] = head[x];
    head[x] = tot;
}
void euler()
{
    stac[++top] = 1;
    while (top > 0)
    {
        int x = stac[top];
        int i = head[x];
        if(i)
        {
            stac[++top] = ver[i];
            head[x] = nxt[i];
        }
        else
        {
            ans[++t] = x;
            top--;
        }
    }
}
int main()
{
    tot = 1;
    scanf("%d%d", &n, &m);
    for (int i = 1; i <= m; i++)
    {
        int x, y;
        scanf("%d%d", &x, &y);
        add(x, y);
        add(y, x);
    }
    euler();
    for (int i = t; i >= 1; i--)
        printf("%d\n", ans[i]);
    return 0;
}
```

	##### [HDOJ]Tempter of the Bone

题目来源：[Problem - 1010 (hdu.edu.cn)](https://acm.hdu.edu.cn/showproblem.php?pid=1010)

代码如下：

```c++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description:  
*****************************************/
#include<iostream>
#include<string.h>
using namespace std;
int n,m,t;
int sx,sy;
int cx,cy;
char a[10][10];
int dir[4][2]={{0,1},{0,-1},{1,0},{-1,0}};
bool res;
void dfs(int x,int y,int index){
    if(x<0 || x>=n || y<0 || y>=m)return;
    if(x==cx && y==cy && index==t){
        res=true;
        return;
    }
    if(res)return;
    int temp=(t-index)-(abs(x-cx)+abs(y-cy));
    if(temp<0 || temp&1 )return;
    for(int i=0;i<4;i++){
        int xx=x+dir[i][0];
        int yy=y+dir[i][1];
        if(a[xx][yy]!='X'){
            a[xx][yy]='X';
            dfs(xx,yy,index+1);
            a[xx][yy]='.';
            if(res)return;
        }
    }
}
int main(){
    while(cin>>n>>m>>t&&(n||m)){
        int wall=0;
        for(int i=0;i<n;i++)
        for(int j=0;j<m;j++){
            cin>>a[i][j];
            if(a[i][j]=='S')sx=i,sy=j;
            else if(a[i][j]=='D')cx=i,cy=j;
            else if(a[i][j]=='X')wall++;
        }
        if(n*m-wall<=t){
            cout<<"NO"<<endl;
            continue;
        }
        res=false;
        a[sx][sy]='X';
        dfs(sx,sy,0);
        if(res)cout<<"YES"<<endl;
        else cout<<"NO"<<endl;
    }
    return 0;
}
```



#### 回溯

##### [LeetCode]组合

题目来源：https://leetcode-cn.com/problems/combinations/

问题描述：给定两个整数 `n` 和 `k`，返回范围 `[1, n]` 中所有可能的 `k` 个数的组合。

你可以按 **任何顺序** 返回答案。

```c++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description:  
*****************************************/
class Solution {
public:
    vector<int> temp;
    vector<vector<int>> ans;
    void dfs(int cur, int n, int k) {
        if(temp.size()+(n-cur+1)<k){
            return;
        }
        if(temp.size()==k){
            ans.push_back(temp);
            return;
        }
        temp.push_back(cur);
        dfs(cur+1,n,k);
        temp.pop_back();
        dfs(cur+1,n,k);
    }

    vector<vector<int>> combine(int n, int k) {
        dfs(1, n, k);
        return ans;
    }
};
```

##### [LeetCode]N皇后II

题目来源：https://leetcode.cn/problems/n-queens-ii/

问题描述：**n 皇后问题** 研究的是如何将 `n` 个皇后放置在 `n × n` 的棋盘上，并且使皇后彼此之间不能相互攻击。

给你一个整数 `n` ，返回 **n 皇后问题** 不同的解决方案的数量。

~~~c++
/****************************************
  *Author:  陈家乐
  *Contact:  784190546@qq.com
  *Description: 不断尝试，如果不符合条件了就回溯继续往下尝试 
*****************************************/
class Solution {
public:
    int *dp;
    int num;
    int sum=0;
    bool place(int x){
        for(int i=1;i<x;i++){
            if(abs(dp[i]-dp[x]) == x-i || dp[i] == dp[x]){
                return false;
            }
        }
        return true;
    }
    void backtrack(int x){
        if(x == num + 1){
            sum++;
        }else{
            for(int i=1;i<=num;i++){
                dp[x] = i;
                if(place(x)){
                    backtrack(x+1);
                }
            }
        }
    }
    int totalNQueens(int n) {
        num = n;
        dp = new int [num+1];
        memset(dp,0,sizeof(dp));
        backtrack(1);
        return sum;
    }
};
~~~
##### [蓝桥]粘木棍

题目来源：http://lx.lanqiao.cn/problem.page?gpid=T2982

问题描述：有N根木棍，需要将其粘贴成M个长木棍，使得最长的和最短的的差距最小。

```c++
/****************************************
  *Author:  mai
  *Contact:  
  *Description:  N根木棍放到M组，并没有规定每组不可以为空，所以可以枚举每一种情况，将每种可能的差值进行比较，保留最小差值。
*****************************************/
#include <iostream>
using namespace std;
int Min = 32767;
int n, m;
int Value[10];
int Array[10];
void dfs(int index);
int main() {
	cin >> n >> m;
	for (int i = 1; i <= n; i++) {
		cin >> Array[i];
	}
	dfs(1);
	cout << Min;
}
void dfs(int index) {
	if (index > n) {
		int maxValue = Value[1], minValue = Value[1];
		for (int i = 1; i <= m; i++) {
			maxValue = max(maxValue, Value[i]);
			minValue = min(minValue, Value[i]);
		}
		Min = min(Min, maxValue - minValue);
        //结束本次迭代它罪恶的一生
		return;
	}
	for (int i = 1; i <= m; i++) {
        //将第index根棍子粘到第i根长木棍上
		Value[i] += Array[index];
        //继续放下一根
		dfs(index + 1);
        //回溯
		Value[i] -= Array[index];
	}
}
```

##### [蓝桥]自然数拆分

题目来源：http://lx.lanqiao.cn/problem.page?gpid=T1665

问题描述：HJQ同学发现了一道数学题，要求n拆分成若干自然数和的方案.列出所有可能的方案

```c++
/****************************************
  *Author:  mai
  *Contact:  
  *Description:  一一枚举情况和回溯
*****************************************/
#include <iostream>
#include <vector>
using namespace std;
vector<int> result;
void solve(int x, int n);
int main() {
	int n;
	cin >> n;
	slove(1, n);
}
void solve(int x, int n) {
    //当剩余值小于0时不符合条件，即之前所加的数字大于n
	if (n< 0)
		return;
    //n==0 且 result内有数字 就这些数字代表符合恰好分解
	if (n == 0 && result.size() > 1) {
		for (int i = 0; i < result.size() - 1; i++) {
			cout << result[i] << "+";
		}
		cout << result.back() << endl;
		return;
	}
    //咱也不晓得x要累加几次，干脆用while吧
	while (n - x >= 0) {
        //放入符合条件的x
		result.push_back(x);
		solve(x, n - x);
        //回溯
		result.pop_back();
		x++;
	}
}
```

##### **[LeetCode]括号生成**

题目来源:[22. 括号生成 - 力扣（LeetCode）](https://leetcode.cn/problems/generate-parentheses/)

问题描述:数字 `n` 代表生成括号的对数，请你设计一个函数，用于能够生成所有可能的并且 **有效的** 括号组合。

 ```c++
/****************************************
  *Author:  麦
  *Contact:  
  *Description: 根据当前left和right的数量，决定是否放入左括号和右括号，只有当左括号的数量不少于右括号时，才能放入右括号，且左括号的放入优先级要高于右括号
*****************************************/
class Solution {
public:
    vector<string> result;
    void dfs(string cur,int n,int left,int right){
        if(cur.size()==2*n){
            result.push_back(cur);
            return;
        }
        if(left<n){
            cur+="(";
            dfs(cur,n,left+1,right);
            cur.pop_back();
        }
        if(left>right){
            cur+=")";
            dfs(cur,n,left,right+1);
            cur.pop_back();
        }
    }
    vector<string> generateParenthesis(int n) {
        dfs("",n,0,0);
        return result;
    }
};
 ```

##### [力扣 297周赛]公平分发饼干

题目来源:   https://leetcode.cn/problems/fair-distribution-of-cookies/

代码: 

```c++
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description: 数据比肩状态,开搜
   ps:想想dp该怎么写
**********************************************************************************/
class Solution {
    vector<int> q;
    bool st[20];
    int n,m,r=2e9;
public:
    void dfs(int u){
    if (u == m){
        int ma=-2e9;
        for (int i = 0 ; i < m ; i ++){
            if (q[i] > ma) ma = q[i];
        }r= min(r ,ma);
        return;
    }
    for (int i = 0; i < u ; i ++)
        for (int j = i + 1 ; j < u; j ++){
            q[i] += q[j];
            swap(q[j], q[u - 1]);
            dfs(u - 1);//选与不选,常规题啦
            swap(q[j], q[u - 1]);
            q[i]-=q[j];
        }
    }int distributeCookies(vector<int>& a, int k) {
        n=a.size(),m=k,q=a;
        dfs(n);
        return r;
    }
};

```

##### [蓝桥]智能体系列赛

题目来源：[“蓝桥杯”练习系统 (lanqiao.cn)](http://lx.lanqiao.cn/problem.page?gpid=T2993)

问题描述：有一只猴子和一些矿点，知道他们在平面上的坐标，这只猴子要经过这些矿点至少一次。假设这只猴子从点A走到点B所要花费的步数是这两个点的曼哈顿距离（即|A.x-B.x|+|A.y-B.y|），问这只猴子经过这些矿点至少一次所需的最少步数。

```c++
/*********************************************************************************
  *Author:  麦
  *Contact:  
  *Description: 其实就是枚举走到某一个点后，可以到下一个点的所有可能，不断更新最小值
**********************************************************************************/
#include <iostream>
#include <vector>
#include <math.h>
using namespace std;
int x, y, n;
typedef pair<int, int> PII;
vector<PII> A;
int visited[11], Min = 100000;
void dfs(int cur_x, int cur_y, int step, int count);
int main() {
	cin >> x >> y >> n;
	for (int i = 0; i < n; i++) {
		int a, b;
		cin >> a >> b;
		A.push_back({a, b});
	}

	dfs(x, y, 0, 0);
	cout << Min;
}
void dfs(int cur_x, int cur_y, int step, int count) {
	if (count > Min)
		return;
	if (step == n) {
		Min = Min > count ? count : Min;
		return;
	}
	for (int i = 0; i < n; i++) {
		if (visited[i] == 1)
			continue;
		visited[i] = 1;
		int x = A[i].first, y = A[i].second;
		count += labs(cur_x - x) + labs(cur_y - y);
		dfs(x, y, step + 1, count);
		count -= abs(cur_x - x) + abs(cur_y - y);
		visited[i] = 0;
	}
}
```

##### [acwing]顺时针打印矩阵

题目来源：[40. 顺时针打印矩阵 - AcWing题库](https://www.acwing.com/problem/content/description/39/) 

```
/****************************************
  *Author:  crazy
  *Contact:  
  *Description: 顺时针的方向是右下左上，先保持一个方向进行打印，如果下一个点越界或已打印过，就换一个方向。
同时用布尔数组标记访问过的元素，如果下一个点访问过的话，就换一个方向。
*****************************************/
class Solution {

    public int[] printMatrix(int[][] matrix) {
        int n = matrix.length;
        if(n == 0) return new int[0];
        int m = matrix[0].length;
        int[] res = new int[n * m];
        boolean[][] st = new boolean[n][m];
        final int[] dx = {-1, 0, 1, 0};
        final int[] dy = {0, 1, 0, -1};

        int x = 0, y = 0, d = 1, len = 0;
        for(int i = 0; i < n * m; ++i) {
            res[len++] = matrix[x][y];
            st[x][y] = true;
            int a = x + dx[d], b = y + dy[d];
            if(a < 0 || a >= n || b < 0 || b >= m || st[a][b]) {
                d = (d + 1) % 4;
                a = x + dx[d];
                b = y + dy[d];
            }
            x = a;
            y = b;
        }
        return res;
    }
}

。
```



### 字符串

#### KMP算法

##### [debug]

```c++
/*******************************************************
  *Author:  Mai
  *Contact:  
  *Description: 在求next数组遇到的bug
  
	求Next数组的过程中，需要避免字符串长度未转成int直接比较，例如
	if(j<s.size()){
		...
	}
	在调用size()函数将返回一个unsigned(无符号类型) int
	如果此时j=-1,size()=5
	-1<5将不成立，因为5是无符号数，需要转为int
	if(j<int(s.size())){
		...
	}
*******************************************************/
```

##### [POJ]Blue Jeans

题目来源：[3080 -- Blue Jeans (poj.org)](http://poj.org/problem?id=3080)

```c++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description: kmp字符串匹配
*****************************************/
#include<iostream>
#include<string.h>
using namespace std;
const int N=65;
int Next[N];
char s[11][N];
char s2[N];
char s3[N];
int temp,n,t;
void get_next(int n){
    int j=-1;
    Next[0]=j;
    for(int i=1;i<n;i++){
        while(j&&s2[i]!=s2[j+1])j=Next[j];
        if(s2[i]==s2[j+1])j++;
        Next[i]=j;
    }
}
void get_find(int x,int m){
    temp=N;
    for(int k=1;k<n;k++){
        int j=0;
        int p=0;
        for(int i=0;i<x && j<m;i++){
            while(j&&s[k][i]!=s2[j])
                j=Next[j];
            if(s[k][i]==s2[j])j++;
            if(j>p)p=j;
        }
        if(p<temp)temp=p;
    }
}
signed main(){
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    cin>>t;
    while(t--){
        cin>>n;
        for(int i=0;i<n;i++)cin>>s[i];
        int len=0;
        for(int i=0;i<=57;i++){
            strcpy(s2,s[0]+i);
            get_find(60,60-i);
            if(temp>len){
                len=temp;
                strncpy(s3,s[0]+i,len);
                s3[len]='\0';
            }else if(temp==len){
                strncpy(s3,s[0]+i,len);
                s2[len]='\0';
                if(strcmp(s2,s3)<0){
                    strcpy(s3,s2);
                    s3[len]='\0';
                }
            }
        }
        if(len>=3)cout<<s3<<endl;
        else cout<<"no significant commonalities"<<endl;
    }
}
```



#### AC自动机

#### 字典树

#### 后缀数组

### 贪心

##### [LeetCode]跳跃游戏II

题目来源：https://leetcode-cn.com/problems/jump-game-ii/

问题描述:给你一个非负整数数组 nums ，你最初位于数组的第一个位置。数组中的每个元素代表你在该位置可以跳跃的最大长度。你的目标是使用最少的跳跃次数到达数组的最后一个位置。假设你总是可以到达数组的最后一个位置。

```c++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description: 记录当前跳跃点的值，遍历当前点可以跳跃到的点，找到其中可以走最远的点。
*****************************************/
class Solution {
public:
    int res=0;
    int tempindex=0;
    int jump(vector<int>& nums) {
        if(nums.size()==1||nums.size()==0)return res;
        res=1;
        if(nums[0]>=nums.size()-1)return res;
        for(int i=0;i<nums.size();i++){
            int cur=nums[i];
            int temp=INT_MIN;
            for(int j=1;j<=cur;j++){
                if(nums[j+i]+j>temp){
                    temp=nums[j+i]+j;
                    tempindex=j+i;
                }
            }
            if(nums[tempindex]>=nums.size()-tempindex-1){
                res++;
                break;
            }
            res++;
            i=tempindex-1;
        }
        return res;
    }
};
```

##### [力扣] 装满石头的背包的最大数量

题目来源：https://leetcode.cn/problems/maximum-bags-with-full-capacity-of-rocks/
问题描述：现有编号从 0 到 n - 1 的 n 个背包。给你两个下标从 0 开始的整数数组 capacity 和 rocks 。第 i 个背包最大可以装 capacity[i] 块石头，当前已经装了 rocks[i] 块石头。另给你一个整数 additionalRocks ，表示你可以放置的额外石头数量，石头可以往 任意 背包中放置。
请你将额外的石头放入一些背包中，并返回放置后装满石头的背包的最大数量。

```c++
/*********************************************************************************
  *Author:  crazy杨
  *Contact:  1678951138@qq.com
  *Description: 用sort排序从小到大，先装缺最少的石头，再一个一个加上装满背包的数量核心  additionalRocks=a[i];可以减少运行的时间防止超时
  *********************************************************************************/

class Solution {
public:
    int maximumBags(vector<int>& capacity, vector<int>& rocks, int additionalRocks) {
                   int result=0;
        int a[rocks.size()];
          for(int i=0;i<capacity.size();i++){
            a[i]=capacity[i]-rocks[i];
          }
        sort(a,a+rocks.size());
        for(int i=0;i<rocks.size();i++){
            if(a[i]==0){
                result++;
            }else{
                additionalRocks-=a[i];
                if(additionalRocks<0){
                    break;
                }
                result++;
            }
        }
        return result;
    }
};
```

##### [LeetCode]无重叠区间

题目来源：https://leetcode.cn/problems/non-overlapping-intervals/

问题描述：给定一个区间的集合 intervals ，其中 intervals[i] = [starti, endi] 。返回 需要移除区间的最小数量，使剩余区间互不重叠 。

```c++
/****************************************
  *Author:  陈斌
  *Contact: 1198222765@qq.com 
  *Description:  贪心,求移除最小的数量，计算保留最多的可能
*****************************************/
class Solution {
public:
    int eraseOverlapIntervals(vector<vector<int>>& intervals) {
        if(intervals.size()==0)return 0;
        sort(intervals.begin(),intervals.end(),[](const auto&u,const auto&v){
            return u[1]<v[1];
        });
        int n=intervals.size();
        int right=intervals[0][1];
        int ans=1;
        for(int i=1;i<n;i++){
            if(intervals[i][0]>=right){
                ans++;
                right=intervals[i][1];
            }
        }
        return n-ans;
    }
};

```

##### [力扣]  **美化数组的最少删除数** 

题目来源：https://leetcode.cn/problems/minimum-deletions-to-make-array-beautiful/

问题描述：给你一个下标从 0 开始的整数数组 nums ，如果满足下述条件，则认为数组 nums 是一个 美丽数组 ：nums.length 为偶数对所有满足 i % 2 == 0 的下标 i ，nums[i] != nums[i + 1] 均成立注意，空数组同样认为是美丽数组。你可以从 nums 中删除任意数量的元素。当你删除一个元素时，被删除元素右侧的所有元素将会向左移动一个单位以填补空缺，而左侧的元素将会保持 不变 。返回使 nums 变为美丽数组所需删除的 最少 元素数目。

代码:

```c++
/*********************************************************************************
  *Author:  陈高威
  *Contact:  1259234645@qq.com
  *Description:  如果当前数可以作为数对中的第二个数就保留，它的下一个数直接作为下一个数对中的第一个数
**********************************************************************************/
class Solution {
public:
    int minDeletion(vector<int>& nums) {
        int n = nums.size();
        int ans = 0;
        for (int i = 0; i + 1 < n; i++) {
            if (nums[i] == nums[i + 1]) ans++;
            else i++;
        }
        if ((n - ans) % 2) ans++;
        return ans;
    }
};
```

#####  [力扣]  毯子覆盖的最多白色砖块数

题目来源：https://leetcode.cn/problems/maximum-white-tiles-covered-by-a-carpet/

代码:

```c++
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description: 按左边界排序然后每一次取满左边界
   ps:期末炒一波冷饭 QAQ
**********************************************************************************/
class Solution {
public:
int maximumWhiteTiles(vector<vector<int>>& a, int k) {
        sort(a.begin(),a.end());
        int ss=0,pp=0;
        int i,n=a.size();
        int r=a[0][0]+k-1,t=0;
        for(i=0;i<n;i++){
            while(a[i][1]>r){
                ss=max(ss,pp+max(0,r-a[i][0]+1));//在左边界每一次滑到下一个left的时候贪心
                pp-=a[t][1]-a[t][0]+1;
                r+=a[t+1][0]-a[t][0];
                t++;
            }pp+=a[i][1]-a[i][0]+1;//没取满时一直取
            ss=max(ss,pp);
        }return ss;
    }
};
```

##### [力扣]  最大数

题目来源：https://leetcode.cn/problems/largest-number/

代码:

```c++
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description: 直接按字典序比串大小在两个串长度一样的时候是可行的,但长度不一样时就不行了,比如"3"和
  "30","330"当然比"303"要大,但按字典序排的话截到3后面就开始按长度来排了，明显在这里这个规则是有问题
  的,我们在处理较长字符串截取的后半部分的时候应当与较短字符串从头开始比较,这样才能拼接出最大的结果.
**********************************************************************************/
class Solution {
public:
static bool cmp(string &a,string &b){
    int n=a.size(),m=b.size();
        for(int i=0;i<=n+m;i++)
        if(a[i%n]!=b[i%m])return a[i%n]<b[i%m];//循环出两条串的长度即表示前后顺序不影响
        return a<b;//如果最后没有分出大小,则表示前后顺序是不影响的(如"1212"和"12"),那哪个排前都行
}
    string largestNumber(vector<int>& s1) {
        string ss;
        vector<string> a;
        for(int i=0;i<s1.size();i++){
            a.push_back(to_string(s1[i]));//数字转字符串
        }sort(a.begin(),a.end(),cmp);//贪心排序
        int pd=0;
        for(int i=a.size()-1;i>=0;i--){
            if(a[i]!="0")pd=1;//不能有前导0哦
            if(pd)ss=ss+a[i];
        }if(!pd)return "0";
        return ss;
    }
};
```

#### 

##### [力扣]  分发饼干

题目来源：https://leetcode.cn/problems/assign-cookies/

代码：

```java
/****************************************
  *Author:  MA
  *Contact:  897039531@qq.com
  *Description:  贪心
*****************************************/
class Solution {
    public int findContentChildren(int[] g, int[] s) {
        Arrays.sort(g);
        Arrays.sort(s);
        //index做饼干尺寸数组下标
        int index = s.length-1;
        int sum=0;
        //逆序比较孩子胃口与饼干尺寸
        for(int i=g.length-1;i>=0;i--){
            //倘若饼干尺寸满足（大于等于）孩子胃口
            if(index>=0 && s[index]>=g[i]){
                //饼干数组索引前移（尺寸不变或减小）
                index--;
                //满足孩子总数sum++
                sum++;
            }
        }
        return sum;
    }
}
```



##### [力扣]  摆动序列

题目来源：https://leetcode.cn/problems/wiggle-subsequence/

代码：

```c++
/****************************************
  *Author:  MA
  *Contact:  897039531@qq.com
  *Description:  贪心
*****************************************/
class Solution {
public:
    int wiggleMaxLength(vector<int>& nums) {
        //仅有一个元素的序列也视作摆动序列。
        if(nums.size()<=1)return nums.size();
        int curDiff=0;  //当前元素与其后一元素差值
        int preDiff=0;  //上一元素与其后一元素插值
        int result=1;   //将最长摆动子序列末尾元素录入
        for(int i=0;i<nums.size()-1;i++){
            //计算当前元素与其后一元素差值
            curDiff = nums[i + 1] - nums[i];
            //如果满足差值正负出现
            if((curDiff>0 && preDiff<=0)||(curDiff<0 && preDiff>=0)){
                result++;
                //差值传递，以便判定下一个不同符号差值
                preDiff=curDiff;
            }
        }
        return result;
    }
};
```



##### [力扣]  买卖股票的最佳时机 II

题目来源：https://leetcode.cn/problems/best-time-to-buy-and-sell-stock-ii/

代码：

```c++
/****************************************
  *Author:  MA
  *Contact:  897039531@qq.com
  *Description:  贪心
*****************************************/
class Solution {
public:
    int maxProfit(vector<int>& prices) {
        int result=0;
        for(int i=1;i<prices.size();i++){
            int profit=prices[i]-prices[i-1];
            //若股票价格上涨，则视作一次买入卖出，计入总利润
            if(profit>0)result+=profit;
        }
        return result;
    }
};
```



##### [力扣]  跳跃游戏

题目来源：https://leetcode.cn/problems/jump-game

代码：

```c++
/****************************************
  *Author:  MA
  *Contact:  897039531@qq.com
  *Description:  贪心
*****************************************/
class Solution {
public:
    bool canJump(vector<int>& nums) {
        int cover=0;
        if(nums.size()==0)return true;
        for(int i=0;i<=cover;i++){
            //cover记录当前可达最大下标
            cover=max(i+nums[i],cover);
            if(cover>=nums.size()-1)return true;
        }
        return false;
    }
};
```



##### [力扣]  1005. K 次取反后最大化的数组和

题目来源：https://leetcode.cn/problems/maximize-sum-of-array-after-k-negations

代码：

```c++
/****************************************
  *Author:  MA
  *Contact:  897039531@qq.com
  *Description:  贪心
*****************************************/
class Solution {
public:
    int largestSumAfterKNegations(vector<int>& nums, int k) {
        int sum=0;
        sort(nums.begin(),nums.end());
        for(int i=0;i<nums.size();i++){
            //按绝对值大小将负数取反
            if(nums[i]<0 && k>0){
                nums[i]*=-1;
                k--;
            }
            sum+=nums[i];
        }
        sort(nums.begin(),nums.end());
        //若负数全部取反后k未耗尽，则将绝对值最小元素剔除
        return k%2==0?sum:sum-nums[0]*2;
    }
};
```



##### [力扣]  134. 加油站

题目来源：https://leetcode.cn/problems/gas-station/

```c++
/****************************************
  *Author:  MA
  *Contact:  897039531@qq.com
  *Description:  贪心
*****************************************/
class Solution {
public:
    int canCompleteCircuit(vector<int>& gas, vector<int>& cost) {
        //计算当前剩余油量
        int curSum = 0;
        //计算全程剩余油量
        int totalSum = 0;
        int start = 0;
        for(int i = 0;i < gas.size();i++){
            curSum += gas[i]-cost[i];
            totalSum += gas[i]-cost[i];
            //若当前剩余油量小于零，则[1,i]区间不存在出发点
            if(curSum < 0){
                //出发点暂记i + 1
                start = i + 1;
                //重新计算当前剩余油量
                curSum = 0;
            }
        }
        //如果全称剩余油量小于零，则无法绕环路行驶一周
        if(totalSum < 0)return -1;
        return start;
    }
};
```



##### [力扣] 135. 分发糖果

题目来源：https://leetcode.cn/problems/candy/

```c++
/****************************************
  *Author:  MA
  *Contact:  897039531@qq.com
  *Description:  贪心
*****************************************/
class Solution {
public:
    int candy(vector<int>& ratings) {
        vector<int> candy(ratings.size() , 1);
        
        for(int i = 1; i < ratings.size(); i++){
            if(ratings[i] > ratings[i-1])candy[i] = candy[i-1] + 1;
        }
        for(int i = ratings.size()-2; i >= 0; i--){
            if(ratings[i] > ratings[i+1]){
                candy[i] = max(candy[i],candy[i+1]+1); 
            }
        }
        int result = 0;
        for(int i = 0; i<candy.size(); i++){
            result += candy[i];
        }
        return result;
    }
};
```



##### [力扣] 860. 柠檬水找零

题目来源：https://leetcode.cn/problems/lemonade-change/

```c++
/****************************************
  *Author:  MA
  *Contact:  897039531@qq.com
  *Description:  贪心
*****************************************/
class Solution {
public:
    bool lemonadeChange(vector<int>& bills) {
        int five = 0, ten =0;
        for(int num : bills){
            if(num == 5)five++;
            if(num == 10){
                if(five < 0)return false;
                five--;
                ten++;             
            }
            if(num == 20){
                if(five > 0 && ten > 0){
                    five--;
                    ten--;
                }else if(five > 2){
                    five -= 3;
                }else return false;
            }
        }
        return true;
    }
};
```

##### [力扣] 重构字符串

题目来源:https://leetcode.cn/problems/reorganize-string/

代码:

```c++
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description: 先统计每一种字符出现次数,然后取最多和次多的优先接上队尾.
**********************************************************************************/
class Solution {
public:
    string reorganizeString(string s) {
        string ss;
        int a[26]={0},i;
        int n=s.size();
        int m1=0;
        for(i=0;i<s.size();i++){
            a[s[i]-'a']++;
            m1=max(m1,a[s[i]-'a']);
        }if(m1>n/2+n%2)return "";//特判一下特殊情况
        while(1){
            int j=-1,m1=0;
            for(i=0;i<26;i++){
                if(a[i]>m1)m1=a[i],j=i;//优先取出最大的合并
            }if(j>=0)ss+=(j+'a'),a[j]--;
            for(i=0;i<26;i++){
                if(i!=j&&a[i]){ss+=('a'+i),a[i]--;break;}
            }if(!m1)break;
        }return ss;
    }
};
```



##### [力扣] 452. 用最少数量的箭引爆气球

题目来源：https://leetcode.cn/problems/minimum-number-of-arrows-to-burst-balloons

```c++
/****************************************
  *Author:  MA
  *Contact:  897039531@qq.com
  *Description:  贪心
*****************************************/
class Solution {
    static bool cmp(const vector<int>& a, const vector<int>& b){
        return a[0]<b[0];
    }
public:
    int findMinArrowShots(vector<vector<int>>& points) {
        int result = 1;
        sort(points.begin(),points.end(),cmp);

        for(int i = 1; i < points.size(); i++){
            if(points[i][0] > points[i-1][1])result++;
            else points[i][1] = min(points[i-1][1],points[i][1]);
        }
        return result;
    }
};

//sort函数中的cmp比较函数
#include <algorithm>
static bool cmp(const vector<int>& a, const vector<int>& b) {
	if (a[0] == b[0]) return a[1] < b[1];	//[0]相同则比较[1]
	return a[0] > b[0];  					//比较[0]
}
void sort(iterator start, iterator end, StrictWeakOrdering cmp);

//传入数组左开右闭，即[begin, end)
sort(vector.begin(),vector.end(),cmp);
```

##### 

##### [力扣] 435. 无重叠区间

题目来源：https://leetcode.cn/problems/non-overlapping-intervals/

```c++
/****************************************
  *Author:  MA
  *Contact:  897039531@qq.com
  *Description:  贪心
*****************************************/
class Solution {
    static bool cmp(const vector<int>& a, const vector<int>& b){
		//按区间左边界排序
        return a[0] < b[0];
    }
public:
    int eraseOverlapIntervals(vector<vector<int>>& intervals) {
        if (intervals.size() == 0) return 0;
        sort(intervals.begin(), intervals.end(), cmp);

        int result = 1;
        int end = intervals[0][1];

        for(int i = 1; i < intervals.size(); i++){
            //若区间不重叠
            if(end <= intervals[i][0]){
                result++;
                //继承该区间右边界
                end = intervals[i][1];
            }else{
                //若重叠，则选择区间右边界更小者
                end = min(intervals[i][1],end);
            }
        }
        return intervals.size()-result;
    }
};

//计算交叉区间，再减去
class Solution {
    static bool cmp(const vector<int>& a, const vector<int>& b){
        //左右边界排序皆可
        return a[1] < b[1];
    }
public:
    int eraseOverlapIntervals(vector<vector<int>>& intervals) {
        if(intervals.size() == 0)return 0;
        sort(intervals.begin(),intervals.end(),cmp);
        int result = 1;
        for(int i = 1; i < intervals.size(); i++){
            if(intervals[i-1][1] <= intervals[i][0]){
                //若区间不重叠，计数增加
                result++;
            }else{
                //否则选择右边界更小的区间
                intervals[i][1]=min(intervals[i-1][1],intervals[i][1]);
            }
        }
        return intervals.size() - result;
    }
};
```



##### [力扣] 763. 划分字母区间

题目来源：https://leetcode.cn/problems/partition-labels

```c++
/****************************************
  *Author:  MA
  *Contact:  897039531@qq.com
  *Description:  贪心
*****************************************/
class Solution {
public:
    vector<int> partitionLabels(string s) {
        int hash[27] = {0};
        for(int i = 0; i < s.length(); i++){
            //记录该字符下标最大值
            hash[s[i]-'a'] = i;
        }
        vector<int> result;
        int left = 0;
        int right = 0;
        for(int i = 0; i < s.length(); i++){
            //更新当前字段包含字符最大下标
            right = max(right, hash[s[i]-'a']);
            //若到达当前字段包含字符最大下标
            if(i == right){
                //录入当前字母区间长度
                result.push_back(right - left + 1);
                //重置left
                left = i + 1;
            }
        }
        return result;
    }
};
```

##### [PTA]**装箱问题**

题目来源：[题目详情 - 7-3 3007 装箱问题 (pintia.cn)](https://pintia.cn/problem-sets/1558361330024243200/problems/1558361330103934978)

问题描述：一个工厂生产的产品形状都是长方体，高度都是h，主要有1 * 1，2 * 2，3 * 3，4 * 4，5 * 5，6 * 6等6种。这些产品在邮寄时被包装在一个6 * 6 * h的长方体包裹中。由于邮费很贵，工厂希望减小每个订单的包裹数量以增加他们的利润。因此他们需要一个好的程序帮他们解决这个问题。你的任务就是设计这个程序。

```c++
/****************************************
  *Author:  麦
  *Contact:  
  *Description:  思考放入的时候，要考虑能不能塞得下，边角料不能用。咱先把大的给他塞进去，再依次放入小的
*****************************************/
#include <bits/stdc++.h>
using namespace std;
int main() {
	int a[7];
	cin >>  a[1] >> a[2] >> a[3] >> a[4] >> a[5] >> a[6];
	while (a[1] || a[2] || a[3] || a[4] || a[5] || a[6]) {
		//3*3及以上的产品都需要至少一个包裹
		int ans = a[4] + a[5] + a[6] + (a[3] + 3) / 4;
//		int box_2x2 = 0; //包裹能够提供2x2空间的个数
//		box_2x2 = a[4] * 5; //每个4x4产品都能附带5个2x2产品
//		if (4-a[3] % 4 == 1) {
//			box_2x2 += 1;
//		} else if (4-a[3] % 4 == 2) {
//			box_2x2 += 3;
//		} else if (4- a[3] % 4 == 3) {
//			box_2x2 += 5;
//		}
		int box_2x2 = a[4] * 5 + (4 - a[3] % 4) * 2 - 1;
		if (box_2x2 < a[2]) {//如果能提供的2x2空间不足，则增加包裹
			ans++;
		}
		//包裹总体积-产品体积=剩余体积，看看能不能装下1x1的产品
		int box_1x1 = ans * 36 - a[6] * 36 - a[5] * 25 - a[4] * 16 - a[3] * 9 - a[2] * 4;
		if (box_1x1 < a[1]) {
			ans++;
		}
		cout << ans << endl;
		cin >>  a[1] >> a[2] >> a[3] >> a[4] >> a[5] >> a[6];
	}
	return 0;
}
```

##### [HDOJ]To Be an Dream Architect

题目来源：[Problem - 3682 (hdu.edu.cn)](https://acm.hdu.edu.cn/showproblem.php?pid=3682)

代码如下：

```c++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description: 给每一个格子一个标号，删除重复的
*****************************************/
#include<stdio.h>
#include<vector>
#include<algorithm>
using namespace std;
int t,n,m,u,v;
char c,d;
int main(){
    vector<int>a;
    scanf("%d",&t);
    while(t--){
        scanf("%d%d",&n,&m);
        getchar();
        a.clear();
        while(m--){
            scanf("%c%*c%d%*c%c%*c%d%*c",&c,&u,&d,&v);
            if(c == 'X' && d == 'Y')for(int i = 1; i <= n; i++)a.push_back(u*n*n+v*n+i);
			else if(c == 'X' && d == 'Z')for(int i = 1; i <= n; i++)a.push_back(u*n*n+i*n+v);
			else if(c == 'Y' && d == 'X')for(int i = 1; i <= n; i++)a.push_back(v*n*n+u*n+i);
			else if(c == 'Y' && d == 'Z')for(int i = 1; i <= n; i++)a.push_back(i*n*n+u*n+v);
			else if(c == 'Z' && d == 'Y')for(int i = 1; i <= n; i++)a.push_back(i*n*n+v*n+u);
			else if(c == 'Z' && d == 'X')for(int i = 1; i <= n; i++)a.push_back(v*n*n+i*n+u);
        }
        sort(a.begin(),a.end());
        int res=unique(a.begin(),a.end())-a.begin();
        printf("%d\n",res);
    }
    return 0;
}
```

##### [acwing] 火星人

题目来源: https://www.acwing.com/problem/content/description/422/

代码:

```c++
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description: 题目大意就是求一个在给定排列数字典序后m个的排列数,而且每次都只能交换两个数,那每后面一
  个就是把放在前面的不够大的数与后面比它大的最小数交换再把后面的数重排一下,共交换m次,即得到结果.
**********************************************************************************/
#include<bits/stdc++.h>
typedef long long ll;
using namespace std;
int main(){
    int n,m,i,j,k;
    int a[10010];
    scanf("%d%d",&n,&m);
    for(i=0;i<n;i++)scanf("%d",&a[i]);
    while(m--){
        j=n-1;
        while(a[j-1]>a[j])j--;//做加法要从小变大,从前往后找第一个较小的数 
        k=--j;
        while(a[k+1]>a[j]&&k<n-1)k++;//找到第一个次小的数 
        swap(a[k],a[j]);//交换后升序排出的就是下一个数,即+1 
        reverse(a+j+1,a+n);//本就是逆序,倒序即是下一个的结果 
    }for(i=0;i<n;i++)printf("%d ",a[i]);
    return 0;
}
```

##### [leetcode] 收集垃圾的最少总时间

题目来源: https://leetcode.cn/problems/minimum-amount-of-time-to-collect-garbage/

代码:

```c++
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description: 首先我们知道,既然每种垃圾的处理时间都是1分钟,那处理垃圾部分所花的时间就等于这个数组内
  所有串的长度之和,而对于行驶路程所花的时间,贪心地想,三辆垃圾车都只需要在最后一部分该种垃圾存在的房子前
  行驶,所以我们只需要求出每种垃圾最后出现的坐标并将该坐标前所花的时间求和即可.
**********************************************************************************/
class Solution {
public:
    int garbageCollection(vector<string>& a, vector<int>& s) {
        int st=a[0].size();
        int x=-1,y=-1,z=-1;
        int n=a.size();
        int i,j;
        while(x<0||y<0||z<0){
            n--;
            for(i=0;i<a[n].size();i++){
                if(x<0&&a[n][i]=='M')x=n;//金属
                if(y<0&&a[n][i]=='P')y=n;//纸
                if(z<0&&a[n][i]=='G')z=n;//玻璃
            }if(!n)break;
        }n=a.size();
        for(i=1;i<n;i++){
            st+=a[i].size();
            if(i<=x)st+=s[i-1];
            if(i<=y)st+=s[i-1];
            if(i<=z)st+=s[i-1];
        }
        return st;
    }
};
```



### 分治

##### [力扣]  **K 次操作后最大化顶端元素**  

题目来源：https://leetcode.cn/problems/maximize-the-topmost-element-after-k-moves/

问题描述：给你一个下标从 0 开始的整数数组 nums ，它表示一个 栈 ，其中 nums[0] 是栈顶的元素。每一次操作中，你可以执行以下操作 之一 ：如果栈非空，那么 删除 栈顶端的元素。如果存在 1 个或者多个被删除的元素，你可以从它们中选择任何一个，添加 回栈顶，这个元素成为新的栈顶元素。同时给你一个整数 k ，它表示你总共需要执行操作的次数。请你返回 恰好 执行 k 次操作以后，栈顶元素的 最大值 。如果执行完 k 次操作以后，栈一定为空，请你返回 -1 。

代码:

```c++
/*********************************************************************************
  *Author:  陈高威
  *Contact:  1259234645@qq.com
  *Description:如果nums.length=1，k是偶数，栈里就存在1个元素；如果k是奇数，栈里一定没有任何元素，就返回−1。否则，栈顶元素可以是nums的前k−1个数的最大值；nums的第k+1个数（如果有，就出栈前k个数）。
**********************************************************************************/
class Solution {
public:
    int minDeletion(vector<int>& nums) {
        int n = nums.size();
        int ans = 0;
        for (int i = 0; i + 1 < n; i++) {
            if (nums[i] == nums[i + 1]) ans++;
            else i++;
        }
        if ((n - ans) % 2) ans++;
        return ans;
    }
};
```

#####  

### 模拟

##### [PTA] 农田缩减

题目来源：https://pintia.cn/problem-sets/1524738565584617472/problems/1524738565655920643

参考代码如下：

```C++
/*********************************************************************************
  *Author:  crazy杨
  *Contact:  1678951138@qq.com
  *Description:  通过去除其中一个值，再利用max1,max2算出剩下的互相减去的最大值，再利用vector存取每个面积，最后利用sort排序直接取s[0]为最小面积。
*********************************************************************************/

#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
	int n;
	cin >> n;
	vector<int>s;
	int a[n];
	int b[n];
	for (int i = 0; i < n; i++) {
		cin >> a[i] >> b[i];
	}

	for (int i = 0; i < n; i++) {
		int min1 = 0;
		int min2 = 0;
		for (int j = 0; j < n - 1; j++) {
			if (j == i) {
				continue;
			}
			for (int k = j + 1; k < n; k++) {
				int q = fabs(a[j] - a[k]);
				if (k != i) {
					if (q > min1)
						min1 = q;
				}
				int p = fabs(b[j] - b[k]);
				if ( k != i) {
					if (p > min2) {
						min2 = p;
					}
				}
			}
		}
		s.push_back(min2 * min1);
	}
	sort(s.begin(), s.end());
	cout << s[0];
	return 0;
}
```

##### [蓝桥]  十六进制转八进制

题目来源：http://lx.lanqiao.cn/problem.page?gpid=D2985

问题描述：给定n个十六进制正整数，输出它们对应的八进制数。 

代码:

```c++
/*********************************************************************************
  *Author:  陈高威
  *Contact:  1259234645@qq.com
  *Description:  把八和十六进制的二进制表示方式保存起来，然后通过二进制把十六进制转为八进制
**********************************************************************************/
#include<iostream>
#include<string>
#include<cstring>
using namespace std; 

void change(string str,int l){
 string add;
 int i,j,k;

 if(l%3==1){
  add="00";
  str=add+str; 
 }else if (l%3==2){
  add="0";
  str=add+str;
 } 
 
 l=str.length();
 string str1="",str2="",str3="";
 for(k=0;k<l/3;k++){   //三组 
  str1="";
  for(i=k*3;i<k*3+3;i++){ 
   switch(str[i]){
    case '0':str1+="0000";break;
    case '1':str1+="0001";break;
    case '2':str1+="0010";break;
    case '3':str1+="0011";break;
    case '4':str1+="0100";break;
    case '5':str1+="0101";break;
    case '6':str1+="0110";break;
    case '7':str1+="0111";break;
    case '8':str1+="1000";break;
    case '9':str1+="1001";break;
    case 'A':str1+="1010";break;
    case 'B':str1+="1011";break;
    case 'C':str1+="1100";break;
    case 'D':str1+="1101";break;
    case 'E':str1+="1110";break;
    case 'F':str1+="1111";break;
    default:break; 
   }
  }
   
  for(i=0;i<4;i++){    //12/3 4组8进制 
     str2="";
     for(j=i*3;j<i*3+3;j++){
        str2=str2+str1[j];
      }
     if(str2=="000")str3+='0';
     if(str2=="001")str3+='1';
     if(str2=="010")str3+='2';
     if(str2=="011")str3+='3';
     if(str2=="100")str3+='4';
     if(str2=="101")str3+='5';
     if(str2=="110")str3+='6';
     if(str2=="111")str3+='7';
   }
 }
 for(i=0;i<3;i++){ 
  if(str3[0]=='0') str3=str3.erase(0,1); //删除前导0 
  } 
 cout<<str3<<endl;
}

int main(){ 
 int n;
 cin>>n;
 
 string a[n];
 for(int i=0;i<n;i++){
  	cin>>a[i];
 }
 
 for(int i=0;i<n;i++){
  	change(a[i],a[i].length());
 }
 return 0;
}
```

##### [蓝桥] 试题 算法训练 自行车停放

题目来源：http://lx.lanqiao.cn/problem.page?gpid=D3409

代码如下：

```c++
/****************************************
  *Author:  doger
  *Contact:  897955302@qq.com
  *Description: 想好用什么数据结构来存放自行车编号，以及如何来表示左右两边的自行车编号
*****************************************/
#include<iostream>
using namespace std;
//第一个维度表示当前这量自行车；
//a[i][0]表示第i辆车左边的车编号；
//a[i][1]表示第i辆车右边的车编号； 
int a[100001][2];  
int main(){
	int n; //自行车数量 
	cin>>n;
	int x; //第一辆车的编号
	cin>>x; 
	//排好后的第一量车的编号
	int first=x; 
	for(int i=0;i<n-1;i++){
		int x=0;
		int y=0;
		int z;
		cin>>x>>y>>z;
		if(z){ //放在y的右边 
			if(a[y][1]==0){  //这边是空的 
				a[y][1]	= x;
				a[x][0]	= y;	
			}else{  //不空的话就插进去 
				int right = a[y][1];
				a[y][1] = x;
				//更新右边这辆车的左边车;
				a[right][0] = x; 
				//更新x的左右两边
				a[x][0] = y;
				a[x][1] = right;	 
			} 
		} else{ //放在y的左边 
			if(a[y][0]==0){  //左边为空 
				a[y][0]	= x;
				a[x][1]	= y;
				first = x;
			}else{ //不空就插入 
				int left = a[y][0];
				a[y][0] = x;
				a[left][1] = x;
				//更新x
				a[x][0]	= left; 
				a[x][1]	= y;
			}
		} 
	}
	for(int i=0;i<n;i++){
		cout<<first<<" ";
		first = a[first][1];
	}
	return 0;
}
```

#####  [LeetCode]罗马数字转整数

题目来源：https://leetcode.cn/problems/roman-to-integer/

题目描述：例如， 罗马数字 2 写做 II ，即为两个并列的 1 。12 写做 XII ，即为 X + II 。 27 写做  XXVII, 即为 XX + V + II 。

通常情况下，罗马数字中小的数字在大的数字的右边。但也存在特例，例如 4 不写做 IIII，而是 IV。数字 1 在数字 5 的左边，所表示的数等于大数 5 减小数 1 得到的数值 4 。同样地，数字 9 表示为 IX。这个特殊的规则只适用于以下六种情况：

I 可以放在 V (5) 和 X (10) 的左边，来表示 4 和 9。
X 可以放在 L (50) 和 C (100) 的左边，来表示 40 和 90。 
C 可以放在 D (500) 和 M (1000) 的左边，来表示 400 和 900。
给定一个罗马数字，将其转换成整数。

```c++
/****************************************
  *Author:  陈斌
  *Contact:  1198222765@qq.com
  *Description: 无序表unordered_map
*****************************************/
class Solution{
    private:
        unordered_map<char,int> symbolValues={
            {'I',1},
            {'V',5},
            {'X', 10},
            {'L', 50},
            {'C', 100},
            {'D', 500},
            {'M', 1000},
    };
public:
    int romanToInt(string s){
        int ans=0;
        int n=s.size();
        for(int i=0;i<n;i++){
            int value=symbolValues[s[i]];
        if(i!=(n-1) && value< symbolValues[s[i+1]]){
            ans-=value;
        }else{
            ans+=value;
            }   
        }
        return ans;
    } 
};
```

##### [leetcode]除自身以外数组的乘积

题目来源：https://leetcode.cn/problems/product-of-array-except-self/

代码:

```c++
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description: 既然不能用除法,那我们就去把他剖解成几个部分,每个格要填上的乘积是左右两边的乘积和的乘
  积,也就是双序遍历一次求出每个位置左右两边部分独立的乘积值即可.
     ps:笑死,题目不说我压根不会直接想到除.
**********************************************************************************/
class Solution {
public:
    vector<int> productExceptSelf(vector<int>& a) {
        vector<int>ss;
        int i,n=a.size();
        int st=1;//这里的技巧是用滚动思想去代替数组,节省空间
        for(i=0;i<n;i++){//左
            ss.push_back(st);
            st*=a[i];
        }st=1;
        for(i=n-1;i>=0;i--){//右
            ss[i]*=st;
            st*=a[i];
        }return ss;
    }
};
```

##### [HDOJ]猜数字

题目来源：[Problem - 1172 (hdu.edu.cn)](https://acm.hdu.edu.cn/showproblem.php?pid=1172)

代码如下：

```c++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description: 从1000到9999遍历是否符合条件，只有一个结果就输出，有两个结果就Not sure
*****************************************/
#include<stdio.h>
#include<string.h>
#include<string>
#include<iostream>
#include<algorithm>
using namespace std;
int res;
int temp;
int a[105];
int b[105],c[105];
int n;
int xdn[11];
int xdm[11];
void solve(string x){
    int index=0;
    if(res>=2)return;
    for(int i=0;i<n;i++){
        memset(xdm,0,sizeof(xdm));
        int xd=0,wz=0;
        for(int j=0;j<4;j++){
            string num=to_string(a[i]);
            if(x[j]==num[j]){
                wz++;
            }
                xdm[num[j]-'0']++;
        }
        for(int j=0;j<10;j++){
            xd+=min(xdn[j],xdm[j]);
        }
        if(xd==b[i] && wz==c[i]){
            index++;
            continue;
        }else{
            break;
        }
    }
    if(index==n){
        temp=atoi(x.c_str());
        res++;
    }
}
int main(){
    while(scanf("%d",&n)&&n){
        res=0;
        for(int i=0;i<n;i++){
            scanf("%d%d%d",&a[i],&b[i],&c[i]);
        }
        for(int i=1000;i<=9999;i++){
            string m=to_string(i);
            memset(xdn,0,sizeof(xdn));
            for(int j=0;j<4;j++){
                xdn[m[j]-'0']++;
            }
            solve(m);
        }
            if(res==2){
                printf("Not sure\n");
            }
        if(res==1){
            printf("%d\n",temp);
        }else if(res==0){
            printf("Not sure\n");
        } 
    }
    return 0;
}
```

##### [HDOJ]Robot Motion

题目来源：[Problem - 1035 (hdu.edu.cn)](https://acm.hdu.edu.cn/showproblem.php?pid=1035)

代码如下：

```c++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description: dfs模拟
*****************************************/
#include<stdio.h>
#include<string>
#include<string.h>
using namespace std;
int tem;
int bef;
int loo;
int n,m;
int k;
int visit[20][20];
char dir[20][20];
int index;
void dfs(int x,int y){
    if(x<=0 || x>n){
        printf("%d step(s) to exit\n",index);
        return;
    }
    if(y<=0 || y>m){
        printf("%d step(s) to exit\n",index);
        return;
    }
    if(visit[x][y]==2){
        printf("%d step(s) before a loop of %d step(s)\n",bef-loo,loo);
        return;
    }else if(visit[x][y]==1){
        bef=index;
        visit[x][y]=2;
        loo++;
    }else if(visit[x][y]==0){
        visit[x][y]=1;
        index++;
    }
    if(dir[x][y]=='N')dfs(x-1,y);
    else if(dir[x][y]=='W')dfs(x,y-1);
    else if(dir[x][y]=='E')dfs(x,y+1);
    else if(dir[x][y]=='S')dfs(x+1,y);
}
int main(){
    while(scanf("%d%d",&n,&m) && n){
        scanf("%d",&k);
        for(int i=1; i<=n; i++)
			scanf("%s", dir[i]+1);
        loo=0;
        memset(visit,0,sizeof(visit));
        index=0;
        dfs(1,k);
    }
    return 0;
}
```

##### [leetcode] 从字符串中移除星号

题目来源: https://leetcode.cn/problems/removing-stars-from-a-string/

代码:

```c++
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description:  因为是删除左侧的字符串,所以我们从右侧开始记录星号个数会比较方便.
**********************************************************************************/
class Solution {
public:
    string removeStars(string s) {
        string ss;
        int i,n=s.size();
        for(i=n-1;i>=0;i--){
            if(s[i]!='*')ss+=s[i];
            else {
                int t=1;
                while(t){//记录星号个数
                    if(s[--i]!='*')t--;
                    else t++;
                }
            }
        }reverse(ss.begin(),ss.end());
        return ss;
    }
};
```



### 递推

**递推的思想**

> 一个问题的求解需一系列的计算，在已知条件和所求问题之间总存在着某种相互联系的关系，在计算时，如果可以找到前后过程之间的数量关系（即递推式），那么，从问题出发逐步推到已知条件，此种方法叫递推。

**应用场景**

> 递推算法的首要问题是得到相邻的数据项间的关系（即递推关系）。递推算法避开了求通项公式的麻烦，把一个复杂的问题的求解，分解成了连续的若干步简单运算。一般说来，可以将递推算法看成是一种特殊的迭代算法。

##### [PTA]青蛙跳台阶

题目来源：https://pintia.cn/problem-sets/1466688064595234816/problems/1466688064695898116

问题描述：一只青蛙一次可以跳上 1 级台阶，也可以跳上2 级。求该青蛙跳上一个n 级的台阶总共有多少种跳法。一次输入n个数，每个数代表台阶数。

代码:

```c
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description:  对于每一次跳跃青蛙都有两种跳法，1阶和2阶，那么逆着去想就是每一阶台阶有可能是从
  上一阶跳过来的，也有可能是从前一阶跳过来的，所以逆推公式就是A(i)=A(i-1)+A(i-2)
     ps:看起来和斐波那契非常像但思路没多少关系
**********************************************************************************/
#include<stdio.h>
int main(){
	int n;
    scanf("%d",&n);
    while(n--){
        int t,i;
        int a=1,b=1,c=1;//用数组也可以，更方便理解
        scanf("%d",&t);
        for(i=2;i<=t;i++){
            c=a+b;
            a=b,b=c;
        }printf("%d\n",c);
    }
	return 0;
}
```

##### [知乎]汉诺塔的移动次数

题目来源：https://zhuanlan.zhihu.com/p/322440261

问题描述：在实现过经典的汉诺塔后不妨来想想用汉诺塔移动n个盘子的次数吧

代码:

```c
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description:  移动n个盘子的步骤是：先移动n-1个盘到中间座上，然后把最大盘移动到目标座，最后借
  助第一个座把n-1个盘移到目标座，也就是我们要进行(两次移动n-1个盘+移动1次最大盘)的操作，那么递推
  公式就是A(i)=2*A(i-1)+1，初始值是移动一个盘要1次
     ps:所以在汉诺塔传说中的那个和尚是累死的，怪不得没人做到QAQ
**********************************************************************************/
#include<stdio.h>
int main(){
	int n,i;
    scanf("%d",n);
    /*long long int ss=1;
    for(i=2;i<=n;i++){
        ss=2*ss+1;
    }printf("%lld",ss);*///以上是常规写法
    printf("%lld",(1<<n)-1);//骚操作:A(i)=2*A(i-1)+1=2*(A(i-1)+1)-1,在初始值为1的情况下就是2^i-1;
	return 0;
}
```

##### [力扣]找出游戏的获胜者&约瑟夫环问题

题目来源：https://leetcode-cn.com/problems/find-the-winner-of-the-circular-game/

问题描述：由于是很经典的一道题，所以题面不多赘述，大概题意为:以1到n编号的n个人形成一个环链，初始位置在编号为1的人之前，每一轮在当前位置往后的第k个人会被剔除，随后被剔除者的位置为新的当前位置，周而复始直到环内只剩最后一个人时输出最后幸存者编号.

代码:

```c++
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description:	 辣莫还是经典的题型，通常会用模拟作为第一思路，但即便用队列模拟也至少要O(nk)的复
  杂度;那么我们来反着想想，我们既然求的是幸存者的编号，就代表这个人无论在哪一轮都是幸存者！在这个
  思路下我们不妨设每一轮所在当前位置为头(可以看作是第一个位置0)，我们要剔除从头往后的第k人，所以在
  最后一轮要剔除的目标是幸存者时，此时的头(位置0)应该是上一轮被剔除的人的位置，而幸存者的位置
  应该是头的位置往后的第k个位置(0+k)(插入:可以想象现在是一条由当前那么多个人为循环结组成的无限延伸
  的链),且上一轮被剔除的人在上上轮的位置同样是头往后第k个位置(0+k)，而这一轮要被剔除的人也就是幸存
  者在上上轮的位置即是((0+k)+k)，然后一直这么回溯回去我们就可以求出幸存者在上上上上上上.....轮
  (最开始时)的位置，那么思路就出来了，但这里有个问题，那就是在我们想象中这个链是无限延伸的，但我们
  要求的幸存者的位置在最后还是要以编号的形式输出,那一直+k+k不肯定超出编号范围了吗,那我们在第一个回溯
  也就是回溯到:头的位置是倒数第二个人的时候，这个时候幸存者的位置是(0+k),那既然此时循环节是2，直接余
  2不就可以得到幸存者的位置(0+k)%2了吗,同样在第二次回溯的时候循环节是3那就余3,即((0+k)%2+k)%3,以
  此类推,那么就讲完了（不理解你™劈我）。
   ps:那么这一种思路只能用于求最终幸存者的编号,如果变成输出淘汰者的编号那用队列模拟是个不错的选择.
**********************************************************************************/
class Solution {
public:
    int findTheWinner(int n, int k) {
        int ss=0,i;
        for(i=2;i<=n;i++){//如果就1人,那么他就是幸存者
            ss=(ss+k)%i;
        }return ss+1;//这里设编号从0到n-1所以最后要+1,
        			//如果设为1到n那递推式就是(ss+k-1)%i+1并且ss初始值为1;
    }
};
```

##### [leetcode] 知道秘密的人数

题目来源：https://leetcode.cn/problems/number-of-people-aware-of-a-secret/

代码:

```c++
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description:	从第一个人能开始传播起每天就要加上一批新增人员，新增的人数是t天之前发展到t天的人数;
  直到从第一个人开始遗忘时,每天除了继续新增人外还都要剪掉一批人，这批人的数量为k天前发展到k天的人数,所
  以就这三种状态(初始状态加上这两种状态),分类实现即可.
   ps: 牛生牛问题
**********************************************************************************/
class Solution {
    typedef long long ll;
public:
    ll b[1010];//b[i]表示第i天共有几个人
    ll m=1000000007;
    ll ss;
    int peopleAwareOfSecret(int n, int t, int k) {
        int i;
        for(i=0;i<t;i++)b[i]=1;//在t天之前不能传播
        for(i=t;i<k;i++)ss+=b[i-t],b[i]=(ss+1)%m;//在第一个人遗忘前发展出的人数,+1指加上自己
        for(i=k;i<n;i++){//为了看清楚所以分成三个阶段的循环来体现
            ss+=b[i-t]-b[i-k];//从第一个人开始遗忘时就要开始减去第i-k前发展到第i-k天的人数了
            b[i]=ss%m;
        }return (int)(b[n-1]);
    }
};
```



### 动态规划

#### 线性DP

##### [LeetCode]三角形最小路径和

**题目来源：**[120. 三角形最小路径和 - 力扣（LeetCode）](https://leetcode.cn/problems/triangle/submissions/)

**问题描述：**给定一个三角形 triangle ，找出自顶向下的最小路径和。

每一步只能移动到下一行中相邻的结点上。相邻的结点 在这里指的是 下标 与 上一层结点下标 相同或者等于 上一层结点下标 + 1 的两个结点。也就是说，如果正位于当前行的下标 i ，那么下一步可以移动到下一行的下标 i 或 i + 1 。

```c++
/*********************************************************************************
  *Author:  麦
  *Contact:  
  *Description:  每一层第一个结点和最后一个结点相邻的结点都只有一个，中间的结点只能和其上一层相邻的结点相加，每次选择上一层相邻两个结点中小的那个与本结点相加,把最后一层的结果遍历一遍就行了
**********************************************************************************/
//方式1，自上而下，要用二维dp表示
class Solution {
public:
    int dp[201][201];
    int minimumTotal(vector<vector<int>>& triangle) {
        int len = triangle.size();
        if (len == 1) {
            return triangle[0][0];
        }
        dp[0][0] = triangle[0][0];
        for (int i = 1; i < len; i++) {
            dp[i][0] = triangle[i][0] + dp[i - 1][0];
            dp[i][i] = triangle[i][i] + dp[i - 1][i - 1];
        }

        for (int i = 2; i < len; i++) {
            for (int j = 1; j < i; j++) {
                dp[i][j] = min(dp[i - 1][j - 1], dp[i - 1][j]) + triangle[i][j];
            }
        }
        int ans = 1e6;
        for (int i = 0; i < len; i++) {
            ans = min(ans, dp[len - 1][i]);
        }
        return ans;
    }
};
//方式2自下而上，用一维dp即可，更加省空间
class Solution {
public:
    int dp[201];
    int minimumTotal(vector<vector<int>>& triangle) {
        int len = triangle.size();
        if (len == 1) {
            return triangle[0][0];
        }
        //初始化最后一层
        for (int i = 0; i < len; i++) {
            dp[i] = triangle[len - 1][i];
        }
        
        for (int i = len - 2; i >= 0; i--) {
            for (int j = 0; j <= i; j++) {
                //选择下面两个相邻的结点中最小的与当前结点相加
                dp[j] = triangle[i ][j] + min(dp[j], dp[j + 1]);
            }
        }
        return dp[0];
    }
};
```

##### [LeetCode]正则表达式匹配

题目来源：https://leetcode.cn/problems/regular-expression-matching/

问题描述：给你一个字符串 s 和一个字符规律 p，请你来实现一个支持 '.' 和 '*' 的正则表达式匹配。

'.' 匹配任意单个字符
'*' 匹配零个或多个前面的那一个元素
所谓匹配，是要涵盖 整个 字符串 s的，而不是部分字符串。

```C++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description: 前面元素可以匹配新加的元素相等或者'.'当p[j-1]=='*' && s[i-1]&&p[j-2]时有两种情况，删除前面一个字母，或者多个前面字母，有一种为真就可以。
*****************************************/
class Solution {
public:
    bool isMatch(string s, string p) {
        int sLen=s.length();
        int pLen=p.length();
        bool dp[30][30]={false};
        dp[0][0]=true;
        for(int j=1;j<pLen+1;j++){
            if(p[j-1]=='*')dp[0][j]=dp[0][j-2];
        }
        for(int i=1;i<sLen+1;i++){
            for(int j=1;j<pLen+1;j++){
                if(s[i-1]==p[j-1] || p[j-1]=='.'){
                    dp[i][j]=dp[i-1][j-1];
                }else if(p[j-1]=='*'){
                    if(s[i-1]==p[j-2] || p[j-2]=='.'){
                         dp[i][j] = dp[i][j - 2]  || dp[i - 1][j];
                    }else{
                        dp[i][j]=dp[i][j-2];
                    }
                }
            }
        }
        return dp[sLen][pLen];
    }
};
```

##### [LeetCode]完全平方数

题目来源：https://leetcode.cn/problems/perfect-squares/

问题描述：给你一个整数 n ，返回 和为 n 的完全平方数的最少数量 。

完全平方数 是一个整数，其值等于另一个整数的平方；换句话说，其值等于一个整数自乘的积。例如，1、4、9 和 16 都是完全平方数，而 3 和 11 不是。

```c++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description: 
*****************************************/
class Solution {
public:
    int numSquares(int n) {
        vector<int> dp(n+1);
        for(int i=1;i<=n;i++){
            dp[i]=i;
            for(int j=1;j*j<=i;j++){
                dp[i]=min(dp[i],dp[i-j*j]+1);
            }
        }
        return dp[n];
    }
};
```

##### [HDOJ]最少拦截系统

题目来源：https://acm.hdu.edu.cn/showproblem.php?pid=1257

```c++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description: 每次新加一个数字就跟存的拦截高度作比较，如果更低就刷新拦截高度，如果没有比现存的低的就m++保存
*****************************************/
#include<stdio.h>
#include<string.h>
using namespace std;
int n,i,j,x,m;
int dp[1005];
int main(){
    while(scanf("%d",&n)!=EOF){
        dp[1]=0;
        m=0;
        for(i=1;i<=n;i++){
            scanf("%d",&x);
            for(j=1;j<=m;j++){
                if(x<=dp[j]){
                    dp[j]=x;
                    break;
                }                
            }
            if(j>m)dp[++m]=x;
        }
                    printf("%d\n",m);
    }
    return 0;
}
```

~~~c++
/****************************************
  *Author:  陈家乐
  *Contact:  
  *Description: 
*****************************************/
#include<iostream>
#include<cstring>
#include<algorithm>
using namespace std;
int high;
int nums[100010];
int main() {
	int n,num;
	while(cin>>n) {
		memset(nums,0,sizeof(nums));
		num=0;
		for(int i=0; i<n; i++) {
			cin>>high;
			int j;
			for(j=0; j<=num; j++) {
				if(nums[j]>=high) {
					nums[j]=high;
					break;
				}
			}
			if(j>num) { 
				nums[++num]=high;
			}
		}
		cout<<num;
	}
}
~~~



##### [LeetCode]网格中的最小路径代价

题目来源：https://leetcode.cn/problems/minimum-path-cost-in-a-grid/

```c++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description: 
*****************************************/
class Solution {
public:
    int lenall;
    int len;
    int res=INT_MAX;
    int minPathCost(vector<vector<int>>& grid, vector<vector<int>>& moveCost) {
        lenall=grid.size();
        len=grid[0].size();
        vector<vector<int>> dp(lenall,vector<int>(len));
        for(int i=0;i<len;i++){
            dp[0][i]=grid[0][i];
        }
        for(int i=0;i<lenall;i++){
            for(int j=0;j<len;j++){
                int temp=INT_MAX;
                for(int k=0;k<len;k++){
                    temp=min(temp,dp[i-1][k]+grid[i][j]+moveCost[grid[i-1][k]][j]);
                }
                dp[i][j]=temp;
            }
        }
        for(int i=0;i<len;i++){
            res=min(res,dp[lenall-1][i]);
        }
        return res;
    }
};
```

```c++
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description: 贪心地想就是从第二层往后的每一层中的每一个单元格都是上一层中传到当前单元格的最小代价
     ps:水题专场
**********************************************************************************/
class Solution {
public:
    int minPathCost(vector<vector<int>>& a, vector<vector<int>>& b) {
        int i,j,r,n=a.size(),m=a[0].size();
        vector<vector<int>> at;
        at=a;
        for(i=1;i<n;i++){
            for(j=0;j<m;j++){
                int t=10000000;
                for(r=0;r<m;r++){
                    t=min(t,a[i-1][r]+b[at[i-1][r]][j]);
                }a[i][j]+=t;
            }
        }int ss=10000000;
        for(i=0;i<m;i++){
            ss=min(ss,a[n-1][i]);
        }return ss;
    }
};
```

##### [HDOJ]Super Jumping! 

题目来源：https://acm.hdu.edu.cn/showproblem.php?pid=1087

```c++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description: 最大上升子序列和
*****************************************/
#include<stdio.h>
#include<algorithm>
using namespace std;
int n;
int a[1005];
int dp[1005];
int main(){
    while(scanf("%d",&n),n){
        int res=-1;
        for(int i=0;i<n;i++){
            scanf("%d",&a[i]);
            dp[i]=a[i];
            res=max(res,a[i]);
        }
        for(int i=0;i<n;i++){
            for(int j=0;j<i;j++){
                if( a[i]>a[j] && dp[i]<dp[j]+a[i]){
                    dp[i]=dp[j]+a[i];
                    res=max(res,dp[i]);
                }
            }
        }
        printf("%d\n",res);

    }
    return 0;
}
```

##### [acwing]吃水果

题目来源：https://www.acwing.com/problem/content/4499/

代码:

```c++
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description: 约束条件是k个人和其左边相邻的人不同,所以对于从第二个位置开始的每个人,都只有两种状态,
  一个是和左边的水果相同,一个是和左边的水果不同,因为这题的m种水果都是无限的,所以每次把这两种状态对应的
  种数相加即可.
**********************************************************************************/
#include<bits/stdc++.h>
using namespace std;
int dp[2010][2010];
int mod=998244353;
int main(){
	int n,m,k,i,j;
	cin>>n>>m>>k;
	dp[1][0]=m;//左边界初始条件
	for(i=2;i<=n;i++)//最左边不算
	for(j=0;j<=k;j++)
	dp[i][j]+=(dp[i-1][j]+dp[i-1][j-1]*1ll*(m-1)%mod)%mod;
	cout<<dp[n][k];
	return 0;
} 
```

```c++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description: 
*****************************************/
#include<bits/stdc++.h>
using namespace std;
int dp[2005][2005];
const int MOD=998244353;
int main()
{
	int n,m,k;
	cin>>n>>m>>k;
	dp[1][0]=m;
	for(int i=2;i<=n;i++)
		for(int j=0;j<=k;j++){
			dp[i][j]=(dp[i-1][j]+dp[i-1][j-1]*(m-1ll)%MOD)%MOD;
		}
	cout<<dp[n][k];
	return 0;
 } 
```



##### [LeetCode]打家劫舍

题目来源：https://leetcode.cn/problems/house-robber/

代码:

```c++
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description: 对于每间屋子,有两种状态,偷和不偷,选出偷和不偷中最好的情况即可
**********************************************************************************/
class Solution {
public:
    int rob(vector<int>& a) {
        int dp[101]={0},i,j,n=a.size();
        dp[1]=a[0];
        for(i=2;i<=n;i++){
            dp[i]=max(dp[i-2]+a[i-1],dp[i-1]);//当前房子是a[i-1]
        }return dp[n];
    }
};
```

##### [HDOJ]最大连续子序列

题目来源：https://acm.hdu.edu.cn/showproblem.php?pid=1231

```c++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description: 记录当前段的sum,当sum小于0的时候重新记录
*****************************************/
#include<stdio.h>
#include<algorithm>
using namespace std;
int n;
int num[50005];
int main(){
    while(scanf("%d",&n),n){
        for(int i=0;i<n;i++)scanf("%d",&num[i]);
            int sum,from,to;
            int ans,first,last;
            from=to=sum=num[0];
            first=last=ans=num[0];
            for(int i=1;i<n;i++){
                if(sum>0){
                    sum+=num[i];
                    to=num[i];
                }
                else{
                    from=to=sum=num[i];
                }
                if(ans<sum){
                    first=from;
                    last=to;
                    ans=sum;
                }
            }
            if(ans<0)printf("0 %d %d\n",num[0],num[n-1]);
            else printf("%d %d %d\n",ans,first,last);
    }
    return 0;
}
```

##### [acwing]剪绳子

题目来源：[25. 剪绳子 - AcWing题库](https://www.acwing.com/problem/content/24/) 

```c++
/****************************************
  *Author:  杨杰
  *Contact:  
  *Description: 用递归思想利用pow的函数去实现
*****************************************/
if (n <= 3) return 1 * (n - 1);
        int  a = n/3,b=n%3;
        if(b==0) return pow(3,a);
        if(b==1) return pow(3,a-1)*4;
        return pow(3,a)*2;
```

##### [acwing]最长公共上升子序列

题目来源：[272. 最长公共上升子序列 - AcWing题库](https://www.acwing.com/problem/content/description/274/)

```c++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description: 
*****************************************/
#include <iostream>
#include <algorithm>
using namespace std;
const int N = 3010;
int n;
int a[N], b[N];
int f[N][N];
int main()
{
    scanf("%d", &n);
    for (int i = 1; i <= n; i ++ ) cin>>a[i];
    for (int i = 1; i <= n; i ++ ) cin>>b[i];

    for (int i = 1; i <= n; i ++ )
    {
        int maxv = 1;
        for (int j = 1; j <= n; j ++ )
        {
            f[i][j] = f[i - 1][j];
            if (a[i] == b[j]) f[i][j] = max(f[i][j], maxv);
            if (a[i] > b[j]) maxv = max(maxv, f[i - 1][j] + 1);
        }
    }

    int res = 0;
    for (int i = 1; i <= n; i ++ ) res = max(res, f[n][i]);
    cout<<res;
    return 0;
}
```

##### [acwing]分糖果

题目来源：[4498. 指针 - AcWing题库](https://www.acwing.com/problem/content/4501/) 

```
****************************************
  *Author: crazy
  *Contact:  
  *Description: 同背包问题思路一模一样，背包中每个物体分为取与不取，该题中就是正转与反转。注意取模调整，并且因为必须转完
才得到结果所以不能用滚动数组
*****************************************/

#include <bits/stdc++.h>
using namespace std;
const int N = 20;
int ve[N];
bool dp[N][361];
int main(void) {
    int n;
    cin >> n;
    for (int i = 1; i <= n; i++)
        cin >> ve[i];
    dp[0][0] = true;
    for (int i = 1; i <= n; i++)
        for (int m = 0; m <= 360; m++)
            dp[i][m] = dp[i - 1][(360 + m + ve[i]) % 360] || dp[i - 1][(360 + m - ve[i]) % 360];
    cout << (dp[n][0] ? "YES" : "NO");
    return 0;
}


```

##### [LeetCode]接雨水

题目来源：[42. 接雨水 - 力扣（LeetCode）](https://leetcode.cn/problems/trapping-rain-water/submissions/)

```c++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description: 从左向右聪右向左遍历，min最低点减去柱子高度
*****************************************/
class Solution {
public:
    int trap(vector<int>& height) {
        int n=height.size();
        vector<int> leftmax(n);
        vector<int> rightmax(n);
        leftmax[0]=height[0];
        rightmax[n-1]=height[n-1];
        for(int i=1;i<n;i++){
            leftmax[i]=max(height[i],leftmax[i-1]);
        }
        for(int i=n-2;i>=0;i--){
            rightmax[i]=max(rightmax[i+1],height[i]);
        }
        int res=0;
        for(int i=0;i<n;i++)res+=min(leftmax[i],rightmax[i])-height[i];
        return res;
    }
};
```

##### [HDOJ]The All-purpose Zero

题目来源：[Problem - 5773 (hdu.edu.cn)](https://acm.hdu.edu.cn/showproblem.php?pid=5773)

```c++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description: 贪心，输入的时候把0的个数累加，依次减去当前数前面出现0的次数再赋值给数组，输入0的时候不赋值，形成严格递增。求出他们的最长子序列然后加上全部的0数输出。
*****************************************/
#include<iostream>
#include<stdio.h>
#include<algorithm>
#include<string.h>
using namespace std;
const int N=1000005;
int n,temp,res,m,x,index;
int a[N],dp[N];
void LIS(){
    memset(dp,0x7f,index*4);
    int pos;
    for(int i=0;i<index;i++){
        pos=lower_bound(dp,dp+i,a[i])-dp;
        dp[pos]=a[i];
        res=max(pos+1,res);
    }
}
int main(){
    scanf("%d",&n);
    for(int k=1;k<=n;k++){
        res=0;
        temp=0;
        index=0;
        scanf("%d",&m);
        for(int i=0;i<m;i++){
            scanf("%d",&x);
            if(x==0){
                temp++;
            }else{
                a[index++]=(x-temp);
            }
        }
        LIS();
        printf("Case #%d: %d\n",k,res+temp);
    }
    return 0;
}
```

##### [LeetCode] 检查数组是否存在有效部分

题目来源: https://leetcode.cn/problems/check-if-there-is-a-valid-partition-for-the-array/

代码:

```c++
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description: 一共有三种有效的划分状态,所以我们对于当前每一个已经构成有效划分的位置,都分成这三种情
  况去匹配后面可能构成有效划分的位置.
**********************************************************************************/
class Solution {
    int dp[100010];
public:
    bool validPartition(vector<int>& a) {
        int n = a.size(),i;
        dp[0] = 1;
        for(i=0;i<n-1;i++){
            if(dp[i]){//三种状态
                if(a[i]==a[i+1])dp[i+2]=1;
                if(i<n-2){
                    if(a[i]==a[i+1]&&a[i+1]==a[i+2])dp[i+3]=1;
                    if(a[i]+1==a[i+1]&&a[i+1]+1==a[i+2])dp[i+3]=1;
                }
            }
        }return dp[n];
    }
};
```

##### [LeetCode] 最长理想子序列

题目来源: https://leetcode.cn/problems/longest-ideal-subsequence/

代码:

```c++
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description: 统计以每个小写字母为右端点的最长子列,对于每个位置的字母,在假设其为所求子列右端点的情
  况下去搜索左边部分的最长子段
**********************************************************************************/
class Solution {
    int dp[26];
public:
    int longestIdealString(string s, int k) {
        int i,j,n=s.size();
        int ss=1;
        for(i=0;i<n;i++){
            int m=dp[s[i]-'a'];
            for(j='a';j<='z';j++){//枚举以每个字母为右端点的已知最长子列来添加
                if(abs(j-s[i])<=k){
                    m=max(m,dp[j-'a']+1);
                }
            }dp[s[i]-'a']=m;//记录以当前位置为右端点的最长子段
            ss=max(ss,m);
        }return ss;
    }
};
```



#### 区间DP

##### **[力扣]最大子数组和**

**题目来源：**https://leetcode-cn.com/problems/maximum-subarray/

**问题描述：**给你一个整数数组 nums ，请你找出一个具有最大和的连续子数组（子数组最少包含一个元素），返回其最大和。子数组 是数组中的一个连续部分。

```c++
/*********************************************************************************
  *Author:  陈高威
  *Contact:  1259234645@qq.com
  *Description:  找出每个位置连续数组最大和，就是每次找前一个位置最大连续数组最大和
**********************************************************************************/
int maxSubArray(int* nums, int numsSize) {
    int a = 0, max = nums[0];
    for (int i = 0; i < numsSize; i++) {
        a = fmax(a + nums[i], nums[i]);
        max = fmax(maxAns, a);
    }
    return max;
}
```

**[POJ]Multiplication Puzzle**

题目来源:http://poj.org/problem?id=1651

~~~c++
/****************************************
  *Author:  陈家乐
  *Contact:  
  *Description: 
*****************************************/
#include<iostream>
#include<cstring>
#include<algorithm>
using namespace std;
const int N = 110;
int n;
int p[N];
int dp[N][N];
int main() {
	cin>>n;
	for(int i=0; i<n; i++) {
		cin>>p[i];
	}
	memset(dp,0,sizeof(dp));
	for(int d=2; d<n; d++) {
		for(int i=1; i<n-d+1; i++) {
			int j=i+d-1;
			dp[i][j]=dp[i+1][j]+p[i-1]*p[i]*p[j];
			for(int z=i+1; z<j; z++) {
				dp[i][j]=min(dp[i][j],dp[i][z]+dp[z+1][j]+p[i-1]*p[z]*p[j]);
			}
		}
	}
	cout<<dp[1][n-1];
	return 0;
}
~~~

##### [pta]括号序列-改

题目来源: https://pintia.cn/problem-sets/1547937217954693120/problems/1547937218038579203

代码:

```c++
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description: 题目给定串只有3种字符,而?可以是另外两种字符之一,所以我们分两种情况:如果当前遍历到的
  位置上的是左括号(,那么它不能和前面的任意字符匹配出新括号,这个时候只要加上前面的方法数就行了,如果当前
  遍历到的位置上的是右括号),那么左边在已经匹配完的序列上再多一个(与其匹配，然后再加上方法数,而如果是?,
  那就表示两种情况都有可能,把两种情况都加上即可.那么以上思路非常简单,这里就写一下dp的具体做法:为了方便
  我们设dp[i][j]为当前匹配到的字符i之前共有j个左括号(,那么当i位置上的是(时,dp[i][j]=dp[i-1][j-1],
  即前面的i-1个字符中有j-1个(,也就是此位置前的合法序列数,当i位置上的是)时,dp[i][j]=dp[i-1][j+1],
  即前面的i-1个字符中还要多一个(的方法数,当然,如果是?就把两个都加上就好了.
   ps:设成右括号也行,不过匹配情况换一下.
**********************************************************************************/
#include<bits/stdc++.h>
using namespace std;
typedef long long ll; 
ll m=1000000007;
int n;
char b[2010]={0};
int dp[2010][2010]={0};
int main(){
	int i,j;
	cin>>n;
	for(i=1;i<=n;i++)
	cin>>b[i];
	dp[0][0]=1;
	for(i=1;i<=n;i++){
		if(b[i]!=')')for(j=1;j<=i;j++)//从前往后计数,[0,i-1]
            dp[i][j]+=dp[i-1][j-1]%m;//dp[i][j]的方法数量即是左边部分的方法数量
        if(b[i]!='(')for(j=0;j<i;j++)//j>i的部分用不到,所以剪掉
            dp[i][j]+=dp[i-1][j+1]%m;//需要多一个(来匹配当前的)或者?
	}cout<<dp[n][0];
	return 0;
}
```

##### [vjudge]Easy Game

题目来源:https://vjudge.net/contest/531382#problem/E

代码:

```c++
/****************************************
  *Author:  Mai
  *Contact:
  *Description:dp[i][j]代表当前选手在该能在区间[i,j]得到的最优解与对手的差值
  * 状态转移方程：dp[i][j]=max(pre[k]-pre[i-1]-dp[k+1][j],pre[j]-pre[k]-dp[i][k],dp[i][j]);
  * 1、pre[k]-pre[i-1]-dp[k+1][j]代表当前选手从左选取连续的K个数,并减去对手下一次在[k+1,j]区间的最优解
  * 2、pre[j]-pre[k]-dp[i][k]代表当前选手从右边选取连续的K个数，并减去对手下一次在[i,k]区间内的最优解
  * 答案即使 Alice在[1,n]的最优解与对手的差值
*****************************************/
#include <bits/stdc++.h>
using namespace std;
int dp[105][105],pre[105];
int T,N;
int solve(){
    memset(dp, 0, sizeof(dp));
    memset(pre,0,sizeof(pre));
    cin >> N;
    for (int i = 1; i <= N; i++) {
        cin >> dp[i][i];
        pre[i] = pre[i - 1] + dp[i][i];
    }
    for (int L = 2; L <= N; L++) {
        for (int i = 1; i <= N - L + 1; i++) {
            int j = i + L - 1;
            dp[i][j]=pre[j]-pre[i-1];//全选
            for(int k=i;k<j;k++){
                //每次选择从左边选择k个数和从右边选择j-k个数的最优解
                dp[i][j]=max(dp[i][j],max(pre[k]-pre[i-1]-dp[k+1][j],pre[j]-pre[k]-dp[i][k]));
            }
        }
    }
    return dp[1][N];
}
int main() {
    cin >> T;
    for(int i=1;i<=T;i++){
        printf("Case %d: %d\n",i,solve());
    }
}
```

#### 背包DP

##### [acwing]完全背包问题

题目来源：[3. 完全背包问题 - AcWing题库](https://www.acwing.com/problem/content/3/) 

```c++
/*********************************************************************************
  *Author:  crazy杨
  *Contact:  1678951138@qq.com
  *Description:利用取最大值问题，把每个背包可以装的最大利益，然后用线性规划，求出结果
*********/
#include<iostream>
using namespace std;
const int N = 1010;
int f[N];
int v[N],w[N];
int main()
{
    int n,m;
    cin>>n>>m;
    for(int i = 1 ; i <= n ;i ++)
    {
        cin>>v[i]>>w[i];
    }

    for(int i = 1 ; i<=n ;i++)
    for(int j = v[i] ; j<=m ;j++)
    {
            f[j] = max(f[j],f[j-v[i]]+w[i]);
    }
    cout<<f[m]<<endl;
}
```



##### [acwing]01背包问题

题目来源：[2. 01背包问题 - AcWing题库](https://www.acwing.com/problem/content/submission/code_detail/11874685/) 

```c++

/*********************************************************************************
  *Author:  crazy杨
  *Contact:  1678951138@qq.com
  *Description:利用取最大值问题，把每个背包可以装的最大利益，然后用线性规划，求出结果
*********/
#include<bits/stdc++.h>

using namespace std;

const int MAXN = 1005;
int v[MAXN];    // 体积
int w[MAXN];    // 价值 
int f[MAXN][MAXN];  // f[i][j], j体积下前i个物品的最大价值 

int main() 
{
    int n, c;   
    while(cin>>n>>c){
    for(int i = 1; i <=n; i++) 
        cin >> v[i] >>w[i];
  
 
    for(int i = 1; i <= n; i++){
    	for(int j = 1; j <=c; j++)
        {
            //  当前背包容量装不进第i个物品，则价值等于前i-1个物品
            if(j < v[i]) 
                f[i][j] = f[i - 1][j];
            // 能装，需进行决策是否选择第i个物品
            else    
                f[i][j] = max(f[i - 1][j], f[i - 1][j - v[i]] + w[i]);
        }  
	} 
         
    cout << f[n][c] << endl;
}
    return 0;
}
```

##### [acwing]多重背包问题 I

题目来源：https://www.acwing.com/problem/content/description/4/

```c++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description: 
*****************************************/
#include<iostream>
using namespace std;
int v,w;
int dp[1005];
int main(){
    int n,m,c;
    cin>>n>>m;
    for(int i=1;i<=n;i++){
        cin>>v>>w>>c;
        for(int k=1;k<=c;k++)
        for(int j=m;j>=v;j--){
            dp[j]=max(dp[j],dp[j-v]+w);
        }
    }
    cout<<dp[m];
    return 0;
}

```

##### [acwing]多重背包问题 3

题目来源：[6. 多重背包问题 III - AcWing题库](https://www.acwing.com/problem/content/6/) 

```
/****************************************
  *Author:  
  *Contact:  
  *Description: 单调队列优化多重背包
*****************************************/
#include<bits/stdc++.h>
using namespace std;
int n,m,f[20005],q[20005],s[20005];
int main(){
    cin>>n>>m;
    while(n--){
        memcpy(s,f,sizeof f);
        int x,y,z;
        cin>>x>>y>>z;
        for(int j=0;j<x;j++){
            int l=0,r=-1;
            for(int k=j;k<=m;k+=x){
                if(l<=r&&k-x*z>q[l])
                    l++;
                while(l<=r&&s[q[r]]-(q[r]-j)/x*y<=s[k]-(k-j)/x*y)
                    r--;
                if(l<=r)
                    f[k]=max(f[k],s[q[l]]+(k-q[l])/x*y);
                q[++r]=k;
            }
        }
    }
    cout<<f[m];
}

作者：lovely_fkh
链接：https://www.acwing.com/solution/content/128668/
来源：AcWing
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
```



#### 状态压缩DP

#### 树形DP

#### 数位DP

##### [acwing]从1到n整数中1出现的次数

题目来源:https://www.acwing.com/problem/content/description/51/

代码:

```c++
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description: 每一个相邻数位的1的贡献值都是有联系的,对于不是1的高位,他的数量是低位的总量,对于高位为
  1,要加上该位的值,因为每一位都含1,所以可以由低位推出高位,唯一特殊的就是高位为1的部分,在计算时特判即可.
**********************************************************************************/
class Solution {
public:
    int numberOf1Between1AndN_Solution(int n) {
        int ss=0;
        long long a[10]={0},b[10]={0};
        long long i,l=1;a[0]=1,b[0]=10;
        for(i=100;i<=n;i*=10){//预处理最低数位推出最高数位
            a[l]=10*a[l-1]+b[l-1];
            b[l++]=i;
        }for(i=l-1;i>=0;i--){
            if(b[i]<=n){
                ss+=n/b[i]*a[i];//计算低位的贡献推出高位余下部分
                if(2*b[i]<=n)ss+=b[i];//为1时的特判
                else ss+=n-b[i]+1;//计算高位为1到2之间的部分
                n%=b[i];
            }
        }if(n)ss++;//个位余下的单独计算
        return ss;
    }
};
```



#### 计数dp

#### 记忆化搜索

##### [力扣]下降路径最小和II

题目来源：[https://leetcode-cn.com/problems/minimum-falling-path-sum-ii](https://leetcode-cn.com/problems/minimum-falling-path-sum-ii/)

问题描述:给你一个 n x n 整数矩阵 arr ，请你返回 非零偏移下降路径 数字和的最小值。

非零偏移下降路径 定义为：从 arr 数组中的每一行选择一个数字，且按顺序选出来的数字中，相邻数字不在原数组的同一列。

<img src=".\img\下降路径最小和II.jpg" style="float:left" />

```c++
/*********************************************************************************
  *Author: 麦文鹏
  *Contact:  2512834769@qq.com
  *Description:  根据题意只要选取每行里最小的数字加起来就行了，题目唯一的限制就是不能选取该数字正上方的数字，所以要考虑上一行最小的数字是否在该数字正上方，所以要记录上一行的最小值和次小值
**********************************************************************************/
class Solution {
public:
    int minFallingPathSum(vector<vector<int>>& grid) {
        int n=grid.size();
        if(n<2)return grid[0][0];//直接输出
        int dp[n];//dp[i]表示下降到某行时位置i的最小值
        
        
        int Min1=INT_MAX,Min2=INT_MAX,Min_index=-1;//Min1代表最小值，Min2代表次小值
		//先找到第1行的最小值和次小值
        for(int i=0;i<n;i++){
            if(Min1>=grid[0][i]){
                Min2=Min1;
                Min1=grid[0][i];
                Min_index=i;
            }else if(Min2>grid[0][i]){
                Min2=grid[0][i];
            }
        }
		//从第2行开始，每个数字加上上一行的最小值或次小值即是该位置的下降最小值
        for(int i=1;i<n;i++){
            for(int j=0;j<n;j++){
                //Min_index代表上一行最小值的位置
                if(j!=Min_index){
                    dp[j]=Min1+grid[i][j];
                }else{
                    dp[j]=Min2+grid[i][j];
                }

            }
            Min1=INT_MAX,Min2=INT_MAX;
            //因为每次下降最值都会变化，所以现在要更新最值
            for(int j=0;j<n;j++){
                if(Min1>dp[j]){
                    Min2=Min1;
                    Min1=dp[j];
                    Min_index=j;
                }else if(Min2>dp[j]){
                    Min2=dp[j];
                }
            }
        }
        
        for(int i=1;i<n;i++){
            dp[0]=min(dp[i],dp[0]);
        }
        return dp[0];
    }
};
```

### 高级数据结构

#### ST表

##### [洛谷]P1890 gcd区间

题目来源：https://www.luogu.com.cn/problem/P1890#submit

题目描述：给定一行n个正整数a[1]..a[n]。m次询问，每次询问给定一个区间[L,R]，输出a[L]..a[R]的最大公因数。

```c++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description: st表模板
*****************************************/
#include<bits/stdc++.h>
using namespace std;
int dp[1001][15];
int query(int l,int r){
    int j=(int)log2(r-l+1);
    return __gcd(dp[l][j],dp[r-(1<<j)+1][j]);
}
int main(){
    ios::sync_with_stdio(false);
    int n,m;
    cin>>n>>m;
    for(int i=0;i<n;i++){
        cin>>dp[i][0];
    }
    for(int j=1;j<=log2(n);j++)
        for(int i=0;i+(1<<j)-1<n;i++)
            dp[i][j]=__gcd(dp[i][j-1],dp[i+(1<<(j-1))][j-1]);
    while(m--){
        int l,r;
        cin>>l>>r;
        cout<<query(l-1,r-1)<<endl;
    }
    return 0;
}
```

##### [POJ]**Balanced Lineup**

题目来源：http://poj.org/problem?id=3264

题目描述：选择某个区间，计算区间内的最大值与最小值之差。踩坑点：POJ不能使用log2

```c++
/****************************************
  *Author:  Mai
  *Contact:  
  *Description: st模板
*****************************************/
#include <iostream>
#include <cmath>
using namespace std;
const int maxn = 50005;
int a[maxn], t[maxn][20], s[maxn][20];

int main() {
	ios::sync_with_stdio(false), cin.tie(0), cout.tie(0);

	int N, Q;
	cin >> N >> Q;

	for (int i = 1; i <= N; i++)
		cin >> a[i];

	int k = log((double)N) / log(2.0);

	for (int i = 1; i <= N; i++) {
		t[i][0] = s[i][0] = a[i];
	}

	for (int j = 1; j <= k; j++) {
		for (int i = 1; i <= N - (1 << j) + 1; i++) {
			t[i][j] = max(t[i][j - 1], t[i + (1 << (j - 1))][j - 1]);
			s[i][j] = min(s[i][j - 1], s[i + (1 << (j - 1))][j - 1]);
		}
	}
	int l, r;
	while (Q--) {
		cin >> l >> r;
		k = log(double(r - l + 1)) / log(2.0);
		cout << max(t[l][k], t[r - (1 << k) + 1][k]) - min(s[l][k], s[r - (1 << k) + 1][k]) << endl;
	}
	return 0;
}
```

##### [POJ]Frequent values

题目来源：http://poj.org/problem?id=3368

题目描述：查找某个区间内出现次数最多的数字的个数

```c++
/****************************************
  *Author:  Mai
  *Contact:  
  *Description: 数列非逆序排列，可以用dp[i]表示第i位数字的连续个数,再查询dp区间的最大值即可
*****************************************/
#include <iostream>
#include <cmath>
using namespace std;
const int maxn = 100010;
int a[maxn], dp[maxn], st[maxn][20], N, Q;

double log2(int x) {
	return log((double)x) / log(2.0);
}

int query(int l, int r) {
	if (l > r)
		return 0;
	int k = log2(r - l + 1);
	return max(st[l][k], st[r - (1 << k) + 1][k]);
}

int main() {
	while (scanf("%d", &N) && N) {
		memset(a, 0, sizeof(a));
		memset(dp, 0, sizeof(dp));
		memset(st, 0, sizeof(st));
		scanf("%d", &Q);
		for (int i = 1; i <= N; i++) {
			cin >> a[i];
			if (i == 1 || a[i] != a[i - 1]) {
				dp[i] = 1;
			} else {
				dp[i] = dp[i - 1] + 1;
			}
		}
		for (int i = 1; i <= N; i++)
			st[i][0] = dp[i];
		for (int j = 1; j <= log2(N); j++) {
			for (int i = 1; i <= N - (1 << j) + 1; i++) {
				st[i][j] = max(st[i][j - 1], st[i + (1 << (j - 1))][j - 1]);
			}
		}
		int l, r;
		while (Q--) {
			scanf("%d%d", &l, &r);
			int t = l;
            //例如需要查找[]中的数字1 1 [1 1 2 2 2 3] 3
            //如果直接使用query查询，那么会把前面的两个1算进去
            //因此我们需要先把1的个数算出来，然后再算剩下的区间
			while (t <= r && a[t] == a[t - 1]) {
				t++;
			}
			printf("%d\n", max(t - l, query(t, r)));
		}
	}
	return 0;
}
```







#### 并查集

##### [力扣]按公里数计算最大组件大小

题目来源：https://leetcode.cn/problems/largest-component-size-by-common-factor/

题目描述：给定一个由不同正整数的组成的非空数组nums，考虑下面图：

* 有nums.length个节点，按从nums[0]到nums[nums.length-1]标记；
* 只有当nums[i]和nums[j]共用一个大于1的公因数时，nums[i]和nums[j]之间才有一条边。

返回图中最大连通组件的大小。

~~~c++
/*********************************************************************************
  *Author:陈家乐
  *Contact:784190546@qq.com
  *Description:先遍历nums，然后按照sqrt(num)最大值去构建连通图，最后再次遍历nums，按照不同parent计数，取parent计数最大的就是结果。
**********************************************************************************/
//并查集模板
class UnionSet{
public:
    vector<int> parent;
    vector<int> size;
    int n;
public:
    UnionSet(int _n):n(_n),size(_n,1),parent(_n){
        iota(parent.begin(),parent.end(),0);
    }
    int  find(int x){
        return x == parent[x] ?  x : find(parent[x]);
    }
    void Merge(int x,int y){
        x=find(x);
        y=find(y);
        if(x == y){
            return ;
        }
        if(size[x]<size[y]){
            swap(x,y);
        }
        size[x]+=size[y];
        parent[y]=x;
    } 
};
int gcd(int x,int y){
    return y ? gcd(y,x%y) : x;
}
class Solution {
public:
    int largestComponentSize(vector<int>& nums) {
        int m=nums.size();
        int maxNum=INT_MIN;
        for(auto& num:nums){
            maxNum=max(num,maxNum);
        }
        UnionSet un(maxNum+1);
        for(auto& num:nums){
            for(int i=2;i<=sqrt(num);i++){
                if(num % i == 0){
                    un.Merge(num,i);
                    un.Merge(num,num/i);
                }
            }
        }
        unordered_map<int,int> mp;
        int MAX=0;
        for(auto& num:nums){
            ++mp[un.find(num)];
            MAX=max(MAX,mp[un.find(num)]);
        }
        return MAX;
    }
};
~~~

##### [AcWing]社会集群

题目来源：https://www.acwing.com/problem/content/description/1599/

题目描述：在一些社交网络平台上注册账号时，平台总是会要求你填写自己的兴趣爱好，以便找到一些具有相同兴趣爱好的潜在朋友。

社会集群是指一群有共同爱好的人。

给定社交网络中所有人的兴趣爱好，请你找到所有社会群集。

```c++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description: 用vector<int>hobby[]将相同爱好的人放在一起便于合并
*****************************************/
#include<bits/stdc++.h>
using namespace std;
int a[1005];
int cnt[1005];
vector<int> hobby[1005];
int find(int x){
    if(a[x]==x){
        return a[x];
    }else{
        a[x]=find(a[x]);
    }
}
int main(){
    int n;
    cin>>n;
    for(int i=0;i<=n;i++){
        a[i]=i;
    }
    for(int i=1;i<=n;i++){
        int temp;
        scanf("%d:",&temp);
        while(temp--){
            int x;
            cin>>x;
            hobby[x].push_back(i);
        }
    }
    for(int i=1;i<=1000;i++){
        for(int j=1;j<hobby[i].size();j++){
            int x=find(hobby[i][0]),y=find(hobby[i][j]);
            if(x!=y){
                a[x]=y;
            }
        }
    }
    for(int i=1;i<=n;i++)cnt[find(i)]++;
    sort(cnt,cnt+n+1,[](const auto&u,const auto&v){
        return u>v;
    });
    int res=0;
    while(cnt[res])res++;
    cout<<res<<endl;
    for(int i=0;i<res;i++){
        cout<<cnt[i]<<" ";
    }
    return 0;
}
```

##### [洛谷]P1682 过家家

题目来源:https://www.luogu.com.cn/problem/P1682

```c++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description: 将女生朋友合并，取女生与男生相连最小值加上k且不能超过总人数n
*****************************************/
#include<bits/stdc++.h>
using namespace std;
const int N=255;
int a[N];
int visit[N][N];
int cnt[N];
int ans=INT_MAX;
struct gjj{
    int u,v;
}gj[N*N],gjj[N];
int find(int x)
{
    if(a[x]!=x) a[x]=find(a[x]);
    return a[x];
} 
void merge(int x,int y){
    int fx=find(x),fy=find(y);
    if(fx!=fy)a[fx]=fy;
}
int main(){
    int n,m,k,f;
    cin>>n>>m>>k>>f;
    for(int i=1;i<=n;i++)a[i]=i;
    for(int i=1;i<=m;i++){
            cin>>gj[i].u>>gj[i].v;
    }
    for(int i=1;i<=f;i++){
            cin>>gjj[i].u>>gjj[i].v;
            merge(gjj[i].u,gjj[i].v);
    }
    for(int i=1;i<=m;i++){
        int x=find(gj[i].u),y=gj[i].v;
        if(!visit[x][y]){
            visit[x][y]=1;
            cnt[x]++;
        }
    }
    sort(cnt,cnt+n,greater<int>());
	int index=0;
    while(cnt[index]){
        index++;
    }
	ans=cnt[index-1];
    ans+=k;
    printf("%d",min(ans,n)); 
    return 0;
}
```

##### [AcWing]图中的环

题目来源 :https://www.acwing.com/problem/content/description/4219/

代码:

```c++
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description: 问题分为两个部分:环数为1和判断连通性;这里我们利用并查集的合并性质来解决连通性问题,
  不妨把节点看成人,每个人刚开始的代表人都是自己,如果这个图是连通的那么也就是说最后这么多人都会在一个团
  伙(集合)里,那么如果用并查集来把n个人(节点)并到一起的话就需要合并n-1次,就利用这点来判断第一个连通性问
  题;而对于第二个问题也就是环数为1,我们在第一个条件的前提下不难想到:一个图连通(条件1)且环数为1(条件2)
  的充要条件是边数等于节点数,为什么呢,因为如果一个有n个节点的图是连通的,那它至少有n-1条边,而有1个环就
  等于在某两点之间加一条边,那么思路就这样,下面是代码.
**********************************************************************************/
#include<bits/stdc++.h>//本题意指图均为无向图
using namespace std;
const int N = 110;
int n, m;
int p[N];
inline int find(int &x){
	return x==p[x]?x:(p[x]=find(p[x]));//求代表人
}int main(){
    cin >> n >> m;
    for(int i = 1;i <= n;i ++ ) p[i] = i;
    int cnt = n-1;
    for (int i = 0; i < m; i ++ ){
        int a, b;
        scanf("%d%d",&a,&b);
        if(find(a) != find(b)){
            p[find(a)] = find(b);
            cnt -- ;//连通图n个点要有n-1次合并;
        }
    }if(!cnt&&n==m) printf("YES")//在连通性存在的前提下判断环数为1即"n==m"
    else printf("NO");
    return 0;
}
```

##### [AcWing 59周赛]环形联通分量

题目来源:https://www.acwing.com/problem/content/4496/

代码:

```c++
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description: 满足题意的两个必要条件,一个是联通,一个是环形,分两步判断就好了
**********************************************************************************/
#include<bits/stdc++.h>
using namespace std;
const int t =200010;
int n,m,i;
int p[t];
int ss[t];
bool st[t];
int cnt;
inline int find(int &x){return p[x]!=x?p[x]=find(p[x]):p[x];}//查代表人
int main(){
    cin>>n>>m;
    cnt=n;//联通分量个数
    for(i=1;i<=n;i++) p[i]=i;//初始化
    for(i=1;i<=m;i++){
        int x,y;
        cin>>x>>y;
        int x1=find(x),y1=find(y);
        if(x1!=y1){//并集联通
            p[x1]=y1;
            cnt--;//2并1集数减少
        }ss[x]++,ss[y]++;//边数
    }for(i=1;i<=n;i++)//题意所指的环形联通分量中每个点连接的边数都为二
       if(ss[i]!=2&&!st[find(i)]){//去所有联通分量中找某些连接边数不为二的点而淘汰这些点所在的环
           st[find(i)]=true;//判重
           cnt--;
       }
     cout<<cnt;
    return 0;
}
```

##### [POJ]Cube Stacking

题目来源：[1988 -- Cube Stacking (poj.org)](http://poj.org/problem?id=1988)

```c++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description: 建立并查集模型，建立一个son表示当前节所在树的节点数，建立一个dis表示当前节点到根节点的个数，在查找和合并的时候更新他们，输出当前节点所在树的全部节点-到根节点-1
*****************************************/
#include<stdio.h>
#include<iostream>
using namespace std;
const int N=30005;
int n,x,y;
char c;
int temp;
int a[N],son[N],dis[N];
int find(int x)
{
    if(a[x]==x)return x;
    int temp=a[x];
    a[x]=find(a[x]);
    dis[x]+=dis[temp];
    return a[x];
}
int main(){
    scanf("%d",&n);
    for(int i=1;i<=N;i++){
        a[i]=i;
        son[i]=1;
    }
    for(int i=1;i<=n;i++){
        getchar();
        scanf("%c",&c);
        if(c=='M'){
            scanf("%d%d",&x,&y);
            int x1=find(x);
            int y1=find(y);
            if(x1!=y1){
                a[y1]=x1;
                dis[y1]=son[x1];
                son[x1]+=son[y1];
            }
        }else{
            scanf("%d",&x);
            printf("%d\n",son[find(x)]-dis[x]-1);
        }
    }
    return 0;
}
```

```c++
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description: 并查集模板题:实时合并方块集与查询集合内方块数
**********************************************************************************/
#include<bits/stdc++.h>
using namespace std;
int m=100010;
int f[m], cnt[m], s[m];
int find(int x){
    if(x != f[x]){
        cnt[x] += cnt[f[x]];
        f[x] = find(f[x]);
    }return f[x];
}
int main(){
    for(int i=0;i<m;i++) {
        f[i] = i;
        s[i] = 1;
    }int n;
    scanf("%d", &n);
    char ch;
    int u, v;
    for(int i=0;i<n;i++){
        getchar();
        scanf("%c", &ch);
        if(ch=='M'){
            scanf("%d%d",&u,&v);
            int fd=find(u),fb=find(v);
            if(fd!=fb){
                f[fd]=fb;
                cnt[fd]=s[fb];
                s[fb]+=s[fd];
            }
        }else{
            scanf("%d",&u);
            find(u);
            printf("%d\n",cnt[u]);
        }
    }return 0;
}
```

##### [HDOJ]find the most comfortable road

题目来源：[Problem - 1598 (hdu.edu.cn)](https://acm.hdu.edu.cn/showproblem.php?pid=1598)

代码如下:

```c++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description: 
*****************************************/
#include<iostream>
#include<string.h>
#include<algorithm>
#include<limits.h>
using namespace std;
struct node{
    int from,to,len;
}a[100005];
int fa[100005];
int n,m,q;
int find(int x){
    if(x==fa[x])return x;
    return fa[x]=find(fa[x]);
}
bool cmp(node a,node b){
    return a.len<b.len;
}
void merge(int x,int y){
    x=find(x);
    y=find(y);
    if(x!=y)fa[y]=x;
}
int main(){
    while(cin>>n>>m){
        for(int i=0;i<m;i++){
            cin>>a[i].from>>a[i].to>>a[i].len;
        }
        sort(a,a+m,cmp);
    cin>>q;
    while(q--){
        int s,e;
        cin>>s>>e;
        int ans=INT_MAX;
        for(int i=0;i<m;i++){
            for(int j=0;j<=n;j++)fa[j]=j;
            for(int j=i;j<m;j++){
                merge(a[j].from,a[j].to);
                if(find(s)==find(e)){
                    ans=min(ans,a[j].len-a[i].len);
                    break;
                }
            }
        }
            if(ans==INT_MAX)cout<<-1<<endl;
            else cout<<ans<<endl;
        }
    }
    return 0;
}
```

##### [Colored Sticks]

题目来源：http://poj.org/problem?id=2513

```c++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description: 字典树+并查集+欧拉路
*****************************************/
#include <iostream>
#include<string.h>
#define int long long
using namespace std;
const int N=2e6+5;
int tree[N][30];
int fa[N];
int vis[N],sum[N];
int ok;
int cnt;
int p;
int pos1,pos2,root;
string a,b;
int find(int x){
    if(x==fa[x])return x;
    return fa[x]=find(fa[x]);
}
void merge(int x,int y){
    x=find(x);y=find(y);
    if(x!=y)fa[y]=x;
}
int build(string s){
    root=0;
    int len=s.size();
    for(int i=0;i<len;i++){
        int cur=s[i]-'a';
        if(!tree[root][cur])tree[root][cur]=++cnt;
        root=tree[root][cur];
    }
    if(!vis[root])vis[root]=++p;
    if(!fa[p])fa[p]=p;
    return vis[root];
}
signed main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    while(cin>>a>>b){
        pos1=build(a);
        sum[pos1]++;
        pos2=build(b);
        sum[pos2]++;
        merge(pos1,pos2);
    }
    for(int i=1;i<=p;i++){
        if(sum[i]%2)ok++;
        if(find(i)!=find(1)){
            cout<<"Impossible"<<endl;
            return 0;
        }
    }
    if(ok==2 || ok==0){
        cout<<"Possible"<<endl;
    }else cout<<"Impossible"<<endl;
}
```



#### 优先队列

##### [HDOJ]The kth great number

题目来源：[Problem - 4006 (hdu.edu.cn)](https://acm.hdu.edu.cn/showproblem.php?pid=4006)

```c++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description: 优先队列从小到大排序，只保存k个数字，每次输出top，遇到比top大的数字时候删除top，加入这个数字
*****************************************/
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<queue>
#include<functional>
using namespace std;
int main(){
    int n,k;
    while(~scanf("%d%d",&n,&k)){
    priority_queue<int,vector<int>,greater<int> > q;
    for(int i=1;i<=n;i++){
        getchar();
        char c;
        scanf("%c",&c);
        if(c=='I'){
            int x;
            scanf("%d",&x);
            if(q.size()<k){
                q.push(x);
            }else if(x>q.top()){
                q.pop();
                q.push(x);
            }
        }else{
            printf("%d\n",q.top());
        }
    }
    }
    return 0;
}
```

##### [力扣第318场周赛]雇佣K位工人的总代价

题目来源:https://leetcode.cn/problems/total-cost-to-hire-k-workers/

```c++
/****************************************
  *Author:  Mai
  *Contact:  
  *Description: 使用两个优先队列配合双指针维护首尾的最小值
*****************************************/
class Solution {
public:
    long long totalCost(vector<int>& costs, int k, int candidates) {
        int n=costs.size(),vis[n];
        memset(vis,0,sizeof(vis));
        long long ans=0;
        if(candidates*2>=n){
            sort(costs.begin(),costs.end());
            for(int i=0;i<k;i++){
                ans+=costs[i];
            }
            return ans;
        }else{
            priority_queue<int,vector<int>,greater<int>> L,R;
            int i,j;
            for(i=0,j=n-1;i<candidates;){
                L.push(costs[i++]);
                R.push(costs[j--]);
            }
            while(k--){
                int Left=L.empty()?INT_MAX:L.top();
                int Right=R.empty()?INT_MAX:R.top();
                if(Left<=Right){
                    ans+=Left;
                    L.pop();
                    if(i<=j)L.push(costs[i++]);
                }else{
                    ans+=Right;
                    R.pop();
                    if(i<=j)R.push(costs[j--]);
                }
            }
            return ans;
        }
        
    }
};
```



#### 树状数组

##### [POJ]Ultra-QuickSort

题目来源：[2299 -- Ultra-QuickSort (poj.org)](http://poj.org/problem?id=2299)

```c++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description: 树状数组求逆序数，二分排序离散化
*****************************************/
#include<iostream>
#include<algorithm>
#include<string.h>
#define int long long
#define endl '\n'
using namespace std;
const int N=500005;
int tree[N],n;
int w[N],xs[N];
int lowbit(int x){
    return x&-x;
}
void add(int x,int k){
    while(x<=n){
        tree[x]+=k;
        x+=lowbit(x);
    }
}
int sum(int x){
    int ans=0;
    while(x>0){
        ans+=tree[x];
        x-=lowbit(x);
    }
    return ans;
}
int find(int x){
    int l=1,r=n;
    while(l<r){
        int mid=(l+r+1)>>1;
        if(xs[mid]>x)r=mid-1;
        else l=mid;
    }
    return r;
}
signed main(){
    while(cin>>n&&n){
        int res=0;
        memset(tree,0,sizeof tree);
        for(int i=1;i<=n;i++){
            cin>>w[i];
            xs[i]=w[i];
        }
        sort(xs+1,xs+n+1);
        for(int i=1;i<=n;i++){
            int v=find(w[i]);
            res+=i-1-sum(v);
            add(v,1);
        }
        cout<<res<<endl;
    }
}
```

##### [poj]3067-求逆序对

题目来源:http://poj.org/problem?id=3067

题目描述:求多条线交点的个数

```c++
/****************************************
  *Author:  Mai
  *Contact:
  *Description:其本质是求逆序对。对每一条线进行排序，如果x1<=x2且y1<y2,那么这两条线就是正序
  设正序个数为a,逆序个数为b,则第i条线的逆序个数bi=i-ai
*****************************************/
#include <iostream>
#include <cstring>
#include <algorithm>
#include <queue>
#include <vector>
#include <stack>
#define ptf printf
#define sc scanf
using namespace std;
typedef long long ll;
typedef pair<int,int> PII;
const int maxn=1e6+10;
ll c[1010];
PII p[maxn];
bool cmp(const PII &a,const PII &b){
    if(a.first!=b.first)return a.first<b.first;
    return a.second<b.second;
}
int lowbit(int x){return x&(-x);}
void add(int x){for(;x<1010;x+= lowbit(x)){c[x]++;}}
ll sum(int x){
    ll res=0;
    for(;x>0;x-= lowbit(x))res+=c[x];
    return res;
}
ll solve(){
    memset(c,0,sizeof(c));
    int N,M,K;
    sc("%d%d%d",&N,&M,&K);
    for(int i=0;i<K;i++){
        sc("%d%d",&p[i].first,&p[i].second);
    }
    ll ans=0;
    sort(p,p+K,cmp);
    for(int i=0;i<K;i++){
        ans+=(i-sum(p[i].second));
        add(p[i].second);
    }
    return ans;
}

int main() {
    int T;
    sc("%d",&T);
    for(int i=1;i<=T;i++){
        ptf("Test case %d: %lld\n",i,solve());
    }
}
```

##### [vjudge]Gym-237040F 区间修改和区间查询

题目来源:https://vjudge.net/problem/Gym-237040F

题目描述:给定一个长度为n的数组，要求能够进行区间修改和区间查询。

```c++
/****************************************
  *Author:  Mai
  *Contact:
  *Description:这道题最大的坑是少开一个long long 就要WA.
  Σai=n*Σci-Σ(i-1)*ci  i∈[1,n]
  只需要维护两个差分数组 ci和(i-1)*c[i]即可
*****************************************/
#include<bits/stdc++.h>
#define sc scanf
#define ptf printf
#define int long long
using namespace std;
const int maxn=1000010;
int c[maxn],c2[maxn],a[maxn];
int lowbit(int x){return x&(-x);}
int sum(int x){
    int res=0,n=x;
    for(;x>0;x-= lowbit(x)){
        res+=n*c[x]-c2[x];//n*sigma(c1,n)-sigma(c2,n)
    }
    return res;
}
void add(int x,int k){
    int n=x-1;
    for(;x<maxn;x+= lowbit(x)){
        c[x]+=k;
        c2[x]+=n*k;
    }
}
void solve(){
    int n,q,op,k;
    sc("%lld%lld",&n,&q);
    for(int i=1;i<=n;i++){
        sc("%lld",&a[i]);
        add(i,a[i]-a[i-1]);
    }
    int l,r;
    while(q--){
        sc("%lld%lld%lld",&op,&l,&r);
        if(op==1){
            sc("%lld",&k);
            add(l,k),add(r+1,-k);
        }else{
            ptf("%lld\n",sum(r)- sum(l-1));
        }
    }

}

signed main() {
    solve();
}
```

##### [洛谷]区域修改和单点查询

题目来源:https://www.luogu.com.cn/problem/P3368

题目描述:在树状数组单点查询的基础上升级，要求能够进行单点查询和区域修改

```c++
/****************************************
  *Author:  Mai
  *Contact:
  *Description:树状数组进行区域修改需要用到差分数组,设b[x]=a[i]-a[i-1]
  我们构建一个树状数组c[]
   所以我们可以得出以下公式：
   单点查询：a[i]=b[1]+b[2]+b[3]+...+b[i]=sum(i)
   区域修改：Σa[x]+k(i<=x<=j) 等价于 b[i]+k,b[j+1]+k
   即 add(i,k),add(j+1,-k)
*****************************************/
#include <bits/stdc++.h>
#define syncIO ios::sync_with_stdio(false),cin.tie(0),cout.tie(0);
#define sc scanf
#define ptf printf
using namespace std;
typedef long long ll;
const int maxn=1e5+10;
ll c[5*maxn],a[5*maxn];
int N,M;
int lowbit(int x){
    return x&(-x);
}

ll sum(int x){
    ll res=0;
    for(;x>0;x-= lowbit(x))res+=c[x];
    return res;
}
void add(int x,int k){
    for(;x<=N;x+= lowbit(x)){
        c[x]+=k;
    }
}
void solve(){
    int x,y,k,ch;
    sc("%d%d",&N,&M);
    for(int i=1;i<=N;i++){
        sc("%lld",&a[i]);
        add(i,a[i]-a[i-1]);
    }
    while(M--){
        sc("%d",&ch);
        if(ch==1){
            sc("%d%d%d",&x,&y,&k);
            add(x,k),add(y+1,-k);
        }else if(ch==2){
            sc("%d",&x);
            ptf("%lld\n",sum(x));
        }
    }
}

int main() {
    solve();
}
```



##### [poj]3321-子树查询

题目来源:http://poj.org/problem?id=3321

题目描述:给定一个树，每个节点都有一个苹果，第一个操作是求某个节点下苹果的个数，第二个操作是单点修改某个节点的苹果状态

```c++
/****************************************
  *Author:  Mai
  *Contact:
  *Description:这道题的难点在于用dfs序重新确定顺序
  s[i],e[i]代表第i个节点的开始遍历时间和其子树遍历结束时间，即节点i的管辖范围
  这样就可以通过构建树状数组求区间和sum(e[i])-sum(s[i]-1)
*****************************************/
#include <iostream>
#include <vector>
#define syncIO ios::sync_with_stdio(false),cin.tie(0),cout.tie(0);
using namespace std;
typedef long long ll;
const int maxn=1e5+10;
vector<vector<int>> edge(maxn);
int s[maxn],e[maxn],vis[maxn],c[maxn],tot=1,a[maxn];
void dfs(int u){
    s[u]=tot++;
    vis[u]=1;
    for(int i=0;i<edge[u].size();i++){
        int v=edge[u][i];
        if(vis[v])continue;
        dfs(v);
    }
    e[u]=tot-1;
}

int lowbit(int x){
    return x&(-x);
}
int sum(int i){
    int res=0;
    for(;i>0;i-= lowbit(i))res+=c[i];
    return res;
}
void add(int i,int k){
    for(;i<maxn;i+= lowbit(i)){
        c[i]+=k;
    }
}
void solve(){
    int n,m,x,y;
    scanf("%d",&n);
    for(int i=1;i<n;i++){
        scanf("%d%d",&x,&y);
        edge[x].push_back(y);
    }
    dfs(1);
    for(int i=1;i<=n;i++)a[i]=1,add(i,1);
    char ch;
    int num;
    scanf("%d",&m);
    for(int i=0;i<m;i++){
        scanf(" %c%d",&ch,&num);
        if(ch=='Q'){
            cout<<sum(e[num])-sum(s[num]-1)<<endl;
        }else{
            if(a[num])add(s[num],-1);
            else add(s[num],1);
            a[num]^=1;
        }
    }
}

int main() {
    solve();
}
```

##### [poj]1195-矩阵区域查询

题目来源:http://poj.org/problem?id=1195

题目描述:主要是进行区域查询和区域修改，使用二维树状数组完成

```c++
/****************************************
  *Author:  Mai
  *Contact:
  *Description:
*****************************************/
#include <iostream>
#include <cstring>

#define syncIO ios::sync_with_stdio(false),cin.tie(0),cout.tie(0);
using namespace std;
typedef long long ll;
const int maxn = 1100;
int c[maxn][maxn], S;
int lowbit(int x) {
    return x & (-x);
}

int sum(int x, int y) {
    int res = 0;
    for (int i = x; i > 0; i -= lowbit(i)) {
        for (int j = y; j > 0; j -= lowbit(j)) {
            res += c[i][j];
        }
    }
    return res;
}

void add(int x, int y, int k) {
    for (int i = x; i < maxn; i += lowbit(i)) {
        for (int j = y; j < maxn; j += lowbit(j)) {
            c[i][j] += k;
        }
    }
}

void solve() {
    char ch;
    int a, b, x, y;
    while (cin >> ch, ch != '3') {
        if (ch == '0') {
            cin >> S;
            memset(c, 0, sizeof(c));
        } else if (ch == '1') {
            cin >> a >> b >> x;
            add(a + 1, b + 1, x);
        } else if (ch == '2') {
            cin >> a >> b >> x >> y;
            cout << sum(x + 1, y + 1) + sum(a, b) - sum(a, y + 1) - sum(x + 1, b) << endl;
        }
    }
}

int main() {
    syncIO;
    solve();
}
```

#### 线段树

##### [洛谷]P3374 【模板】树状数组 1

题目来源：https://www.luogu.com.cn/problem/P3374

题目描述：如题，已知一个数列，你需要进行下面两种操作：

- 将某一个数加上 x*x*
- 求出某区间每一个数的和

```C++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description: 线段树的单点修改和区间查询
*****************************************/
#include<bits/stdc++.h>
using namespace std;
int n,m,ans;
int input[500010];
struct node{
    int left,right,num;
}tree[2000010];
inline void build(int left,int right,int index){
    tree[index].left=left;
    tree[index].right=right;
    if(left==right){
        tree[index].num=input[left];
        return;
    }
    int mid=(right+left)>>1;
    build(left,mid,index*2);
    build(mid+1,right,index*2+1);
    tree[index].num=tree[index*2].num+tree[index*2+1].num;
}
//单点修改
inline void my_plus(int index,int dis,int k){
    tree[index].num+=k;
    if(tree[index].left==tree[index].right)return;
    if(dis<=tree[index*2].right)my_plus(index*2,dis,k);
    if(dis>=tree[index*2+1].left)my_plus(index*2+1,dis,k);
}
//查询区间和
inline void search(int index,int l,int r){
    if(tree[index].left>=l && tree[index].right<=r){
        ans+=tree[index].num;
        return;
    }
    if(tree[index*2].right>=l)search(index*2,l,r);
    if(tree[index*2+1].left<=r)search(index*2+1,l,r);
}
int main(){
    cin>>n>>m;
    for(int i=1;i<=n;i++)scanf("%d",&input[i]);
    //构造二叉树
    build(1,n,1);
    for(int i=1;i<=m;i++){
        int a,b,c;
        scanf("%d%d%d",&a,&b,&c);
        if(a==1){
            my_plus(1,b,c);
        }else{
            ans=0;
            search(1,b,c);
            printf("%d\n",ans);
        }
    }
    return 0;
}
```

##### [洛谷]P3372 【模板】线段树 1

题目来源：https://www.luogu.com.cn/problem/P3372

题目描述：如题，已知一个数列，你需要进行下面两种操作：

1. 将某区间每一个数加上 k。
2. 求出某区间每一个数的和。

```c++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description: 线段树的区间修改和区间查询
*****************************************/
#include <bits/stdc++.h>
#define ll long long
using namespace std;
ll n,m;
ll input[100005];
struct node
{
    ll l,r,data;
    ll lt;    
}tree[500005];
inline void build(ll l,ll r,ll index)
{
    tree[index].lt=0;
    tree[index].l=l;
    tree[index].r=r;
    if(l==r)
    {
        tree[index].data=input[l];
        return ;
    }
    ll mid=(l+r)/2;
    build(l,mid,index*2);
    build(mid+1,r,index*2+1);
    tree[index].data=tree[index*2].data+tree[index*2+1].data;
    return ;
}
inline void push_down(ll index)
{
    if(tree[index].lt!=0)
    {
        tree[index*2].lt+=tree[index].lt;
        tree[index*2+1].lt+=tree[index].lt;
        ll mid=(tree[index].l+tree[index].r)/2;
        tree[index*2].data+=tree[index].lt*(mid-tree[index*2].l+1);
        tree[index*2+1].data+=tree[index].lt*(tree[index*2+1].r-mid);
        tree[index].lt=0;
    }
    return ;
}
inline void up_data(ll index,ll l,ll r,ll k)
{
    if(tree[index].r<=r && tree[index].l>=l)
    {
        tree[index].data+=k*(tree[index].r-tree[index].l+1);
        tree[index].lt+=k;
        return ;
    }
    push_down(index);
    if(tree[index*2].r>=l)
        up_data(index*2,l,r,k);
    if(tree[index*2+1].l<=r)
        up_data(index*2+1,l,r,k);
    tree[index].data=tree[index*2].data+tree[index*2+1].data;
    return ;
}
inline ll search(ll index,ll l,ll r)
{
    if(tree[index].l>=l && tree[index].r<=r)
        return tree[index].data;
    push_down(index);
    ll num=0;
    if(tree[index*2].r>=l)
        num+=search(index*2,l,r);
    if(tree[index*2+1].l<=r)
        num+=search(index*2+1,l,r);
    return num;
}
int main()
{
    cin>>n>>m;
    for(ll i=1;i<=n;i++)
        cin>>input[i];
    build(1,n,1);
    for(ll i=1;i<=m;i++)
    {
        ll f;
        cin>>f;
        if(f==1)
        {
            ll a,b,c;
            cin>>a>>b>>c;
            up_data(1,a,b,c);
        }else{
            ll a,b;
            cin>>a>>b;
            printf("%lld\n",search(1,a,b));
        }
    }
    return 0;
}
```





### 数论

##### [蓝桥]  计算质数和

题目来源：http://lx.lanqiao.cn/problem.page?gpid=T1447

问题描述：编写一个程序，由用户输入一个整数n（n≤2000），然后计算出前n个质数的和。例如：如果n=3，那么结果为2+3+5=10；如果n=7，那么结果为2+3+5+7+11+13+17=58。 

代码:

```c++
/*********************************************************************************
  *Author:  陈高威
  *Contact:  1259234645@qq.com
  *Description:  判断是否为质数，是质数加起来并记录次数
**********************************************************************************/
#include <iostream>
#include <cmath>
using namespace std;

int zhi(int n){
	int i;
	if(n<2) return 0;
	for(i=2;i<=sqrt(n);i++){
		if(n%i==0) return 0;
	}
	return 1;
}
int main(){
	int n,count=0;
	cin>>n;
	int s=0;
	int i=0;
	while(count!=n){
		i++;
		if(zhi(i)){
			count++;
			s+=i;
		}
	} 
	cout<<s;
	return 0;
} 

```

##### [力扣]  找到指定长度的回文数

题目来源：https://leetcode.cn/problems/find-palindrome-with-fixed-length/

问题描述：给你一个整数数组 queries 和一个 正 整数 intLength ，请你返回一个数组 answer ，其中 answer[i] 是长度为 intLength 的 正回文数 中第 queries[i] 小的数字，如果不存在这样的回文数，则为 -1 。回文数 指的是从前往后和从后往前读一模一样的数字。回文数不能有前导 0 。

代码:

```c++
/*********************************************************************************
  *Author:  陈高威
  *Contact:  1259234645@qq.com
  *Description:  根据回文数长度，从小到大构造回文数；length = 3, [l,r]=[10, 100], 数据为[10,11,12], 回文数为[101, 111, 121];length = 4, [l,r]=[10, 100], 数据为[10,11,12], 回文数为[1001, 1111, 1221];length = 5, [l,r]=[100, 1000], 数据为[100,101,102], 回文数为[10001, 10101, 10201];length = 6, [l,r]=[100, 1000], 数据为[100,101,102], 回文数为[100001, 101101, 102201];知道如何构造就可以试着写了
**********************************************************************************/
class Solution {
public:
    vector<long long> kthPalindrome(vector<int>& queries, int intLength) {
        int n = queries.size();
        vector<long long> ans(n, -1);

        int left  = pow(10, (intLength + 1) / 2 - 1);
        int right = left * 10;

        for (int i = 0; i < n; i++) {
            if (queries[i] > right - left) {
                continue;
            }
 
            int cur = left + queries[i] - 1;
            long long sum = cur;
            int x = (intLength % 2 == 1) ? cur / 10 : cur;
            while (x > 0) {
                sum = sum * 10 + x % 10;
                x /= 10;
            }
            ans[i] = sum;
        }
        return ans;
    }
};


```

#####  [力扣]  检查[好数组]

题目来源：https://leetcode.cn/problems/check-if-it-is-a-good-array/

问题描述：给你一个正整数数组 ，你需要从中任选一些子集，然后将子集中每一个数乘以一个 任意整数，并求出他们的和。假如该和结果为 1，那么原数组就是一个「好数组」，则返回 True；否则请返回 False。

代码:

```c++
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description: 先简化一下，假设只对两个数而不是一个集合进行操作，那么这两个数要凑出1毫无疑问这两
  个数至必须要互质(假设不互质,即存在大于1的最大公因子,那么两个数不管分别乘上什么数都会有最大公因子，
  并且不管他们怎么乘，最后他们的和或者差都是那个最大公因子的倍数,且他们的线性集包含这个因子的每一个自
  然数倍,而这个因子>1，所以是不可能凑出1来的)，那么反过来呢，如果两个数是互质的，那么他们一定能凑出1
  吗，答案是肯定的(假设他们互质,那么他们不管怎么乘最后的结果一定是1的倍数,而他们的线性集包含1),那么两
  个数的情况已经了然，多个数其实一样，假设3个数，那么只要前两个数能凑出一个和第三个数互质的数就行了，而
  前两个数如果不互质，那他们凑出的数一定是他们最大公因子的倍数,那么要这个凑出的数和第三个数互质最起码也
  要是他们那个最大公因子和第三个数互质,也就是三个数互质转为2个数互质,那么多个数互质也可以转为两个数互
  质，也就是这么多个数的最大公因子为1即可.
     ps:那么做完我才知道这居然还是个定理(裴蜀定理),orz.
**********************************************************************************/
class Solution {
public:
    bool isGoodArray(vector<int>& a) {
       int i,n=a.size();
       int pp=a[0];//特殊条件是只有一个数1
       for(i=1;i<n&&pp-1;i++){
           pp=gcd(pp,a[i]);
       }return pp==1;
    }
};
```

##### [POJ]Sumdiv

题目来源：[1845 -- Sumdiv (poj.org)](http://poj.org/problem?id=1845)

```c++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description: 
*****************************************/
#include<iostream>
#define int long long
using namespace std;
const int mod=9901;
const int N=3500;
int x[N],y[N];
int qmi(int a,int b){int ans=1;for(;b;b>>=1,a=a*a%mod)if(b&1)ans=ans*a%mod;return ans;}
int sum(int a,int b){//二分求等比数列 同余模定理
    if(b==0)return 1;
    if(b%2==1)return (sum(a,b/2)*(1+qmi(a,b/2+1)))%mod;
    else return (sum(a,b/2-1)*(1+qmi(a,b/2+1))+qmi(a,b/2))%mod;
}
signed main(){
    int a,b,p,j=0,ans=1;
    cin>>a>>b;
    for(int i=2;i*i<=a;){//整数分解
        if(a%i==0){
            x[j]=i;
            y[j]=0;
            while(!(a%i)){
                y[j]++;
                a/=i;
            }
            j++;
        }
        if(i==2)i++;//跳过偶数
        else i+=2;
    }
    if(a!=1){//特判质数
        x[j]=a;
        y[j++]=1;
    }
    for(int i=0;i<j;i++){
        ans=(ans*sum(x[i],y[i]*b)%mod)%mod;//同余模定理+约数和公式
    }
    cout<<ans<<endl;
}
```

##### [POJ]C Looooops

题目来源：[2115 -- C Looooops (poj.org)](http://poj.org/problem?id=2115)

```c++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description: exgcd模板A+Cx≡B(mod 2^k)
*****************************************/
#include<iostream>
#define int long long
using namespace std;
int a,b,c,k;
int exgcd(int a,int b,int& x,int &y){
    if(!b){
        x=1;
        y=0;
        return a;
    }
    int t=exgcd(b,a%b,y,x);
    y-=a/b*x;
    return t;
}
void solve(int a,int b,int c){
    int x,y;
    int d=exgcd(a,b,x,y);
    if(c%d!=0)cout<<"FOREVER"<<endl;
    else{
        x*=(c/d);
        b=b/d;
        cout<<(x%b+b)%b<<endl;
    }
}
signed main(){
    while(cin>>a>>b>>c>>k &&a+b+c+k){
        solve(c,1ll<<k,(b-a));
    }
}
```



### 图论

#### 最小生成树

##### [POJ]Truck History

题目来源：[1789 -- Truck History (poj.org)](http://poj.org/problem?id=1789)

```c++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description: prim
*****************************************/
#include<iostream>
#include<string.h>
#define endl '\n'
#define int long long
using namespace std;
const int N=2005;
const int INF=0x3f3f3f3f;
int n,ans,temp;
int dis[N],vis[N];
int map[N][N];
char s[N][7];
int cmp(char s1[],char s2[]){
    int num=0;
    for(int i=0;i<=6;i++)
        if(s1[i]!=s2[i])num++;
    return num;
}
void prim(){
    memset(vis,0,sizeof vis);
    for(int i=1;i<n;i++)dis[i]=map[0][i];
    ans=0;
    vis[0]=1;
    for(int i=0;i<n;i++){
        int m=INF,k=-1;
        for(int i=0;i<n;i++){
            if(!vis[i] && dis[i]<m){
                m=dis[i];
                k=i;
            }
        }
        vis[k]=1;
        ans+=dis[k];
        for(int j=0;j<n;j++){
            if(!vis[j] && map[k][j]<dis[j])dis[j]=map[k][j];
        }
    }
}   
signed main(){
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    while(cin>>n){
        if(n==0)break;
        for(int i=0;i<n;i++)cin>>s[i];
        memset(map,0,sizeof map);
        for(int i=0;i<n;i++){
            for(int j=i+1;j<n;j++){
                map[i][j]=map[j][i]=cmp(s[i],s[j]);
            }
        }
        prim();
        cout<<"The highest possible quality is 1/"<<ans<<"."<<endl;
    }
}
```

##### [POJ]Highways

题目来源：[2485 -- Highways (poj.org)](http://poj.org/problem?id=2485)

```c++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description: kruskal
*****************************************/
#include<iostream>
#include<string.h>
#define int long long
#define endl '\n'
using namespace std;
const int INF=0x3f3f3f3f;
const int N=505;
int map[N][N],fa[N];
int n,minn,a,b,res;
int find(int x){
    if(x==fa[x])return x;
    return fa[x]=find(fa[x]);
}
void merge(int x,int y){
    x=find(x);y=find(y);
    if(x!=y)fa[y]=x;
}
signed main(){
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t;
    cin>>t;
    while(t--){
        res=0;
        cin>>n;
        for(int i=1;i<=n;i++)fa[i]=i;
        for(int i=1;i<=n;i++){
            for(int j=1;j<=n;j++){
                cin>>map[i][j];
            }
        }
        for(int i=0;i<n-1;i++){
            minn=INF;
            for(int i=1;i<=n;i++)
                for(int j=1;j<=n;j++){
                    if(i==j)continue;
                    if(map[i][j]<minn && find(i)!=find(j)){
                        a=i;
                        b=j;
                        minn=map[i][j];
                    }
                }
                merge(a,b);
                res=max(res,map[a][b]);
        }
        cout<<res<<endl;
    }
}
```



#### 最短路径

##### [POJ]Currency Exchange

题目来源：[1860 -- Currency Exchange (poj.org)](http://poj.org/problem?id=1860)

```c++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description: 反向bellman-ford
*****************************************/
//#include<bits/stdc++.h>
#include<iostream>
#include<string.h>
#define int long long
//#define endl '\n'
using namespace std;
double n,m;
int cnt,s;
double v;
struct node{
    int a,b;
    double r,c;
}e[5005];
double dis[5005];
inline void add(int x,int y,double r,double c){
    e[++cnt].a=x;
    e[cnt].b=y;
    e[cnt].r=r;
    e[cnt].c=c;
}
inline bool Bellman_Ford(){
    memset(dis,0,sizeof dis);
    dis[s]=v;
    for(int i=1;i<n;i++){
        bool flag=false;
        for(int j=1;j<=cnt;j++){
            if(dis[e[j].b]<(dis[e[j].a]-e[j].c)*e[j].r){
                dis[e[j].b]=(dis[e[j].a]-e[j].c)*e[j].r;
                flag=true;
            }
        }
        if(!flag)return false;
    }
    for(int i=1;i<=cnt;i++){
        if(dis[e[i].b]<(dis[e[i].a]-e[i].c)*e[i].r)return true;
    }
    return false;
}
signed main(){
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    cin>>n>>m>>s>>v;
    for(int i=1;i<=m;i++){
        int x,y;
        double a1,a2,b1,b2;
        cin>>x>>y>>a1>>a2>>b1>>b2;
        add(x,y,a1,a2);
        add(y,x,b1,b2);
    }
    if(Bellman_Ford())cout<<"YES"<<endl;
    else cout<<"NO"<<endl;
}
```



##### [POJ]Wormholes

题目来源：[3259 -- Wormholes (poj.org)](http://poj.org/problem?id=3259)

```c++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description: bellman-ford队列优化
*****************************************/
//#include<bits/stdc++.h>
#include<iostream>
#include<string.h>
#include<queue>
#include<algorithm>
#define int long long
//#define endl '\n'
using namespace std;
const int N=5505;
const int inf=0x3f3f3f3f;
int f,n,m,w,s,e,t,cnt;
struct node{
    int to,next,w;
}edge[N];
int first[N];
int dist[N];
int vis[N];
int num[N];
void add(int u,int v,int w){
    edge[++cnt].to=v;
    edge[cnt].w=w;
    edge[cnt].next=first[u];
    first[u]=cnt;
}
int SPFA_min(){
    memset(vis,0,sizeof vis);
    memset(num,0,sizeof num);
    queue<int>q;
    dist[1]=0;
    q.push(1);
    num[1]++;
    while(q.size()){
        int u=q.front();
        q.pop();
        vis[u]=0;//u点被当作出发点 vis[u]=0 即为u被pop队列
        for(int i=first[u];i!=0;i=edge[i].next){
            int v=edge[i].to;
            if(dist[v]>dist[u]+edge[i].w){
                dist[v]=dist[u]+edge[i].w;
                if(!vis[v]){//vis是用来判断在不在队列中的
                    vis[v]=1;//这个点到过就是vis[v]=1;
                    q.push(v);//
                    num[v]++;
                    if(num[v]>(n-1))return 1;
                }
            }
        }
    }
    return 0;
}
signed main(){
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    cin>>f;
    while(f--){
        cin>>n>>m>>w;
        cnt=0;
        memset(first,0,sizeof first);
        for(int i=1;i<=n;i++)dist[i]=inf;
        for(int i=1;i<=m;i++){
            cin>>e>>s>>t;
            add(e,s,t);
            add(s,e,t);
        }
        for(int i=1;i<=w;i++){
            cin>>e>>s>>t;
            add(e,s,-t);
        }
        if(SPFA_min())cout<<"YES"<<endl;
        else cout<<"NO"<<endl;
    }
}
```

##### [POJ]昂贵的聘礼

题目来源：[1062 -- 昂贵的聘礼 (poj.org)](http://poj.org/problem?id=1062)

```c++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description: 枚举区间[L-M+i, L+i]bellman-ford
*****************************************/
#include<iostream>
#include<vector>
#include<algorithm>
#include<string.h>
#include<queue>
#define int long long
using namespace std;
const int N=5505;
const int inf=0x3f3f3f3f;
int n,m,p,l,x,t,v;
int temp1,temp2;
int res,cnt;
int bq;
int vis[N];
int ten[N];
int te;
int pd[N];
int bj;
int pp[N];
struct node{
    int to;
    int w;//边的权重
    int next;
}edge[N];
int dbs[N];
int first[N];
int dist[N];
int num[N];
void Beelman_Ford(){
    memset(vis,0,sizeof vis);
    for(int i=1;i<=n;i++)dist[i]=inf;
    queue<int>q;
    dist[1]=0;
    q.push(1);
    int index=0;
    while(q.size()){
        index++;
        if(index==n)return;
        int u=q.front();
        q.pop();
        vis[u]=0;//u点被当作出发点 vis[u]=0 即为u被pop队列
        for(int i=first[u];i!=0;i=edge[i].next){
            int v=edge[i].to;
            if(pp[v]>=temp1 && pp[v]<=temp2)
            if(dist[v]>dist[u]+edge[i].w){
                dist[v]=dist[u]+edge[i].w;
                res=min(dist[v]+dbs[v],res);
                if(!vis[i]){
                    vis[v]=1;
                    q.push(v);
                }
            }
        }
    }
}
void add(int u,int v,int w){
    edge[++cnt].to=v;
    edge[cnt].w=w;
    edge[cnt].next=first[u];
    first[u]=cnt;
}
signed main(){
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    cin>>m>>n;//等级差距限制 物品总数
    for(int i=1;i<=n;i++){
        cin>>p>>l>>x;
        dbs[i]=p;
        pp[i]=l;
        if(i==1){
            res=p;
            bj=l;
            bq=p;
        }
        for(int j=0;j<x;j++){
            cin>>t>>v;//替代品编号   优惠价格
            add(i,t,v);
        }
    }
    for(int i=0;i<=m;i++){
        memset(pd,0,sizeof pd);
        temp1=bj-m+i;
        temp2=bj+i;
        for(int j=temp1;j<=temp2;j++){
            pd[j]=1;
        }
        //过每一个边 如果这个边的to点等级在pd范围内 即 pp=1;
        Beelman_Ford();
    }
    cout<<res<<endl;
}
```

##### [POJ]Stockbroker Grapevine

题目来源：[1125 -- Stockbroker Grapevine (poj.org)](http://poj.org/problem?id=1125)

```c++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description: Floy模板 每两个点之间的最短路径
*****************************************/
#include<iostream>
#include<string.h>
#define endl '\n'
using namespace std;
const int N=105;
const int INF=0x3f3f3f3f;
int n,m,res,sg,temp;
int map[N][N];
signed main(){
    while(cin>>n){
        if(n==0)break;
        res=INF;
        memset(map,INF,sizeof map);
    for(int i=1;i<=n;i++){
        cin>>m;
        for(int j=0;j<m;j++){
            int x,y;
            cin>>x>>y;
            map[i][x]=y;
        }
    }
    for(int k=1;k<=n;k++)
        for(int i=1;i<=n;i++)
            for(int j=1;j<=n;j++)
                map[i][j]=min(map[i][j],map[i][k]+map[k][j]);
    for(int i=1;i<=n;i++){
        temp=0;
        for(int j=1;j<=n;j++){
            if(i==j)continue;
            if(map[i][j]>temp){
                temp=map[i][j];
            }
        }
        if(res>temp){
            res=temp;
            sg=i;
        }
    }
    cout<<sg<<" "<<res<<endl;
    }
}
```

##### [POJ]Frogger

题目来源：[2253 -- Frogger (poj.org)](http://poj.org/problem?id=2253)

```c++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description: dijkstra
*****************************************/
#include<iostream>
#include<string.h>
#include<algorithm>
#include<cmath>
#include<iomanip>
#define int long long
#define endl '\n'
using namespace std;
const int N=10;
const int INF=0x3f3f3f;
double map[N][N];
double x[N],y[N],d[N],vis[N];
int n;
double dis(double x1,double y1,double x2,double y2){
    return sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));
}
void init()
{
	memset(map,0,sizeof(map));
	for(int i=0;i<N;i++)
	d[i] = INF;
	memset(vis,0,sizeof(vis));
}
signed main(){
    ios::sync_with_stdio(false);cout.tie(0);cin.tie(0);
    int q=1;
    while(cin>>n){
        if(n==0)break;
        init();
    for(int i=0;i<n;i++){
        cin>>x[i]>>y[i];
    }
    for(int i=0;i<n-1;i++)
        for(int j=1;j<n;j++)
            map[i][j]=map[j][i]=dis(x[i],y[i],x[j],y[j]); 
    for(int i=0;i<n;i++){
        double minn=INF;
        d[0]=0;//源节点到该节点的最小距离
        int k;
        for(int j=0;j<n;j++){
            if(!vis[j] && d[j]<minn){
                k=j;
                minn=d[j];
            }
        }
        vis[k]=1;
        for(int j=0;j<n;j++){
            d[j]=min(d[j],max(d[k],map[k][j]));// min(max(起点到k这个点的跳跃距离，k到新节点的距离),新节点到原点的距离)
        }
    }
    cout<<"Scenario #"<<q++<<endl<<"Frog Distance = "<<fixed<<setprecision(3)<<d[1]<<endl<<endl;
    }
}
```

```c++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description: prim实现
*****************************************/
#include<iostream>
#include<string.h>
#include<algorithm>
#include<cmath>
#include<iomanip>
#include<string>
#define int long long
#define endl '\n'
using namespace std;
const int N=205;
const int INF=0x3f3f3f;
double map[N][N];
double x[N],y[N];
int n,q=1;
double dis(double x1,double y1,double x2,double y2){
    return sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));
}
void prim(){
    double lowcost[N];
    int vis[N];
    for(int i=1;i<n;i++){
        lowcost[i]=map[0][i];
        vis[i]=0;
    }

    double ans=0;
    vis[0]=1;
    for(int i=0;i<n;i++){
        int j=0;
        for(int k=0;k<n;k++){
            if(!vis[k] && lowcost[k]<lowcost[j])j=k;
        }
        if(j==0)break;
        ans=max(ans,lowcost[j]);
        vis[j]=1;
        if(j==1)break;
        for(int k=0;k<n;k++){
            if(!vis[k] && map[j][k]<lowcost[k]){
                lowcost[k]=map[j][k];
            }
        }
    }
    cout<<"Scenario #"<<q++<<endl<<"Frog Distance = "<<fixed<<setprecision(3)<<ans<<endl<<endl;
    return;
}
signed main(){
    ios::sync_with_stdio(false);cout.tie(0);cin.tie(0);
    while(cin>>n){
        if(n==0)break;
    for(int i=0;i<n;i++){
        cin>>x[i]>>y[i];
    }
    for(int i=0;i<n-1;i++)
        for(int j=1;j<n;j++)
            map[i][j]=map[j][i]=dis(x[i],y[i],x[j],y[j]); 
    prim();
    }
}
```



#### 最大流

#### 最小割

### 计算几何

### 其他

##### [POJ]**Solution to the** *n*  Queens Puzzle

题目来源：[3239 -- Solution to the n Queens Puzzle (poj.org)](http://poj.org/problem?id=3239)

```c++
/****************************************
  *Author:  陈斌
  *Contact:  
  *Description: 构造法O(1)N皇后特解
*****************************************/
#include<iostream>
using namespace std;
signed main(){
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int m;
    while(cin>>m&&m){
        if(m%6!=2 && m%6!=3){
            if(m%2==0){
                for(int i=2;i<=m;i+=2)cout<<i<<" ";
                for(int i=1;i<=m-1;i+=2)cout<<i<<" ";
                cout<<endl;
            }else{
                for(int i=2;i<=m-1;i+=2)cout<<i<<" ";
                for(int i=1;i<=m-2;i+=2)cout<<i<<" ";
                cout<<m<<" ";
                cout<<endl;
            }
        }else if(m%6==2 || m%6==3){
            if(m%2==0){
                int n=m/2;
                if(n%2==0){
                    for(int i=n;i<=m;i+=2){
                        cout<<i<<" ";
                    }
                    for(int i=2;i<=n-2;i+=2){
                    cout<<i<<" ";
                    }
                    for(int i=n+3;i<=m-1;i+=2){
                    cout<<i<<" ";
                    }
                    for(int i=1;i<=n+1;i+=2){
                    cout<<i<<" ";
                    }
                    cout<<endl;
                }else{
                    for(int i=n;i<=m-1;i+=2){
                        cout<<i<<" ";
                    }
                    for(int i=1;i<=n-2;i+=2){
                        cout<<i<<" ";
                    }
                    for(int i=n+3;i<=m;i+=2){
                        cout<<i<<" ";
                    }
                    for(int i=2;i<=n+1;i+=2){
                        cout<<i<<" ";
                    }
                    cout<<endl;
                }
                
            }else{
                int n=(m-1)/2;
                if(n%2==0){
                    for(int i=n;i<=m-1;i+=2){
                        cout<<i<<" ";
                    }
                    for(int i=1;i<=n-2;i+=2){
                        cout<<i<<" ";
                    }
                    for(int i=n+3;i<=m-2;i+=2){
                        cout<<i<<" ";
                    }
                    for(int i=1;i<=n+1;i+=2){
                        cout<<i<<" ";
                    }
                    cout<<m<<" ";
                    cout<<endl;
                }else{
                    for(int i=n;i<=m-2;i+=2){
                        cout<<i<<" ";
                    }
                    for(int i=1;i<=n-2;i+=2){
                        cout<<i<<" ";
                    }
                    for(int i=n+3;i<=m-1;i+=2){
                        cout<<i<<" ";
                    }
                    for(int i=2;i<=n+1;i+=2){
                        cout<<i<<" ";
                    }
                    cout<<m<<" ";
                    cout<<endl;
                }
            }
        }
    }
}
```



#### 滑动窗口

##### [leetcode]最小重复子串

题目来源:   https://leetcode.cn/problems/minimum-window-substring/

代码:

```c++
/*********************************************************************************
  *Author:  张皓杰
  *Contact:  2049155086@qq.com
  *Description: 这题用滑动窗口思想就非常合适,题目有两个条件:覆盖和最小,那么前提肯定是先覆盖,在此基础
  上不断压缩宽度.
**********************************************************************************/
class Solution {
public:
    string minWindow(string s, string t) {
            string ss;
            int m1=100000;
            int a[128]={0};
            int b[128]={0};
            int n=s.size(),i,ans=0;
            int l=0,r=0;
            int mp=0;
            int m=t.size();
            for(i=0;i<m;i++){
                if(!a[t[i]])mp++;//字符种数
                a[t[i]]++;//每种个数
                b[t[i]]=1;//标记
            }for(;l<n&&r<n;r++){
                if(b[s[r]]){
                    a[s[r]]--;
                    if(!a[s[r]])ans++;
                }if(ans==mp){//覆盖条件满足时滑动求最小长度
                    do{
                        if(b[s[l]])a[s[l]]++;l++;
                    }while(a[s[l-1]]<=0);l--;
                    if(m1>r-l+1){//比较筛选并记录
                        m1=r-l+1;
                        ss=s.substr(l,r-l+1);
                    }ans--;l++;
                }
            }return ss;
    }
};
```



## 算法模板

## 刷题感想

Test：要多刷 要多刷 要多刷
