#	基础算法部分

## 二分查找

### 二分查找详解与模版

~~~c++
//二分查找左闭右开
//参数：查找数组，查找的left和right下标，查找值，返回：查找值的下标，找不到返回-1
//[left,right]
ll search(vector <ll> arr, ll left, ll right, ll target) {
    while (left <= right) {
        ll mid = left + (right - left) / 2;
        if (arr[mid] > target) {
            right = mid - 1;
        }
        else if (arr[mid] < target) {
            left = mid + 1;
        }
        else {
            return mid;
        }
    }
    return -1;
}


//二分查找左闭右开
//[left,right)
ll search(vector <ll> arr, ll left, ll right, ll target) {
    while (left < right) {
        ll mid = left + (right - left) / 2;
        if (arr[mid] > target) {
            right = mid;
        }
        else if (arr[mid] < target) {
            left = mid + 1;
        }
        else {
            return mid;
        }
    }
    return -1;
}
~~~



## 排序算法

### 堆排序

~~~c++
//对一开始乱序数组更新为大根堆形式的数组
void heapInit(vector <int>& arr) {
    for (int i = 0; i < arr.size(); i++) {
        //id:当前子结点，fa：id的父结点
        int id = i;
        int fa = (i - 1) / 2;
        while (arr[id] > arr[fa]) {
            swap(arr[id], arr[fa]);
            id = fa;
            fa = (id - 1) / 2;
        }
    }
}
//从fa开始，长度为lenth的数组arr变为大根堆
void heapOther(vector <int> &arr, int fa, int lenth) {
    int left = fa * 2 + 1;
    int right = fa * 2 + 2;
    //分三种状态，有左右子节点时进行操作，有左子节点的时候进行操作，无左右节点则不操作
    //上述三种状态可以通过判断left是否越界简化为：进行操作，不进行操作
    while (left < lenth) {
        int bigid;
        if (arr[left] < arr[right] && right < lenth) { bigid = right; }
        else { bigid = left; }
        //如果是父级大于左右节点，则说明已经是大根堆，不必操作了，退出循环
        if (arr[fa] > arr[bigid]) { break; }
        swap(arr[bigid], arr[fa]);
        //更新
        fa = bigid;
        left = fa * 2 + 1;
        right = fa * 2 + 2;
    }
}
void heapsort(vector<int>& arr) {
    int lenth = arr.size();
    //先构造一个大根堆(相当于初始化)
    //从下标0到最后一位下标构建大根堆
    heapInit(arr);
    while (lenth > 1) {
        //将首位(最大值)与末尾交换，之后就只用对数组长度lenth-1进行排序
        swap(arr[0], arr[lenth - 1]);
        lenth--;
        //上面交换了就不是大根堆了，我们就将数组更新为大根堆
        heapOther(arr, 0, lenth);
    }
};
~~~



## 字符串算法

### 字符串哈希（Hash）

#### 算法详解与模版

算法详解：[【算法学习】字符串哈希（Hash）_字符串hash-CSDN博客](https://blog.csdn.net/Mikchy/article/details/103995537)

hash，其实就是将一个东西映射成另一个东西，类似Map，key对应value。

那么字符串Hash，其实就是：构造一个数字使之唯一代表一个字符串。但是为了将映射关系进行一一对应，也就是，一个字符串对应一个数字，那么一个数字也对应一个字符串。

用字符串Hash的目的是，我们如果要比较一个字符串，我们不直接比较字符串，而是比较它对应映射的数字，这样子就知道两个“子串”是否相等。从而达到，子串的Hash值的时间为 O(1)，进而可以利用“空间换时间”来节省时间复杂的。



![image-20240517214142826](C:\Users\21145\AppData\Roaming\Typora\typora-user-images\image-20240517214142826.png)



自然溢出hash方法：（开longlong或者无符号longlong）

![image-20240517213544475](C:\Users\21145\AppData\Roaming\Typora\typora-user-images\image-20240517213544475.png)

单hash方法：

![image-20240517213745268](C:\Users\21145\AppData\Roaming\Typora\typora-user-images\image-20240517213745268.png)

双hash方法:

![image-20240517213816638](C:\Users\21145\AppData\Roaming\Typora\typora-user-images\image-20240517213816638.png)

![image-20240517215404195](C:\Users\21145\AppData\Roaming\Typora\typora-user-images\image-20240517215404195.png)

![image-20240517215351434](C:\Users\21145\AppData\Roaming\Typora\typora-user-images\image-20240517215351434.png)

模版

~~~c++
#define ull unsigned long long	//如果用自然溢出longlong不够用就用无符号longlong

//mod和P要为素数，这样重的概率会低
//P可取：29,131,13331
const ll N = 1e6, mod = 1e9 + 7, P = 29;

ll n;
string s;
ll p[N], has[N];      //P的i次方，s[i]的哈希值

//取得字符串子串[l,r]对应的哈希值
ll get_hash(ll l, ll r) { return (has[r] - (has[l - 1] * p[r - l + 1]) % mod + mod) % mod; }

void solve() {
	cin >> s;
	ll n = s.size();
	s = '$' + s;
	p[0] = 1;
	//初始化哈希值
	for (ll i = 1; i <= n; i++) {
		p[i] = (p[i - 1] * P) % mod;
		has[i] = (has[i - 1] * P + s[i]) % mod;
	}
}
~~~

#### 例题：眩晕（码蹄集钻石）

题目链接：

[码题集OJ-3D眩晕 (matiji.net)](https://www.matiji.net/exam/brushquestion/38/4446/16A92C42378232DEB56179D9C70DC45C)

~~~c++
#include<bits/stdc++.h>
#define ll long long
#define PI 3.1415926
#define ull unsigned long long
using namespace std;

//mod和P要为素数，这样重的概率会低
const ll N = 1e6, mod = 1e9 + 7, P = 29;

ll n, m;
string st, sm;
ll p[N], ht[N], hm[N];      //P的i次方，st[i]的哈希值，sm[i]的哈希值

//取得字符串子串[l,r]对应的哈希值
ll get_ht(ll l, ll r) { return (ht[r] - (ht[l - 1] * p[r - l + 1]) % mod + mod) % mod; }
ll get_hm(ll l, ll r) { return (hm[r] - (hm[l - 1] * p[r - l + 1]) % mod + mod) % mod; }

//二分查找字符串，如果有一半的子串哈希值等于模版串那部分的哈希值，则可以跳过节省时间。
//最终返回两个串中不同字符的个数
ll fin(ll l, ll r, ll i) {
	if (l == r) { return sm[l - i] != st[l]; }
	ll mid = l + (r - l) / 2, res = 0;
	if (get_ht(l, mid) != get_hm(l - i, mid - i)) { res += fin(l, mid, i); }
	if (res > 3) { return res; }      //因为题目原因，只要大于3就可以判死刑了，直接返回节省巨多的时间
	if (get_ht(mid + 1, r) != get_hm(mid + 1 - i, r - i)) { res += fin(mid + 1, r, i); }
	return res;
}

void solve() {
	cin >> st >> sm;
	ll n = st.size(), m = sm.size();
	st = '$' + st;
	sm = '&' + sm;
	p[0] = 1;
	//初始化哈希值
	for (ll i = 1; i <= n; i++) {
		p[i] = (p[i - 1] * P) % mod;
		ht[i] = (ht[i - 1] * P + st[i]) % mod;
	}
	for (ll i = 1; i <= m; i++) {
		hm[i] = (hm[i - 1] * P + sm[i]) % mod;
	}
	ll ans = 0;
	//滑动窗口
	for (ll i = 1; i <= n - m + 1; i++) {
		ans += (fin(i, i + m - 1, i - 1) <= 3);
	}
	cout << ans;
}

int main() {
	ll T = 1;
	cin >> T;
	while (T--) {
		solve();
		cout << endl;

	}
	return 0;

}
~~~

#### 判断回文模版

~~~c++
class Solution {
public:
    static const int mod=998244353,P=13331;
    int f[2025],n;
    long long p[2025],has1[2025],has2[2025];  
    int get1(int l, int r) { return (has1[r] - (has1[l - 1] * p[r - l + 1]) % mod + mod) % mod; }
    int get2(int l, int r) { return (has2[l] - (has2[r + 1] * p[r - l + 1]) % mod + mod) % mod; }
    //判断[l,r]是否为回文串，O(1);
    bool check(int l,int r){
        int l1,r1,l2,r2,mid=(l+r)>>1;
        if((r-l+1)&1){l1=l,r1=mid-1,l2=mid+1,r2=r;}
        else{l1=l,r1=mid,l2=mid+1,r2=r;}
        return get1(l1,r1)==get2(l2,r2);
    }
    bool checkPartitioning(string s) {
        int n=s.size();
        s='*'+s;
        p[0] = 1;
        for (int i = 1; i <= n; i++) {
            p[i] = (p[i - 1] * P) % mod;
            has1[i] = (has1[i - 1] * P + s[i]) % mod;
        }
        for (int i = n; i >= 1; i--) {
            has2[i] = (has2[i + 1] * P + s[i]) % mod;
        }
        for(int i=2;i<=n-1;i++){
            for(int j=i;j<=n-1;j++){
                if(check(1,i-1) && check(i,j) && check(j+1,n)){
                    return 1;
                }
            }
        }
        return 0;
    }
};
~~~



#### 例题：Reverse the String

题目链接：[Problem - H - Codeforces](https://codeforces.com/gym/103495/problem/H)

~~~c++
#include <bits/stdc++.h>
#define ll long long

using namespace std;

const ll N=2e5+10,mod=1e9+7,P=131;

ll n;
string s;

ll p[N],h1[N],h2[N],cnt[N];

void Init(){
    memset(h1, 0, sizeof h1);
    memset(h2, 0, sizeof h2);
    memset(cnt,0,sizeof cnt);
}

ll get1(ll l, ll r) {
    return (h1[r] - h1[l - 1] * p[r - l + 1] % mod + mod) % mod;
}
 
ll get2(ll l, ll r) {
    return (h2[l] - h2[r + 1] * p[r - l + 1] % mod + mod) % mod;
}


ll check(ll L,ll r1,ll r2){
	if(get1(L,r1) != get1(r2-(r1-L),r2)){
		ll l=0,r=(r1-L);
		while(l<=r){
			ll mid=(l+r)>>1;
			if(get2(r1-mid,r1)!=get2(r2-mid,r2)){r=mid-1;}
			else{l=mid+1;}
		}
		return s[(r1-l)]<s[r2-l];
	}
	else{
		ll l=0,r=(r2-r1-1);
		while(l<=r){
			ll mid=(l+r)>>1;
			if(get1(r1+1,r1+1+mid)!=get2(r2-(r1-L+1)-mid,r2-(r1-L+1))){
				r=mid-1;
			}
			else{
				l=mid+1;
			}
		}
		return s[(r1+1+l)]<s[r2-(r1-L+1)-l];
	}
}

void solve() {
	Init();
	cin >> s;
	n=s.size();
	s='*'+s;
	for (ll i = 1; i <= n; i++) {
        h1[i] = (h1[i - 1] * P + s[i]) % mod;
        cnt[s[i] - 'a']++;
    }
 
    for (ll i = n; i >= 1; i--) {
        h2[i] = (h2[i + 1] * P + s[i]) % mod;
    }
 
    ll l = -1, w = -1;
    bool f = 1;
    for (ll i = 1; i <= n; i++) {
        ll k = s[i] - 'a';
        cnt[k]--;
        for (ll j = 0; j < k; j++) {
 
            if (cnt[j]) {
                l = i;
                w = j;
                f = 0;
                break;
            }
        }
        if (!f)break;
    }
 
    if (l == -1) {
        for(ll i=1;i<=n;i++){cout << s[i];}
        return;
    }
    ll r = -1;
    for (ll i = n; i > l; i--) {
        if (s[i] - 'a' == w) {
            if (r == -1 || check(l, i, r)) {
                r = i;
            }
        }
    }
	for (ll i = 1; i <= l - 1; i++)cout << s[i];
    for (ll i = r; i >= l; i--)cout << s[i];
    for (ll i = r + 1; i <= n; i++)cout << s[i];
}


int main() {
	ios::sync_with_stdio(0);
	cin.tie(0), cout.tie(0);
	p[0] = 1;
    for (ll i = 1; i < N; i++){p[i] = p[i - 1] * P % mod;}
	ll T = 1;
	cin >> T;
	while (T--) {
		solve();
		cout << '\n';
	}
	return 0;
}
~~~



### KMP匹配字符串算法

#### KMP详解与模版

代码详解：[[KMP\][C++]KMP模板 - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/504380448)

next数组作用详解：[【KMP】从原理上详解next数组和nextval数组-CSDN博客](https://blog.csdn.net/coding_with_smile/article/details/125521122)

下面代码的next数组记录：**记录的是到对应位为止的最长相等前后缀的长度**

next还有其他方法和写法请看详解

~~~c++
//注意：模版里的next数组初始化都写在了函数里，视情况而定，如果一个字符串匹配一大堆字符串要匹配好几次的时候可以提前将next初始化节省时间。

//初始化next数组，a文本串，b匹配串
vector<ll> getNext(string b) {
    ll j = 0,m=b.size();
    vector<ll>kmp_next(m+5);
    kmp_next[0] = 0;
    for (ll i = 1; i < m; i++) {
        //当这一位不匹配时，将j指向此位之前最大公共前后缀的位置
        while (j > 0 && b[i] != b[j]) { j = kmp_next[j - 1]; }
        //如果这一位匹配，j++，继续匹配下一位
        if (b[i] == b[j]) { j++; }
        kmp_next[i] = j;    //更新next[i]的值
    }
    return kmp_next;
}

//kmp寻找位置,在a里找b，返回第一个找到的下标，若没有则返回-1
ll kmp_findpos(string a, string b) {
    ll i, j = 0, ans = -1;  //如果找不到就-1
    ll n = a.size(), m = b.size();
    vector<ll>kmp_next = getNext(b);    //初始化next数组(可以提前处理写在外面)
    for (i = 0; i < n; i++) {
        while (j > 0 && b[j] != a[i]) { j = kmp_next[j - 1]; }
        if (b[j] == a[i]) { j++; }
        //如果子串完全匹配，则进行题目所需要的相关操作
        if (j == m) {
            ans = i - m + 1;
            break;
        }
    }
    return ans;
}

//kmp匹配次数,b在a中出现的次数
ll kmp_cnt(string a, string b) {
    ll i, j = 0, ans = 0;
    ll n = a.size(), m = b.size();
    vector<ll>kmp_next = getNext(b);
    for (i = 0; i < n; i++) {
        while (j > 0 && b[j] != a[i]) { j = kmp_next[j - 1]; }
        if (b[j] == a[i]) { j++; }
        if (j == m) {
            ans++;
        }
    }
    return ans;
}

//kmp寻找所有位置，在a里找所有b，返回存多个下标的数组
vector<ll> kmp_findall(string a, string b) {
    ll i, j = 0;
    ll n = a.size(), m = b.size();
    vector<ll>ans;
    vector<ll>kmp_next = getNext(b);
    for (i = 0; i < n; i++) {
        while (j > 0 && b[j] != a[i]) { j = kmp_next[j - 1]; }
        if (b[j] == a[i]) { j++; }
        if (j == m) {
            ans.push_back(i - m + 1);
        }
    }
    return ans;
}
~~~



### Manacher（马拉车）算法

一种专门解决回文串的算法

#### Manacher详解与模版

视频详解：[马拉车算法 | Coding Club_哔哩哔哩_bilibili](https://www.bilibili.com/video/BV1Sx4y1k7jG/?spm_id_from=333.337.search-card.all.click&vd_source=c4cdf942e0eb96f40a89387e745c7015)

代码详解：[5. 最长回文子串 - 力扣（LeetCode）](https://leetcode.cn/problems/longest-palindromic-substring/)

~~~c++
//马拉车
//返回字符串s中最长的回文子串
//时间:O(n)
string Manacher(string s) {
    ll n = s.size();
    string ns = "#";
    for (ll i = 0; i < n; i++) {
        ns += s[i];
        ns += '#';
    }
    ll m = ns.size();
    vector<ll>p(m);     //记录下标i时最大扩展
    ll left = 0, right = -1;    //记录最大回文串的左右下标
    ll c = 0, r = 0;    //最大蘑菇的中心c和最右侧达到的位置r
    for (ll i = 1; i < m-1; i++) {
        if (i <= r) { p[i] = min(r - i, p[2 * c - i]); }    //开始扩展时通过最大蘑菇得到目前起码可以扩展长度
        while (i - p[i] - 1 >=0 && i + p[i] + 1<m && ns[i - p[i] - 1] == ns[i + p[i] + 1]) { p[i]++; }  //扩展
        //更新最大蘑菇
        if (p[i] + i > r) {
            r = p[i] + i;
            c = i;
        }
        if (p[i] * 2 + 1 > right - left) {
            left = i - p[i];
            right = i + p[i];
        }
    }
    string ans;
    for (ll i = left; i <= right; i++) {
        if (ns[i] == '#') { continue; }
        ans += ns[i];
    }
    return ans;
}
~~~



### 字典树

#### 字典树模版

视频链接：[F06 字典树(Trie)_哔哩哔哩_bilibili](https://www.bilibili.com/video/BV1yA4y1Z74t/?spm_id_from=333.337.search-card.all.click&vd_source=c4cdf942e0eb96f40a89387e745c7015)

图文链接：[【数据结构】字典树TrieTree图文详解-CSDN博客](https://blog.csdn.net/qq_49688477/article/details/118879270)

~~~c++
const ll N=3e6+5;

ll trie[N][26],cnt[N],id;
//添加字符串
void insert(string s){
	ll p=0;
	for(ll i=0;i<s.size();i++){
		ll j=s[i]-'a';
		if(!trie[p][j]){trie[p][j]=++id;}
		p=trie[p][j];
	}
	cnt[p]++;
}
//查找字符串
ll find_s(string s){
	ll p=0;
	for(ll i=0;i<s.size();i++){
		ll j=s[i]-'a';
		if(!trie[p][j]){return 0;}
		p=trie[p][j];
	}
	return cnt[p];
}
//巧妙初始化
void Init(){
	for(ll i=0;i<=id;i++){
		for(ll j=0;j<26;j++){
			trie[i][j]=0;
		}
	} 
	for(ll i=0;i<=id;i++){cnt[i]=0;}
	id=0;
}
~~~





#### 字典树模版题（洛谷）

题目链接：[P8306 【模板】字典树 - 洛谷 | 计算机科学教育新生态 (luogu.com.cn)](https://www.luogu.com.cn/problem/P8306)

~~~c++
#include<bits/stdc++.h>
#define ll long long
#define PI 3.1415926

using namespace std;

const ll N = 3e6 + 5, INF = 1e18, mod = 1e9 + 7;

//10+26+26=62;		//题目只有数字，小写字母 
ll n,q;

//ch[i][j]：字典树,从i编号节点往j字符走所形成的字符串的节点编号 
//cnt[i]：存以这个编号节点为结尾所形成的字符串个数 
//id: 为字符串编号 
ll ch[N][65],cnt[N],id=0;

//将字符转映射值 
ll ctoi(char c){
	if(c>='a' && c<='z'){
		return c-'a'+26 + 10;
	}
	else if(c>='A' && c<='Z'){
		return c-'A'+10;
	}
	else{
		return c-'0';
	}
}

//往字典树里添加字符串 
void insert(string s){
	ll p=0;								//初始时统一在0节点 
	for(ll i=0;i<s.size();i++){			//遍历字符串 
		ll j=ctoi(s[i]);				//获得字符的映射 
		if(!ch[p][j]){ch[p][j]=++id;}	//如果没有就建立 
		p=ch[p][j];						//走过来 
		cnt[p]++;						//这个子串个数加一 
	}
}

//在字典树中找字符串，返回改字符串在字典树中的个数 
ll find_s(string s){
	ll p=0;
	for(ll i=0;i<s.size();i++){
		ll j=ctoi(s[i]);
		if(!ch[p][j]){return 0;}		//如果找不到说明没有，直接返回0 
		p=ch[p][j];	
	}
	return cnt[p];						//否则返回该字符串的个数 
}

//这个初始化很巧妙
void Init(){
	//因为建立了多少节点是id管理的，所以id存的就是上一次建树所用的节点，为了节省时间，直接用id初始化，再将id归0 
	for(ll i=0;i<=id;i++){
		for(ll j=0;j<65;j++){
			ch[i][j]=0;
		}
	}
	for(ll i=0;i<=id;i++){cnt[i]=0;} 
	id=0;
} 


void solve() {
	Init();
	string s;
	cin >> n >> q;
	for(ll i=0;i<n;i++){
		cin >> s;
		insert(s);
	}
	while(q--){
		cin >> s;
		cout << find_s(s) <<endl;
	}
}

int main() {
	ll T = 1;
	cin >> T;
	while (T--) {
		solve();
		//cout << endl;
	}
	return 0;

}
~~~







## 前后缀与拆分

### 前后缀模版

~~~c++
//前缀和(返回的数组长度为n+1)
//区间[L,R]的和公式为：sum[R]-sum[L-1]; (1<=L<=R<=n)
vector<ll> PrefixSum(vector<ll> a) {
    ll n = a.size();
    vector<ll>ans(n+1);
    for (ll i = 1; i <= n; i++) {
        ans[i] = a[i - 1] + ans[i - 1];
    }
    return ans;
}

//后缀和(返回的数组长度为n+1)
//区间[L,R]的和公式为：sum[L-1]-sum[R]; (1<=L<=R<=n)
vector <ll> SuffixSum(vector<ll> a) {
    ll n = a.size();
    vector<ll>ans(n + 1);
    for (ll i = n-1; i >=0; i--) {
        ans[i] = a[i] + ans[i + 1];
    }
    return ans;
}
~~~



## 分块

### 分块实现区查区改（区间和）

~~~c++
#include<bits/stdc++.h>
#define ll long long

using namespace std;

const ll N = 55;

//block:每块有block个元素
//t:一共分为了t块
//st[i]:第i个块左边界的元素下标
//ed[i]:第i个块右边界的元素下标
//sum[i]:第i个块的元素总和
//add[i]:第i个块的增量标记 
ll block, t, st[N], ed[N], pos[N], sum[N], add[N];
ll a[N], n;		//原数组 

//分块初始化 
void Init_block(ll n) {
	block = sqrt(n);
	t = n / block;
	if (n % block) { t++; }
	for (ll i = 1; i <= t; i++) {
		st[i] = (i - 1) * block + 1;
		ed[i] = i * block;
	}
	ed[t] = n;
	for (ll i = 1; i <= n; i++) {
		pos[i] = (i - 1) / block + 1;
	}

	for (ll i = 1; i <= t; i++) {
		for (ll j = st[i]; j <= ed[i]; j++) {
			sum[i] += a[j];
		}
	}
}

//区间修改，[l,r]增加d 
void change(ll l, ll r, ll d) {
	ll p = pos[l], q = pos[r];
	if (p == q) {
		for (ll i = l; i <= r; i++) { a[i] += d; }
		sum[p] += d * (r - l + 1);
	}
	else {
		for (ll i = p + 1; i <= q - 1; i++) { add[i] += d; }
		for (ll i = l; i <= ed[p]; i++) { a[i] += d; }
		sum[p] += d * (ed[p] - l + 1);
		for (ll i = st[q]; i <= r; i++) { a[i] += d; }
		sum[q] += d * (r - st[q] + 1);
	}
}

//查询区间,[l,r]的总和 
ll ask(ll l, ll r) {
	ll p = pos[l], q = pos[r];
	ll ans = 0;
	if (p == q) {
		for (ll i = l; i <= r; i++) { ans += a[i]; }
		ans += add[p] * (r - l + 1);
	}
	else {
		for (ll i = p + 1; i <= q - 1; i++) { ans += sum[i] + add[i] * (ed[i] - st[i] + 1); }
		for (ll i = l; i <= ed[p]; i++) { ans += a[i]; }
		ans += add[p] * (ed[p] - l + 1);
		for (ll i = st[q]; i <= r; i++) { ans += a[i]; }
		ans += add[q] * (r - st[q] + 1);
	}
	return ans;
}

void solve() {
	cin >> n;
	for (ll i = 1; i <= n; i++) {
		cin >> a[i];
	}
	Init_block(n);
	ll m; cin >> m;
	while (m--) {
		ll l, r, d;
		cin >> l >> r >> d;
		change(l, r, d);
		cin >> l >> r;
		cout << ask(l, r) << endl;
	}

}

int main() {
	ll T = 1;
	cin >> T;
	while (T--) {
		solve();
		cout << endl;
	}
	return 0;

}
~~~

### 莫队分块模版

~~~c++
#include <bits/stdc++.h>
#define ll long long

using namespace std;

const ll N = 1e6 + 3, mod = 998244353, INF = 0x3f3f3f3f, eps = 1e-7;

//block:记录块数
//pos每个元素所在的块
//cnt存每个元素出现次数
//res记录[l,r]元素种类个数
ll n,m,block;
ll a[N],pos[N],ans[N],cnt[N],res;

//存查询块
struct node {
	ll l, r, id;
}q[N];

bool cmp(node x, node y) {
	//左端点按块排序
	if (pos[x.l] != pos[y.l]) {
		return pos[x.l] < pos[y.l];
	}
	//奇偶性优化，加上性能更好一些
	if (pos[x.l] & 1) {return x.r > y.r;}
	return x.r < y.r;
}

//缩小区间
void del(ll x) {
	cnt[a[x]]--;
	if (cnt[a[x]] == 0) { res--; }
}

//扩大区间
void add(ll x) {
	if (cnt[a[x]] == 0) { res++; }
	cnt[a[x]]++;
}

void solve() {
	cin >> n;
	block = sqrt(n);
	for (ll i = 1; i <= n; i++) { 
		cin >> a[i];
		//第i个元素所在的块
		pos[i] = (i - 1) / block + 1;
	}
	cin >> m;
	//离线处理,id记录原位置
	for (ll i = 1; i <= m; i++) {
		cin >> q[i].l >> q[i].r;
		q[i].id = i;
	}
	//对于所有查询进行排序
	sort(q + 1, q + 1 + m, cmp);
	ll l = 1, r = 0;
	for (ll i = 1; i <= m; i++) {
		while (l < q[i].l) { del(l++); }
		while (r > q[i].r) { del(r--); }
		while (l > q[i].l) { add(--l); }
		while (r < q[i].r) { add(++r); }
		ans[q[i].id] = res;
	}
	for (ll i = 1; i <= m; i++) {
		cout << ans[i] << endl;
	}


}

int main() {
	ios::sync_with_stdio(0); cin.tie(NULL); cout.tie(NULL);
	ll T = 1;
	//cin >> T;
	while (T--) {
		solve();
		//cout << endl;
	}
	return 0;
}
~~~



## 堆顶堆（双STL结构）

### Rainbow Subarray

题目链接：[Problem - K - Codeforces](https://codeforces.com/gym/104901/problem/K)

~~~c++
#include <bits/stdc++.h>
#define ll long long

using namespace std;

const ll N = 1e6 + 10, mod = 1e9 + 7, P = 13331;

ll n, k, sum1, sum2;
ll a[N];
//multiset(排序但不去重)
multiset<ll>st1;				//存大于等于中位数的
multiset<ll, greater<ll>>st2;	//存小于等于中位数的 

//删除两个堆中其中一个x元素
void del(ll x) {
	if (st1.count(x)) {
		st1.erase(st1.find(x));	//注意find这么写，find(st1.begin(),st1.end(),x)会超时，推测这种写法复杂度为On
		sum1 -= x;
	}
	else {
		st2.erase(st2.find(x));
		sum2 -= x;
	}
}

//调整两个堆
void tz() {
	//如果st1比st2大2，就把st1中的最小值给st2
	if (st1.size()>st2.size() && st1.size()-st2.size()>=2) {
		ll t = *st1.begin();
		st1.erase(st1.begin());
		st2.insert(t);
		sum1 -= t;
		sum2 += t;
	}
	//如果st2比st1大，就把st2中的最大值给st1
	else if (st2.size()>st1.size()) {
		ll t = *st2.begin();
		st2.erase(st2.begin());
		st1.insert(t);
		sum2 -= t;
		sum1 += t;
	}
}


void solve() {
	sum1 = sum2 = 0;
	st1.clear(), st2.clear();
	cin >> n >> k;
	for (ll i = 1; i <= n; i++) {
		cin >> a[i];
		a[i] -= i;
	}
	ll l = 1, ans = 0;
	for (ll r = 1; r <= n; r++) {
		//如果当前数大于等于st1或者st1没东西，就塞进去 
		if (!st1.size() || a[r] >= *st1.begin()) {
			st1.insert(a[r]);
			sum1 += a[r];
		}
		else {
			st2.insert(a[r]);
			sum2 += a[r];
		}
		tz();
		//如果区间花费大于k就缩左指针 
		while (l<r && (*st1.begin()) * (st2.size() - st1.size()) + sum1 - sum2 > k) {
			del(a[l]);
			l++;
			tz();
		}
		ans = max(ans, r - l + 1);
	}
	cout << ans;
}


int main() {
	ios::sync_with_stdio(0);
	cin.tie(0), cout.tie(0);
	ll T = 1;
	cin >> T;
	while (T--) {
		solve();
		cout << '\n';
	}
	return 0;
}
~~~



## 二维偏序

### **Diagonal Separation**

题目链接：[D - Diagonal Separation](https://atcoder.jp/contests/abc386/tasks/abc386_d)

~~~c++
#include <bits/stdc++.h>
#define ll long long
#define lowbit(x) x&-x
using namespace std;

const ll N = 2e6 + 10, mod = 1e9 + 7;

ll tree[N], n, m;
void add(ll x, ll d) {
	while (x < N) {
		tree[x] += d;
		x += lowbit(x);
	}
}

ll query(ll x) {
	ll ans = 0;
	while (x > 0) {
		ans += tree[x];
		x -= lowbit(x);
	}
	return ans;
}

ll sum(ll l, ll r) {
	return query(r) - query(l - 1);
}

vector<ll>a;					//存离散化
vector<pair<ll, ll>>mpw, mpb;	//黑白坐标

//离散找rank，从1开始
ll get(ll x) {
	return lower_bound(a.begin(), a.end(), x) - a.begin() + 1;
}

void solve() {
	cin >> n >> m;
	for (ll i = 1; i <= m; i++) {
		ll x, y; char c;
		cin >> x >> y >> c;
		if (c == 'W') { mpw.push_back({ x,y }); }
		else { mpb.push_back({ x,y }); }
		a.push_back(x);
		a.push_back(y);
	}
	sort(a.begin(), a.end());
	a.erase(unique(a.begin(), a.end()), a.end());
	sort(mpw.begin(), mpw.end());
	sort(mpb.begin(), mpb.end());

	//题意需要满足write_x<=black_y && write_y<=black_y;
	//以write_x大到小枚举每一个write_x,将black中black_x比write_x大的所有black_y添加进树状数组
	//此时查询区间[wirte_y,无穷大],如果总和有值，说明有write_x<=black_y && write_y<=black_y
	ll r = mpb.size()-1;
	for (ll i = mpw.size()-1; i >=0; i--) {
		ll x = get(mpw[i].first), y = get(mpw[i].second);
		while (r>=0 && get(mpb[r].first) >= x) {
			add(get(mpb[r].second), 1);
			r--;
		}
		if (sum(y,N-1) > 0) {
			cout << "No";
			return;
		}
	}
	cout << "Yes";
}

int main() {
	ios::sync_with_stdio(0);
	cin.tie(0), cout.tie(0);
	ll T = 1;
	//cin >> T;
	while (T--) {
		solve();
		//cout << '\n';
	}
	return 0;
}
~~~







# 图算法部分

## 建图算法

### 链式前向星

视频详解：[图论003链式前向星_哔哩哔哩_bilibili](https://www.bilibili.com/video/BV1Rr4y1871K/?spm_id_from=333.337.search-card.all.click&vd_source=c4cdf942e0eb96f40a89387e745c7015)

链式前向星详解与模版

~~~c++
/*
u v dis
1 2 18
2 3 12
2 4 20
1 5 2

边的起点：{边的终点，边的权值，边的编号}
1: {5,2,4}->{2,18,1}
2: {4,20,3}->{3,12,2}
3: NULL
4: NULL
5: NULL

edge[{2,18，-1},{3,12，-1},{4,20,2},{5,2,1}，……]	//{to,w,next} {边终点，边权值，下一个边的编号}

*/
~~~

算法模版

~~~c++
const ll N = 2e5+10;	//边的最大数量

ll head[N];
ll id = 0;
struct Edge {
    ll to, w, next; // to代表边的终点，w代表边的权值，next下面一个边
}edge[2*N];	//如果单向就N，双向2*N

//加边,u->v
void add_edge(int u, int v, int w) {
    edge[id].to = v;
    edge[id].w = w;
    edge[id].next = head[u];
    head[u] = id;
    id++;
}

//遍历一个节点（x）所有的边
//while写法
ll u = head[x];
while (u != -1) {
    ll to = edge[u].to;
    ll w = edge[u].w;
    //……(相应操作)
    u = edge[u].next;
}
//for写法
for(ll u=head[x];u!=-1;u=edge[u].next){
    ll to = edge[u].to;
    ll w = edge[u].w;
    //……(相应操作)
}
~~~

## 最短路径算法

### Dijkstra算法

视频详解：[04. Dijkstra 算法 寻找有权图中最短路 Finding Shortest Path in Weighted Graphs_哔哩哔哩_bilibili](https://www.bilibili.com/video/BV1hc41187JZ?p=4&vd_source=c4cdf942e0eb96f40a89387e745c7015)

#### dijkstra算法基础与模版

![image-20240127163455885](C:\Users\21145\AppData\Roaming\Typora\typora-user-images\image-20240127163455885.png)

算法模版

~~~c++
#include<bits/stdc++.h>
#include<unordered_map>
#include <unordered_set>
#include <deque>
#include<algorithm>
#define ll long long
using namespace std;

const ll mod = 1e9 + 7,N=1e5+5,INF=0x3f3f3f3f;
ll n, m, s; //n点m线，s起点

ll head[N];
ll id = 0;
struct Edge{
    ll to, w, next;
}edge[300100];    //多少边就开多大，如果双向就开2倍

//加边：u->v,权重w
void add_edge(ll u, ll v, ll w) {
    edge[id].to = v;
    edge[id].w = w;
    edge[id].next = head[u];
    head[u] = id;
    id++;
}

ll dist[N],vist[N]; //dist[i]起点到i点的最短距离,vist记忆化

void Init() {
    memset(dist, INF, sizeof(dist));
    memset(vist, false, sizeof(vist));
}

//点s到各个点的最短距离
void dijkstra(ll s)
{
    Init();
    //图的遍历
    //注意，优先队列默认大根堆，为了解决题目，要转换成小跟堆，下面是转小根堆方法
    priority_queue<pair<ll,ll>, vector<pair<ll,ll>>, greater<pair<ll,ll>>> heap;
    heap.push({ 0, s });//first表示距离，second表示结点
    dist[s] = 0;
    while (heap.size())
    {
        pair<ll,ll> t = heap.top();
        heap.pop();
        ll u = t.second, d = t.first;
        if (vist[u] == 1) continue;	//记忆化
        vist[u] = 1;
        //a->b, 已知a的距离和边权，去更新b的距离
        for (ll i = head[u]; i != -1; i = edge[i].next)
        {
            ll v = edge[i].to;ll w = edge[i].w;
            if (dist[v] > d + w)
            {
                dist[v] = d + w;
                heap.push({ dist[v], v });
            }
        }
    }
}

void solve() {
    memset(head, -1, sizeof(head));
    cin >> n >> m >> s;
    for (ll i = 0; i < m; i++) {
        ll u, v, w;
        cin >> u >> v >> w;
        add_edge(u, v, w);
    }
    dijkstra(s);
    for (ll i = 1; i <= n; i++) {
        cout << dist[i] << " ";
    }
}

int main() {
    ll T = 1;
    //cin >> T;
    while (T--) {
        solve();
        cout << endl;
    }
    return 0;

}
~~~

#### (dij)PTA变式例题

题目链接：[L3-1 直捣黄龙 - 天梯赛备赛2024第二场 (pintia.cn)](https://pintia.cn/problem-sets/1764287810636337152/exam/problems/1764287847219056652?type=7&page=0)

~~~c++
#include<bits/stdc++.h>
#include<unordered_map>
#include <unordered_set>
#include <deque>
#include<algorithm>
#define ll long long

using namespace std;

const ll INF = 0x3f3f3f3f;

ll n, k;
string be, en;         //起始城镇和终点城镇
map<string, ll>stl;    //(string to long)每个城镇对应一个编号
map<ll, string>lts;    //(long to string)每个编号对应一个城市
map<string, ll>nums;   //城镇对应敌人数量
ll edge[205][205];     //城镇之间的距离

map<ll, ll>path;       //记录u->v最终路线，知v得u
ll citycnt[205],enemycnt[205],road[205];    //城市数量，敌人数量，有几种途径数量

ll dist[205],vist[205];     //最短路，记忆化
void Init() {
    for (ll i = 0; i < 205; i++) { dist[i] = INF, road[i] = 1; }
}

void dij(ll s) {
    Init();
    priority_queue<pair<ll, ll>, vector<pair<ll, ll>>, greater<pair<ll, ll>>> heap;
    heap.push({ 0,s });
    dist[s] = 0;
    path[s] = -1;   //起点的前驱是无
    while (!heap.empty()) {
        pair<ll, ll>t = heap.top();
        heap.pop();
        ll u = t.second, d = t.first;
        if (vist[u]) { continue; }
        vist[u] = 1;
        for (ll v = 0; v < 205; v++) {
            if (edge[u][v] == -1) { continue; }
            ll w = edge[u][v];
            if (dist[v] > d + w) {
                dist[v] = d + w;
                path[v] = u;
                citycnt[v] = citycnt[u] + 1;
                enemycnt[v] = enemycnt[u] + nums[lts[v]];
                road[v] = road[u];
                heap.push({ dist[v],v });
            }
            //如果最短路一样
            else if (dist[v] == d + w) {
                road[v] += road[u];
                //则比较途经城市数量，如果此时的u->v能让途径的城市更多就更新
                if (citycnt[u]+1 > citycnt[v]) {
                    path[v] = u;
                    citycnt[v] = citycnt[u] + 1;
                    enemycnt[v] = enemycnt[u] + nums[lts[v]];
                }
                //如果途经城市数量都一样
                else if (citycnt[u] + 1 == citycnt[v]) {
                    //则比较杀敌数，如果此时u->v能杀敌更多，就更新
                    if (enemycnt[u] + nums[lts[v]] > enemycnt[v]) {
                        path[v] = u;
                        enemycnt[v] = enemycnt[u] + nums[lts[v]];
                    }
                }   
            }
        }
    }

}

//排序规则：1.最快  2.最多城镇  3.杀敌最多
void solve() {
    memset(edge, -1, sizeof(edge));
    cin >> n >> k >> be >> en;
    string temp_s; ll temp_l;
    set<string>st;  //后面发现城市与城市建距离时会加入新的城市，所以用set去重并更新编号和城镇的信息
    for(ll i=0;i<n-1;i++){
        cin >> temp_s >> temp_l;
        nums[temp_s] = temp_l;
        st.insert(temp_s);
    }

    //对城市进行编号，同时编号也对应城市
    ll id = 0;
    for (auto p : nums) {
        stl[p.first] = id;
        lts[id] = p.first;
        id++;
    }

    string tu, tv; ll w;
    for (ll i = 0; i < k; i++) {
        cin >> tu >> tv >> w;
        if (!st.count(tu)) { st.insert(tu); stl[tu] = id; lts[id] = tu; id++; }
        if (!st.count(tv)) { st.insert(tv); stl[tv] = id; lts[id] = tv; id++; }
        ll u = stl[tu];
        ll v = stl[tv];
        edge[u][v] = w;
        edge[v][u] = w;
    }

    ll be_ll = stl[be], en_ll = stl[en];
    dij(be_ll);

    //找路
    vector<string> ans_path;
    ll pre = path[en_ll];
    ans_path.push_back(en);
    //直到走到起点，起点的前驱是-1
    while (pre != -1) {
        ans_path.push_back(lts[pre]);
        pre = path[pre];
    }
    reverse(ans_path.begin(), ans_path.end());

    ll flag = 0;
    for (string str : ans_path) {
        if (flag) { cout << "->"; }
        cout << str;
        flag = 1;
    }
    cout << endl << road[en_ll] << " " << dist[en_ll] << " " << enemycnt[en_ll];
}


int main() {
    ll T = 1;
    //cin >> T;
    while (T--) {
        solve();
        cout << endl;
    }
    return 0;

}
~~~





### Floyd算法

#### Floyd算法基础与模版

视频讲解：[图-最短路径-Floyd(弗洛伊德)算法_哔哩哔哩_bilibili](https://www.bilibili.com/video/BV19k4y1Q7Gj/?spm_id_from=333.337.search-card.all.click&vd_source=c4cdf942e0eb96f40a89387e745c7015)

代码详解：[C/C++ 最短路径-Floyd算法 (路径的保存和输出)_弗洛伊德算法怎么打印最短两点之间最短路径的点-CSDN博客](https://blog.csdn.net/Carizy/article/details/107248085)

~~~c++
#include<bits/stdc++.h>
#include<unordered_map>
#include <unordered_set>
#include <deque>
#include<algorithm>
#define ll long long
using namespace std;

const ll N = 5050,INF=0x3f3f3f3f,mod= 998244354;
ll n, m;  //n点m边

ll graph[N][N]; //图
ll dist[N][N], path[N][N];  ////dist[i][j]表示从i->j的最短距离,path[i][j]记录从i->j，j的前面的一个点

//时间复杂度为(n^3)n为节点数
void Floyd() {
    //初始化
    for (ll i = 0; i <= n; i++) {
        for (ll j = 0; j <= n; j++) {
            dist[i][j] = graph[i][j];
            if (dist[i][j] != INF && i != j) { path[i][j] = i; }
            else { path[i][j] = -1; }
        }
    }
    //i->k->j,Floyd核心部分
    for (ll k = 0; k <= n; k++) {
        for (ll i = 0; i <= n; i++) {
            for (ll j = 0; j <= n; j++) {
                if (dist[i][k] + dist[k][j] < dist[i][j]) {
                    dist[i][j] = dist[i][k] + dist[k][j];
                    path[i][j] = k;
                }
            }
        }
    }
}

//寻找x->y的最短路径途径
void findpath(ll x,ll y) {
    if (path[x][y] == x) { return; }
    ll t = path[x][y];
    cout << "->" << t;
    findpath(t, y);
    return;
}

void Init() {
    for (ll i = 0; i <= n; i++) {
        for (ll j = 0; j <= n; j++) {
            if (i == j) { graph[i][j] = 0; continue; }
            graph[i][j] = INF;
        }
    }
}

void solve() {
    cin >> n >> m;
    //建图
    Init();
    for (ll i = 0; i < m; i++) {
        ll u, v, w;
        cin >> u >> v >> w;
        graph[u][v] = min(w,graph[u][v]);
        //graph[u][v] = min(w, graph[u][v]);//无向(加min防止题目当老六，可去)
    }
    //求最短
    Floyd();
    
    //进行题意要求的相应操作
    ll to, from;
    cin >> to >> from;
    ll mindis = dist[to][from];
    cout << to << "到" << from << "的最短距离为：" << mindis << endl;
    cout << "路径为：";
    cout << to;
    findpath(to,from);
    cout << "->" << from;
}

int main() {
    ll T = 1;
    //cin >> T;
    while (T--) {
        solve();
        cout << endl;
    }
    return 0;
}


~~~

### Spfa算法

#### Spfa算法基础与模版

视频详解：[SPFA算法_哔哩哔哩_bilibili](https://www.bilibili.com/video/BV1RK4y1d7ct?p=4&vd_source=c4cdf942e0eb96f40a89387e745c7015)

代码详解：[SPFA 模板_c++ sfpa 模板-CSDN博客](https://blog.csdn.net/sinat_35866463/article/details/78344698)

~~~c++
#include<bits/stdc++.h>
#include<unordered_map>
#include <unordered_set>
#include <deque>
#include<algorithm>
#define ll long long
using namespace std;

const ll mod = 1e9 + 7,N=1e5+5,INF=0x3f3f3f3f;
ll n, m, s; //n点m线，s起点

ll head[N];
ll id = 0;
struct Edge{
    ll to, w, next;
}edge[300100];    //多少边就开多大，如果双向就开2倍

//加边：u->v,权重w
void add_edge(ll u, ll v, ll w) {
    edge[id].to = v;
    edge[id].w = w;
    edge[id].next = head[u];
    head[u] = id;
    id++;
}

ll dist[N],vist[N]; //dist[i]起点到i点的最短距离,vist[i]判断i点目前是否在队列里

void Init() {
    memset(dist, INF, sizeof(dist));
    memset(vist, false, sizeof(vist));
}

//点s到其他点的最短距离
void spfa(ll s) {
    Init();
    dist[s] = 0;
    queue<ll>q;
    q.push(s);
    while (!q.empty()) {
        ll u = q.front();
        q.pop();
        vist[u] = false;
        for (ll i = head[u]; i != -1; i = edge[i].next) {
            ll v = edge[i].to;
            if (dist[v] > dist[u] + edge[i].w) {
                dist[v] = dist[u] + edge[i].w;
                if (!vist[v]) {
                    vist[v] = true;
                    q.push(v);
                }
            }
        }
    }
}

void solve() {
    memset(head, -1, sizeof(head));
    cin >> n >> m >> s;
    for (ll i = 0; i < m; i++) {
        ll u, v, w;
        cin >> u >> v >> w;
        add_edge(u, v, w);
    }
    spfa(s);
    for (ll i = 1; i <= n; i++) {
        cout << dist[i] << " ";
    }

}

int main() {
    ll T = 1;
    //cin >> T;
    while (T--) {
        solve();
        cout << endl;
    }
    return 0;

}
~~~

## 最小生成树算法

### Prim算法

#### Prim算法基础与模版

视频讲解：[06. Prim算法 寻找最小生成树 Prim's Algorithm for Minimum Spanning Trees_哔哩哔哩_bilibili](https://www.bilibili.com/video/BV1hc41187JZ?p=6&vd_source=c4cdf942e0eb96f40a89387e745c7015)

代码详解：[最小生成树——Prim算法（详细图解）_最小生成树prim算法-CSDN博客](https://blog.csdn.net/qq_62213124/article/details/121597780)

~~~c++
#include<bits/stdc++.h>
#include<unordered_map>
#include <unordered_set>
#include <deque>
#include<algorithm>
#define ll long long
using namespace std;

const ll N = 1000,INF=0x3f3f3f3f;
ll edge[N][N];
ll dist[N], vist[N] = {0};
ll ans = 0;
ll n, m;	//n:节点个数；m:边的个数

void prim() {
    dist[1] = 0;//把点1加入集合S，点1在集合S中，将它到集合的距离初始化为0
    vist[1] = 1;//表示点1已经加入到了S集合中
    //这一步相当于更新其他节点与目前最小生成树的最小距离
    for (ll i = 2; i <= n; i++) {dist[i] = min(dist[i], edge[1][i]);}
    for (ll i = 2; i <= n; i++) {
        ll temp = INF;
        ll t = -1;
        for (ll j = 2; j <= n; j++) {
            if (!vist[j] && dist[j] < temp) {
                temp = dist[j];
                t = j;
            }
        }
        //如果找不到说明不能生成最小树
        if (t == -1) { ans = INF; return; }
        vist[t] = 1;
        ans += dist[t];
        //最小生成树变化，更新其他节点与最小生成树的最小距离
        for (ll j = 2; j <= n; j++) { dist[j] = min(dist[j], edge[t][j]); }
    }
}

void solve() {
    cin >> n >> m;
    //将两节点的距离初始化为无穷大，表示一开始无节点相连
    memset(edge, INF, sizeof(edge));
    memset(dist, INF, sizeof(dist));
    for (ll i = 0; i < m; i++) {
        ll b, e,dis;
        cin >> b >> e>>dis;
        edge[b][e] = dis;
        edge[e][b] = dis;
    }
    prim();
    if (ans == INF) { cout << "不能生成最小树"; }
    else { cout << ans; }
}

int main() {
    ll T = 1;
    //cin >> T;
    while (T--) {
        solve();
        cout << endl;
    }
    return 0;

}
~~~



### kruskal算法

#### kruskal算法基础与模版

视频讲解：[07. Kruskal算法 寻找最小生成树 Kruskal's Algorithm for Minimum Spanning Trees_哔哩哔哩_bilibili](https://www.bilibili.com/video/BV1hc41187JZ?p=7&vd_source=c4cdf942e0eb96f40a89387e745c7015)

代码详解：[C++实现kruskal算法_kruskal算法c++-CSDN博客](https://blog.csdn.net/qq_46027243/article/details/108903948)

~~~c++
#include<bits/stdc++.h>
#include<unordered_map>
#include <unordered_set>
#include <deque>
#include<algorithm>
#define ll long long
using namespace std;

ll n, m;  //结点，边
//u->v
struct Edge {
    ll u, v, w;     //左端点u,右端点v，权值w
};
vector<Edge>edge;   //存边的情况

//并查集部分
//---------------------------------------------
vector<ll>fa;
//初始化
void Init() {
    //加一防止结点从1开始
    fa.resize(n+1);
    for (ll i = 0; i < n+1; i++) {
        fa[i] = i;
    }
}
//找父节点
int tofind(int x) {
    if (x == fa[x]) { return x; }
    else {
        fa[x] = tofind(fa[x]);
        return fa[x];
    }
}
//合并
void unionn(int i, int j) {
    int i_fa = tofind(i);
    int j_fa = tofind(j);
    if (i_fa == j_fa) { return; }       //防止形成环
    fa[i_fa] = j_fa;
}
//---------------------------------------------

bool cmp(struct Edge a, struct Edge b) {
    return a.w < b.w;
}

//存最小生成树的情况
vector<ll>ans;
void kruskal() {
    Init();
    sort(edge.begin(), edge.end(), cmp);
    //遍历每条线，又因为最小生成树的长度为节点数-1，所以当答案数组存的长度为n-1时可以提前结束循环
    for (ll i = 0; i < m && ans.size() < n - 1; i++) {
        ll u = edge[i].u;
        ll v = edge[i].v;
        ll w = edge[i].w;
        if (tofind(u) != tofind(v)) {
            ans.push_back(w);
            unionn(u, v);
        }
    }
    if (ans.size() != n - 1) {
        cout << "orz";      //不是连通图
        return;
    }
    //此时最小生成树生成完毕，按题意对最小生成树进行相应查询或操作
    ll res = 0;
    for (ll x : ans) {
        res += x;
    }
    cout << res;
}

void solve() {
    cin >> n >> m;
    edge.resize(m);
    for (ll i = 0; i < m; i++) {
        cin >> edge[i].u >> edge[i].v >> edge[i].w;
    }
    kruskal();
}

int main() {
    ll T = 1;
    //cin >> T;
    while (T--) {
        solve();
        cout << endl;
    }
    return 0;

}
~~~



## 连通性问题

### 并查集

并查集详解与模版

~~~c++
const int N = 100000;
ll fa[N];
void Init(ll n) {
	for (ll i = 0; i <= n + 2; i++) { fa[i] = i; }
}
ll fin(ll x) {
	if (x == fa[x]) { return x; }
	fa[x] = fin(fa[x]);
	return fa[x];
}
void unionn(ll x, ll y) {
	ll fx = fin(x), fy = fin(y);
	if (fx == fy) { return; }
	fa[fx] = fy;
}
~~~

#### 带权并查集

带权并查集模版

视频讲解：[【neko算法课】并查集/带权并查集【8期】_哔哩哔哩_bilibili](https://www.bilibili.com/video/BV1vr4y1i76y/?spm_id_from=333.337.search-card.all.click&vd_source=c4cdf942e0eb96f40a89387e745c7015)

查找时的向量计算，见图：

![image-20240726113626161](C:\Users\21145\AppData\Roaming\Typora\typora-user-images\image-20240726113626161.png)

合并时的向量计算，见图：

![image-20240726113550863](C:\Users\21145\AppData\Roaming\Typora\typora-user-images\image-20240726113550863.png)

~~~c++
const ll N = 2e5 + 10, mod = 1e9+7;

ll fa[N],r[N];		//节点父亲，节点对应向量的权值 
void Init(ll n){
	for(ll i=0;i<=n;i++){fa[i]=i;r[i]=0;}
}

ll fin(ll x){
	if(x==fa[x]){return fa[x];}
	ll fx=fin(fa[x]);
	r[x]=r[x]+r[fa[x]];			//向量计算(注意，有时候的关系需要进行取模运算)
	fa[x]=fx;
	return fa[x];
}

//x->y,向量权值为d 
//注意带权并查集是有方向的 
void unionn(ll x,ll y,ll d){
	ll fx=fin(x),fy=fin(y);
	if(fx==fy){return;}
	fa[fx]=fy;
	r[fx]=d+r[y]-r[x];			//向量计算(注意，有时候的关系需要进行取模运算)
}
~~~

模版题-食物链

[P2024 [NOI2001\] 食物链 - 洛谷 | 计算机科学教育新生态 (luogu.com.cn)](https://www.luogu.com.cn/problem/P2024)

~~~c++
#include <bits/stdc++.h>
#define ll long long
#define lowbit(x) x&-x 
using namespace std;

const ll N = 2e5 + 10, mod = 1e9 + 7;

ll fa[N],r[N];
void Init(){
	for(ll i=0;i<N;i++){fa[i]=i;r[i]=0;}
}

ll fin(ll x){
	if(x==fa[x]){return fa[x];}
	ll fx=fin(fa[x]);
	r[x]=(r[x]+r[fa[x]])%3;
	fa[x]=fx;
	return fa[x];
}

//x->y
ll unionn(ll x,ll y,ll d){
	ll fx=fin(x),fy=fin(y);
	if(fx==fy){
		return (r[x]-r[y]+3)%3!=d;
	}
	fa[fx]=fy;
	r[fx]=(d+r[y]-r[x]+3)%3;
	return 0;
}

ll n,k,d,x,y,ans;
void solve() {
	Init();
	cin >> n >> k;
	for(ll i=1;i<=k;i++){
		cin >> d >> x >> y;
		if(x>n || y>n || (x==y && d==2)){ans++;continue;}
		ans+=unionn(x,y,d-1);
	}
	cout << ans;
}

int main() {
	ll T = 1;
	//cin >> T;
	while (T--) {
		solve();
		//putchar('\n');
	}
	return 0;
}
~~~



#### 例题树上找最长简单路径

 [A-LCT_2024牛客暑期多校训练营4 (nowcoder.com)](https://ac.nowcoder.com/acm/contest/81599/A)

~~~c++
#include <bits/stdc++.h>
#define ll long long
#define lowbit(x) x&-x 
using namespace std;

const ll N = 1e6 + 10, mod = 1e9 + 7;

ll fa[N],r[N],dp[N];	//dp[i]表示以该节点为根结点的最长简单路径
void Init(ll n){
	for(ll i=0;i<=n;i++){fa[i]=i;r[i]=0;dp[i]=0;}
}

ll fin(ll x){
	if(x==fa[x]){return fa[x];}
	ll fx=fin(fa[x]);
	r[x]=r[x]+r[fa[x]];
	fa[x]=fx;
	return fa[x];
}

//x->y
void unionn(ll x,ll y){
	ll fx=fin(x),fy=fin(y);
	if(fx==fy){return;}
	fa[fx]=fy;
	r[fx]=1+r[y]-r[x];
	dp[fy]=max(dp[fy],dp[fx]+r[fx]);
}

ll n,u,v,c;
void solve() {
	cin >> n;
	Init(n);
	for(ll i=1;i<=n-1;i++){
		cin >> v >> u >> c;
		unionn(u,v);
		cout << dp[c] << " ";
	}
}

int main() {
	ios::sync_with_stdio(0);
	cin.tie(0);cout.tie(0);
	ll T = 1;
	cin >> T;
	while (T--) {
		solve();
		cout << '\n';
	}
	return 0;
}
~~~



### Tarjan算法

Tarjan 算法是基于**[深度优先搜索](https://link.zhihu.com/?target=https%3A//en.wikipedia.org/wiki/Depth-first_search)**的算法，用于求解图的连通性问题。Tarjan 算法可以在线性时间内求出无向图的割点与桥，进一步地可以求解无向图的双连通分量；同时，也可以求解有向图的强连通分量、必经点与必经边。

视频详解：[[算法\]轻松掌握tarjan强连通分量_哔哩哔哩_bilibili](https://www.bilibili.com/video/BV19J411J7AZ/?spm_id_from=333.337.search-card.all.click&vd_source=c4cdf942e0eb96f40a89387e745c7015)

#### Tarjan模版

模版题链接：[P2863 [USACO06JAN\] The Cow Prom S - 洛谷 | 计算机科学教育新生态 (luogu.com.cn)](https://www.luogu.com.cn/problem/P2863)

~~~c++
#include<bits/stdc++.h>
#include<unordered_map>
#include <unordered_set>
#include <deque>
#include<algorithm>
#define ll long long
using namespace std;
const ll N = 1e5+5;
ll n, m;    //n点m边
ll ans;
//链式建图-----------
ll head[N];
ll id = 0;
struct Edge{
    ll to, w, next;
}edge[N];

void add_edge(ll u, ll v,ll w) {
    edge[++id].to = v;
    edge[id].w = w;
    edge[id].next = head[u];
    head[u] = id;
}
//-------------------

//Tarjan-------------
//dfn[i]:i点的实际访问时间
//low[i]:i点通过回溯可以达到的最小访问时间
//vis[i]:判断i是否在栈中
ll dfn[N],vis[N],low[N];
ll Time;            //为点赋予时间
stack <ll> stTarjan;
void Tarjan(ll u) {
    dfn[u] = low[u] = ++Time;
    stTarjan.push(u);
    vis[u] = 1;
    for (ll i = head[u]; i != -1; i = edge[i].next) {
        ll v = edge[i].to;
        if (!dfn[v]) {
            Tarjan(v);
            low[u] = min(low[u], low[v]);
        }
        else if (vis[v]) {
            low[u] = min(low[u], dfn[v]);
        }
    }
    if (low[u] == dfn[u]) {
        ll cnt = 0;		//题目要求部分
        while (1) {
            ll temp = stTarjan.top();
            stTarjan.pop();
            vis[temp] = 0;
            cnt++;
            if (temp == u) { break; }
        }
        if (cnt > 1) {
            ans++;
        }
    }
}
//-------------------

void solve() {
    memset(head, -1, sizeof(head));
    cin >> n >> m;
    for (ll i = 0; i < m; i++) {
        ll u, v;
        cin >> u >> v;
        add_edge(u, v, 1);
    }
    for (ll i = 1; i <= n; i++) {
        if (!dfn[i]) { Tarjan(i); }
    }
    cout << ans;

}



int main() {
    ll T = 1;
    //cin >> T;
    while (T--) {
        solve();
        cout << endl;
    }
    return 0;

}
~~~





#### Tarjan缩点

模版题目：[P2341 [USACO03FALL / HAOI2006\] 受欢迎的牛 G - 洛谷 | 计算机科学教育新生态 (luogu.com.cn)](https://www.luogu.com.cn/problem/P2341)

~~~c++
#include<bits/stdc++.h>
#include<unordered_map>
#include <unordered_set>
#include <deque>
#include<algorithm>
#define ll long long
using namespace std;
const ll N = 1e5 + 5;
ll n, m;    //n点m边

//链式建图-----------
ll head[N];
ll id = 0;
struct Edge {
    ll from, to, w, next;
}edge[N];

void add_edge(ll u, ll v, ll w) {
    edge[++id].to = v;
    edge[id].from = u;
    edge[id].w = w;
    edge[id].next = head[u];
    head[u] = id;
}
//-------------------


//Tarjan缩点后-------
ll color_id = 0;        //记录缩点编号也相当于缩点后点的个数
ll color[N];            //记录每个点被赋予的颜色
ll color_cnt[N];        //记录缩点后，每个缩点共缩了几个点
//-------------------


//Tarjan-------------
//dfn[i]:i点的实际访问时间
//low[i]:i点通过回溯可以达到的最小访问时间
//vis[i]:判断i是否在栈中
ll dfn[N], vis[N], low[N];
ll Time;            //为点赋予时间
stack <ll> stTarjan;
void Tarjan(ll u) {
    dfn[u] = low[u] = ++Time;
    stTarjan.push(u);
    vis[u] = 1;
    for (ll i = head[u]; i != -1; i = edge[i].next) {
        ll v = edge[i].to;
        if (!dfn[v]) {
            Tarjan(v);
            low[u] = min(low[u], low[v]);
        }
        else if (vis[v]) {
            low[u] = min(low[u], low[v]);
        }
    }
    if (low[u] == dfn[u]) {
        ll cnt = 0;
        color_id++;
        while (1) {
            ll temp = stTarjan.top();
            stTarjan.pop();
            vis[temp] = 0;
            color[temp] = color_id;
            cnt++;
            if (temp == u) { break; }
        }
        color_cnt[color_id] = cnt;
    }
}
//-------------------



ll cd[N];   //题目要求记录点的出度
void solve() {
    memset(head, -1, sizeof(head));
    cin >> n >> m;
    vector<pair<ll, ll>>memo;
    for (ll i = 0; i < m; i++) {
        ll u, v;
        cin >> u >> v;
        add_edge(u, v, 1);
        memo.push_back({ u,v });
    }
    for (ll i = 1; i <= n; i++) {
        if (!dfn[i]) { Tarjan(i); }
    }

    id = 0;
    memset(head, -1, sizeof(head));
    memset(edge, 0, sizeof(edge));
    for (ll i = 0; i < m; i++) {
        ll u = memo[i].first;
        ll v = memo[i].second;
        if (color[u] == color[v]) { continue; }
        add_edge(color[u], color[v], 1);
        cd[color[u]] = 1;
    }

    //题目要求操作
    ll cnt_rd = 0, ans_id, ans;
    for (ll i = 1; i <= color_id; i++) {
        if (cd[i] == 0) { cnt_rd++; ans_id = i; }
    }
    if (cnt_rd == 1) {
        ans = color_cnt[ans_id];
        cout << ans;
    }
    else {
        cout << 0;
    }
}

int main() {
    ll T = 1;
    //cin >> T;
    while (T--) {
        solve();
        cout << endl;
    }
    return 0;

}
~~~

#### Tarjan例题P3387 【模板】缩点（这特么是模版题？）

题目链接：[P3387 【模板】缩点 - 洛谷 | 计算机科学教育新生态 (luogu.com.cn)](https://www.luogu.com.cn/problem/P3387)

~~~c++
#include<bits/stdc++.h>
#include<unordered_map>
#include <unordered_set>
#include <deque>
#include<algorithm>
#define ll long long
using namespace std;
const ll N = 1e5 + 5;
ll n, m;    //n点m边
ll value[N],newValue[N];


//链式建图-----------
ll head[N];
ll id = 0;
struct Edge {
    ll from, to, w, next;
}edge[N];

void add_edge(ll u, ll v, ll w) {
    edge[++id].to = v;
    edge[id].from = u;
    edge[id].w = w;
    edge[id].next = head[u];
    head[u] = id;
}
//-------------------


//Tarjan缩点后-------
ll color_id = 0;
ll color[N];
//-------------------


//Tarjan-------------
//dfn[i]:i点的实际访问时间
//low[i]:i点通过回溯可以达到的最小访问时间
//vis[i]:判断i是否在栈中
ll dfn[N], vis[N], low[N];
ll Time;            //为点赋予时间
stack <ll> stTarjan;
void Tarjan(ll u) {
    dfn[u] = low[u] = ++Time;
    stTarjan.push(u);
    vis[u] = 1;
    for (ll i = head[u]; i != -1; i = edge[i].next) {
        ll v = edge[i].to;
        if (!dfn[v]) {
            Tarjan(v);
            low[u] = min(low[u], low[v]);
        }
        else if (vis[v]) {
            low[u] = min(low[u], low[v]);
        }
    }
    if (low[u] == dfn[u]) {
        ll sum = 0;
        color_id++;
        while (1) {
            ll temp = stTarjan.top();
            stTarjan.pop();
            vis[temp] = 0;
            color[temp] = color_id;
            sum += value[temp];
            if (temp == u) { break; }
        }
        newValue[color_id] = sum;
    }
}
//-------------------


//topu---------------
ll rd[N];
vector<ll>topusort;
ll dist[N];
ll topu() {
    queue<ll>que;
    for (ll i = 1; i <= color_id; i++) {
        if (!rd[i]) { que.push(i); dist[i] = newValue[i]; }
    }
    while (!que.empty()){
        ll u = que.front();
        que.pop();
        topusort.push_back(u);  //此段对此题多余
        for (ll i = head[u]; i != -1; i = edge[i].next) {
            ll v = edge[i].to;
            dist[v] = max(dist[v], dist[u] + newValue[v]);
            rd[v]--;
            if (!rd[v]) { que.push(v); }
        }
    }
    ll ans = 0;
    for (ll i = 1; i <= color_id; i++) {
        ans = max(ans, dist[i]);
    }
    return ans;

}
//-------------------



//思路:
//1.用Tarjan分出强联通分量
//2.重新建图进行缩点 
//3.用拓扑排序进行dp计算最大价值
void solve() {
    memset(head, -1, sizeof(head));
    cin >> n >> m;
    for (ll i = 1; i <= n; i++) { cin >> value[i]; }
    vector<pair<ll, ll>>memo;
    for (ll i = 0; i < m; i++) {
        ll u, v;
        cin >> u >> v;
        add_edge(u, v, 1);
        memo.push_back({ u,v });
    }
    //强联通
    for (ll i = 1; i <= n; i++) {
        if (!dfn[i]) { Tarjan(i); }
    }
    //重新建图
    id = 0;
    memset(head, -1, sizeof(head));
    memset(edge, 0, sizeof(edge));
    for (ll i = 0; i < m; i++) {
        ll u = memo[i].first;
        ll v = memo[i].second;
        if (color[u] == color[v]) { continue; }
        add_edge(color[u], color[v], 1);
        rd[color[v]]++;
    }
    //拓扑+dp
    cout << topu();
}



int main() {
    ll T = 1;
    //cin >> T;
    while (T--) {
        solve();
        cout << endl;
    }
    return 0;

}
~~~

#### Puzzle: Wagiri（牛客多校）

题目链接：(https://ac.nowcoder.com/acm/contest/81601/D)

~~~c++
#include <bits/stdc++.h>
#define ll long long
#define lowbit(x) x&-x 
using namespace std;

const ll N = 2e5 + 10, mod = 998244353;

//cnt:记录答案图的边数
//use[N]:记录第i条边最终是否使用在答案图中
//ok[N]:记录第i条边为"Lun"还是"Qie"
//fa[N]:并查集，该节点父亲
//dfn[N]:i点的实际访问时间
//low[N]:i点通过回溯可以达到的最小访问时间
//tot:给点赋予访问时间
//color[N]:第i个结点属于哪个强联通
//id:给强联通分量进行编号
//vector g[N]:只存为Lun的边所形成的图
ll n, m, cnt;
ll use[N], ok[N], fa[N];
ll dfn[N], low[N],tot;	//trajan
ll color[N], id;	//缩点
vector<ll>g[N];
struct edge {
	ll u, v;
}eg[N];

//并查集--------------------------------------
void Init(ll n) {
	for (ll i = 0; i <= n + 2; i++) { fa[i] = i; }
}
ll fin(ll x) {
	if (x == fa[x]) { return x; }
	fa[x] = fin(fa[x]);
	return fa[x];
}
void unionn(ll x, ll y) {
	ll fx = fin(x), fy = fin(y);
	if (fx == fy) { return; }
	fa[fx] = fy;
}
//---------------------------------------------


stack <ll> st;
//tarjan边双写法
void tarjan(ll u, ll pre) {
	dfn[u] = low[u] = ++tot;
	st.push(u);
	for (ll v : g[u]) {
		if (!dfn[v]) {
			tarjan(v, u);
			low[u] = min(low[u], low[v]);
		}
		else if (v != pre) {
			low[u] = min(low[u], dfn[v]);
		}
	}
	if (dfn[u] == low[u]) {
		id++;
		while (1) {
			ll tmp = st.top();
			st.pop();
			color[tmp] = id;
			if (tmp == u) { break; }
		}
	}
}

//1.只存lun的边形成一个图，对这个图进行tarjan寻找强联通分量
//2.对强联通进行连边，并查集操作即可，上面两个操作可以使Lun边必联通，不满足条件的Lun边自动舍弃
//3.保证不形成环的情况下尽可能去连Qie边，判断不形成环只需要用并查集：fin(x)!=fin(y)
//4.最终判断所有点的父亲是否唯一，唯一说明大家都连上了，并且通过123操作已经满足题意，为YES，否则为NO

void solve() {
	cin >> n >> m;
	Init(n);
	for (ll i = 1; i <= m; i++) {
		ll u, v; string s;
		cin >> u >> v >> s;
		eg[i] = { u,v };
		if (s == "Lun") {
			ok[i] = 1;
			g[u].push_back(v);
			g[v].push_back(u);
		}
	}
	for (ll i = 1; i <= n; i++) {
		if (dfn[i]) { continue; }
		tarjan(i, 0);
	}
	for (ll i = 1; i <= m; i++) {
		if (ok[i] != 1) { continue; }
		ll u = eg[i].u, v = eg[i].v;
		if (color[u] == color[v]) {
			unionn(u, v);
			use[i] = 1;
			cnt++;
		}
	}
	for (ll i = 1; i <= m; i++) {
		if (ok[i] != 0) { continue; }
		ll u = eg[i].u, v = eg[i].v;
		if (fin(u) == fin(v)) { continue; }
		unionn(u, v);
		use[i] = 1;
		cnt++;
	}
	set<ll>kd;
	for (ll i = 1; i <= n; i++) {
		kd.insert(fin(i));
	}
	if (kd.size() != 1) { cout << "NO"; return; }
	cout << "YES" << endl;
	cout << cnt << endl;
	for (ll i = 1; i <= m; i++) {
		ll u = eg[i].u, v = eg[i].v;
		if (use[i] == 1) {
			cout << u << " " << v << endl;
		}
	}
}

int main() {
	ios::sync_with_stdio(0);
	cin.tie(0); cout.tie(0);
	ll T = 1;
	//cin >> T;
	while (T--) {
		solve();
		//cout << endl;
	}
	return 0;
}
~~~







### 拓扑排序

#### 拓扑排序详解与模版

拓扑排序详解：[拓扑排序（Topological Sorting）-CSDN博客](https://blog.csdn.net/lisonglisonglisong/article/details/45543451)

在图论中，拓扑排序是一个有向无环图，所有顶点的线性序列。且该序列必须满足下面两个条件：

1.每个顶点出现且只出现一次。
2.若存在一条从顶点 A 到顶点 B 的路径，那么在序列中顶点 A 出现在顶点 B 的前面。
有向无环图才有拓扑排序，非DAG图没有拓扑排序一说。

![img](https://img-blog.csdn.net/20150507001028284)

它是一个 DAG 图，那么如何写出它的拓扑排序呢？这里说一种比较常用的方法：

1.从 DAG 图中选择一个 没有前驱（即入度为0）的顶点并输出。
2.从图中删除该顶点和所有以它为起点的有向边。
3.重复 1 和 2 直到当前的 DAG 图为空或当前图中不存在无前驱的顶点为止。后一种情况说明有向图中必然存在环。

![img](https://img-blog.csdn.net/20150507001759702)

于是，得到拓扑排序后的结果是 { 1, 2, 4, 3, 5 }。

通常，一个有向无环图可以有**一个或多个**拓扑排序序列。

拓扑排序模版题：[B3644 【模板】拓扑排序 / 家谱树 - 洛谷 | 计算机科学教育新生态 (luogu.com.cn)](https://www.luogu.com.cn/problem/B3644)

~~~c++
#include<bits/stdc++.h>
#include<unordered_map>
#include <unordered_set>
#include <deque>
#include<algorithm>
#define ll long long
using namespace std;

const ll N = 5e5+5;

vector<ll>tu[N];	//建图
ll du[N];			//每个结点的入度
ll n;
void Topsort() {
   
    queue <ll> que;
    //初始时将入度为0的结点入队
    for (ll i = 1; i <= n; i++) {
        if (!du[i]) { que.push(i); }
    }
    ll cnt = 0;		//记录输出个数
    while (!que.empty()) {
        ll u = que.front();
        que.pop();
        cout << u << " ";
        cnt++;
        for (ll x : tu[u]) {
            du[x]--;
            if (!du[x]) { que.push(x); }
        }
    }
    cout << endl;
    //输出个数小于n表示此图不是拓扑排序结构
    if (cnt < n) { cout << "出题人！你恶心我！！！"; }
    //else { cout << "出题人你大大滴呦西！"; }
}
void solve() {
    cin >> n;
    //建图并更新结点的入度
    for (ll u = 1; u <= n; u++) {
        ll v;
        while (1) {
            cin >> v;
            if (v == 0) { break; }
            tu[u].push_back(v);
            du[v]++;
        }
    }
    
    Topsort();
}

int main() {
    ll T = 1;
    //cin >> T;
    while (T--) {
        solve();
        cout << endl;
    }
    return 0;

}
~~~

#### 章鱼图的判断

题目链接：[RC-u4 章鱼图的判断 - 2024 睿抗机器人开发者大赛CAIP-编程技能赛-本科组（省赛） (pintia.cn)](https://pintia.cn/problem-sets/1813039306479005696/exam/problems/type/7?problemSetProblemId=1813039385617129475&page=0)

~~~c++
#include <bits/stdc++.h>
#define ll long long
#define lowbit(x) x&-x 
using namespace std;

const ll N = 2e5 + 10, mod = 998244353;

ll n,m,cnt,ans,du[N];
vector<ll>g[N];

void dfs(ll u){
	du[u]=0;
	for(ll v:g[u]){
		if(!du[v]){continue;}
		dfs(v);
		cnt++;
	}
}

void top(){
	queue<ll>q;
	for(ll i=1;i<=n;i++){if(du[i]==1){q.push(i);}}
	while(q.size()){
		ll u=q.front();
		q.pop();
		du[u]--;
		for(ll v:g[u]){
			du[v]--;
			if(du[v]==1){
				q.push(v); 
			}
		}
	}
}
 
void Init(ll n){
	for(ll i=0;i<=n+2;i++){g[i].clear();}
}
 
void solve() {
	ll u,v;
	cin >> n >> m;
	Init(n);
	for(ll i=1;i<=m;i++){
		cin >> u >> v;
		g[u].push_back(v);
		g[v].push_back(u);
		du[u]++,du[v]++;
	}
	top();

    //去除两个环共用一个点的情况
	for(ll i=1;i<=n;i++){
		if(du[i] && du[i]!=2){dfs(i);}
	}
    
	ans=0;
	for(ll i=1;i<=n;i++){
		if(du[i]!=2){continue;}
		cnt=1;
		dfs(i);
		ans++;
	}
	if(ans==1){cout << "Yes" << " " << cnt;}
	else {cout << "No" << " " << ans;}
}

int main() {
	ios::sync_with_stdio(0);
	cin.tie(0); cout.tie(0);
	ll T = 1;
	cin >> T;
	while (T--) {
		solve();
		cout << endl;
	}
	return 0;
}
~~~





## 树算法

### 最近公共祖先(LCA)

#### LCA模版题

题目链接：[P3379 【模板】最近公共祖先（LCA） - 洛谷 | 计算机科学教育新生态 (luogu.com.cn)](https://www.luogu.com.cn/problem/P3379)

~~~
#include<bits/stdc++.h>
#define ll long long
//关闭流
#define endl '\n'
#define buff ios::sync_with_stdio(false),cin.tie(nullptr),cout.tie(nullptr);
//

using namespace std;

//TOP为2^j中j的范围，根据题目范围决定，当然也可以无脑开的稍微大一些
const ll N = 5e5 + 2, TOP = 21;

//链式前向星建图部分-------------------------
ll head[N];
ll id = 0;
struct Edge {
	ll to, w, next; // to代表边的终点，w代表边的权值，next下面一个边
}gh[N * 2];	//如果单向就N，双向2*N

//加边,u->v
void add_edge(ll u, ll v, ll w) {
	gh[id].to = v;
	gh[id].w = w;
	gh[id].next = head[u];
	head[u] = id;
	id++;
}
//-----------------------------------------


ll n, m, s;
ll dp[N][TOP];		//dp[i][j],节点i的第2^j个祖先是谁
ll deep[N];			//保存深度

//dfs遍历图初始化deep，dp为LCA做准备
void dfs(ll u, ll fa) {
	deep[u] = deep[fa] + 1;
	dp[u][0] = fa;
	for (ll i = 1; i <= TOP - 1; i++) {
		dp[u][i] = dp[dp[u][i - 1]][i - 1];	//u往上走2^i等于u往上走2^(i-1)步后再走2^(i-1)步
	}
	//此处是链式前向星遍历，其余存图方法自行修改
	for (ll i = head[u]; i != -1; i = gh[i].next) {
		ll v = gh[i].to;
		if (v == fa) { continue; }
		dfs(v, u);
	}
}

//返回x节点与y节点的公共祖先节点
ll LCA(ll x, ll y) {
	//为了方便，将x作为深度最大的节点，相当于深度更低一些
	if (deep[x] < deep[y]) { swap(x, y); }
	//让x跳到与y同深度的节点
	for (ll i = TOP - 1; i >= 0; i--) {
		if (deep[dp[x][i]] >= deep[y]) {
			x = dp[x][i];
		}
	}
	//如果跳完为同一节点则直接输出
	if (x == y) { return x; }
	//否则两个节点一起跳
	for (int i = TOP - 1; i >= 0; i--) {
		if (dp[x][i] != dp[y][i]) {
			x = dp[x][i];
			y = dp[y][i];
		}
	}
	//跳完之后它们的父节点就是最近公共祖先
	return dp[x][0];
}

void solve() {
	memset(head, -1, sizeof(head));
	cin >> n >> m >> s;
	for (ll i = 0; i < n - 1; i++) {
		ll u, v;
		cin >> u >> v;
		add_edge(u, v, 1);
		add_edge(v, u, 1);
	}
	dfs(s, 0);
	while (m--) {
		ll a, b;
		cin >> a >> b;
		cout << LCA(a, b) << endl;
	}
}

int main() {
	//关闭流
	ios::sync_with_stdio(0); cin.tie(NULL); cout.tie(NULL);
	//
	ll T = 1;
	//cin >> T;
	while (T--) {
		solve();
		cout << endl;
	}
	return 0;

}
~~~

#### LCA变式例题：景区导游（蓝桥）

题目链接：[9.景区导游 - 蓝桥云课 (lanqiao.cn)](https://www.lanqiao.cn/problems/3516/learning/?subject_code=1&group_code=4&match_num=14&match_flow=1&origin=cup)

~~~
思路：
a->b->c,原本路线和sum=get_dist[a,b]+get_dist[b,c];
跳过b，a->c则路线变为：sum-get_dist[a,b]-get_dist[b,c]+get_dist[a,c];
get_dist怎么计算？
可以计算每个节点到根节点的距离dist[i]	//根节点到i的距离
假设a,b的最近公共祖先节点为c;
则get_dist[a,b]=dist[a]+dist[b]-dist[c]*2;
dist怎么算？			dfs遍历一下图
最近公共祖先节点怎么算？   LCA算法
~~~



~~~c++
#include<bits/stdc++.h>
#define ll long long
using namespace std;

const ll N = 1e5 + 2;

vector<pair<ll, ll> > gh[N];
ll path[N];
ll dp[N][21];
ll deep[N];
ll dist[N];

void dfs(ll u,ll fa){
	deep[u]=deep[fa]+1;
	dp[u][0]=fa;
	for(ll i=1;i<=20;i++){
		dp[u][i]=dp[dp[u][i-1]][i-1];
	}
	for(auto p:gh[u]){
		ll v=p.first,w=p.second;
		if(v==fa){continue;}
		dist[v]=dist[u]+w;
		dfs(v,u);
	}
}

ll LCA(ll x,ll y){
	if(deep[x]<deep[y]){swap(x,y);}
	for(ll i=20;i>=0;i--){
		if(deep[dp[x][i]]>=deep[y]){
			x=dp[x][i];
		}
	}
	if(x==y){return x;}
	for(int i=20;i>=0;i--){
		if(dp[x][i]!=dp[y][i]){
			x=dp[x][i];
			y=dp[y][i];
		}
	}
	return dp[x][0];
}

ll get_dist(ll x, ll y) {
	if (x == 0 || y == 0) { return 0; }
	return dist[x] + dist[y] - 2 * dist[LCA(x, y)];
}

void solve() {
	ll n, k;
	cin >> n >> k;
	for (ll i = 0; i < n - 1; i++) {
		ll u, v, w;
		cin >> u >> v >> w;
		gh[u].push_back({ v,w });
		gh[v].push_back({ u,w });
	}
	dfs(1,0);
	ll sum = 0;
	for (ll i = 1; i <= k; i++) {
		cin >> path[i];
		sum += get_dist(path[i], path[i - 1]);
	}
	for (int i = 1; i <= k; i++) {
		ll dist1 = get_dist(path[i], path[i - 1]);
		ll dist2 = get_dist(path[i], path[i + 1]);
		ll dist3 = get_dist(path[i - 1], path[i + 1]);
		ll ans = sum - dist1 - dist2 + dist3;
		cout << ans << " ";
	}
}

int main() {
	ll T = 1;
	//cin >> T;
	while (T--) {
		solve();
		cout << endl;
	}
	return 0;

}
~~~

#### LCA变式例题：机房（蓝桥BFS写法）

题目链接：[9.机房 - 蓝桥云课 (lanqiao.cn)](https://www.lanqiao.cn/problems/2198/learning/?subject_code=1&group_code=4&match_num=13&match_flow=2&origin=cup)

~~~c++
#include<bits/stdc++.h>
#define ll long long
#define PI 3.1415926

using namespace std;

const ll N = 1e5 + 5, INF = 1e18, mod = 1e9 + 7, Top = 21;

ll n, m, u, v;

vector<ll>gh[N];
ll dist[N];
ll dp[N][Top];
ll deep[N];

queue <ll> q;
void bfs() {
	deep[1] = 1;
	q.push(1);
	dp[1][0] = 0;
	dist[1] = gh[1].size();
	while (q.size()) {
		ll u = q.front();
		q.pop();
		for (ll v : gh[u]) {
			if (v == dp[u][0]) { continue; }
			q.push(v);
			dist[v] = dist[u] + gh[v].size();
			dp[v][0] = u;
			deep[v] = deep[u] + 1;
			for (ll i = 1; i <= Top - 1; i++) {
				dp[v][i] = dp[dp[v][i - 1]][i - 1];
			}
		}
	}
}

ll LCA(ll x, ll y) {
	if (deep[x] < deep[y]) { swap(x, y); }
	for (ll i = Top - 1; i >= 0; i--) {
		if (deep[dp[x][i]] >= deep[y]) {
			x = dp[x][i];
		}
	}
	if (x == y) { return x; }
	for (ll i = Top - 1; i >= 0; i--) {
		if (dp[x][i] != dp[y][i]) {
			x = dp[x][i];
			y = dp[y][i];
		}
	}
	return dp[x][0];
}

void solve() {
	cin >> n >> m;
	for (ll i = 0; i < n - 1; i++) {
		cin >> u >> v;
		gh[u].push_back(v);
		gh[v].push_back(u);
	}
	bfs();
	while (m--) {
		cin >> u >> v;
		ll w = LCA(u, v);
		ll dot = gh[w].size();		//多减了要加上
		ll ans = dist[u] + dist[v] - dist[w] * 2+dot;
		cout << ans << endl;
	}

}

int main() {
	ll T = 1;
	//cin >> T;
	while (T--) {
		solve();
		//cout << endl;
	}
	return 0;

}
~~~







### 树上拆分

#### 树上拆分详解

tip:树上拆分要用到LCA算法得到两个节点的最近公共祖先

树上拆分详解：[树上差分详解 - TEoS - 博客园 (cnblogs.com)](https://www.cnblogs.com/TEoS/p/11376676.html)

![image-20240410144428502](C:\Users\21145\AppData\Roaming\Typora\typora-user-images\image-20240410144428502.png)

~~~
原数列的前缀和的差分数组还是原数列，原数列的差分数组的前缀和也是原数列，这就是差分被称为前缀和的逆运算的原因，也是差分可以用来优化操作的原因。
~~~

![img](https://cdn.luogu.org/upload/pic/64016.png)



点拆分

~~~c++
设将u,v之间的路径上所有的点权增加x,o为x,y的最近公共祖先,o=LCA(u,v),p为o的父节点，则：
diff:拆分数组

diff[u]+=x,diff[v]+=x,diff[o]-=x,diff[p]-=x;
~~~

![img](https://cdn.luogu.org/upload/pic/64018.png)

边差分

~~~c++
设将u,v之间的路径上所有的边，边权增加x,o为x,y的最近公共祖先,o=LCA(u,v)，则：
diff:拆分数组

diff[u]+=x,diff[v]+=x,diff[o]-=2*x;
~~~



拆分还原为原数组

点拆分：只要dfs一遍，遍历时统计以每个节点为根的树的节点的权值和，就是当前节点的最终权值！

边拆分：同样地，只要dfs一遍，遍历时统计以每个节点为根的树的节点的权值和，就是当前节点到父亲节点的边的最终权值了！

#### 树上拆分例题：砍树（蓝桥）

题目链接：[10.砍树 - 蓝桥云课 (lanqiao.cn)](https://www.lanqiao.cn/problems/3517/learning/?subject_code=1&group_code=4&match_num=14&match_flow=1&origin=cup)

~~~c++
#include<bits/stdc++.h>
#define ll long long

using namespace std;

const ll N = 1e5 + 5;

struct edge{
	ll v, id;	//u->v,记录v和这条边的编号id
};

ll n, m;
vector<edge> gh[N];
ll dp[N][21];		//LCA倍增记录节点的祖先,dp[i][j]记录节点i的第2^j的父节点
ll deep[N];			//记录节点深度

ll ln[N];			//记录每个点对应的线 
ll d[N];			//拆分 

void dfs(ll u, ll fa) {
	deep[u] = deep[fa] + 1;
	dp[u][0] = fa;
	for (ll i = 1; i <= 20; i++) {
		dp[u][i] = dp[dp[u][i - 1]][i - 1];
	}
	for (ll i = 0; i < gh[u].size(); i++) {
		ll v = gh[u][i].v;
		ll id = gh[u][i].id;
		if (v == fa) { continue; }
		ln[v] = id;
		dfs(v, u);
	}
}

ll LCA(ll x, ll y) {
	if (deep[x] < deep[y]) { swap(x, y); }
	for (ll i = 20; i >= 0; i--) {
		if (deep[dp[x][i]] >= deep[y]) {
			x = dp[x][i];
		}
	}
	if (x == y) { return x; }
	for (ll i = 20; i >= 0; i--) {
		if (dp[x][i] != dp[y][i]) {
			x = dp[x][i];
			y = dp[y][i];
		}
	}
	return dp[x][0];
}

//将拆分数组还原为原数组
void dfs_sum(ll u) {
	for (int i = 0; i < gh[u].size(); i++) {
		int v = gh[u][i].v;
		if (v == dp[u][0]) { continue; }
		dfs_sum(v);
		d[ln[u]] += d[ln[v]];
	}
}

void solve() {
	cin >> n >> m;
	for (ll i = 1; i <= n - 1; i++) {
		ll x, y;
		cin >> x >> y;
		gh[x].push_back({ y,i });
		gh[y].push_back({ x,i });
	}
	dfs(1, 0);
	for (ll i = 0; i < m; i++) {
		ll x, y;
		cin >> x >> y;
		ll z = LCA(x, y);
		//拆分
		d[ln[x]]++, d[ln[y]]++, d[ln[z]] -= 2;
	}
	dfs_sum(1);
	ll ans = -1;
	for (ll i = n; i >= 1; i--) {
		if (d[i] == m) {
			ans = i;
			break;
		}
	}
	cout << ans;
}

int main() {
	ll T = 1;
	//cin >> T;
	while (T--) {
		solve();
		cout << endl;
	}
	return 0;

}
~~~



### 二叉搜索树：根据树的前序遍历得到后序遍历

#### 二叉搜索树前序转后序详解

算法详解：[二叉搜索树的性质：根据树的前序遍历得到后序遍历_对于有n个结点的二叉搜索树给定-CSDN博客](https://blog.csdn.net/weixin_45755831/article/details/115422224)

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210403223602327.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTc1NTgzMQ==,size_16,color_FFFFFF,t_70#pic_center)

我们可以得出他的前序序列： 5 2 1 4 8 7 9。后序遍历为： 1 4 2 7 9 8 5。
分割可得：5 是 root，2，1，4 是 left，8，7，9 是 right。
这样我们得出每一小段。
设最左边为 root，最右边为 tail，然后我们再设两个未知数，i 和 j。
所以，left的范围是[root+1, j]（root+1的原因是root是root不在left内），right的范围是[i, tail]。
又因为后序遍历的顺序是：left -> right -> root 。
我们先保存每段的 left 然后是 right，最后才是 root。
这里我们将进行递归，将每个 left 和 right 段分成三段直到不能分开。

~~~c++
//此代码展示左边节点全小于等于根节点，右边节点全部大于根节点的搜索规则

//dfs(头节点在数组中的下标，尾节点在数组中的下标)
void dfs(ll root, ll tail) {
    if (root > tail) { return; }
    //i为root+1表示不包括根节点
    ll i = root + 1, j = tail;
    //从i开始往下找，找到不满足搜索条件时停止，从j开始往上找，找不到满足搜索条件时停止
    while (i < tail && tree[i] <= tree[root]) { i++; }
    while (j >= root && tree[j] > tree[root]) { j--; }
    //如果i与j之差不为1则表示该树不满足二叉搜索树的条件
    if (i - j != 1) { return; }
    //把一棵树分三部分，root为根节点，[root+1,j]是左树，[i,tail]是右树
    //后序遍历顺序为：左边，右边，根节点
    dfs(root + 1, j);
    dfs(i, tail);
    ans.push_back(tree[root]);
}
~~~



#### 例题：这是二叉搜索树吗？(PTA)

题目链接：[L2-004 这是二叉搜索树吗？ - 团体程序设计天梯赛-练习集 (pintia.cn)](https://pintia.cn/problem-sets/994805046380707840/exam/problems/994805070971912192?type=7&page=0)

~~~c++
#include<bits/stdc++.h>
#define ll long long

using namespace std;

const ll N = 1e5 + 5;

vector<ll>tree;
vector<ll>ans;
ll id = 0;
ll flag;
void dfs(ll root, ll tail) {
    if (root > tail) { return; }
    //i为root+1表示不包括根节点
    ll i = root + 1, j = tail;
    if (!flag) {
        //从i开始往下找，找到不满足搜索条件时停止，从j开始往上找，找不到满足搜索条件时停止
        while (i <= tail && tree[i] < tree[root]) { i++; }
        while (j > root && tree[j] >= tree[root]) { j--; }
    }
    else {
        while (i <= tail && tree[i] >= tree[root]) { i++; }
        while (j > root && tree[j] < tree[root]) { j--; }
    }
    //如果i与j之差不为1则表示该树不满足二叉搜索树的条件
    if (i - j != 1) { return; }
    //把一棵树分三部分，root为根节点，[root+1,j]是左树，[i,tail]是右树
    //后序遍历顺序为：左边，右边，根节点
    dfs(root + 1, j);
    dfs(i, tail);
    ans.push_back(tree[root]);
}

void solve() {
    ll n; cin >> n;
    tree.resize(n);
    for (ll i = 0; i < n; i++) {
        cin >> tree[i];
    }
    flag = 0;
    dfs(0, n - 1);
    if (ans.size()==n) {
        cout << "YES"<<endl;
        cout << ans[0];
        for (ll i = 1; i < n; i++) {
            cout << " " << ans[i];
        }
        return;
    }
    ans.clear();
    flag = 1;
    dfs(0, n - 1);
    if (ans.size()==n) {
        cout << "YES" << endl;
        cout << ans[0];
        for (ll i = 1; i < n; i++) {
            cout << " " << ans[i];
        }
        return;
    }
    cout << "NO";
}

int main() {
    ll T = 1;
    //cin >> T;
    while (T--) {
        solve();
        //cout << endl;
    }
    return 0;

}
~~~

### 基环树

![image-20250205131003600](C:\Users\21145\AppData\Roaming\Typora\typora-user-images\image-20250205131003600.png)



#### P2607 [ZJOI2008] 骑士

题目链接：[P2607 [ZJOI2008\] 骑士 - 洛谷 | 计算机科学教育新生态](https://www.luogu.com.cn/problem/P2607)

~~~c++
#include<bits/stdc++.h>
#define ll long long
using namespace std;

const ll N = 1e6 + 10, mod = 998244353, P = 131;

ll n,a[N],ans,r1,r2,vis[N],w[N]; 
vector<ll> g[N];
ll f[N][2];	//u选与不选的最大 

//寻找一个环上的任意两点 
void find(ll u,ll rt){
	vis[u]=1;
	for(ll v:g[u]){
		if(v==rt){r1=u,r2=v;return;}
		if(vis[v]){continue;}
		find(v,rt);
	}
}

//以rt为根进行树形dp，但删掉了r1与r2这条边 
void dfs(ll u,ll rt){
	f[u][0]=0,f[u][1]=w[u];
	for(ll v:g[u]){
		if(v==rt){continue;}	//删了一个边，如果有边连到根结点，就是删的那条边，跳过 
		dfs(v,rt);
		f[u][0]+=max(f[v][0],f[v][1]);
		f[u][1]+=f[v][0];
	}
}

//思想：基环树，删掉环中任意一边，以这条边的两个点进行树形dp 
void solve() {
	cin >> n;
	for(ll v=1,u;v<=n;v++){
		cin >> w[v] >> u;
		g[u].push_back(v);
	}
	for(ll i=1;i<=n;i++){
		if(vis[i]){continue;}
		r1=r2=-1;
		find(i,i);
		if(r1==-1){continue;}
		dfs(r1,r1);
		ll res1=f[r1][0];
		dfs(r2,r2);
		ll res2=f[r2][0];
		ans+=max(res1,res2);
	}
	cout << ans;
}


int main() {
	ios::sync_with_stdio(0);
	cin.tie(0); cout.tie(0);
	ll T = 1;
	//cin >> T;
	while (T--) {
		solve();
		//cout << '\n';
	}
	return 0;

}
~~~



# 动态规划部分

## 背包问题

### 01背包问题

算法讲解：[[纯手推！！！\]动态规划背包问题汇总 01背包 完全背包 多重背包 二维数组 一维滚动数组_哔哩哔哩_bilibili](https://www.bilibili.com/video/BV1Kz4y1W7U9/?spm_id_from=333.337.search-card.all.click&vd_source=c4cdf942e0eb96f40a89387e745c7015)

模版例题：[2. 01背包问题 - AcWing题库](https://www.acwing.com/problem/content/2/)

~~~c++
//二维01背包写法
ll dp[1005][1005];  //dp[i][j]  第i件物品容量为j时的最大价值
void solve() {
    ll n, v;
    cin >> n >> v;
    vector<ll>weight(n+1),value(n+1);    //weight体积，value价值
    for (ll i = 1; i <= n; i++) { cin >> weight[i] >> value[i]; }
    for (ll i = 0; i <= n; i++) { dp[i][0] = 0; dp[0][i] = 0; } //初始化dp数组
    for (ll i = 1; i <= n; i++) {
        for (ll j = 1; j <= v; j++) {
            //如果不够装时
            if (j - weight[i] < 0) {
                dp[i][j] = dp[i - 1][j];
            }
            //如果够装则有装和不装两种选择
            else {
                dp[i][j] = max(dp[i - 1][j - weight[i]] + value[i], dp[i - 1][j]);
            }
        }
    }
    cout << dp[n][v];
}
~~~

~~~c++
//一维01背包写法
ll dp[1005];  //dp[j]  当容量为j时，这n件物品的价值最大是多少
void solve() {
    ll n, v;    //n件物品，最大容量为v
    cin >> n >> v;
    vector<ll>weight(n+1),value(n+1);    //weight体积，value价值
    for (ll i = 1; i <= n; i++) { cin >> weight[i] >> value[i]; }
    for (ll i = 0; i <= n; i++) { dp[i] = 0; } //初始化dp数组
    for (ll i = 1; i <= n; i++) {
        for (ll j = v; j >= weight[i]; j--) {
            dp[j] = max(dp[j],dp[j-weight[i]]+value[i]);
        }
    }
    cout << dp[v];
}
~~~

### 完全背包问题

模版例题：[3. 完全背包问题 - AcWing题库](https://www.acwing.com/problem/content/3/)

~~~c++
//二维完全背包写法
ll dp[1002][1002];
void solve() {
    ll n, v;    //物品个数与容量
    cin >> n >> v;
    vector<ll>weight(n+1), value(n+1);  //物品容量与价值
    for (ll i = 1; i <= n; i++) {
        cin >> weight[i] >> value[i];
    }
    for (ll i = 0; i <= n; i++) { dp[i][0] = 0; dp[0][i] = 0; }
    for (ll i = 1; i <= n; i++) {
        for (ll j = 1; j <= v; j++) {
            //如果无法选就继承上一层的状态
            dp[i][j] = dp[i - 1][j];
            if (weight[i] <= j) {
                //否则就选择装和不装
                dp[i][j] = max(dp[i][j], dp[i][j - weight[i]]+value[i]);
            }
        }
    }
    cout << dp[n][v];
}
~~~

~~~c++
//一维完全背包写法
ll dp[1002];
void solve() {
    ll n, v;    //物品个数与容量
    cin >> n >> v;
    vector<ll>weight(n+1), value(n+1);  //物品容量与价值
    for (ll i = 1; i <= n; i++) {
        cin >> weight[i] >> value[i];
    }
    //初始化
    for (ll i = 0; i <= v; i++) { dp[i] = 0;}
    //优化过程，如果选不了则继承上一层
    //选不了的情况只有j<weight[i]，所以j遍历的时候可以直接从weight[i]遍历，保证了j>=weight[i];
    //就不用管上一层的情况了，只用看这一层的情况，也就是在二维时全部为dp[i][……]的形式。
    //所以可以直接把dp[i]这个空间删掉，只用dp[j],就表示n个物品时，空间为j时的最大价值
    for (ll i = 1; i <= n; i++) {
        for (ll j = weight[i]; j <= v; j++) {
            dp[j] = max(dp[j], dp[j - weight[i]] + value[i]);
        }
    }
    cout << dp[v];
}
~~~

~~~c++
//总结：一维01背包和一维完全背包的差别就在01从最大到最小遍历，完全从最小到最大遍历
~~~

## 状态压缩DP

### 状态压缩方法：康托展开

康托展开简介：
官方简介：

康托展开是一个全排列到一个自然数的双射，常用于构建哈希表时的空间压缩。 康托展开的实质是计算当前排列在所有由小到大全排列中的顺序，因此是可逆的。

通俗简介：

康托展开可以求解一个排列的序号，比如：12345 序号为 1  ，12354序号为2，按字典序增加编号递增，依次类推。
康托逆展开可以求解一个序号它对应的排列是什么。
![image-20240523174901646](C:\Users\21145\AppData\Roaming\Typora\typora-user-images\image-20240523174901646.png)

~~~c++
//例如排列012345，因为有0的存在，12345可以表示为012345或者123450，因此可以通过康托展开给每一个排序编号。通过编号进行状态的压缩

ll f[20];
//预处理阶乘
void jc() {
	f[0] = f[1] = 1;
	for (ll i = 2; i <= 19; i++) { f[i] = f[i - 1] * i; }
}

//康托算法，排列字符串转编号
ll kt_sti(string s) {
	ll ans = 1;
	ll len = s.size();
	for (ll i = 0; i < len; i++) {
		ll cnt = 0;						//计数之后有几个比自己小
		for (ll j = i + 1; j < len; j++) {
			if (s[i] > s[j]) { cnt++; }	
		}
		ans += cnt * f[len - i - 1];	//计算公式
	}
	return ans;
}

//康托算法，编号转字符串排列
string kt_its(ll k) {
	string b = "012345678";			//编号为1的排列
	string ans = "";
	ll len = 0,n=b.size();
	k--;	//以1作为起始编号，因此减一
	for (ll i = 1; i <= n; i++) {
		ll t = k / f[n - i];		//计算之后有几个比自己小的
		k %= f[n - i];				
		ans += b[t];				//那就加上目前剩余的第t大的				
		b.erase(b.begin() + t);		//加完删除，相当于用过了
	}
	return ans;
}

~~~



### 例题：八数码难题（洛谷P1379）

~~~c++
#include<bits/stdc++.h>
#include <unordered_map>
#define ll long long
#define PI 3.1415926
#define ull unsigned long long
#define EPS 1e-6
using namespace std;

const ll N = 1e5+5,  mod = 3333, INF = 0x3f3f3f3f;

string str;
ll f[9];
set<ll>vis;
ll xx[4] = { 0,0,-1,1 };
ll yy[4] = { 1,-1,0,0 };

//预处理阶乘
void jc() {
	f[0] = f[1] = 1;
	for (ll i = 2; i <= 8; i++) { f[i] = f[i - 1] * i; }
}

//康托算法，排列字符串转编号
ll kt_sti(string s) {
	ll ans = 1;
	ll len = s.size();
	for (ll i = 0; i < len; i++) {
		ll cnt = 0;						//计数之后有几个比自己小
		for (ll j = i + 1; j < len; j++) {
			if (s[i] > s[j]) { cnt++; }	
		}
		ans += cnt * f[len - i - 1];	//计算公式
	}
	return ans;
}

//康托算法，编号转字符串排列
string kt_its(ll k) {
	string b = "012345678";			//编号为1的排列
	string ans = "";
	ll len = 0,n=b.size();
	k--;	//以1作为起始编号，因此减一
	for (ll i = 1; i <= n; i++) {
		ll t = k / f[n - i];		//计算之后有几个比自己小的
		k %= f[n - i];				
		ans += b[t];				//那就加上目前剩余的第t大的				
		b.erase(b.begin() + t);		//加完删除，相当于用过了
	}
	return ans;
}


queue<ll>q;
ll bfs() {
	ll sum = 0;
	ll b = kt_sti(str);
	q.push(b);
	vis.insert(b);
	while (q.size()) {
		sum++;
		ll size = q.size();
		while (size--) {
			ll t = q.front();
			q.pop();
			string now = kt_its(t);
			if (now == "123804765") { return sum - 1; }
			ll z_r, z_c;
			for (ll i = 0; i < 9; i++) {
				if (now[i] == '0') {
					z_r = i / 3;
					z_c = i % 3;
				}
			}
			for (ll i = 0; i < 4; i++) {
				ll nr = z_r + xx[i];
				ll nc = z_c + yy[i];
				if (nr < 0 || nr >= 3 || nc < 0 || nc >= 3) { continue; }
				string ns = now;
				swap(ns[z_r*3+z_c], ns[nr*3+nc]);
				ll nn = kt_sti(ns);
				if (vis.count(nn)) { continue; }
				vis.insert(nn);
				q.push(nn);
			}
			
		}
	}
	return -1;
}

void solve() {
	cin >> str;
	ll ret=bfs();
	if (ret == -1) {
		cout << "他奶奶滴，出题人，你阴我是吧";
	}
	else {
		cout << ret;
	}
}

int main() {
	jc();
	ll T = 1;
	//cin >> T;
	while (T--) {
		solve();
		//cout << endl;

	}
	return 0;

}
~~~



## 数位dp

### 模版

~~~c++
ll dp[N][N][2][2];
//pos:当前位
//sum:状态
//lead:是否有前导零
//limit:是否上限
ll dfs(ll pos, ll sum, bool lead, bool limit) {
	ll ans = 0;
	if (pos == 0) { return sum; }
	if (dp[pos][sum][lead][limit] != -1) { return dp[pos][sum][lead][limit]; }
	ll up = limit ? num[pos] : 9;
	for (ll i = 0; i <= up; i++) {
		//……相应操作
	}
	dp[pos][sum][lead][limit] = ans;
	return ans;
}
~~~



## 倍增

### Permute K times(atcoder)

例题：[My Submissions - AtCoder Beginner Contest 367](https://atcoder.jp/contests/abc367/submissions/me)

~~~c++
#include <bits/stdc++.h>
#define ll long long

using namespace std;

const ll N = 3e5 + 10, mod = 998244353;

ll n,k;
ll f[N][70],a[N];	//第i个位置走2^j步到哪个位置 
void solve() {
	cin >> n >> k;
	for(ll i=1;i<=n;i++){cin >> f[i][0];}
	for(ll i=1;i<=n;i++){cin >> a[i];}
	
	for(ll j=1;j<64;j++){				//注意因为看的是第i个位置的j-1,因此j这一层应该在外层，i位置在里层		
		for(ll i=1;i<=n;i++){
			f[i][j]=f[f[i][j-1]][j-1];	//i走2^j步等价于i走2^(j-1)后再走2^(j-1); 
		}
	}
	for(ll i=1;i<=n;i++){
		ll pos=i;
		for(ll j=0;j<64;j++){
			if((k>>j)&1){
				pos=f[pos][j];
			}
		}
		cout << a[pos] << " ";
	}
}

int main() {
	ios::sync_with_stdio(0);
	cin.tie(0), cout.tie(0);
	ll T = 1;
	//cin >> T;
	while (T--) {
		solve();
		//cout << '\n';
	}
	return 0;
}
~~~

## 换根dp

题目链接：[834. 树中距离之和 - 力扣（LeetCode）](https://leetcode.cn/problems/sum-of-distances-in-tree/)

~~~c++
class Solution {
public:
    int d[30010],cnt[30010],n;
    vector<int>g[30010];
    vector<int>f;
    void dfs(int u,int fa){
        cnt[u]=1;
        for(int v:g[u]){
            if(v==fa){continue;}
            d[v]=d[u]+1;
            dfs(v,u);
            cnt[u]+=cnt[v];
        }
    }
    void dfs2(int u,int fa){
        for(int v:g[u]){
            if(v==fa){continue;}
            f[v]=f[u]+n-2*cnt[v];
            dfs2(v,u);
        }
    }

    vector<int> sumOfDistancesInTree(int m, vector<vector<int>>& edges) {
        n=m;
        f.resize(m);
        for(auto p:edges){
            g[p[0]].push_back(p[1]);
            g[p[1]].push_back(p[0]);
        }
        d[0]=0;
        dfs(0,-1);
        int s=0;
        for(int i=0;i<m;i++){s+=d[i];}
        f[0]=s;
        dfs2(0,-1);
        return f;
    }
};
~~~





## 让人意想不到的一些dp状态定义

~~~c++
题目链接：https://www.lanqiao.cn/problems/3512/learning/?subject_code=1&group_code=4&match_num=14&match_flow=1&origin=cup
题目大意：
删除最少多少个数使剩下序列是接龙序列？
接龙序列：12 21 15 532 219 96	(首末相接)
5
11 121 22 12 2023
    
dp[i]:以i结尾能接龙的最大个数
因此dp只需要开10个空间给0~9存放
    
l:当前数以l开头，r:当前数以r结尾
状态转移方程：dp[r]=max(dp[l]+1,dp[r]);
含义：拿到一个数，要么接，要么不接，看看接l时与不接的时候哪个大。这里的dp[r]相当于不接，已经有一个以右端为末尾的接龙序列比自己接了l的更大，没必要接了。
    
 ---------------------------------------------------------------------------------------------------------
如果遇到末尾连续多少个，则定义f[i][j];	//前i位，末尾连续j个时
~~~







# 高级数据结构部分

## ST表

### ST表模版题（区间查询最大值）

视频详解：[A12 ST表 RMQ问题_哔哩哔哩_bilibili](https://www.bilibili.com/video/BV1Sj411o7jp/?spm_id_from=333.337.search-card.all.click&vd_source=c4cdf942e0eb96f40a89387e745c7015)

题目链接：[P3865 【模板】ST 表 && RMQ 问题 - 洛谷 | 计算机科学教育新生态 (luogu.com.cn)](https://www.luogu.com.cn/problem/P3865)

~~~
#include <bits/stdc++.h>
#define ll long long
using namespace std;

const ll N = 1e6 + 10, mod = 998244353;

//快速输入(整数)
inline void read(ll& a){
	ll s = 0, w = 1;
	char ch = getchar();
	while (ch < '0' || ch>'9'){if (ch == '-')w = -1;ch = getchar();}
	while (ch >= '0' && ch <= '9'){s = s * 10 + ch - '0';ch = getchar();}
	a = s * w;
}

//快速输出(整数)
void write(ll x){
    if(x<0)putchar('-'),x=-x;
    if(x>9)write(x/10);
    putchar(x%10+'0');
    return;
}


ll n,m;
ll f[N][40];
ll query(ll l,ll r){
	ll k=log2(r-l+1);
	return max(f[l][k],f[r-(1<<k)+1][k]);
}

void solve() {
	read(n);read(m);
	ll t;
	for(ll i=1;i<=n;i++){
		read(t);
		f[i][0]=t;
	}
	for(ll j=1;j<=log2(n);j++){
		for(ll i=1;i<=n-(1<<j)+1;i++){
			f[i][j]=max(f[i][j-1],f[i+(1<<(j-1))][j-1]);
		}
	}
	ll l,r;
	while(m--){
		read(l);read(r);
		write(query(l,r));
		putchar('\n');
	}
}

int main() {
	//ios::sync_with_stdio(0);
	//cin.tie(0), cout.tie(0);
	ll T = 1;
	//cin >> T;
	while (T--) {
		solve();
		//cout << '\n';
	}
	return 0;
}
~~~



## 线段树

### 线段树基础与模版

~~~c++
#include<bits/stdc++.h>
#include<unordered_map>
#include <unordered_set>
#include <deque>
#include<algorithm>
#define ll long long
using namespace std;

const ll N = 1e5;

ll tree[N * 4];
ll tag[N * 4];
ll arr[N * 4];
ll ls(ll p) { return p << 1; }
ll rs(ll p) { return p << 1 | 1; }

//从下往上传值
void push_up(ll p) {
    tree[p] = tree[ls(p)] + tree[rs(p)];        //求和
    //tree[p] = min(tree[ls(p)], tree[rs(p)]);  //求最值
}

//建立线段树
void build(ll p, ll pl, ll pr) {
    if (pl == pr) { tree[p] = arr[pl]; return; }
    ll mid = (pl + pr) / 2;
    build(ls(p), pl, mid);
    build(rs(p), mid + 1, pr);
    push_up(p);
}

void addtag(ll p, ll pl, ll pr, ll d) {
    tag[p] += d;
    tree[p] += d * (pr - pl + 1);
}

void push_down(ll p, ll pl, ll pr) {
    if (!tag[p]) { return; }
    ll mid = (pl + pr) / 2;
    addtag(ls(p), pl, mid, tag[p]);
    addtag(rs(p), mid + 1, pr, tag[p]);
    tag[p] = 0;
}

//区间[L,R]进行修改并更新线段树
void update(ll L, ll R, ll p, ll pl, ll pr, ll d) {
    if (L <= pl && pr <= R) {
        addtag(p, pl, pr, d);
        return;
    }
    push_down(p, pl, pr);
    ll mid = (pl + pr) / 2;
    if (L <= mid) { update(L, R, ls(p), pl, mid, d); }
    if (R > mid) { update(L, R, rs(p), mid + 1, pr, d); }
    push_up(p);
}


//查询区间
ll query(ll L, ll R, ll p, ll pl, ll pr) {
    if (L <= pl && pr <= R) { return tree[p]; }
    push_down(p, pl, pr);
    ll res = 0;
    ll mid = (pl + pr) / 2;
    if (L <= mid) { res += query(L, R, ls(p), pl, mid); }
    if (R > mid) { res += query(L, R, rs(p), mid + 1, pr); }
    return res;
}


void solve() {
    ll n,m;
    cin >> n >> m;
    for (ll i = 1; i <= n; i++) { cin >> arr[i]; }
    build(1, 1, n);
    while (m--){
        ll flag;
        cin >> flag;
        if (flag == 1) {
            ll l, r, k;
            cin >> l >> r >> k;
            update(l, r, 1, 1, n, k);
        }
        else if (flag == 2) {
            ll l, r;
            cin >> l >> r;
            cout << query(l, r, 1, 1, n) << endl;
        }      
    }
    
}

int main() {
    ll T = 1;
    //cin >> T;
    while (T--) {
        solve();
        cout << endl;
    }
    return 0;

}
~~~

### Interval Selection（线段树解决区间交集）

题目链接：[ Interval Selection](https://ac.nowcoder.com/acm/contest/81602/D)

~~~c++
#include <bits/stdc++.h>
#define ll long long

using namespace std;

const ll N = 2e5 + 10, mod = 1e9 + 7;


ll n, k, a[N];
ll lst[N], pk1[N],pk[N],tree[N<<4],mi[N<<4],tag[N<<4];

//预处理找出所有i的对应的前k个的下标(pk)与前k+1个的下标(pk1)以及前1个的下标(lst)
void Init() {
	map<ll, vector<ll>>mp;
	for (ll i = 1; i <= n; i++) { mp[a[i]].push_back(i); }
	for (ll i = 0; i <= n; i++) { lst[i] = pk[i] = pk1[i] = 0; }
	for (auto p : mp) {
		vector<ll>t = p.second;
		ll len = t.size();
		for (ll i = 0; i < len; i++) {
			if (i - 1 >= 0) { lst[t[i]] = t[i - 1]; }
			if (i - k + 1 >= 0) { pk[t[i]] = t[i - k + 1]; }
			if (i - k >= 0) { pk1[t[i]] = t[i - k]; }
		}
	}
}

//寻找区间内0的个数，记录最小值，没有比0更小的了
//因此如果满足最小为0的时候才把最小值个数处理
//线段树维护最小值和最小值的个数
//查询时判断最小值是否为0，不是0则这个区间内肯定没0
//线段树部分-------------------------------------------------------------
void push_up(ll p) {
	tree[p] = 0;
	mi[p] = min(mi[p << 1], mi[p << 1 | 1]);
	if (mi[p] == mi[p << 1]) { tree[p] += tree[p << 1]; }
	if (mi[p] == mi[p << 1 | 1]) { tree[p] += tree[p << 1 | 1]; }
}

void push_down(ll p) {
	if (!tag[p]) { return; }
	mi[p << 1] += tag[p], mi[p << 1 | 1] += tag[p];
	tag[p << 1] += tag[p], tag[p << 1 | 1] += tag[p];
	tag[p] = 0;
}

void build(ll p, ll l, ll r) {
    tag[p]=0;//细节注意，多组样例，如果把tag写在l==r判断里不会将全部tag初始化，而mi和tree会被后面的push_up覆盖，因此不用管
	if (l == r) { mi[p] = 0; tree[p] = 1;return; }
	ll mid = l + r >> 1;
	build(p << 1, l, mid);
	build(p << 1 | 1, mid + 1, r);
	push_up(p);
}

void update(ll p, ll l, ll r, ll L, ll R,ll d) {
	if (L > R) { return; }	//细节注意，因为R可能会为0，使得L>R，此时无需进行更新操作，直接退出
	if (L <= l && r <= R) {mi[p] += d;tag[p] += d;return;}
	push_down(p);
	ll mid = l + r >> 1;
	if (L <= mid) { update(p << 1, l, mid, L, R,d); }
	if (mid < R) { update(p << 1 | 1, mid + 1, r, L, R,d); }
	push_up(p);
}

ll query(ll p, ll l, ll r, ll L, ll R) {
	if (L <= l && r <= R) { return (mi[p]==0)?tree[p]:0; }
	push_down(p);
	ll mid = l + r >> 1;
	ll res = 0;
	if (L <= mid) { res+=query(p << 1, l, mid, L, R); }
	if (mid < R) { res+=query(p << 1 | 1, mid + 1, r, L, R); }
	return res;
}
//---------------------------------------------------------------------

//求区间交集技巧：
//如[l1,r1]∩[l2,r2]
//则将[l1,r1]以外的区间：[1,l1)全部+1，(r1,n]全部+1；
//则将[l2,r2]以外的区间：[1,l2)全部+1，(r2,n]全部+1；
//最后统计数组中仍然为0的位置，即为交集部分

//题意符合条件需要[l,r]中所有的数的个数恰好出现k次
//假设：i当前位置，pre[k]从i开始与a[i]元素相等的前k个数的位置
//则在区间(pre[k+1],pre[k]]满足a[i]元素个数为k
//那每次枚举i时，都对目前已有的区间进行交集，最终的交集就是当前位的贡献
//如果当前位的元素区间已经在了，就把旧的区间删掉（区间-1），添加新的区间（区间+1）
void solve() {
	cin >> n >> k;
	for (ll i = 1; i <= n; i++) {cin >> a[i];}
	Init();
	build(1, 1, n);
	ll ans = 0;
	for (ll i = 1; i <= n; i++) {
		if (lst[i]) {
			update(1, 1, n, 1, pk1[lst[i]], -1);
			update(1, 1, n, pk[lst[i]] + 1, lst[i], -1);
		}
		update(1, 1, n, 1, pk1[i], 1);
		update(1, 1, n, pk[i] + 1, i, 1);
		ans += query(1, 1, n, 1, i);
	}
	cout << ans;
}

int main() {
	ll T = 1;
	cin >> T;
	while (T--) {
		solve();
		cout << endl;
	}
	return 0;
}
~~~

### 线段树二分

题目链接：[2940. 找到 Alice 和 Bob 可以相遇的建筑 - 力扣（LeetCode）](https://leetcode.cn/problems/find-building-where-alice-and-bob-can-meet/)

方法一：线段树二分查找第一个大于等于x

~~~c++
class Solution {
public:
    static const int N=5e4+10;
    int tree[N<<4],n;
    vector<int>copy;
    void build(int p,int l,int r){
        if(l==r){tree[p]=copy[l-1];return;}
        int mid=(l+r)>>1;
        build(p<<1,l,mid);
        build(p<<1|1,mid+1,r);
        tree[p]=max(tree[p<<1],tree[p<<1|1]);
    }


    int query(int p,int l,int r,int L,int R){
        if(L<=l && r<=R){return tree[p];}
        int mid=(l+r)>>1;
        int ans=0;
        if(L<=mid){ans=query(p<<1,l,mid,L,R);}
        if(mid<R){ans=max(ans,query(p<<1|1,mid+1,r,L,R));}
        return ans;
    }

	//找到区间范围内第一个大于等于x的位置，二分区间[l~mid]的最大值,如果[l~mid]的最大值大于x，则r=mid-1,否则l=mid+1,最终就	能找到第一个大于等于x的下标位置。
    int check(int l,int r,int x){
        if(query(1,1,n,l,r)<x){return -1;}
        while(l<=r){
            int mid=(l+r)>>1;
            if(query(1,1,n,l,mid)<x){
                l=mid+1;
            }
            else{
                r=mid-1;
            }
        }
        return l-1;
    }


    vector<int> leftmostBuildingQueries(vector<int>& heights, vector<vector<int>>& queries) {
        n=heights.size();
        copy=heights;
        build(1,1,n);
        vector<int>ans;
        for(auto &q:queries){
            int a=q[0],b=q[1];
            if(a>b){swap(a,b);}
            if(a==b){ans.push_back(a);continue;}
            if(heights[a]<heights[b]){ans.push_back(b);continue;}
            int mx=max(heights[a],heights[b])+1;
            int t=check(b+1,n,mx);
            ans.push_back(t);
        }
        return ans;
    }
};
~~~

方法二：直接查找第一个大于等于x，此代码用的是大于x

~~~c++
class Solution {
public:
    static const int N=5e4+10;
    int tree[N<<4],n;
    vector<int>copy;
    void build(int p,int l,int r){
        if(l==r){tree[p]=copy[l-1];return;}
        int mid=(l+r)>>1;
        build(p<<1,l,mid);
        build(p<<1|1,mid+1,r);
        tree[p]=max(tree[p<<1],tree[p<<1|1]);
    }

    //区间寻找第一个大于x
    int query(int p,int l,int r,int L,int x){
        if(tree[p]<=x){return -1;}	//如果区间最大值小于等于x则证明区间内无大于x的，直接返回-1
        if(l==r){return l-1;}
        int mid=(l+r)>>1;
        //如果存在交集，就先靠左找
        if(L<=mid){
            int ans=query(p<<1,l,mid,L,x);
            if(ans!=-1){return ans;}	//如果找到了直接返回
        }
        //否则如果左侧找不到就去右侧找
        return query(p<<1|1,mid+1,r,L,x);
    }

    vector<int> leftmostBuildingQueries(vector<int>& heights, vector<vector<int>>& queries) {
        n=heights.size();
        copy=heights;
        build(1,1,n);
        vector<int>ans;
        for(auto &q:queries){
            int a=q[0],b=q[1];
            if(a>b){swap(a,b);}
            if(a==b || heights[a]<heights[b]){ans.push_back(b);continue;}
            int mx=max(heights[b],heights[a]);
            int t=query(1,1,n,b+1,mx);
            ans.push_back(t);
        }
        return ans;
    }
};
~~~





## 树状数组

### 模版(区改单查)

题目链接[Problem - 1556 (hdu.edu.cn)](https://acm.hdu.edu.cn/showproblem.php?pid=1556)

~~~
#include <bits/stdc++.h>
#define ll long long
#define lowbit(x) x&-x 
using namespace std;

const ll N=1e5+10,mod=1e9+7;


ll n;
ll tree[N];		//存差分数组，通过差分实现区改，
void update(ll x,ll d){
	while(x<N){
		tree[x]+=d;
		x+=lowbit(x);
	}
}

//单点a[i]=差分数组前i项的和
ll sum(ll x){
	ll ans=0;
	while(x>0){
		ans+=tree[x];
		x-=lowbit(x);
	}
	return ans;
}

void solve(){
	while(1){
		cin >> n;
		if(n==0){break;}
		memset(tree,0,sizeof(tree));
		for(ll i=1;i<=n;i++){
			ll l,r;
			cin >> l >> r;
			update(l,1);
			update(r+1,-1);
		}
		for(ll i=1;i<=n;i++){
			cout << sum(i) << " ";
		}
		cout << endl;
	}
}

int main(){
	ll T=1;
	//cin >> T;
	while(T--){
		solve();
		//cout << endl;
	}
	return 0;
}
~~~

### 模版（区查区改）

题目链接：[P3372 【模板】线段树 1 - 洛谷 | 计算机科学教育新生态 (luogu.com.cn)](https://www.luogu.com.cn/problem/P3372)

~~~
#include <bits/stdc++.h>
#define ll long long
#define lowbit(x) x&-x 
using namespace std;

const ll N=1e5+10,mod=1e9+7;

ll n,m,a[N],tree1[N],d[N],tree2[N];

void update1(ll x,ll w){while(x<N){tree1[x]+=w;x+=lowbit(x);}}
void update2(ll x,ll w){while(x<N){tree2[x]+=w;x+=lowbit(x);}}
ll sum1(ll x){ll ans=0;while(x>0){ans+=tree1[x];x-=lowbit(x);}return ans;}
ll sum2(ll x){ll ans=0;while(x>0){ans+=tree2[x];x-=lowbit(x);}return ans;}

void update(ll l,ll r,ll x){
	update1(l,x);
	update1(r+1,-x);
	update2(l,(l-1)*x);
	update2(r+1,r*-x);
}

ll query(ll l,ll r){
	ll ans=(r*sum1(r)-sum2(r))-((l-1)*sum1(l-1)-sum2(l-1));
	return ans;
}


//运用了公式推出
//a1+a2+a3+a4+……+ak=k*(d1+d2+d3+……+dk)-((1-1)*d1+(2-1)*d2+(3-1)*d3+……+(k-1)*dk);
//因此用两个树状数组，一个计算d1~dk的和,已经计算(1-1)*d1~(k-1)*dk的和 
void solve(){
	ll n,m;
    cin >> n >> m;
    for (ll i = 1; i <= n; i++) { 
		cin >> a[i];
		d[i]=a[i]-a[i-1];
		update1(i,d[i]);
		update2(i,(i-1)*d[i]);
	}
    while (m--){
        ll flag;
        cin >> flag;
        if (flag == 1) {
            ll l, r, k;
            cin >> l >> r >>k;
            update(l,r,k);
        }
        else if (flag == 2) {
            ll l, r;
            cin >> l >> r;
            cout << query(l,r) <<endl;
        }      
    }
}

int main(){
	ll T=1;
	//cin >> T;
	while(T--){
		solve();
		//cout << endl;
	}
	return 0;
}
~~~

### 逆序对

题目链接：[P1908 逆序对 - 洛谷 | 计算机科学教育新生态 (luogu.com.cn)](https://www.luogu.com.cn/problem/P1908)

~~~c++
#include <bits/stdc++.h>
#define ll long long
#define lowbit(x) x&-x 
using namespace std;

const ll N=5e5+10,mod=1e9+7;

struct node{ll x,id;}a[N];

ll n,tree[N],rk[N];

ll cmp(node p,node q){
	if(p.x!=q.x){return p.x<q.x;}
	return p.id<q.id;
}

void update(ll x,ll d){while(x<N){tree[x]+=d;x+=lowbit(x);}}
ll sum(ll x){ll ans=0;while(x>0){ans+=tree[x];x-=lowbit(x);}return ans;}

void solve(){
	cin >> n;
	for(ll i=1;i<=n;i++){cin >> a[i].x;a[i].id=i;}
	sort(a+1,a+1+n,cmp);
	for(ll i=1;i<=n;i++){rk[a[i].id]=i;}
	ll ans=0;
	for(ll i=n;i>=1;i--){
		update(rk[i],1);
		ans+=sum(rk[i]-1);
	}
	cout << ans;
}

int main(){
	ll T=1;
	//cin >> T;
	while(T--){
		solve();
		//cout << endl;
	}
	return 0;
}
~~~



### 例题：Mishka and Interesting sum（洛谷）

题目链接：[Mishka and Interesting sum - 洛谷 | 计算机科学教育新生态 (luogu.com.cn)](https://www.luogu.com.cn/problem/CF703D)

~~~c++
#include <bits/stdc++.h>
#define ll long long
using namespace std;

const ll N = 1e6 + 2, mod = 998244353, INF = 0x3f3f3f3f, eps = 1e-7;

ll lowbit(ll x) { return x & (-x); }

struct node {
	ll l, r, id;
}q[N];

ll n, m, a[N], s[N], pre[N], ans[N], tree[N];
map<ll, ll>id;

void add(ll x, ll k) {
	if (x == 0) { return; }
	for (; x <= n; x += lowbit(x)) { tree[x] ^= k; }
}
ll ask(ll x) {
	ll res = 0;
	for (; x; x -= lowbit(x)) { res ^= tree[x]; }
	return res;
}

bool cmp(node x, node y) {
	return x.r < y.r;
}


//区间内: 1 1 4 2 3 4 5
//偶数个的异或和为：1^4
//偶数个的异或和:区间内不同数的异或^区间内所有数异或和
//(1^1^4^2^3^4^5)^(1^2^3^4^5)=1^4
void solve() {
	cin >> n >> m;
	for (ll i = 1; i <= n; i++) {
		cin >> a[i];
		s[i] = s[i - 1] ^ a[i];	//异或前缀快速求区间内的异或值
		pre[i] = id[a[i]];		//记录当前位的数的上一个位置
		id[a[i]] = i;
	}
	for (ll i = 1; i <= m; i++) {
		q[i].id = i;
		cin >> q[i].l >> q[i].r;
	}
	sort(q + 1, q + 1 + m, cmp);
	ll t = 0;
	for (ll i = 1; i <= m; i++) {
		while (t < q[i].r) {
			t++;
			add(t, a[t]);
			add(pre[t], a[pre[t]]);
		}
		ans[q[i].id] = ask(q[i].r) ^ ask(q[i].l - 1) ^ s[q[i].r] ^ s[q[i].l - 1];
	}
	for (ll i = 1; i <= m; i++) {
		cout << ans[i] << endl;
	}
}

int main() {
	ios::sync_with_stdio(0); cin.tie(NULL); cout.tie(NULL);
	ll T = 1;
	//cin >> T;
	while (T--) {
		solve();
		//cout << endl;
	}
	return 0;
}
~~~

### 查询区间内大于等于x的个数(码蹄集)

题目链接：https://www.matiji.net/exam/brushquestion/49/4497/C2CBD34082148550EF198C50D10DBDC7

~~~c++
#include <bits/stdc++.h>
#define ll int
#define lowbit(x) x&-x
using namespace std;

const ll N = 1e6 + 10, mod = 1e9 + 7;

struct query {ll l, r, x, old;}q[N];    //离线处理的q数组，区间l~r,比x大的,old为排序前的下标
struct arr {ll x, old;}a[N];            //原数组a,x为a[i]的值，old为排序前的下标

//tree树状数组
//res存q个询问的答案
//vis判断比这个大的数目前处理过了没有，因为我们只需要处理一次
ll n, m, vis[N], tree[N], res[N];       
ll cmp1(query aa, query bb) { return aa.x > bb.x; }
ll cmp2(arr aa, arr bb) { return aa.x < bb.x; }
void update(ll x, ll d) { while (x < N) { tree[x] += d; x += lowbit(x); } }
ll ask(ll x) { ll ans = 0; while (x > 0) { ans += tree[x]; x -= lowbit(x); }return ans; }

//直接看官方视频题解就能理解代码了
void solve() {
	cin >> n >> m;
	for (ll i = 1; i <= n; i++) { cin >> a[i].x; a[i].old = i; }
	for (ll i = 1; i <= m; i++) { cin >> q[i].l >> q[i].r >> q[i].x; q[i].old = i; }
	sort(a + 1, a + 1 + n, cmp2);
	sort(q + 1, q + 1 + m, cmp1);
	ll id = n;
	for (ll i = 1; i <= m; i++) {
		if (!vis[q[i].x]) {         //这个数没处理过就处理一下，大于等于这个数的所有下标在树状数组中+1   
			vis[q[i].x] = 1;
			while (id >= 1 && a[id].x >= q[i].x) {
				update(a[id].old, 1);
				id--;
			}
		}
		res[q[i].old] = ask(q[i].r) - ask(q[i].l - 1);
	}
	for (ll i = 1; i <= m; i++) {cout << res[i] << endl;}
}

int main() {
	ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);
	ll T = 1;
	//cin >> T;
	while (T--) {
		solve();
		//cout << endl;
	}
	return 0;
}
~~~

#### 查找区间内小于等于x的个数（外带离散化）

题目链接：https://acm.hdu.edu.cn/showproblem.php?pid=4417

~~~
#include <bits/stdc++.h>
#define ll int
#define lowbit(x) x&-x
using namespace std;

const ll N = 4e5 + 10, mod = 1e9 + 7;

struct query {ll l, r, x, old;}q[N];
struct arr {ll x, old;}a[N];

set<ll>b;		//辅助离散b
map<ll, ll>mp;	//每个数对应的排名
ll n, m, vis[N], tree[N], res[N],rk;
ll cmp1(query aa, query bb) { return aa.x < bb.x; }
ll cmp2(arr aa, arr bb) { return aa.x > bb.x; }
void update(ll x, ll d) { while (x < N) { tree[x] += d; x += lowbit(x); } }
ll ask(ll x) { ll ans = 0; while (x > 0) { ans += tree[x]; x -= lowbit(x); }return ans; }
void Init() { for (ll i = 0; i <= 2*n; i++) { tree[i] = 0; vis[i] = 0; }rk = 0; b.clear(); mp.clear(); }


//细节部分注意，离散的时候查询和原数组的值都要放进去进行排序
//离散后的最多个数为2*N,因此初始化的时候一定要是2*N
void solve() {
	cin >> n >> m;
	Init();
	for (ll i = 1; i <= n; i++) { cin >> a[i].x; a[i].old = i; b.insert(a[i].x); }
	for (ll i = 1; i <= m; i++) { cin >> q[i].l >> q[i].r >> q[i].x; q[i].l++, q[i].r++; q[i].old = i; b.insert(q[i].x); }
	for (ll x : b) {mp[x] = ++rk;}
	for (ll i = 1; i <= n; i++) { a[i].x = mp[a[i].x]; }
	for (ll i = 1; i <= m; i++) { q[i].x = mp[q[i].x]; }
	sort(a + 1, a + 1 + n, cmp2);
	sort(q + 1, q + 1 + m, cmp1);
	ll id = n;
	for (ll i = 1; i <= m; i++) {
		if (!vis[q[i].x]) {
			vis[q[i].x] = 1;
			while (id >= 1 && a[id].x <= q[i].x) {
				update(a[id].old, 1);
				id--;
			}
		}
		res[q[i].old] = ask(q[i].r) - ask(q[i].l - 1);
	}
	for (ll i = 1; i <= m; i++) {cout << res[i] << endl;}
}

int main() {
	ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);
	ll T = 1,Case=1;
	cin >> T;
	while (T--) {
		cout << "Case " << Case << ":" << endl;
		solve();
		cout << endl;
		Case++;
	}
	return 0;
}
~~~





## 主席树

[Problem - 4417 (hdu.edu.cn)](https://acm.hdu.edu.cn/showproblem.php?pid=4417)

区间内查询小于等于x的个数

~~~c++
#include <bits/stdc++.h>
#define ll int

using namespace std;

const ll N = 2e5 + 10, mod = 1e9 + 7;

//因为可持续化线段树创建了n颗树，优化后每颗树占logn的空间，因此总空间位n*logn,假设n为1e6,则log1e6≈20多
//如果是N<<4相当于N*(2^4)=N*16,16<20,肯定不够，开(2^5)=32肯定就够了。因此空间开为N<<5
struct node {
	ll l, r, sum;
}tree[N << 5];

//a[N]:原数组 b[N]:a数组的离散化 cnt:对每个节点分配编号 root[N]:第i颗树的根节点编号
ll n, m, a[N], b[N], cnt, root[N];
map<ll, ll>mp;	//排序去重后让a数组快速找到b数组中同一个数的下标，此操作也可以用二分找

//多组数据初始化
void Init(ll len) {
	for (ll i = 0; i <= len; i++) {
		root[i] = 0;
	}
	cnt = 0;
	mp.clear();
}

//建第一颗线段树，其实不用建
ll build(ll l, ll r) {
	ll id = ++cnt;
	tree[id].sum = 0;
	ll mid = (l + r) >> 1;
	if (l < r) {
		tree[id].l = build(l, mid);
		tree[id].r = build(mid + 1, r);
	}
	return id;
}


//建一颗只有logn节点的新线段树，注意这里x传的是下标
ll update(ll pre, ll l, ll r, ll x) {
	ll id = ++cnt;	//为节点分配编号
	tree[id].l = tree[pre].l;
	tree[id].r = tree[pre].r;
	tree[id].sum = tree[pre].sum + 1;
	ll mid = (l + r) >> 1;
	if (l < r) {
		if (x <= mid) { tree[id].l = update(tree[pre].l, l, mid, x); }
		else { tree[id].r = update(tree[pre].r, mid + 1, r, x); }
	}
	return id;
}

//第v颗线段树与第u颗线段树相减在区间[l,r]去查询小于或等于x的个数，注意这里x传的是下标
ll query(ll u, ll v, ll l, ll r, ll x) {
	if (l == r) { return tree[v].sum - tree[u].sum; }
	ll mid = (l + r) >> 1;
	ll sum = tree[tree[v].l].sum - tree[tree[u].l].sum;
	ll ans = 0;
	if (x <= mid) { ans+= query(tree[u].l, tree[v].l, l, mid, x); }
	else {
		ans += sum;
		ans+= query(tree[u].r, tree[v].r, mid + 1, r, x);			
	}
	return ans;
}


void solve() {
	cnt = 0;
	cin >> n >> m;
	Init(n);
	for (ll i = 1; i <= n; i++) { cin >> a[i]; b[i] = a[i]; }
	//离散三步曲：排序，去重，赋值(方便查找位置)
	sort(b + 1, b + 1 + n);							
	ll size = unique(b + 1, b + 1 + n) - (b + 1);
	for (ll i = 1; i <= size; i++) { mp[b[i]] = i; }

	//建n颗树
	root[0] = build(1, size);			//因为默认初始化为0所以此步其实没必要
	for (ll i = 1; i <= n; i++) {
		root[i] = update(root[i - 1], 1, size, mp[a[i]]);
	}

	while (m--) {
		ll l, r, k;
		cin >> l >> r >> k;
		l++, r++;		//这里下标+1因为题目从0~n-1,此代码线段树从1~n,因此查找的时候把下标都加1
		//找一个小于等于x的，就要找到那个小于等于x的最大值，upper找大于x的，下标减一相当于找到了小于等于x的最大值
		ll pos = upper_bound(b + 1, b + 1 + size, k) - b;
		pos--;
		if (!pos) { cout << 0; continue; }
		cout << query(root[l - 1], root[r], 1, size, pos) << endl;
	}
}

int main() {
	ios::sync_with_stdio(0); cin.tie(0), cout.tie(0);
	ll T = 1,Case=1;
	cin >> T;
	while (T--) {
		cout << "Case " << Case << ":" << endl;
		solve();
		cout << endl;
		Case++;
	}
	return 0;
}
~~~



# 数论部分

## gcd与lcm性质

gcd:greatest common divisor 最大公约数

lcm:least common multiple 最小公倍数

gcd和lcm在数论中非常重要，gcd更为重要一些，在处理lcm相关问题时，大部分情况都要转成gcd问题

### 辗转相除法

辗转相除法(也叫欧几里得算法)是求解gcd最常用的算法

辗转相除法利用了以下定理(a>=b>=0):

​										gcd(a,b)=gcd(a-b,b)		//定理1

​												gcd(a,0)=a				//定理2

推广定理1，假设a>=kb>=0:

​				gcd(a,b)=gcd(a-b,b)=gcd(a-2b,b)=……=gcd(a-kb,b)

​										gcd(a,b)=gcd(a%b,b)

既然如此，就不断往下重复这个过程，a和b总是不断变小的，最终必然得到定理2的公式，从而求出最大公因数

可以证明每次操作至少让数字减小一半，简单证明一下：

假设最开始a=A,b=B,a>=b>=0

1)如果b<=a/2,那么就有a%b<b<a/2

2)如果b>a/2,那么就有a%b=a-b<=a/2

因此，时间复杂度按最坏情况来为log2(A)，实机运行时比这个快的多

代码模版

~~~c++
ll gcd(ll a,ll b){
	return !b?a:gcd(b,a%b);
}
~~~

lcm则要简单的多，利用性质a×b=gcd(a,b)×lcm(a,b)可以得到：

lcm(a,b)=a/gcd(a,b)*b;	//这么写是防止a×b爆数据

代码模版

~~~c++
ll lcm(ll a,ll b){
	return a*gcd(a,b)/b;
}
~~~

### 深究gcd与lcm在唯一分解中的性质

对于两个数字a,b,我们先将他们的质因数分解出来。

![image-20240724160520938](C:\Users\21145\AppData\Roaming\Typora\typora-user-images\image-20240724160520938.png)

这里的p1~pn表示的是a和b的所有质因子的并集，如果某个数字不存在质因子pi,则可以认为pi的指数为0。

举一个例子，当a=12,b=42时。

![image-20240724160738228](C:\Users\21145\AppData\Roaming\Typora\typora-user-images\image-20240724160738228.png)

gcd(a,b)就是a,b唯一分解中，质因子的指数取最小之后的乘积，也就是:

![image-20240724161158833](C:\Users\21145\AppData\Roaming\Typora\typora-user-images\image-20240724161158833.png)



lcm(a,b)就是a,b唯一分解中，质因子的指数取最大之后的乘积，也就是:

![image-20240724161457706](C:\Users\21145\AppData\Roaming\Typora\typora-user-images\image-20240724161457706.png)

这也解释了为什么a×b=gcd(a,b)×lcm(a,b)

![image-20240724161749206](C:\Users\21145\AppData\Roaming\Typora\typora-user-images\image-20240724161749206.png)

因此如果遇到了gcd与lcm相关题目，大部分情况下可以往分解质因数这个方向进行思考

## 因数与质因数与约数

因数分解和质因数分解都是非常重要的数学基础

这两种算法的时间复杂度都是O(根号n)，可以求得某个数字的所有因数或所有质因数。

tip:因数和因子，质因数和质因子是相同概念，仅仅表述不同。

### 因数分解

~~~c++
vector<ll> fac;
void pd(ll x){
	//写n/i目的防止i*i爆数据 
	for(ll i=1;i<=x/i;i++){
		if(x%i){continue;}
		fac.push_back(i);
		if(i!=x/i){fac.push_back(x/i);}	//避免i*i=n时重复放i 
	}
} 
~~~

### 质因数分解

~~~c++
ll id;
ll di[N],zh[N];
void pd(ll x){
	id=0;
	//写n/i目的防止i*i爆数据 
	for(ll i=2;i<=x/i;i++){
		if(x%i){continue;}
		ll z=0;	//指数
		while(x%i==0){x/=i;z++;}
		di[++id]=i;
		zh[id]=z;
	}
	if(x>1){di[++id]=x;zh[id]=1;}
} 
~~~

### 约数定理

约数定理包括约数个数定理和约数和定理（约数又名因数，这下看懂了）

1)约数个数定理

根据质因数分解定理，我们容易推导出约数个数定理

![image-20240724164623929](C:\Users\21145\AppData\Roaming\Typora\typora-user-images\image-20240724164623929.png)

记cnt(N)为N的约数个数定理

![image-20240724164705966](C:\Users\21145\AppData\Roaming\Typora\typora-user-images\image-20240724164705966.png)

解释一下，对于第i个质因子pi,可以选择0~ai个，即有(ai-0+1)种选法,根据排列组合进行相乘。

2）约数和定理

![image-20240724165453751](C:\Users\21145\AppData\Roaming\Typora\typora-user-images\image-20240724165453751.png)

![image-20240724165509591](C:\Users\21145\AppData\Roaming\Typora\typora-user-images\image-20240724165509591.png)

![image-20240724165522339](C:\Users\21145\AppData\Roaming\Typora\typora-user-images\image-20240724165522339.png)

可以发现虽然公式是O1，但分解质因数还是根号n，那我直接在分解质因数的时候就可以把约数个数和约数和算出来了。

没错，对于一个数据来说确实如此，但如果是一个范围内的数据的话，就需要依靠欧拉筛等筛法与公式进行优化了。



## 素数算法

### 欧式筛法

#### 模版

~~~c++
ll isprime[N];
ll prime[N];
ll n;
ll cnt;

void euler(){
    memset(isprime, true, sizeof(isprime)); // 先全部标记为素数
    isprime[1] = 0,isprime[0]=0; 
    for(ll i = 2; i <= n; i++) // i从2循环到n（外层循环）
    {
        if(isprime[i]) {prime[++cnt] = i;}
        // 如果i没有被前面的数筛掉，则i是素数
        for(ll j = 1; j <= cnt && i * prime[j] <= n; j++){
            isprime[i * prime[j]] = false;
            if(i % prime[j] == 0) {break;}
            // 最神奇的一句话，如果i整除prime[j]，退出循环
            // 这样可以保证线性的时间复杂度
        }
    }
}

~~~



### 埃式筛法

#### 模版

~~~c++
const int N = 1e5 + 5;
int isprime[N];

void sieve() {
    for (int i = 2; i < N; i++) { isprime[i] = 1; }
    for (int i = 2; i*i <= N; i++) {
        if (!isprime[i]) { continue; }
        for (int j = i * i; j <= N; j += i) {
            isprime[j] = 0;
        }
    }
}
~~~



### 区间筛

#### 模版

~~~c++
#include <bits/stdc++.h>
#define ll long long

using namespace std;

const ll N = 1e6 + 10, mod = 1e9 + 7;

ll cnt_tmp = 0, cnt = 0;
ll is_prime_tmp[N], is_prime[N], prime_tmp[N], prime[N];

//算出[2,根号b]之间的所有质数
void sieve(ll x) {
	ll t = sqrt(x);
	for (ll i = 0; i <= t; i++) { is_prime_tmp[i] = 1; }
	for (ll i = 2; i <= t; i++) {
		if (is_prime_tmp[i]) { prime_tmp[++cnt_tmp] = i; }
		for (ll j = 2; i * j <= t; j++) {
			is_prime_tmp[i*j] = 0;
		}
	}
}

//通过[2-根号b]的质数来筛区间[a,b]的指数
void segment_sieve(ll a, ll b) {
	for (ll i = 0; i < b-a+1; i++) { is_prime[i] = 1; }
	for (ll i = 1; i <= cnt_tmp; i++) {
		ll p = prime_tmp[i];
		//cout << p << endl;
		for (ll j = max(2LL, (a + p - 1) / p) * p; j <= b; j += p) {
			is_prime[j - a] = 0;
		}
	}
}

void solve() {
	ll a, b;
	cin >> a >> b;
	sieve(b);
	segment_sieve(a, b);
	for (ll i = 0; i < b-a+1; i++) {
		if (is_prime[i]) {
			cout << a + i << " ";
		}
	}

}

int main() {
	ios::sync_with_stdio(0);
	cin.tie(0), cout.tie(0);
	ll T = 1;
	cin >> T;
	while (T--) {
		solve();
		cout << '\n';
	}
	return 0;
}
~~~





## 高精度计算

### 高精度加法

~~~c++
string add(string x,string y){
	reverse(x.begin(),x.end());
	reverse(y.begin(),y.end());
	while(x.size()<y.size()){x+='0';}
	while(x.size()>y.size()){y+='0';}
	string z;
	ll carry=0,len=x.size();
	for(ll i=0;i<len;i++){
		z+=(x[i]-'0'+y[i]-'0'+carry)%10+'0';
		carry=(x[i]-'0'+y[i]-'0'+carry)/10;
	}
	if(carry){z+='1';}
	reverse(z.begin(),z.end());
	return z;
}
~~~

### 高精度全家桶

~~~c++
//原文链接：https://blog.csdn.net/actioncms/article/details/105148549

//......................................................................................
int myCompare(string a,string b)//大数比较设定a>b返回1 a<b返回-1 a=b返回0 
{
	if(a.size()!=b.size())//如果字符串长度不同那么长度大的那个就是更大的大数 
	{
		if(a.size()>b.size())
			return 1;
		else 
			return -1;
	}
	else //如果字符串长度相同我们通过直接比较字符串的字典序来判断大小 
	{
		if(a>b)
			return 1;
		else if(a==b)
			return 0;
		else 
			return -1;
	}
}
//......................................................................................
string myAdd(string a,string b)			//高精度加法 
{
	int n=max(a.size(),b.size())+1;    
	vector<int>ans(n,0);//开辟一个足够存储结果的数组，结果的位数最多为 位数最多的那个数的位数加一(考虑进位)
	int i=a.size()-1,j=b.size()-1,k=n-1;//从个位开始通过模拟竖式进行大数加法 
	while(i>=0&&j>=0)//当两数均未加完时 
	{
		ans[k--]=(a[i--]-'0')+(b[j--]-'0');//我们让我们的数组存储每一位相加的结果注意将字符串转为整型数字 
	}
	//检测是否有某个数的高位未加完将其直接存入数组中 
	while(j>=0)
	{
		ans[k--]=(b[j--]-'0');
	}
	while(i>=0)
	{
		ans[k--]=(a[i--]-'0');
	}
	string c="";//创建一个字符串去存储答案 
	for(int i=n-1;i>0;i--)//因为之前的竖式加每一位都没考虑进位所以我们从最后开始检查进位 
	{//因为是加法如果有进位最多也就进一 
		if(ans[i]>=10)//如果大于10说明应该进位那么我们让此位减10它的高一位加1 
		{
			ans[i]-=10;
			ans[i-1]++;
		}
		c.insert(0,1,ans[i]+'0');//处理后的结果转化为字符插入结果字符的首位
	} 
	
	if(ans[0]>0)//检查最最高位是否大于0如果两数相加没有进位那么这一位就是0我们就不必存储它否则则放入字符串 
	{
		c.insert(0,1,ans[0]+'0');
	}
	return c;//返回答案 
}
//......................................................................................
string mySubtract(string a,string b)//高精度减法(整体思想同加法) 
{
	string c="";               //创建一个字符串去存储答案 
	if(myCompare(a,b)==0)      //先比较一下两数大小如果相等我们直接返回0即可 
		return "0";
	if(myCompare(a,b)==-1)//如果a<b那么我们交换两数的值同时在答案字符串中先放入一个负号 
	{
		swap(a,b);
		c.push_back('-');
	}
	int n=a.size();
//开辟一个足够存储结果的数组  减法结果的位数最多为位数最多的那个数的位数我们保证了a为的大数所以就是a的位数 
	vector<int>ans(n,0);
	int i=a.size()-1,j=b.size()-1,k=n-1;//从个位开始模拟竖式减法 
	int t=0;//表示低位的借位情况  0:无借位   1:有借位 
	while(i>=0)                         //同加法一样模拟运算我们知道a是大数所以a遍历完竖式才完成 
	{
	    char nowb;//被减数当前位有数就正常减 没有数说明就是0
	    if(j>=0) nowb=b[j];
	    else nowb='0';
		ans[k]=a[i]-nowb-t;//对应位相减同时减去低位的借位
		if(ans[k]<0)//如果结果小于0 则向高位借一位
		{
			t=1;
			ans[k]+=10;
		} 
		else t=0;  //否则向高位无借位
		k--,i--,j--;  //继续判断高一位
	}
	bool flag=true;//这里与加法不同高位可能出现多个零开头的情况我们需要找到第一不是零的地方再存数 
	for(int i=0;i<n;i++)
	{
		if(flag&&ans[i]==0)//如果当前数为0且未存数则不执行存数操作 
			continue;
		flag=false;        //一旦存入数更改标志位 
		c.push_back(ans[i]+'0');	
	}	 
	return c;              //返回结果 
}
//......................................................................................
string myMultiply(string a,string b)//高精度乘法 
{
	if(a=="0"||b=="0")   //其中有一个为0时直接返回0 
		return "0";
	vector<int>ans;      //开辟一个足够存储结果的数组
	int n=a.size(),m=b.size();
	ans.resize(n+m,0);   //乘法结果的位数最多为两数的位数之和
	for(int i=0;i<n;i++) //这里从高位开始和从低位开始无所谓我们将两数相乘的结果不考虑放到对应位上最后去检测进位即可 
	{                    //例如个位乘百位的结果 以及十位乘十位的结果 都放到百位上 
		for(int j=0;j<m;j++)
		{
			ans[i+j+1]+=(a[i]-'0')*(b[j]-'0');
		}
	}
	for(int i=n+m-1;i>0;i--)      //我们从低位开始检查进位 
	{
		if(ans[i]>=10)            //如果大于10说明有进位但乘法的进位不一定只进1 
		{
			ans[i-1]+=(ans[i]/10);//高位加地位的高于个位部分 
			ans[i]%=10;           //低位对十求余 
		}
	} 
	string c ="";   //创建字符串存储答案 
	bool flag=true; //同减法一样找到第一个不是0的位置开始存数 
	for(int t=0;t<n+m;t++)
	{
		if(flag&&ans[t]==0)
			continue;
		flag=false;
		c.push_back(ans[t]+'0');	
	}	 
	return c;      //返回答案 
}
//......................................................................................
vector<string>myDivide(string a,string b)//高精度除法
{
	vector<string>ans(2,"0");//先创建两个字符串空间去存储答案一个存商一个存余数 
	if(myCompare(a,b)==-1)   //如果被除数比除数小商为0余数为a返回答案即可 
	{
		ans[1]=a;
		return ans;
	}
	else if(myCompare(a,b)==0)//如果被除数与除数相等商为1余数为0返回答案即可
	{
		ans[0]="1";
		return ans;
	}
	else              //否则我们需要模拟除法的竖式来进行计算 
	{
		string res="";//创建存储商的字符串 
		int m1=a.size(),m2=b.size();
		string a1=a.substr(0,m2-1);
		for(int i=m2-1;i<m1;i++)     //模拟竖式从高位开始依次取数减去除数能减几个该位商的当前位就是几 
		{	
			if(a1=="0")			     //如果a1为0为了防止a1出现0开头的情况我们将它清空 
				a1=""; 
			a1.push_back(a[i]);      //我们从被除数中取一个数放入a1继续减 
			int e=0;
			while(myCompare(a1,b)>=0)//当a1大于等于除数时便一直减同时e累加 
			{
				e++;
				a1=mySubtract(a1,b);
			}
			if(res.size()||e)        //如果res不为空或者e不为0我们存储他 
				res.push_back(e+'0');
		}
		ans[0]=res;   //最后res就是商 
		ans[1]=a1;    //a1就是余数 
		return ans;   //返回答案 
	}
}
//......................................................................................
string myFactorial(string a)//高精度阶乘 
{/*我们还可以利用高精度减法和乘法实现大数的阶乘(最大可运行出10000左右的阶乘)*/
	if(a=="1")
		return a;
	else 
		return myMultiply(a,myFactorial(mySubtract(a,"1")));
}
————————————————
~~~



## 逆元

### 模版

~~~c++
const ll N = 1e5 + 3, mod = 998244353;

ll f[N];

//预处理1!~n!
void Init(){
	f[0]=f[1]=1;
	for(ll i=2;i<N;i++){
		f[i]=(f[i-1]*i)%mod;
	}
}

//欧几里得
void ex_gcd(ll a,ll b,ll &x,ll &y,ll &d){
	if(!b){d=a,x=1,y=0;return;}
	ex_gcd(b,a%b,y,x,d);
	y-=x*(a/b);
}

//求逆元，t为逆元值，p为mod值
ll inv(ll t,ll p){
	ll d,x,y;
	ex_gcd(t,p,x,y,d);
	return d==1?(x%p+p)%p:-1;
}

//Cab,a个里选b个
ll C(ll a,ll b){
	ll ans=f[a]*inv(f[b],mod)%mod*inv(f[a-b],mod)%mod;
	return ans;
}

//快速幂
ll ksm(ll a,ll b){
	ll res=1;
	while(b){
		if(b&1){res=(res*a)%mod;}
		b>>=1;
		a=(a*a)%mod;
	}
	return res%mod;
}
~~~



~~~c++
#include<bits/stdc++.h>
#define ll long long
using namespace std;

const ll N = 3e5 + 10, mod = 998244353, MX = 2e18;

ll n, x[2], y[2],p,q;
ll f[N],inf[N];	//预处理阶乘和阶乘的逆元

//快速幂
ll ksm(ll a, ll b) {
	ll res = 1;
	while (b) {
		if (b & 1) { res = res * a % mod; }
		b >>= 1;
		a = a * a % mod;
	}
	return res % mod;
}

//逆元
ll inv(ll a) {
	return ksm(a, mod - 2);
}

//阶乘
void fun() {
	f[0] = 1, inf[0] = 1;
	for (ll i = 1; i <= N; i++) {
		inf[i] = inf[i - 1] * inv(i) % mod;
		f[i] = f[i - 1] * i % mod;
	}
}

//排列组合
ll C(ll a, ll b) {
	return f[a] * inf[b] % mod * inf[a - b] % mod;
}

void solve() {
	cin >> x[0] >> y[0] >> x[1] >> y[1] >> n >> p >> q;
	ll cx = x[1] - x[0], cy = y[1] - y[0];
	if (cx + cy != n || cx < 0 || cy < 0) { cout << 0; return; }
	ll ans = C(n, cx) * ksm(p * inv(100) % mod, cx)%mod *  ksm(q * inv(100) % mod, cy)%mod;
	cout << ans;
}

int main() {
	ios::sync_with_stdio(0);
	cin.tie(0);
	cout.tie(0);
	fun();
	ll T = 1;
	cin >> T;
	while (T--) {
		solve();
		cout << endl;
	}
	return 0;

}
~~~

## 哥德巴赫猜想

~~~c++
//1.任一大于2的偶数都可写成两个质数之和。（规定1为质数的情况下）
//2.任一大于5的整数都可写成三个质数之和。
~~~



## 极角排序,凸包,凹包



### 凸包

模版例题：[P2742 [USACO5.1\] 圈奶牛Fencing the Cows /【模板】二维凸包 - 洛谷 | 计算机科学教育新生态 (luogu.com.cn)](https://www.luogu.com.cn/problem/P2742)

~~~c++
#include <bits/stdc++.h>
#define ll long long

using namespace std;

const ll N = 1e6 + 10, mod = 1e9 + 7;

struct dot{
	double x,y;
};
dot p[N],st[N];		//存点与模拟栈 
//计算叉乘 向量(a1->a2)×向量(b1->b2) 
double cross(dot a1,dot a2,dot b1,dot b2){return (a2.x-a1.x)*(b2.y-b1.y)-(b2.x-b1.x)*(a2.y-a1.y);}

double dis(dot a,dot b){return sqrt((a.y-b.y)*(a.y-b.y)*1.0+(a.x-b.x)*(a.x-b.x))*1.0;}

ll cmp(dot a,dot b){
	ll t=cross(p[1],a,p[1],b);
	if(t>0){return 1;}
	if(t==0 && dis(p[1],a)<dis(p[1],b)){return 1;};	//如果在同一条直线上就判断距离 
	return 0;
}

ll n;
void solve() {
	cin >> n;
	for(ll i=1;i<=n;i++){cin >> p[i].x >> p[i].y;}
	//p[1]得到坐标最小的点，一定在凸包内 
	for(ll i=2;i<=n;i++){
		if((p[i].y<p[1].y) || (p[i].y==p[1].y && p[i].x<p[1].x)){
			swap(p[i].y,p[1].y);
			swap(p[i].x,p[1].x);
		}
	}
	//以p[1]为原点进行极角排序 
	sort(p+2,p+1+n,cmp);
	ll cnt=1;
	st[1]=p[1];
	for(ll i=2;i<=n;i++){
		while(cnt>1 && cross(st[cnt-1],st[cnt],st[cnt],p[i])<=0){
			cnt--;
		} 
		cnt++;
		st[cnt]=p[i];
	}
	st[cnt+1]=p[1];	//形成一个环 
	double ans=0;
	for(ll i=1;i<=cnt;i++){
		ans+=dis(st[i],st[i+1]);
	}
	printf("%.2lf",ans);
	
}

int main() {
	ios::sync_with_stdio(0);
	cin.tie(0), cout.tie(0);
	ll T = 1;
	//cin >> T;
	while (T--) {
		solve();
		//cout << '\n';
	}
	return 0;
}
~~~

#### 求凸包的模版

~~~c++
#include <bits/stdc++.h>
#define ll long long

using namespace std;

const ll N = 3e5 + 10, mod = 998244353;

struct Point {
	double x, y;
};

//计算叉乘 向量(a1->a2)×向量(b1->b2) 
double cross(Point a1, Point a2, Point b1, Point b2) {
	return (a2.x - a1.x) * (b2.y - b1.y) - (b2.x - b1.x) * (a2.y - a1.y);
}

//计算ab两点间距离
double dis(Point a, Point b) {
	return sqrt((a.y - b.y) * (a.y - b.y) + (a.x - b.x) * (a.x - b.x));
}

Point cmp_c;	//按cmp_c这一点对所有点进行极角排序
ll cmp(Point a, Point b) {
	ll t = cross(cmp_c, a, cmp_c, b);
	if (t > 0) { return 1; }
	if (t == 0 && dis(cmp_c, a) < dis(cmp_c, b)) { return 1; }
	return 0;
}

//将点变为凸包 (传参为点集与点个数)
ll Point_num;	//凸包点个数 
vector<Point> ConvexHull(vector<Point>& p, ll len) {
	for (ll i = 2; i <= len; i++) {
		if ((p[i].y < p[1].y) || (p[i].y == p[1].y && p[i].x < p[1].x)) {
			swap(p[i].y, p[1].y);
			swap(p[i].x, p[1].x);
		}
	}
	cmp_c = p[1];
	sort(p.begin() + 2, p.begin() + 1 + len, cmp);
	ll cnt = 1;
	vector<Point>st(len+10);
	st[1] = p[1];
	for (ll i = 2; i <= len; i++) {
		while (cnt > 1 && cross(st[cnt - 1], st[cnt], st[cnt], p[i]) <= 0) {
			cnt--;
		}
		st[++cnt] = p[i];
	}
	st[cnt + 1] = p[1];
	Point_num = cnt;
	return st;
}

ll n;
void solve() {
	cin >> n;
	vector<Point> p(n + 10);
	for (ll i = 1; i <= n; i++) { cin >> p[i].x >> p[i].y; }
	vector<Point>tb = ConvexHull(p, n);
	double ans = 0;
	for (ll i = 1; i <= Point_num; i++) {
		ans += dis(tb[i], tb[i + 1]);
	}
	printf("%.2lf", ans);
}

int main() {
	ios::sync_with_stdio(0);
	cin.tie(0), cout.tie(0);
	ll T = 1;
	//cin >> T;
	while (T--) {
		solve();
		//cout << '\n';
	}
	return 0;
}
~~~



### 求凸包直径与周长

[H-Two Convex Polygons_2024牛客暑期多校训练营9 (nowcoder.com)](https://ac.nowcoder.com/acm/contest/81604/H)

~~~c++
#include <bits/stdc++.h>
#define ll long long
#define PI 3.141592653589793238462643
using namespace std;

const ll N = 3e6 + 10, mod = 998244353;

struct Point {
	double x, y;
};

struct Line {
	Point x, y;
};

//计算叉乘 向量(a1->a2)×向量(b1->b2) 
double cross(Point& a1, Point& a2, Point& b1, Point& b2) {
	return (a2.x - a1.x) * (b2.y - b1.y) - (b2.x - b1.x) * (a2.y - a1.y);
}

//计算ab两点间距离
double dis(Point& a, Point& b) {
	return sqrt((a.y - b.y) * (a.y - b.y) + (a.x - b.x) * (a.x - b.x));
}

Point cmp_c;	//按cmp_c这一点对所有点进行极角排序
ll cmp(Point a, Point b) {
	ll t = cross(cmp_c, a, cmp_c, b);
	if (t > 0) { return 1; }
	if (t == 0 && dis(cmp_c, a) < dis(cmp_c, b)) { return 1; }
	return 0;
}

//将点集变为凸包 (传参为点集与点个数)
vector<Point> ConvexHull(vector<Point>& p, ll len) {
	for (ll i = 1; i < len; i++) {
		if ((p[i].y < p[0].y) || (p[i].y == p[0].y && p[i].x < p[0].x)) {
			swap(p[i].y, p[0].y);
			swap(p[i].x, p[0].x);
		}
	}
	cmp_c = p[0];
	sort(p.begin() + 1, p.begin() + len, cmp);
	ll id = 0;
	vector<Point>st(len+10);
	st[0] = p[0];
	for (ll i = 1; i < len; i++) {
		while (id >= 1 && cross(st[id - 1], st[id], st[id], p[i]) <= 0) {
			id--;
		}
		st[++id] = p[i];
	}
	st[++id] = p[0];
	vector<Point>res(id + 1);
	for (ll i = 0; i <= id; i++) { res[i] = st[i]; }
	return res;
}

//求凸包周长
double get_len(vector<Point>& p) {
	ll len = p.size();
	double res = 0;
	for (ll i = 0; i < len - 1; i++) {
		res += dis(p[i], p[i + 1]);
	}
	return res;
}

//得到点到直线的距离
double DisPL(Point& p, Line& eg) {
	return fabs(cross(eg.x, p, eg.x, eg.y) / dis(eg.x, eg.y));
}

//得到凸包直径
double Diameter(vector<Point>& p) {
	ll len = p.size();
	if (len == 3) { return dis(p[1], p[0]); }
	ll pos = 0;
	double res = 0;
	for (ll i = 0; i < len-1; i++) {
		Line li = { p[i],p[i + 1] };
		while (DisPL(p[pos], li) <= DisPL(p[(pos + 1) % len], li)) {
			pos = (pos + 1) % len;
		}
		res = max({ res,dis(p[i],p[pos]),dis(p[i + 1],p[pos]) });
	}
	return res;
}

//ans=A的周长+B的直径*2*π
void solve() {
	ll n,tx,ty;
	double ans = 0;
	cin >> n;
	vector<Point> p(n);
	for (ll i = 0; i < n; i++) { cin >> tx >> ty;p[i].x=tx;p[i].y=ty; }	//细节把整形赋给double，否则会超时
	vector<Point>A = ConvexHull(p, n);
	ans += get_len(A);
    
    cin >> n;
	p.clear();
	p.resize(n);
	for (ll i = 0; i < n; i++) { cin >> tx >> ty;p[i].x=tx;p[i].y=ty;  }
	vector<Point>B = ConvexHull(p, n);
	double mx_len = Diameter(B);
	ans += 2 * mx_len * PI;
	printf("%.16lf", ans);
}

int main() {
	ios::sync_with_stdio(0);
	cin.tie(0), cout.tie(0);
	ll T = 1;
	cin >> T;
	while (T--) {
		solve();
        putchar('\n');
	}
	return 0;
}
~~~

## 快速傅里叶变换(FFT)

###  **Fine Triplets** 

题目链接：[G - Fine Triplets](https://atcoder.jp/contests/abc392/tasks/abc392_g)

~~~c++
#include<bits/stdc++.h>
#define ll long long
#define lowbit(x) x&-x
using namespace std;


const ll N = 1e6 + 10, mod = 1e9 + 7;
const double PI=acos(-1);


vector<complex<double>> FFT(vector<complex<double>> A,ll n,ll op){
	vector<ll> R(n);
	for(ll i=1;i<n;i++){R[i]=R[i/2]/2+(i&1)*(n/2);}
	for(ll i=0;i<n;i++){
		if(i<R[i]){
			swap(A[i],A[R[i]]);
		}
	}
	for(ll m=2;m<=n;m<<=1){
		complex<double> we({cos(2*PI/m),sin(2*PI/m)*op});
		for(ll i=0;i<n;i+=m){
			complex<double> w({1,0});
			for(ll j=0;j<m/2;j++,w*=we){
				complex<double> x=A[i+j],y=A[i+j+m/2]*w;
				A[i+j]=x+y,A[i+j+m/2]=x-y;
			}
		}
	}
	return A;
}

//返回A与B的卷积
vector<ll> convol(vector<ll> A, vector<ll> B){
	ll na=A.size(),nb=B.size();
	ll n=1;
	while(n<na+nb){n<<=1;}
	vector<complex<double>> AA(n),BB(n),C;
	for(ll i=0;i<na;i++){AA[i]={1.0*A[i],0};}
	for(ll i=0;i<nb;i++){BB[i]={1.0*B[i],0};}
	AA=FFT(AA,n,1);
	BB=FFT(BB,n,1);
	for(ll i=0;i<n;i++){AA[i]*=BB[i];}
	C=FFT(AA,n,-1);
	vector<ll>res(n);
	for(ll i=0;i<n;i++){
		res[i]=C[i].real()/n+0.5;
	}
	return res;
}

void solve() {
	ll n;
	cin >> n;
	vector<ll>p(N);
	for(ll i=1,x;i<=n;i++){
		cin >> x;
		p[x]=1;
	}
	ll ans=0;
	auto C=convol(p,p);
	for(ll i=0;i<N;i++){
		if(p[i]==1){
			ans+=(C[2*i]-1)/2;
		}
	}
	cout << ans;
}


int main() {
	ios::sync_with_stdio(0);
	cin.tie(0); cout.tie(0);
	ll T = 1;
	//cin >> T;
	while (T--) {
		solve();
		//cout << '\n';
	}
	return 0;

}
~~~



# 博弈论

### sg函数

视频链接：https://www.bilibili.com/video/BV1eT411B7A8/?spm_id_from=333.337.search-card.all.click



~~~sg
公平游戏：
1. 两名选手

2. 交替进行某种游戏规定的操作，每操作一次，选手可以在有限的操作（操作必须合法）集合中任选一种。

3. 对于游戏的任何一种可能的局面，合法操作集合只取决于这个局面本身，不取决于其它因素（跟选手，以前的所有操作无关）

4. 如果当前选手无法进行合法的操作，即合法集合为空，则为负。
------------------------------------------------------------------------------
P-position：后手必胜

N-position：先手必胜

定义：

1.无法进行任何移动的局面是P-position。
2.可以移动到P-position的局面是N-position。  
3.所有移动都导致N-position的局面是P-position。

性质：

1. 所有的终结点都是P-position。
2. 从任何N-position操作，至少有一种方式进入P-position。
3. 无论如何操作， 从P-position都只能进入N-position。

~~~

~~~
操作：
1.根据题意暴力打表算出每个数的sg值
2.根据暴力打表找出规律
3.通过规律快速求出a1到an的所有sg值，如果a1^a2^a3……^an≠0，则先手必胜，否则先手必败
~~~

#### 例题一：Dividing Game

[F - Dividing Game (atcoder.jp)](https://atcoder.jp/contests/abc368/tasks/abc368_f)

~~~c++
#include <bits/stdc++.h>
#define ll long long
using namespace std;

const ll N = 1e5 + 10, mod = 998244353;


ll a[N], n;

//根据暴力打表发现一个数的sg值为它的因数个数
ll sg(ll x) {
    ll sum = 0;
    for (ll i = 2; i*i <= x; i++) {
        while (x % i == 0) {
            x /= i;
            sum++;
        }
    }
    if (x != 1) { sum++; x /= x; }
    return sum;
}

void solve() {
    cin >> n;
    ll ans = 0;
    for (ll i = 1; i <= n; i++) { 
        cin >> a[i];
        ll t = sg(a[i]);
        ans ^= t;
    }
    if (ans != 0) {
        cout << "Anna";
    }
    else {
        cout << "Bruno";
    }
	

}

int main() {
	ios::sync_with_stdio(0);
	cin.tie(0), cout.tie(0);
	ll T = 1;
	//cin >> T;
	while (T--) {
		solve();
		//cout << '\n';
	}
	return 0;
}
~~~

#### 例题二：模版题石头问题

题目链接：[P2197 【模板】Nim 游戏 - 洛谷 | 计算机科学教育新生态 (luogu.com.cn)](https://www.luogu.com.cn/problem/P2197)

暴力打表求sg并观察规律

~~~c++
#include <bits/stdc++.h>
#define ll long long
using namespace std;

const ll N = 1e5 + 10, mod = 998244353;


ll a[N], n;

//暴力打表发现一个数的sg就是它本身
ll sg(ll x) {
    if (x == 0) { return 0; }
    set<ll>mex;
    for (ll i = 1; i <= x; i++) {
        mex.insert(x - i);
    }
    ll id = 0;
    for (ll t : mex) {
        if (t != id) {
            return id;
        }
        id++;
    }
    return id;

}

void solve() {
    for (ll i = 1; i <= 1000; i++) {
        cout << i << ":" << sg(i) << endl;
    }
}

int main() {
	ios::sync_with_stdio(0);
	cin.tie(0), cout.tie(0);
	ll T = 1;
	//cin >> T;
	while (T--) {
		solve();
		//cout << '\n';
	}
	return 0;
}
~~~

通过规律快速得出sg并AC

~~~c++
#include <bits/stdc++.h>
#define ll long long
using namespace std;

const ll N = 1e5 + 10, mod = 998244353;

ll t, n;

//打表发现规律，一个数的sg就是它本身
ll sg(ll x) {return x;}

void solve() {
	ll ans = 0;
    cin >> n;
    for (ll i = 1; i <= n; i++) {
		cin >> t;
		ans ^= sg(t);
    }
	if (ans) { cout << "Yes"; }
	else { cout << "No"; }
}

int main() {
	ios::sync_with_stdio(0);
	cin.tie(0), cout.tie(0);
	ll T = 1;
	cin >> T;
	while (T--) {
		solve();
		cout << '\n';
	}
	return 0;
}
~~~





# 通用模版

## 快速输入与快速输出

~~~c++
//快速输入(整数)
inline void read(ll& a){
	ll s = 0, w = 1;
	char ch = getchar();
	while (ch < '0' || ch>'9'){if (ch == '-')w = -1;ch = getchar();}
	while (ch >= '0' && ch <= '9'){s = s * 10 + ch - '0';ch = getchar();}
	a = s * w;
}

//快速输出(整数)
void write(ll x){
    if(x<0)putchar('-'),x=-x;
    if(x>9)write(x/10);
    putchar(x%10+'0');
    return;
}

~~~

## 巧妙位运算

![image-20240521142720337](C:\Users\21145\AppData\Roaming\Typora\typora-user-images\image-20240521142720337.png)

## __int128输入与输出

~~~c++
inline void read(__int128& x)
{
	long long tmp; cin >> tmp;
	x = tmp;
}

inline void write(__int128 x)
{
	if (x < 0) cout << '-', x = -x;
	if (x > 9) write(x/10);
	cout << char(x%10+'0');
}

~~~



# 赛后总结部分

### 024年1月17日第一学期上半pta算法期末考试

#### 第一题：程序员买包子

反思：看题太慢

#### 第二题：猜帽子游戏

反思：太久没做pta，格式限制死死的，末尾不能带换行符也算是一种格式。

#### 第三题：剪切粘贴

反思：题目较长选择跳过没问题，但是最后30分钟应该回来做这个，这个属于简单题，而不是选择做繁琐的倒数第三题。补题时没仔细审题，没有看到若粘贴不了就粘贴到末尾这一句

#### 第七题：分寝室

反思：题目明确了男女寝人数不能小于2，赛时限制了男生的人数不小于2，但是运算时忘记限制了女生人数，注意考虑特判和限制条件。

#### 第八题：老板的作息表

反思：

1.对于时间输入，例如2005:08:30，就用scanf且直接用%d即可，例：scanf("%d:%d:%d",&h,&m,&s);	c语言会自动将05转化成5整形类型。

2.对于一串字符串输入且中间带空格，需要用getline(),但是假设之前有cin,回车后有一个\n还存着，这时候如果getline,getline会自动把\n拿掉，为了解决这个问题，可以在cin和getline之间放一个cin.gets();将\n提前拿掉，之后就正常了。

例子代码：

~~~c++
void solve() {
    int n; cin >> n;	//例如输入123回车，此时n=123，但还有个回车(\n)没被拿掉
    string str;
    cin.get();			//cin与getline之间插入一个cin.get()，将\n拿掉
    getline(cin, str);	//之后就可以正常输入带空格的字符串了
    getline(cin, str);
    cout << str;
}
~~~

#### 第九题：堆宝塔

反思：也是审题不仔细问题，忘记把B放给A导致wa一发

#### 第十题：寻宝图

反思：

1.注意输入样例，这里输入样例01都是连起来的，说明输入的时候无间隔符，输入的是字符串，要自己拆分

2.数组开小了，可以用resize()对全局变量数组vector进行分配大小，例如：

~~~c++
vector <vector <ll>>arr;
void solve() {
    ll row, col; cin >> row >> col;
    arr.resize(row);
    for (ll i = 0; i < row; i++) { arr[i].resize(col); }
    for (ll i = 0; i < row; i++) {
        for (ll j = 0; j < col; j++) {
            arr[i][j] = i * j;
            cout << arr[i][j] << " ";
        }
        cout << endl;
    }
}
~~~

### IOI与OI赛后总结

#### IOI总结

1.最重要的是先拿大分，一些比较恶心的特判可能只占一小部分分数，不用太过在意，先往下写，等签到都结束后再稍微想想漏的分数

2.大模拟要绕过，会占很多时间，甚至可能漏掉一些细节，等无题能做时再做

3.最后预留10分钟进行骗分和补分，骗分要讲究技巧，补分先补可能性大的题

#### OI总结

1.没有把握的题用暴力拿分，对于1e9这种数据的暴力可以开1e6赚分

2.不要乱猜，对于自己思路最好进行验证，用多组数据证明，并且要重点注意边界问题

3.最后留几分钟进行骗分。

4.对于n*(n-1)这种乘法运算，注意是否会超long long数据。



## 2024年浙江ZCPC省赛

1.欢迎来到浙江，这里很卷，你与对手的实力差距比你想象的还大，此次省赛好比婴儿与大力士掰手腕，输的一塌糊涂。

2.不能只会签到，还是要学习写难题，学习新的算法，更难的算法，可能现在还不是时候，但难题是我们一定要碰并且学习的，否则只有被薄纱的份。

3.英语得学一下了，翻译真的很痛苦。

4.当一个思想wa掉后，就别再顺着队友的思想了。要去找茬，找队友的茬，可能队友的思想是对的，只是细节问题，但这是队友考虑的事，不要陪着队友考虑细节问题，去找思路漏洞，最好再想新方法。

5.重开代码写时不能无脑复制部分代码，要看看复制的代码是否万无一失，没把握的情况选择重写，因为时间很充裕，改bug的时间在70%的情况下远远大于再写一遍的时间。

6.脑子一定要清醒，不要急，一急什么都写不好。

7.这次是真的菜的跟鸡一样，让我们如同井底之蛙一样，菜就多练。





## 2024年天梯赛总结

1.天梯赛采用DEV编译器，因为方便与习惯的关系需要调整为c++11，进入下图操作后输入-std=c++11

![image-20240422145324705](C:\Users\21145\AppData\Roaming\Typora\typora-user-images\image-20240422145324705.png)



2.平常用Visual Studio 2022写代码，因为功能强大，所以在写的时候错误会提醒，但是DEV不会，比赛用DEV导致一些细节错误。教训就是平常写代码用DEV或者VS。（这次比赛一个逗号和分号的细节问题，你说气不气）

3.一些不怎么用到但又需要用到的官方函数要去写笔记了，比赛中的 replace() 替换字符串函数就不会用了。

4.一些手写的算法，数据结构以及实现其他功能的函数要多些多背板了，平常就别再贴板子了，自己无模版手打一遍。（例如按某字符把字符串切成几段，迪杰斯特拉dij，埃式筛法，欧式筛法）

5.速度！速度！速度！很重要，此次天梯赛速度没跟上，还有比赛没结束就别放松，抓紧时间，平常练习时进行计时。

6.注意做题心态，别因为上一题有几分没过做下一题就被打扰到。

7.永远的最后一句：菜，就多练

## 2024年蓝桥杯省赛总结

1.知识点补充，“期望值”很早就出现过了，比赛的时候虽然猜对了，但是还是要去学习一下，包括其他的一些知识点名词。

2.平常别贴模版了，算法要自己手打，最好进行一个规划单独练习某部分算法，不仅可以手打模版，并且还要深入学习，不能只学一个模版，导致这次比赛大多都是暴力骗分。

3.发现做完后剩余时间还算充裕，可以学习和写一下对拍，这次比赛不会写对拍，编的样例都很弱，不知道自己是否写错。

4.一些难的算法可以先搁置着，先把基础的算法（dij，并查集，欧筛……）打熟悉，水平提升了就一定要接触难题了（当然对于目前来说还是后话）。

5.永远的最后一句：菜，就多练



## 2024年蓝桥杯国赛总结

1.蓝桥杯尼玛的就是个圈钱杯

2.样例复制时一定要看一下是否正确，第一题还好检查了一下，不然连第一题送分都寄了。

3.当题目重点提示了某信息，并且你也找到了该信息所对应的特判或者处理方式，就要特判掉或者处理掉，不能因为这样操作麻烦带着“应该不用这样”的侥幸心理而放弃处理。

4.不要因为暂时不理解题意而认为某题很难而跳过，回过头来还是要写的，这次第二题一道很简单的暴力就能找出答案结果因为闲烦，看不懂而放弃。最后的时间也不去做，不应该。

5.比赛剩余1小时不要急，选择性价比高的策略，把没写过的骗分骗一下，再去尝试做可能能做出来的，这次套圈问题有二分答案+暴力的稳定思路，可是因为着急而放弃了，因此最后的时间也要保持头脑清晰。

6.不要害怕暴力思想太过超时就放弃，哪怕是n的4次，n的6次，都比只输出一个0,1骗的好。

7.能感觉到难的一点思路都没有的就及时写暴力，不要浪费时间在想一道无把握的题目。

8.永远的最后一句：菜，就多练



# 细节部分

## memset细节

为地址str开始的n个字节赋值c，注意：是逐个字节赋值，str开始的n个字节中的每个字节都赋值为c。
（1） 若str指向char型地址，value可为任意字符值；
（2） 若str指向非char型，如int型地址，要想赋值正确，value的值只能是-1或0，因为-1和0转化成二进制后每一位都是一样的，设int型占4个字节，则-1=0XFFFFFFFF, 0=0X00000000。

稳一点用for最好

原文链接：https://blog.csdn.net/Supreme7/article/details/115431235

## 小细节

~~~c++
（1）DEV调c++11:  -std=c++11

（2）double类型过大时，cout输出会以科学计数法输出

（3）如果mod值为常数，例如const int mod=1e9+7;这种，编译器会优化，但是mod值是变量，就会耗一些时间。
即使是常数取模运算，加减法也比常数取模运算来的快
虽然两种方法的时间复杂度都算O(1),但是1*O(1)与10*O(1)的复杂度也是有差距的。
如果TLE了可以尝试用加减法代替取模试试。

（4）a/b % Mod = a % (b * Mod) /b % Mod;(仅限b*mod不越界)

（5）double转string保留两位小数且保证精度
	double t=num*(1.0-discount/100.0);
    stringstream ss;
    ss<<std::fixed<<std::setprecision(2)<<t;
    ans+=" $"+ss.str();
（6） bool operator< (const Student& tmp) const
    {
        return ChineseGrade > tmp.ChineseGrade;
    }
（7）(a&b)+(a|b)==a+b;
（8）cmp返回值的实际意义是传入a、b两个参数，a在前b在后的排序是否是正确的，若是正确的返回1（true），否则返回0（false）
（9）如果超过32位的移位，类似1<<p,这种形式一定要写成1LL<<p,非常非常重要
（10）交互题不要开同步流，因为交互题问完就得答，但同步流会将所有答案最后输出
~~~



