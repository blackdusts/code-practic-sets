import  requests
from lxml import etree
baseUrl = 'http://poj.org/userstatus?user_id='
user = {'陈斌':'1198222765',
        '陈家乐':'MelodyC',
        '张皓杰':'2049155086',
		'麦文鹏':'Maweipro',
		'杨杰':'13667038810'}
print("POJ平台刷题数量统计")
for name,id in user.items():
    url = baseUrl+id
    # print(url)
    response = requests.get(url)
    # print(response.status_code)
    html = response.text
    # print(html)
    ehtml = etree.HTML(html)
    sovled = ehtml.xpath("//center//tr[3]/td[2]/a/text()")[0]
    outInfo = name + " :  " + sovled + "  Problems!"
    print(outInfo)