import requests
import time
userID = {
    "hai-shang-ling-guang-8x":"张皓杰",
    "c-n-m":"余瑞",
    "mai-c1":"麦文鹏",
    "U8CjpKptH0":"黄燊锐",
    "cosysl":"陈斌",
	"su-li-cx-9":"常天俊",
	"crazy-mendelcaf":"龚孝天",
    "xing-yao-9f":"陈硕",
    "strange-hert2ezf":"谢强",
    "alex-27r":"向文寿",
    "star_xing":"郑明星",
    "nice-kx":"胡明涛",
    "festive-mestorf1iu":"周靖",
    "hui-yin-ping":"洪逸鹏",
    "bai-8ue":"李浩然",
    "optimistic-chatterjeeirv":"曹政业",
    "charming-hellmanuy5":"陈雨灏",
    "jolly-7amarrqxo":"叶智豪",
    "kind-pike2nj":"徐智成",
    "MWhovCanBc":"潘腾宇",
    "nostalgic-bardeen05t":"梁均博",
    "pedantic-huglesxa":"汪静辉",
    "6oofy-williamsyv5":"唐毓杰",
    "stoic-haibt1vr":"蒋宇翔",
    "qui2zical-stonebrakerzkr":"胡晋诚",
    "exciting-goldstineznj":"季琪轩",
    "ash-tz":"季林豪",
    "yun-xre":"夏凌云"
    # "dreamy-cartwrightu8o":"汪宇超",
    # "dear-m-8":"马草原",
    # "WDrzwQKET8":"毛彦洋",
    # "PgkGQi5lwR":"高焱淼", 
    # "stupefied-hofstadterxea":"孙琦",  
    # "shi-wu-dao-liu":"丁振业",
    # "earfquake-1":"俞超然",
    # "mezyc12301":"张雨晨",
    # "long-1kx":"郑万隆",
    # "skirtsna":"连逸坤",
}

def getUrls(sites):
    urls = []
    # 只获取力扣前2000名的信息，即前80页（每页25人信息）
    for i in range(1,81): 
        url = 'https://leetcode.cn/contest/api/ranking/weekly-contest-'+str(sites)+'/?pagination='+str(i)+'&region=local'
        urls.append(url)
    time.sleep(0.1)    
    return urls

def getDates(urls):
    dataList = []
    for url in urls:
        res = requests.get(url)
        if res.status_code!=200:
            break
        datas = res.json()['total_rank']
        dataList = dataList + datas
    return dataList

def getRanks(dataList):
    for i in dataList:
        if i['username'] in userID.keys():
            name = userID[i['username']]
            rank = str(i['rank']+1)
            score = str(i['score'])
            print(name+'同学排名'+rank+",得分"+score)

if __name__ == "__main__":
    sites = input("输入力扣周赛的场次：")
    urls = getUrls(sites)
    dataList = getDates(urls)
    getRanks(dataList)