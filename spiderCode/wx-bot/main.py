import requests
from fastapi import FastAPI, Form
import uvicorn

app = FastAPI()

def sendMsg(to, msg):
    token = "xiguapi"
    url = "http://wxBotWebhook:3001/webhook/msg/v2?token=" + token
    data = {
        "to": to,
        "data": {
            "content": msg
        }
    }
    header = {"Content-Type": "application/json"}
    resp = requests.post(url=url, json=data, headers=header).text
    print(resp)


@app.post("/receive_msg")
async def print_json(type: str = Form(), content: str = Form()):
    print(content)
    sendMsg("Mai","你好")
    return {"message": "Data received and printed"}



if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8080)