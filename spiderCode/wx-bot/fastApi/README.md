# 艾希匹希督战员

## 概述

魔改了陆老师的爬虫代码

这是一个微信机器人，可以在比赛前推送消息

也可以通过关键词触发`@bot 查询比赛` or `@bot 查询周赛`,获取近三天数据

## 快速部署

`默认你已经安装了docker-compose`

将本文件夹拷贝到服务器目录,cd到wx-bot文件夹下

```bash
# 载入wechatbot镜像
docker loads < wechatbot.tar 
# 构建
docker-compose up -d --build
# 登录
docker logs -f wx-bot_wxBotWebhook_1
# 或访问http://服务器ip:3001/login?token=mai
```

mai为自定义字段，建议在docker-compose.yml中进行修改



## 如何指定群发对象?

打开wx-bot/fastApi/main.py文件,修改该位置

```python
"""
{
	"to":"用户名 or 群名称",
	"isRoom":"是用户还是群聊"
}
"""
subscriber = [
    {"to": "Mai", "isRoom": False},
    {"to": "东阳老年开放大学一队", "isRoom": True}
]

```
