import datetime
import json
from typing import List

import requests
from fastapi import FastAPI, Form
import uvicorn
from ContestSpider import ContestSpider, Contest

from apscheduler.schedulers.background import BackgroundScheduler

app = FastAPI()
scheduler = BackgroundScheduler()  # 定时任务
spider = ContestSpider()  # 爬虫

"""
    赛前推送定时器：定时爬取比赛，若半小时内有比赛，检查是否已推送过，若未推送过，推送消息并
"""
contestPushSet = set()
"""
推送目标
"""
subscriber = [
    {"to": "Mai", "isRoom": False},
    {"to": "东阳老年开放大学一队", "isRoom": True}
]


def sendMsg(msg):
    token = "mai"
    url = "http://wxBotWebhook:3001/webhook/msg/v2?token=" + token
    data = []
    for sub in subscriber:
        tmp = {"to": sub.get('to'), "data": {"content": msg}, "isRoom": sub.get('isRoom')}
        data.append(tmp)
    print("发送的消息为", data)
    header = {"Content-Type": "application/json"}

    resp = requests.post(url=url, json=data, headers=header).text
    print(resp)


# 获取比赛信息
def getContestData():
    data = spider.getData()
    data = sorted(data, key=lambda x: x.startTime.date())
    msg = "近三天比赛一览😋\n截止" + datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S') + "\n\n"
    for contest in data:
        msg += contest.__str__() + "\n\n"
    sendMsg(msg)


def pushContestResultAfterContest(contest):
    """
    获取比赛结果并推送
    :return:
    """
    # msg = contest.contestName + "已结束"
    # sendMsg(msg)
    print(contest.contestName, "已结束", datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
    pass


def pushMessageBeforeContest(contestList: List[Contest]):
    """
    设置赛前消息推送定时器
    :param contestList:
    :return:
    """
    now = datetime.datetime.now()
    # 过滤无用数据
    contestList = filter(lambda x: x.contestName != -1 and x.startTime > now, contestList)

    for contest in contestList:
        if contest.contestName not in contestPushSet:
            contestPushSet.add(contest.contestName)
            msg = "赛前消息推送🎈:\n\n" + contest.contestName + " 马上就要开始了\n比赛链接：" + contest.url

            run_date = contest.startTime - datetime.timedelta(minutes=30)
            print(contest.contestName, "即将被推送")
            print("推送时间", run_date)
            scheduler.add_job(sendMsg, "date", run_date=run_date, args=[msg])
            scheduler.add_job(pushContestResultAfterContest, "date", run_date=run_date, args=[contest])


def updateContestInfo():
    """
    更新比赛数据
    :return:
    """
    contestList = spider.getData()
    print("更新比赛数据", datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
    # 手动模拟数据
    # contestList = [
    #     Contest("cf", "demo1",
    #             datetime.datetime(2024, 3, 1, 11, 45, 0),
    #             datetime.datetime(2024, 3, 1, 11, 45, 1), "demo-url"),
    #     Contest("cf", "demo2",
    #             datetime.datetime(2024, 3, 1, 11, 46, 0),
    #             datetime.datetime(2024, 3, 1, 11, 46, 1), "demo-url")
    # ]
    pushMessageBeforeContest(contestList)  # 赛前提醒：为每一场比赛设置定时器


@app.on_event("startup")
async def app_start():
    scheduler.add_job(updateContestInfo, 'interval', minutes=10)
    scheduler.start()


@app.post("/receive_msg")
async def print_json(type: str = Form("default"),
                     content: str = Form("default"),
                     source: str = Form("default")):
    print(content)
    print(source)
    source_data: dict = json.loads(source)

    # 提及bot
    if "@bot" in content:
        if "查询" in content and ("周赛" in content or "比赛" in content):
            getContestData()

    return {"message": "最新周赛已发送"}


@app.get("/")
async def index():
    return "Hello,World"


if __name__ == '__main__':
    uvicorn.run(app="main:app", host="127.0.0.1", port=8000, reload=True)
