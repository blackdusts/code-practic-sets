import json
from datetime import datetime, timedelta
from typing import List

import requests
from lxml import etree


# 比赛 实体类
class Contest(object):
    def __init__(self, contestType, contestName, start, end, url):
        self.contestName = contestName
        self.startTime = start
        self.endTime = end
        self.contestType = contestType
        self.url = url

    def __str__(self):
        return f"比赛名称：{self.contestName}\n开始时间：{self.startTime}\n结束时间：{self.endTime}\n比赛链接：{self.url}"

    def check(self):
        return self.endTime - self.startTime <= timedelta(hours=6) and self.endTime <= (
                datetime.now() + timedelta(days=3))


class ContestSpider(object):
    def __init__(self):
        # 初始化
        self.urls = {
            "Codeforces": ['https://codeforces.com/api/contest.list?gym=false'],
            "Atcoder": ["https://atcoder.jp/contests"],
            "Nowcoder": ["https://ac.nowcoder.com/acm/contest/vip-index"],
            "Leetcode": ['https://leetcode.cn/graphql']
        }

    def getData(self):
        contest_data = []
        for contest_type, url in self.urls.items():
            try:
                # 根据比赛类型选择不同的数据过滤器
                if contest_type == "Codeforces":
                    # contest_data[contest_type] = self.filterForCodeforces(url[0], contest_type)
                    contest_data += self.filterForCodeforces(url[0], contest_type)
                elif contest_type == "Atcoder":
                    # contest_data[contest_type] = self.filterForAtcoder(url[0], contest_type)
                    contest_data += self.filterForAtcoder(url[0], contest_type)
                elif contest_type == "Nowcoder":
                    contest_data += self.filterForNowCoder(url[0], contest_type)
                elif contest_type == "Leetcode":
                    contest_data += self.filterForLeetcode(url[0], contest_type)
            except Exception as e:
                print("发生异常")
                contest_data += [Contest(-1, contest_type + "爬虫被屏蔽", datetime.now(), datetime.now(), "")]

        return filter(lambda x: x.check(), contest_data)

    # TODO coderforces
    def filterForCodeforces(self, url, contestType):
        # 数据过滤器，不同站点使用不同过滤器
        result = requests.get(url).json()['result']
        contestList = []
        for contest in result:
            if contest['phase'] == "FINISHED":
                # 跳过已结束的比赛
                continue
            contestName = contest['name']
            startTime = datetime.fromtimestamp(contest['startTimeSeconds'])
            endTime = datetime.fromtimestamp(contest['startTimeSeconds'] + contest['durationSeconds'])
            url = "https://codeforces.com/contestRegistration/" + str(contest['id'])
            contestList.append(Contest(contestType, contestName, startTime, endTime, url))
        return contestList

    # TODO Atcoder
    def filterForAtcoder(self, url, contestType):
        headers = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) "
                                 "AppleWebKit/537.36 (KHTML, like Gecko) "
                                 "Chrome/122.0.0.0 Safari/537.36 Edg/122.0.0.0"}
        html = requests.get(url=url, headers=headers).text
        tree = etree.HTML(html)
        result = tree.xpath('//div[@id="contest-table-upcoming"]//tbody//tr')

        contestList = []
        for dom in result:
            contestName = dom.xpath("./td[2]/a/text()")[0]
            startTime = dom.xpath("./td[1]/a/time/text()")[0].split("+")[0]
            startTime = datetime.strptime(startTime, "%Y-%m-%d %H:%M:%S")
            # 日本时区比中国晚一个小时
            startTime -= timedelta(hours=1, minutes=0)
            length = dom.xpath("./td[last()-1]/text()")[0]
            # length = datetime.strptime(length, "%H:%M")
            hh, mm = map(int, length.split(":"))

            endTime = startTime + timedelta(hours=hh, minutes=mm)
            url = "https://atcoder.jp" + dom.xpath("./td[2]/a/@href")[0]
            contestList.append(Contest(contestType, contestName, startTime, endTime, url))
        return contestList

    # TODO 牛客
    def filterForNowCoder(self, url, contestType):
        headers = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) "
                                 "AppleWebKit/537.36 (KHTML, like Gecko) "
                                 "Chrome/122.0.0.0 Safari/537.36 Edg/122.0.0.0"}
        html = requests.get(url=url, headers=headers).text
        tree = etree.HTML(html)
        result = tree.xpath('//div[@class="nk-content"]/div[2]/div')
        contestList = []
        for dom in result:
            json_text = dom.xpath("./@data-json")
            if len(json_text) == 0:
                continue
            json_text = json_text[0]
            data_id = dom.xpath("./@data-id")[0]
            url = "https://ac.nowcoder.com/acm/contest/" + data_id
            json_text = json_text.replace("&quot;", "\"")
            json_obj = json.loads(json_text)
            contestName = json_obj['contestName']  # 比赛名
            # datetime.fromtimestamp
            startTime = datetime.fromtimestamp(json_obj['contestStartTime'] // 1000)
            endTime = datetime.fromtimestamp(json_obj['contestEndTime'] // 1000)
            contestList.append(Contest(contestType, contestName, startTime, endTime, url))
        return contestList

    # TODO 力扣
    def filterForLeetcode(self, url, contestType):
        data = {
            "operationName": "null",
            "variables": {},
            "query": "{\n  contestUpcomingContests {\n    containsPremium\n    title\n    cardImg\n    titleSlug\n    description\n    startTime\n    duration\n    originStartTime\n    isVirtual\n    isLightCardFontColor\n    company {\n      watermark\n      __typename\n    }\n    __typename\n  }\n}\n"
        }
        resp = requests.post(url, json=data).json()['data']['contestUpcomingContests']
        contestList = []
        for contest in resp:
            contestName = contest['title']
            startTime = datetime.fromtimestamp(contest['startTime'])
            endTime = datetime.fromtimestamp(contest['startTime'] + contest['duration'])
            url = "https://leetcode.cn/contest/" + contest['titleSlug']
            contestList.append(Contest(contestType, contestName, startTime, endTime, url))

        return contestList


# 自动发送微信信息
def sendMsg(to, msg):
    token = "mai"
    url = "http://118.25.192.142:3001/webhook/msg/v2?token=" + token
    data = {
        "to": to,
        "data": {
            "content": msg
        }
    }
    header = {"Content-Type": "application/json"}
    resp = requests.post(url=url, json=data, headers=header).text
    print(resp)


if __name__ == '__main__':
    spider = ContestSpider()
    data = spider.getData()
    data = sorted(data, key=lambda x: x.startTime.date())
    msg = "近三天比赛一览标😋\n\n"
    for contest in data:
        msg += contest.__str__() + "\n\n"
    sendMsg("Mai", msg)
