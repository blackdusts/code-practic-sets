# 艾希匹希督战员

## 概述

基于 wechaty 的旨在解放双手的微信机器人，结合爬虫进行全自动化爬取数据，使用docker-compose一键部署。它能够：

- 自动爬取近期比赛场次，在赛前半小时自动推送到微信
- 在近期的某一场比赛结束之后，自动爬取排名结果，并推送到微信
- 每天自动爬取成员题目提交数据，并保存至数据库中
- 能够自定义比赛推送，自定义比赛暂不支持赛后结果爬取
- 使用预定义的指令实现更多功能

体验完整功能请访问：https://gitee.com/maweipro/Online-Judge-Spider

## 基于docker-compose部署

`git clone https://gitee.com/blackdusts/code-practic-sets.git && cd Online-Judge-Spider`

根据需要修改docker-compose.yml的配置，不修改也可正常使用

**安装和启动服务** `docker-compose up -d`

所有服务都显示**done**之后，输入`docker logs -f wx-bot_wxBotWebhook_1`

扫描控制台所显示的二维码，即可完成登录



**注意：**

**mysql容器可能后于fastapi启动，这将导致fastapi无法连接mysql**

**使用`docker-compose up -d fastapi重启服务即可`**

