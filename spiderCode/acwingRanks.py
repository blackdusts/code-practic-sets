# 运行该文件需要先安装requests包
import requests
import re
import time
# 各位同学的acwing账号id以及对应的姓名
userID = {
    "172421":"张皓杰",
    "167799":"余瑞"  ,
    "167593":"麦文鹏",
    "167600":"陈斌",
    "260840":"黄燊锐",
    "235808":"常天俊",
    "227276":"向文寿",
    "358669":"龚孝天",
    "362588":"陈硕",
    "362540":"谢强",
    "150094":"郑明星",
    "37943":"胡明涛",
    "362252":"周靖",
    "362453":"洪逸鹏",
    "361894":"李浩然",
    "364993":"梁均博",
    "364064":"徐智成",
    "362254":"曹政业",
    "362277":"陈雨灏",
    "363664":"潘腾宇",
    "367289":"叶智豪",
    "391852":"汪静辉",
    "369151":"季琪轩",
    "388850":"季林豪",
    "403080":"夏凌云",
    "404691":"唐毓杰",
    "405442":"蒋宇翔",
    # "364407":"游叶婕",
    # "362292":"汪宇超",
    # "247269":"宓启煊",
    # "171396":"马草原",
    # "164494":"陈家乐",
    # "":"杨杰",   # 未知
    # "260843":"张雨晨",
    # "260842":"郑万隆",
    # "260841":"吴福康",
    # "260419":"连逸坤",
    # "259946":"任志聪",
    # "260862":"俞超然",
    # "260851":"孙琦",  
    # "260850":"丁振业",
    # "247289":"李哲",
    # "247284":"李豪",
    # "247233":"庄丞杰",
    # "247379":"李苏汶",
    # "260839":"高焱淼",
    # "259846":"毛彦洋",
}

dataPattern = r'\d{3,7}/1/">第 (\d{1,3}).*?\s.*?排名：(\d{1,4}) </span>\s.*?\s.*?\s.*?\s.*?题目 (\d)\s.*?\s.*?</span>'

def generateUrls():
    urls = []
    for key in userID.keys():
        urls.append("https://www.acwing.com./user/myspace/activity/" + key + "/1/competition/")
    return urls

def getRanks(urls,number):
    ranks = {}
    for url in urls:
        # 获得用户名
        uid = re.findall(r"\d{5,7}",url)
        uname = userID[uid[0]]
        # 获得题数和排名
        res = requests.get(url)
        texts = res.text
        cnumberAndranks = re.findall(dataPattern,texts)
        for i in cnumberAndranks:
            if i[0]==number:
                ranks[uname]=[i[1],i[2]]
        time.sleep(0.3) # 延时几秒，避免被封
    return ranks  

def printRanks(ranks):
    sortResults = sorted(ranks.items(),key=lambda v:int(v[1][0]),reverse=False)
    for i in sortResults:
        print(i[0]+"在本次周赛中AC了"+i[1][1]+"题,排名"+i[1][0])
    for value in userID.values():
        if value not in ranks:
            print(value + "未参加本次比赛！！！") 
            
            
if __name__ == "__main__":
    number = input("请输入要查询的周赛场次：")
    urls = generateUrls()
    ranks = getRanks(urls,number)
    printRanks(ranks)
