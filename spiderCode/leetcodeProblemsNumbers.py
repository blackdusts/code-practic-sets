import requests
import json
userID = {
    "hai-shang-ling-guang-8x":"张皓杰",
    "c-n-m":"余瑞",
    "mai-c1":"麦文鹏",
    "U8CjpKptH0":"黄燊锐",
    "cosysl":"陈斌",
	"su-li-cx-9":"常天俊",
    "xing-yao-9f":"陈硕",
	"crazy-mendelcaf":"龚孝天",
    "optimistic-chatterjeeirv":"曹政业",
    "alex-27r":"向文寿",
    "charming-hellmanuy5":"陈雨灏",
    "strange-hert2ezf":"谢强",
    "kind-pike2nj":"徐智成",
    "star_xing":"郑明星",
    "nice-kx":"胡明涛",
    "festive-mestorf1iu":"周靖",
    "hui-yin-ping":"洪逸鹏",
    "bai-8ue":"李浩然",
    "MWhovCanBc":"潘腾宇",
    "jolly-7amarrqxo":"叶智豪",
    "nostalgic-bardeen05t":"梁均博",
    "pedantic-huglesxa":"汪静辉",
    "6oofy-williamsyv5":"唐毓杰",
    "stoic-haibt1vr":"蒋宇翔",
    "qui2zical-stonebrakerzkr":"胡晋诚",
    "exciting-goldstineznj":"季琪轩",
    "ash-tz":"季林豪",
    "yun-xre":"夏凌云",
    "ming-yue-o5":"杨子涵"
    # "dreamy-cartwrightu8o":"汪宇超",
    # "trusting-proskuriakovajnz":"游叶婕",
    # "ren-zhi-cong":"任志聪",
    # "wo-xiang-xie-dai-ma":"吴福康",
    # "youthful-murdockbfn":"庄丞杰",
    # "bai-lan-zhu-zhe":"李哲",
    # "naughty-cerfikz":"杨杰",
    # "patient-nr":"李苏汶",
    # "T8GbL5cyQc":"李豪",
    # "dear-m-8":"马草原",
    # "WDrzwQKET8":"毛彦洋",
    # "PgkGQi5lwR":"高焱淼", 
    # "stupefied-hofstadterxea":"孙琦",  
    # "shi-wu-dao-liu":"丁振业",
    # "earfquake-1":"俞超然",
    # "mezyc12301":"张雨晨",
    # "long-1kx":"郑万隆",
    # "skirtsna":"连逸坤",
}
 
def getNums():
    problemNum = []
    url = "https://leetcode.cn/graphql/"
    for uid in userID.keys():
        # post参数最难调，csrf码可能需要更换使用
        header = {
            "x-csrftoken":"n2o6m0oX3jI9evL2fxurbLsnW7mAV6xk1i7omqTuXKUgaqkYSdG56bYxNYPDN4iU",
            "content-type":"application/json"
        }
        payload = {
            "query":"\n    query userQuestionProgress($userSlug: String!) {\n  userProfileUserQuestionProgress(userSlug: $userSlug) {\n    numAcceptedQuestions {\n      difficulty\n      count\n    }\n    numFailedQuestions {\n      difficulty\n      count\n    }\n    numUntouchedQuestions {\n      difficulty\n      count\n    }\n  }\n}\n    ",
            "variables":{"userSlug":uid}
        }
        res = requests.post(url,data=json.dumps(payload),headers=header)
        userData = res.json() # 转字典格式
        detail = [userID[uid]] # [用户名，简单题数，中等题数，困难题数]
        for i in userData['data']['userProfileUserQuestionProgress']['numAcceptedQuestions']:
            detail.append(i['count'])
        problemNum.append(detail)
    return problemNum

def getContents(sections,oddEven):
    problemNum = []
    url = 'https://leetcode.cn/graphql/noj-go/'
    for uid in userID.keys():
        header = {
            "x-csrftoken":"n2o6m0oX3jI9evL2fxurbLsnW7mAV6xk1i7omqTuXKUgaqkYSdG56bYxNYPDN4iU",
            "content-type":"application/json"
        }
        payload = {
            "query":"\n    query userContestRankingInfo($userSlug: String!) {\n  userContestRanking(userSlug: $userSlug) {\n    attendedContestsCount\n    rating\n    globalRanking\n    localRanking\n    globalTotalParticipants\n    localTotalParticipants\n    topPercentage\n  }\n  userContestRankingHistory(userSlug: $userSlug) {\n    attended\n    totalProblems\n    trendingDirection\n    finishTimeInSeconds\n    rating\n    score\n    ranking\n    contest {\n      title\n      titleCn\n      startTime\n    }\n  }\n}\n    ",
            "variables":{"userSlug":uid},
            "operationName":"userContestRankingInfo"
        }
        res = requests.post(url,data=json.dumps(payload),headers=header)
        userData = res.json() # 转字典格式
        try:
            print(userID[uid]+'同学力扣竞赛排名位于全国前'+str(userData['data']['userContestRanking']['topPercentage']) + "%")
            historyData = userData['data']['userContestRankingHistory']
            words = " 场周赛"
            if oddEven==2:
                words = " 场双周赛"
            words = "第 " + sections + words
            for i in range(0,len(historyData)):
                if words==historyData[i]['contest']['title']:
                    if historyData[i]['attended']:
                        print(userID[uid]+"同学本次比赛排名："+str(historyData[i]['ranking']))
                    else:
                        print(userID[uid]+"同学本次未参赛")
        except:
            print(userID[uid]+'同学从未参加过力扣周赛！！！')

def printNumberRanks(proNum):
    newRank = sorted(proNum,key=lambda v:(v[1]+v[2]+v[3]),reverse=True)
    for i in newRank:
        sum = i[1]+i[2]+i[3]
        print(i[0] + "同学在力扣上完成"+str(sum)+"题,其中简单题" + str(i[1])+"道，中等题"+str(i[2])+"道，困难题"+str(i[3])+"道")

    
if __name__ == "__main__":
    choice = int(input("请输入查询力扣信息类别\n(1表示输出力扣刷题排名,2表示输出力扣竞赛信息):"))
    if choice==1:
        probleNum = getNums()
        printNumberRanks(probleNum)
    if choice==2:
        oddEven = input("请输入是否为双周赛(1表示单周赛,2表示双周赛):")
        sections = input("请输入周赛的场次:")
        getContents(sections,oddEven)
    