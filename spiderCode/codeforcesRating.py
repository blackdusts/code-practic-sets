import requests
from lxml import etree
import datetime
userID = {
    "tjf4":"张皓杰",
    "yurui111":"余瑞",
    "zjgs_Mai":"麦文鹏",
    "huang_shenrui":"黄燊锐",
    "Cosysl":"陈斌",
	"slcx":"常天俊",
    "caozhengye":"曹政业",
    "BananaWolf":"龚孝天",
    "xing_yao":"陈硕",
    "Epiphanyi":"胡明涛",
    "xws":"向文寿",
    "shixiaomaa":"陈雨灏",
    "YonagiKei":"叶智豪",
    "ljlbl":"梁均博",
    "Shimly":"徐智成",
    "Msss11":"季琪轩",
    "feirantongxue":"李浩然",
    "bskbsk":"周靖",
    "yushang":"潘腾宇",
    "AXC2134123":"",
    "xinjian":"",
    "a2754816395":"胡晋诚",
    "Bei__":"蒋宇翔",
    "dlbbcfss":"季林豪",
    "ShuaiXQ":"谢强",
    "qaq_npc":"汪静辉",
    # "deargugugu":"马草原",
    # "myywuhu":"毛彦洋",
    # "G_YMm":"高焱淼", 
    # "JKXF":"孙琦",  
    # "Dddd123":"丁振业",
    # "EARFQUAKE":"俞超然",
    # "Nineteen_Zhang":"张雨晨",
    # "Zwl-me":"郑万隆",
    # "Skirtsna9266":"连逸坤",
    # "TaiMing":"汪宇超",
    # "sakura_tear":"唐毓杰",
}
def getRatings():
    dataList = []
    for id in userID.keys():
        url = 'https://codeforces.com/profile/' + id
        res = requests.get(url)
        ehtml = etree.HTML(res.text)
        rating = ehtml.xpath('//div[@class="info"]/ul/li[1]/span[1]/text()')[0]
        dataList.append([userID[id],int(rating)])
    return dataList

def sortRating(dataList):
    rank = sorted(dataList,key=lambda v:v[1],reverse=True)
    for i in rank:
        print(i[0] + '同学cf积分为:' + str(i[1]))
    
if __name__ == "__main__":
    print('当前时间：',datetime.date.today())
    datalist = getRatings()
    sortRating(datalist) 