题目来源：

codeforces(17年以后)、vjudge(17年之前)



# 题型分类

签到题(⭐)常出类型：

模拟、排序、贪心



中等题(⭐⭐到⭐⭐⭐)常出题型:

模拟、贪心、思维、排序、二分、最短路、优先队列、并查集

分块、背包、字符串哈希、字典树、线段树、主席树、数位dp

打表、数学



困难题(⭐⭐⭐⭐到⭐⭐⭐⭐⭐)题型：

线段树、权值线段树、主席树、扫描线、树状数组、李超线段树

前缀和、期望dp、二分、背包、优先队列、单调队列

欧拉筛、扩展欧几里得、差分约束、组合数学、博弈

马拉车、拓扑排序、树型dp、字符串哈希、匈牙利算法

几何、凸包、大模拟、FFT、CDQ分治、分治NTT



# 19th

| 题目                          | 类型                             | 难度  |
| ----------------------------- | -------------------------------- | ----- |
| B、JB Loves Comma             | 模拟                             | ⭐     |
| C、JB Wants to Earn Big Money | 贪心                             | ⭐     |
| A、JB Loves Math              | 贪心                             | ⭐     |
| L、Candy Machine              | 排序、双指针                     | ⭐⭐    |
| G、Easy Glide                 | 最短路径                         | ⭐⭐    |
| M、BpbBppbpBB                 | 思维、解方程                     | ⭐⭐⭐   |
| I、Barbecue                   | 字符串哈希                       | ⭐⭐⭐   |
| J、Frog                       | 几何                             | ⭐⭐⭐⭐  |
| F、Easy Fix                   | 可持久化线段树、扫描线、树状数组 | ⭐⭐⭐⭐  |
| E、Easy Jump                  | 期望dp                           | ⭐⭐⭐⭐⭐ |
| K、Dynamic Reachability       | 图论、动态规划                   | ⭐⭐⭐⭐⭐ |
| D、The Profiteer              | 二分、背包、                     | ⭐⭐⭐⭐⭐ |
| H、A=B                        | 思维                             | ⭐⭐⭐⭐⭐ |



# 18th

| 题目                      | 类型                                   | 难度  |
| ------------------------- | -------------------------------------- | ----- |
| A、League of legends      | 模拟                                   | ⭐     |
| C、Cube                   | 模拟                                   | ⭐     |
| M、Game Theory            | 思维                                   | ⭐⭐    |
| J、Grammy and Jewelry     | 最短路径、多重背包                     | ⭐⭐    |
| L、String Freshman        | 模拟                                   | ⭐⭐    |
| F、Fair Distribution      | 分块                                   | ⭐⭐⭐   |
| G、Wall Game              | 并查集                                 | ⭐⭐⭐   |
| I、Grammy and Ropes       | 思维(蛮抽象的)                         | ⭐⭐⭐   |
| D、Shortest Path Query    | 最短路径dijkstra                       | ⭐⭐⭐⭐  |
| B、Restore Atlantis       | 二维树状数组、线段树、扫描线、优先队列 | ⭐⭐⭐⭐⭐ |
| H、Grammy and HearthStone | 构造                                   | ⭐⭐⭐⭐⭐ |
| E、Specially Supper Rare  |                                        | ⭐⭐⭐⭐⭐ |
| K、Grammy's Kingdom       |                                        | ⭐⭐⭐⭐⭐ |



# 17th

| 题目                       | 类型                       | 难度  |
| -------------------------- | -------------------------- | ----- |
| K、Killing the Brute-force | 模拟                       | ⭐     |
| A、AD2020                  | 模拟                       | ⭐     |
| I、Invoking the Magic      | 离散化、并查集             | ⭐⭐    |
| G、Gliding                 | 排序、动态规划             | ⭐⭐⭐   |
| C、Crossword Validation    | 字典树                     | ⭐⭐⭐   |
| E、Easy DP Problem         | 可持久化线段树             | ⭐⭐⭐   |
| B、Bing Packing Problem    | 线段树、二分               | ⭐⭐⭐   |
| H、Huge Clouds             | 思维、几何                 | ⭐⭐⭐⭐  |
| F、Finding a Sample        | 枚举                       | ⭐⭐⭐⭐  |
| L、List of Products        | 欧拉筛、排序、二分、双指针 | ⭐⭐⭐⭐⭐ |
| J、Just an Old Problem     | 最小生成树、动态规划       | ⭐⭐⭐⭐⭐ |
| D、Dividing the Points     | 几何                       | ⭐⭐⭐⭐⭐ |



# 16th

| 题目                       | 类型                   | 难度  |
| -------------------------- | ---------------------- | ----- |
| G、Lucky 7 in the Pocket   | 模拟                   | ⭐     |
| F、Abbreviation            | 模拟、字符串           | ⭐     |
| I、Fibonacci in the Pocket | 斐波那契数列打表题     | ⭐⭐    |
| H、Singing Everywhere      | 思维、模拟             | ⭐⭐    |
| E、Sequence in the Pocket  | 思维                   | ⭐⭐    |
| J、Welcome Party           | 并查集、思维、优先队列 | ⭐⭐⭐   |
| B、Element swapping        | 数学、思维             | ⭐⭐⭐   |
| K、String in the Pocket    | 马拉车算法             | ⭐⭐⭐⭐  |
| C、Array in the Pocket     | 模拟、贪心             | ⭐⭐⭐⭐  |
| D、Traveler                | 构造、思维             | ⭐⭐⭐⭐⭐ |
| A、Vertices in the Pocket  | 权值线段树、二分       | ⭐⭐⭐⭐⭐ |
| M、Trees in the Pocket     | 拓扑排序、思维         | ⭐⭐⭐⭐⭐ |
| L、Square on the Plane     | 数值分析+分类讨论      | ⭐⭐⭐⭐⭐ |



# 15th

| 题目                             | 类型             | 难度  |
| -------------------------------- | ---------------- | ----- |
| M、Lucky 7                       | 模拟             | ⭐     |
| A、Peak                          | 模拟             | ⭐     |
| B、King of Karaoke               | 模拟             | ⭐⭐    |
| L、Doking Doking Literature Club | 排序、贪心       | ⭐⭐    |
| J、CONTINUE...?                  | 模拟             | ⭐⭐    |
| K、Mahjong Sorting               | 思维、模拟       | ⭐⭐⭐   |
| F、Now Loading!!!                | 前缀和、二分     | ⭐⭐⭐⭐  |
| D、Sequence Swapping             | 线性dp           | ⭐⭐⭐⭐  |
| I、Magic Points                  | 几何、构造、思维 | ⭐⭐⭐⭐  |
| C、Maigc 12 Months               | 组合数学         | ⭐⭐⭐⭐⭐ |
| E、LIS                           | 差分约束         | ⭐⭐⭐⭐⭐ |
| H、Game on a Tree                | 树型dp           | ⭐⭐⭐⭐⭐ |
| G、JUMPin'  Jump UP              | 字符串哈希、思维 | ⭐⭐⭐⭐⭐ |



# 14th

| 题目                             | 类型         | 难度  |
| -------------------------------- | ------------ | ----- |
| A、Cooking Competition           | 模拟         | ⭐     |
| B、Problem Preparation           | 模拟         | ⭐     |
| D、Let's Chat                    | 区间合并     | ⭐⭐    |
| C、What Kind of Friends Are You? | 模拟         | ⭐⭐    |
| E、Seven segment Display         | 数位DP       | ⭐⭐⭐   |
| F、Heap Partition                | 贪心         | ⭐⭐⭐   |
| G、Yet Another Game of Stones    | 博弈         | ⭐⭐⭐⭐  |
| H、Binary Tree Restoring         | DFS          | ⭐⭐⭐⭐  |
| I、Domino Tiling                 | 大模拟、暴力 | ⭐⭐⭐⭐⭐ |
| J、Card Game                     | 李超线段树   | ⭐⭐⭐⭐⭐ |
| K、Final Defense Line            | 匈牙利算法   | ⭐⭐⭐⭐⭐ |
| L、Chiaki Sequence               | 单调队列     | ⭐⭐⭐⭐⭐ |
| M、Sequence to Sequence          | 思维         | ⭐⭐⭐⭐⭐ |



# 13th

| 题目                   | 类型             | 难度  |
| ---------------------- | ---------------- | ----- |
| A、Apples and Ideas    | 模拟             | ⭐     |
| L、Very Happy Great BG | 模拟             | ⭐     |
| C、Defuse the Bomb     | 模拟             | ⭐     |
| I、People Counting     | 模拟             | ⭐     |
| K、Highway Project     | Dijkstra最短路   | ⭐⭐    |
| D、The Lucky Week      | 打表、思维       | ⭐⭐⭐   |
| E、Modulo Query        | 区间取模         | ⭐⭐⭐⭐  |
| F、Kpop Music Party    | 思维、贪心       | ⭐⭐⭐⭐  |
| B、More Health Points  | 点分治、斜率优化 | ⭐⭐⭐⭐⭐ |
| G、Substring Counting  | 模拟             | ⭐⭐⭐⭐⭐ |
| H、Himalayas           | 线段树、差分     | ⭐⭐⭐⭐⭐ |
| J、Rush on the Cube    |                  | ⭐⭐⭐⭐⭐ |



# 12th

| 题目                        | 类型                   | 难度  |
| --------------------------- | ---------------------- | ----- |
| L、Demacia of the Ancients  | 模拟                   | ⭐     |
| A、Ace of Aces              | 排序                   | ⭐     |
| H、May Day Holiday          | 模拟                   | ⭐⭐    |
| G、Lunch Time               | 排序                   | ⭐⭐    |
| J、Convert QWERTY to Dvorak | 暴力、打表             | ⭐⭐⭐   |
| D、Beauty of Array          | 思维                   | ⭐⭐⭐⭐  |
| B、Team Formation           | 数学、异或             | ⭐⭐⭐⭐  |
| K、Capture the Flag         | 大模拟                 | ⭐⭐⭐⭐⭐ |
| C、Convex Hull              | 计算几何、凸包         | ⭐⭐⭐⭐⭐ |
| I、Earthstone Keeper        | 最短路                 | ⭐⭐⭐⭐⭐ |
| E、Floor Function           | 扩展欧几里得、同余方程 | ⭐⭐⭐⭐⭐ |
| F、Permutation Graph        | FFT、CDQ分治、分治NTT  | ⭐⭐⭐⭐⭐ |



