## [ZOJ]Fire Net

https://zoj.pintia.cn/problem-sets/91827364500/problems/91827364501



## [ZOJ]Crashing Balloon

[ZOJ (pintia.cn)](https://zoj.pintia.cn/problem-sets/91827364500/problems/91827364502)



## [ZOJ]Anagrams by Stack

[ZOJ (pintia.cn)](https://zoj.pintia.cn/problem-sets/91827364500/problems/91827364503)



## [ZOJ] Jugs

[ZOJ (pintia.cn)](https://zoj.pintia.cn/problem-sets/91827364500/problems/91827364504)



## [ZOJ]Gnome Tetravex

[ZOJ (pintia.cn)](https://zoj.pintia.cn/problem-sets/91827364500/problems/91827364507)



## [ZOJ]Do the Untwist

[ZOJ (pintia.cn)](https://zoj.pintia.cn/problem-sets/91827364500/problems/91827364505)



## [HDU]The kth great number

http://acm.hdu.edu.cn/showproblem.php?pid=4006



## [POJ]Fence Repair

[3253 -- Fence Repair (poj.org)](http://poj.org/problem?id=3253)



## [POJ]The Average

http://poj.org/problem?id=2833



## [POJ]Expedition

http://poj.org/problem?id=2431



## [POJ]**Balanced Lineup**

http://poj.org/problem?id=3264
