## OJ平台刷题顺序汇总

### 蓝桥杯刷题顺序

#### 第一阶段

水题阶段，掌握基本的语法，并学会基本的调试；题目如下：

A+B问题

http://lx.lanqiao.cn/problem.page?gpid=T2460

枚举——特殊的数字

http://lx.lanqiao.cn/problem.page?gpid=T46

输入输出——圆的面积

http://lx.lanqiao.cn/problem.page?gpid=T3

语法基础——字母图形

http://lx.lanqiao.cn/problem.page?gpid=T7

语法基础——回文数

http://lx.lanqiao.cn/problem.page?gpid=T47

语法基础——数列特征

http://lx.lanqiao.cn/problem.page?gpid=T8

语法基础——序列求和

http://lx.lanqiao.cn/problem.page?gpid=T2

语法基础——时间显示

http://lx.lanqiao.cn/problem.page?gpid=T2909

排序——数列排序

http://lx.lanqiao.cn/problem.page?gpid=T52

递归——Fibonacci数列

http://lx.lanqiao.cn/problem.page?gpid=T4

查找——查找整数

http://lx.lanqiao.cn/problem.page?gpid=T9

语法基础——字母大小写转换

http://lx.lanqiao.cn/problem.page?gpid=T726

语法基础——输出一个倒等腰三角形

http://lx.lanqiao.cn/problem.page?gpid=T1399

语法基础——二进制数

http://lx.lanqiao.cn/problem.page?gpid=T1798

语法基础——字串逆序

http://lx.lanqiao.cn/problem.page?gpid=T1736

语法基础——求平均

http://lx.lanqiao.cn/problem.page?gpid=T1455

语法基础——最大值与最小值的计算

http://lx.lanqiao.cn/problem.page?gpid=T1792

语法基础——大小写转换

http://lx.lanqiao.cn/problem.page?gpid=T216

#### 第二阶段

主要集中在排序、递归、搜索（枚举、二分查找）这几类算法上，题目如下：

排序——四数排序

http://lx.lanqiao.cn/problem.page?gpid=T2493

排序——成绩排名

http://lx.lanqiao.cn/problem.page?gpid=T551

排序——成绩分析

http://lx.lanqiao.cn/problem.page?gpid=D3108

枚举——找数字

http://lx.lanqiao.cn/problem.page?gpid=T3094

枚举——特别数的和

http://lx.lanqiao.cn/problem.page?gpid=T2701

递归——递归输出

http://lx.lanqiao.cn/problem.page?gpid=T594

递归&&二分查找——二分法查找数组元素

http://lx.lanqiao.cn/problem.page?gpid=T781

枚举——最小距离

http://lx.lanqiao.cn/problem.page?gpid=T2948

递归——汉诺塔

http://lx.lanqiao.cn/problem.page?gpid=T750

递归——逆向输出各位数字

http://lx.lanqiao.cn/problem.page?gpid=T1380

递归——根据前、中序遍历求后序遍历

http://lx.lanqiao.cn/problem.page?gpid=T2149

递归——未名湖边的烦恼

http://lx.lanqiao.cn/problem.page?gpid=T303

枚举——简单加法

http://lx.lanqiao.cn/problem.page?gpid=T332

枚举——寻找三位数

http://lx.lanqiao.cn/problem.page?gpid=T2038

排序——区间k大数查询

http://lx.lanqiao.cn/problem.page?gpid=T11

字符串——字符串匹配

http://lx.lanqiao.cn/problem.page?gpid=T1598

排序——从大到小排序

http://lx.lanqiao.cn/problem.page?gpid=T1523

排序——逆序排列

http://lx.lanqiao.cn/problem.page?gpid=T215

#### 第三阶段

主要集中在数学题、模拟题、搜索（DFS、BFS）等几类算法上，题目如下：

数学题——猴子吃包子

http://lx.lanqiao.cn/problem.page?gpid=T632

数学题——质数

http://lx.lanqiao.cn/problem.page?gpid=T723

模拟——十六进制转八进制

http://lx.lanqiao.cn/problem.page?gpid=D2985

数学题——N车

http://lx.lanqiao.cn/problem.page?gpid=T2671

搜索（BFS）——跳马

http://lx.lanqiao.cn/problem.page?gpid=T2987

模拟——转圈游戏

http://lx.lanqiao.cn/problem.page?gpid=T2660

搜索（DFS）——粘木棍

http://lx.lanqiao.cn/problem.page?gpid=T2982

搜索（DFS）——车的放置

http://lx.lanqiao.cn/problem.page?gpid=T2981

双指针——公共元素

http://lx.lanqiao.cn/problem.page?gpid=T2949

摩尔投票——JOE的众数

http://lx.lanqiao.cn/problem.page?gpid=T2666

模拟——自行车停放

http://lx.lanqiao.cn/problem.page?gpid=T2667

数学&&枚举——共线

http://lx.lanqiao.cn/problem.page?gpid=T2668

模拟——阶乘

http://lx.lanqiao.cn/problem.page?gpid=T1625

搜索（BFS）——九宫重排

http://lx.lanqiao.cn/problem.page?gpid=T2939

数学题——数正方形

http://lx.lanqiao.cn/problem.page?gpid=T2721

搜索（BFS）——逃跑的牛

http://lx.lanqiao.cn/problem.page?gpid=T1640

#### 第四阶段

主要集中在字符串操作、简单动态规划和贪心算法上，题目如下：

字符串——P0603

http://lx.lanqiao.cn/problem.page?gpid=T2628

动态规划——吃鱼丸

http://lx.lanqiao.cn/problem.page?gpid=T2689

动态规划——进击的青蛙

http://lx.lanqiao.cn/problem.page?gpid=T2662

动态规划——秘密行动

http://lx.lanqiao.cn/problem.page?gpid=T2994 

动态规划——最大连续子段和

http://lx.lanqiao.cn/problem.page?gpid=T2977

动态规划——最大区间和

http://lx.lanqiao.cn/problem.page?gpid=T2684

动态规划——印章

http://lx.lanqiao.cn/problem.page?gpid=T3002

贪心——积木大赛

http://lx.lanqiao.cn/problem.page?gpid=T2659

动态规划——完全背包问题

http://lx.lanqiao.cn/problem.page?gpid=T2423

动态规划——无聊的逗

http://lx.lanqiao.cn/problem.page?gpid=T2992

字符串——串中取3个字符的取法

http://lx.lanqiao.cn/problem.page?gpid=T3080

贪心——排队接水

http://lx.lanqiao.cn/problem.page?gpid=T1668

贪心——区间覆盖问题

http://lx.lanqiao.cn/problem.page?gpid=D2998

动态规划——不重叠的线段

http://lx.lanqiao.cn/problem.page?gpid=T747

动态规划——分割项链

http://lx.lanqiao.cn/problem.page?gpid=T786

#### 第五阶段

主要集中在并查集、高精度计算等。

并查集&&dfs——发现环

http://lx.lanqiao.cn/problem.page?gpid=D3112

高精度——阶乘

http://lx.lanqiao.cn/problem.page?gpid=T1625

### 力扣刷题顺序

#### 第一阶段——数据结构

##### 数组和链表

21.合并两个有序链表

https://leetcode.cn/problems/merge-two-sorted-lists/

148.排序链表

https://leetcode.cn/problems/sort-list/

206.反转链表

https://leetcode.cn/problems/reverse-linked-list/

##### 栈和队列

155.最小栈

https://leetcode.cn/problems/min-stack/

225.用队列实现栈

https://leetcode.cn/problems/implement-stack-using-queues/

232.用栈实现队列

https://leetcode.cn/problems/implement-queue-using-stacks/

20. 有效的括号

https://leetcode.cn/problems/valid-parentheses/

150.逆波兰表达式求值

https://leetcode.cn/problems/evaluate-reverse-polish-notation/

71.简化路径

https://leetcode.cn/problems/simplify-path/

##### 二叉树

144.二叉树的前序遍历

https://leetcode.cn/problems/binary-tree-preorder-traversal/

94.二叉树的中序遍历

https://leetcode.cn/problems/binary-tree-inorder-traversal/

589.N 叉树的前序遍历

https://leetcode.cn/problems/n-ary-tree-preorder-traversal/

102.二叉树的层序遍历

https://leetcode.cn/problems/binary-tree-level-order-traversal/

### ZOJ刷题顺序

#### 第一阶段——基础编程

重在阅读理解。

ZOJ 1045 HangOver

https://zoj.pintia.cn/problem-sets/91827364500/problems/91827364544

ZOJ 1037 Gridland

https://zoj.pintia.cn/problem-sets/91827364500/problems/91827364536

ZOJ 1049 I Think I Need a Houseboat



ZOJ 1058 Currency Exchange



ZOJ 1067 Color Me Less



ZOJ 1078 Palindrom Numbers



ZOJ 1086 Octal Fractions



ZOJ 1089 Lotto



ZOJ 1090 The Circumference of the Circle



ZOJ 1095 Humble Numbers



ZOJ 1099 HTML



ZOJ 1105 FatMouse's Tour



ZOJ 1115 Digital Roots



ZOJ 1122 Clock



ZOJ 1139 Rectangles



ZOJ 1151 Word Reversal



ZOJ 1152 A Mathematical Curiosity



ZOJ 1154 Niven Numbers



ZOJ 1164 Software CRC



ZOJ 1168 Function Run Fun



ZOJ 1195 Blowing Fuses



ZOJ 1200 Mining



ZOJ 1209 April Fool's Joke



ZOJ 1212 Mountain Landscape



ZOJ 1241 Geometry Made Simple



ZOJ 1243 URLs



ZOJ 1244 Definite Values

#### 第二阶段——模拟题

ZOJ 1003 Crashing Balloon



ZOJ 1006 Do the Untwist



ZOJ 1005 Jugs



ZOJ 1009 Enigma



ZOJ 1010 Area



ZOJ 1021 The Willy Memorial Program



ZOJ 1024 Calendar Game



ZOJ 1036 Enigma 2



ZOJ 1039 Number Game



ZOJ 1042 W's Cipher



ZOJ 1051 A New Growth Industry



ZOJ 1052 Algernon's Noxious Emissions



ZOJ 1056 The Worm Turns



ZOJ 1057 Undercut



ZOJ 1063 Space Station Shielding



ZOJ 1066 Square Ice



ZOJ 1071 Follow My Logic



ZOJ 1072 Microprocessor Simulation



ZOJ 1073 Round and Round We Go



ZOJ 1088 System Overload



ZOJ 1098 Simple Computers



ZOJ 1121 Reserve Bookshelf



ZOJ 1143 Date Bugs



ZOJ 1144 Robbery



ZOJ 1146 LC-Display



ZOJ 1153 Tournament Seeding



ZOJ 1160 Biorhythms



ZOJ 1176 Die and Chessboard



ZOJ 1178 Booklet Printing



ZOJ 1182 Keeps Going and Going and …



ZOJ 1184 Counterfeit Dollar



ZOJ 1187 Parallel Deadlock



ZOJ 1194 Going in Circles on Alpha Centauri



ZOJ 1207 The Knight, the Princess, and the Dragons



ZOJ 1208 Roll the Die!



ZOJ 1215 Bowl



ZOJ 1218 Ratio



ZOJ 1219 Pizza Anyone?



ZOJ 1224 Stats



ZOJ 1225 Scramble Sort



ZOJ 1239 Hanoi Tower Troubles Again!



ZOJ 1246 Instant Complexity



ZOJ 1247 There's Treasure Everywhere!



ZOJ 1250 Always On the Run

#### 第三阶段——字符串

ZOJ 1014 Operand



ZOJ 1038 T9



ZOJ 1044 Index Generation



ZOJ 1046 Double Vision



ZOJ 1050 Start Up the Startup



ZOJ 1068 P,MTHBGWB



ZOJ 1109 Language of FatMouse



ZOJ 1111 Poker Hands



ZOJ 1116 A Well-Formed Problem



ZOJ 1126 Bio-Informatics



ZOJ 1159 487-3279



ZOJ 1170 String Matching



ZOJ 1174 Skip Letter Code



ZOJ 1175 Word Process Machine



ZOJ 1179 Finding Rectangles



ZOJ 1181 Word Amalgamation



ZOJ 2727 List the Books

#### 第四阶段——基础数据结构、大数运算

ZOJ 1004 Anagrams by Stack



ZOJ 1011 NTA



ZOJ 1062 Trees Made to Order



ZOJ 1061 Web Navigation



ZOJ 1094 Matrix Chain Multiplication



ZOJ 1097 Code the Tree



ZOJ 1156 Unscrambling Images



ZOJ 1167 Trees on the Level



ZOJ 1205 Martian Addition



ZOJ 1210 Reciprocals



ZOJ 1962 How Many Fibs?

#### 第五部分——搜索

ZOJ 1002 Fire Net



ZOJ 1008 Gnome Tetravex



ZOJ 1019 Illusive Chase



ZOJ 1047 Image Perimeters



ZOJ 1054 For the Porsche



ZOJ 1055 Oh, Those Achin' Feet



ZOJ 1069 Plato's Blocks



ZOJ 1075 Set Me



ZOJ 1079 Robotic Jigsaw



ZOJ 1080 Direct Subtraction



ZOJ 1084 Channel Allocation



ZOJ 1085 Alien Security



ZOJ 1091 Knight Moves



ZOJ 1101 Gamblers



ZOJ 1103 Hike on a Graph



ZOJ 1129 Erdos Numbers



ZOJ 1136 Multiple



ZOJ 1142 Maze



ZOJ 1148 The Game



ZOJ 1162 The Same Game



ZOJ 1190 Optimal Programs



ZOJ 1191 The Die Is Cast



ZOJ 1192 It's not a Bug, It's a Feature!



ZOJ 1204 Additive equations



ZOJ 1217 Eight



ZOJ 1229 Gift?!



ZOJ 1245 Triangles



ZOJ 2881 Full Tank?



#### 第六部分——动态规划

ZOJ 1013 Great Equipment

ZOJ 1027 Human Gene Functions

ZOJ 1074 To the Max

ZOJ 1093 Monkey and Banana

ZOJ 1100 Mondriaan's Dream

ZOJ 1102 Phylogenetic Trees Inherited

ZOJ 1107 FatMouse and Cheese

ZOJ 1108 FatMouse's Speed

ZOJ 1132 Railroad

ZOJ 1147 Formatting Text

ZOJ 1149 Dividing

ZOJ 1163 The Staircases

ZOJ 1183 Scheduling Lectures

ZOJ 1196 Fast Food

ZOJ 1206 Win the Bonus

ZOJ 1227 Free Candies

ZOJ 1234 Chopsticks

ZOJ 1733 Common Subsequence

ZOJ 1880 Tug of War

ZOJ 2845 The Best Travel Design

ZOJ 2882 Nested Dolls

ZOJ 3070 Colored stones

ZOJ 3541 The Last Puzzle



#### 第七部分——贪心与回溯

ZOJ 1012 Mainframe

ZOJ 1025 Wooden Sticks

ZOJ 1029 Moving Tables

ZOJ 1076 Gene Assembly

ZOJ 1145 Dreisam Equations

ZOJ 1157 A Plug for UNIX

ZOJ 1171 Sorting the Photos

ZOJ 1161 Gone Fishing

ZOJ 1665 Rational Irrationals

ZOJ 3118 Highway

ZOJ 1166 Anagram Checker

ZOJ 1213 Lumber Cutting

ZOJ 2734 Exchange Cards

#### 第八部分——图论

ZOJ 1015 Fishing Net

ZOJ 1053 FDNY to the Rescue!

ZOJ 1059 What's In a Name

ZOJ 1064 Roads Scholar

ZOJ 1082 Stockbroker Grapevine

ZOJ 1083 Frame Stacking

ZOJ 1092 Arbitrage

ZOJ 1117 Entropy

ZOJ 1118 N-Credible Mazes

ZOJ 1119 SPF

ZOJ 1127 Roman Forts

ZOJ 1130 Ouroboros Snake

ZOJ 1134 Strategic Game

ZOJ 1137 Girls and Boys

ZOJ 1140 Courses

ZOJ 1141 Closest Common Ancestors

ZOJ 1150 S-Trees

ZOJ 1186 Street Directions

ZOJ 1197 Sorting Slides

ZOJ 1203 Swordfish

ZOJ 1221 Risk

ZOJ 1232 Adventure of Super Mario

ZOJ 1542 Network

ZOJ 1935 XYZZY

ZOJ 2797 106 miles to Chicago

ZOJ 2832 Efficient Codes

ZOJ 3010 The Lamp Game

#### 第九部分——几何与数学

ZOJ 1007 Numerical Summation of a Series

ZOJ 1010 Area

ZOJ 1026 Modular multiplication of polynomials

ZOJ 1028 Flip and Shift

ZOJ 1030 Farmland

ZOJ 1032 Area 2

ZOJ 1034 Cog-Wheels

ZOJ 1041 Transmitters

ZOJ 1081 Points Within

ZOJ 1096 Subway

ZOJ 1104 Leaps Tall Buildings

ZOJ 1110 Dick and Jane

ZOJ 1112 Equidistance

ZOJ 1114 Problem Bee

ZOJ 1123 Triangle Encapsulation

ZOJ 1125 Floating Point Numbers

ZOJ 1128 Atlantis

ZOJ 1133 Smith Numbers

ZOJ 1158 Treasure Hunt

ZOJ 1165 Laser Lines

ZOJ 1185 Metal Cutting

ZOJ 1193 Reflections

ZOJ 1199 Point of Intersection

ZOJ 1248 Video Surveillance

ZOJ 1177 K-Magic Number

ZOJ 1180 Self Numbers

ZOJ 1188 DNA Sorting

ZOJ 1189 Numbers That Count

ZOJ 1198 Single-Player Games

ZOJ 1201 Inversion

ZOJ 1202 Divide and Count

ZOJ 1222 Just the Facts

ZOJ 1238 Guess the Number

ZOJ 1186 Street Directions